using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestVersionStringConverter {
		private class TestObject {
			[JsonConverter(typeof(VersionStringConverter))]
			public Version? Version {
				get; set;
			}
		}

		[Theory]
		[InlineData("\"15\"", "15.0")]
		[InlineData("\"15.0\"", "15.0")]
		[InlineData("\"15.8\"", "15.8")]
		[InlineData("\"015.8\"", "15.8")]
		[InlineData("\"1621057832.18635883\"", "1621057832.18635883")]
		[InlineData("\"1621057832.18635883.0.1\"", "1621057832.18635883.0.1")]
		public void TestDeserialize(string jsonValue, string expected) {
			var expectedValue = Version.Parse(expected);
			var json = $"{{ \"{nameof(TestObject.Version)}\": {jsonValue} }}";
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Version;
			Assert.Equal(expectedValue, result);
		}

		[Fact]
		public void TestDeserializeNull() {
			var json = $"{{ \"{nameof(TestObject.Version)}\": null }}";
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Version;
			Assert.Null(result);
		}


		[Theory]
		[InlineData("\"\"")]
		[InlineData("\"a;sfogihad;sgih\"")]
		[InlineData("-10")]
		[InlineData("10e8")]
		[InlineData("\"10e8\"")]
		[InlineData("\"1E8\"")]
		[InlineData("10.8")]
		[InlineData("\"10.5e8\"")]
		[InlineData("\"2017-12-31T20:40:23.447Z\"")]
		[InlineData("false")]
		[InlineData("true")]
		public void TestDeserializeException(string badData) {
			var json = $"{{ \"{nameof(TestObject.Version)}\": {badData} }}";
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("0.0", "\"0.0\"")]
		[InlineData("1.0", "\"1.0\"")]
		[InlineData("0.558682814", "\"0.558682814\"")]
		public void TestWrite(string version, string expected) {
			var buffer = new ArrayBufferWriter<byte>();
			using var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new VersionStringConverter();

			converter.Write(jsonWriter, Version.Parse(version), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
		[Fact]
		public void TestWriteNull() {
			var buffer = new ArrayBufferWriter<byte>();
			using var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new VersionStringConverter();

			converter.Write(jsonWriter, null, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal("null", result);
		}
	}
}
