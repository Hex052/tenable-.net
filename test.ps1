if (Test-Path ./coverage) {
	Remove-Item -Recurse -Force -Confirm:$false ./coverage
}
New-Item -ItemType Directory ./coverage | Out-Null

[int] $ERROR = 0
try {
	Get-Command coverlet -ErrorAction Stop | Out-Null
	dotnet tool update --global coverlet.console
}
catch {
	dotnet tool install --global coverlet.console
	$ERROR = $LASTEXITCODE
	if ($ERROR -ne 0) {
		Write-Error "Failed to install coverlet.console!"
		exit $ERROR
	}
}

## Works, but kinda jank with the for loop
dotnet build
$ERROR = $LASTEXITCODE
if ($ERROR -ne 0) {
	Write-Error "Build fail! Cannot test without building first!"
	exit $ERROR
}
foreach ($dir in @("Tenable.Base.Tests", "Tenable.IO.Tests")) {
	foreach ($framework in @("net5.0")) {
		coverlet "${dir}/bin/Debug/${framework}/" `
			--target dotnet `
			--targetargs "test --no-build --no-restore ${dir}" `
			--output "./coverage/" `
			--format lcov `
			--format opencover `
			--format json `
			--format cobertura `
			--merge-with "./coverage/coverage.json" `
			--exclude "[Tenable.IO.Tests]*" `
			--exclude "[Tenable.Base.Tests]*"
		if ($LASTEXITCODE -ne 0) {
			$ERROR = 1
		}
	}
}
exit ${ERROR}
