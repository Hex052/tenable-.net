using System.Text.Json.Serialization;

namespace Tenable.IO.Server.Results {
	public class ServerStatus {
		[JsonPropertyName("code")]
		public int Code {
			get; protected set;
		}
		[JsonPropertyName("status")]
		public string Status {
			get; protected set;
		}

		[JsonConstructor]
		public ServerStatus(int code, string status) {
			this.Code = code;
			this.Status = status;
		}
	}
}
