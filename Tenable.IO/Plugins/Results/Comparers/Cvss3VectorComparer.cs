using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class Cvss3VectorComparer : EqualityComparer<Cvss3Vector> {
		public Cvss3VectorComparer() {
		}

		[Pure]
		public override bool Equals([AllowNull] Cvss3Vector x, [AllowNull] Cvss3Vector y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.AccessComplexity == y.AccessComplexity
					&& x.AttackVector == y.AttackVector
					&& x.AttackComplexity == y.AttackComplexity
					&& x.UserInteraction == y.UserInteraction
					&& x.Scope == y.Scope
					&& x.PrivilegesRequired == y.PrivilegesRequired
					&& x.ConfidentialityImpact == y.ConfidentialityImpact
					&& x.IntegrityImpact == y.IntegrityImpact
					&& x.AvailabilityImpact == y.AvailabilityImpact
					&& x.Raw == y.Raw;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Cvss3Vector obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				return (ReferenceEquals(obj.AccessComplexity, null) ? 0 : obj.AccessComplexity.GetHashCode())
					+ (ReferenceEquals(obj.AttackVector, null) ? 0 : obj.AttackVector.GetHashCode())
					+ (ReferenceEquals(obj.AttackComplexity, null) ? 0 : obj.AttackComplexity.GetHashCode())
					+ (ReferenceEquals(obj.UserInteraction, null) ? 0 : obj.UserInteraction.GetHashCode())
					+ (ReferenceEquals(obj.Scope, null) ? 0 : obj.Scope.GetHashCode())
					+ (ReferenceEquals(obj.PrivilegesRequired, null) ? 0 : obj.PrivilegesRequired.GetHashCode())
					+ (ReferenceEquals(obj.ConfidentialityImpact, null) ? 0 : obj.ConfidentialityImpact.GetHashCode())
					+ (ReferenceEquals(obj.IntegrityImpact, null) ? 0 : obj.IntegrityImpact.GetHashCode())
					+ (ReferenceEquals(obj.AvailabilityImpact, null) ? 0 : obj.AvailabilityImpact.GetHashCode())
					+ (ReferenceEquals(obj.Raw, null) ? 0 : obj.Raw.GetHashCode());
			}
		}
	}
}
