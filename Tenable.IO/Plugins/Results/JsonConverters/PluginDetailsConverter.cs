using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.JsonConverters {
	public class PluginDetailsConverter : JsonConverter<PluginDetails?> {
		public override bool HandleNull => true;
		public override PluginDetails? Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			IReadOnlyList<PluginAttribute>? attributes = null;
			string? familyName = null;
			string? name = null;
			int? id = null;
			Dictionary<string, JsonElement>? extensionData = null;

			if (reader.TokenType == JsonTokenType.Null) {
				return null;
			}
			else if (reader.TokenType != JsonTokenType.StartObject) {
				throw new JsonException("not object");
			}

			while (reader.Read()) {
				if (reader.TokenType == JsonTokenType.EndObject) {
					if (!id.HasValue) {
						throw new JsonException("too few keys");
					}
					try {
						return new PluginDetails(attributes!, familyName!, name!, id.Value) { ExtensionData = extensionData };
					}
					catch (ArgumentNullException exc) {
						throw new JsonException("too few keys", exc);
					}
				}
				else if (reader.TokenType != JsonTokenType.PropertyName) {
					throw new JsonException("expected property name");
				}
				var str = reader.GetString()!;
				switch (str) {
					case "attributes":
						if (!ReferenceEquals(attributes, null)) {
							throw this._duplicateKey(str);
						}
						else if (!reader.Read()) {
							throw this._prematureEnd();
						}
						try {
							attributes = ((JsonConverter<IReadOnlyList<PluginAttribute>>)options.GetConverter(typeof(IReadOnlyList<PluginAttribute>)))
								.Read(ref reader, typeof(IReadOnlyList<PluginAttribute>), options) ?? throw this._invalidValue(str);
						}
						catch (ArgumentNullException exc) {
							throw this._invalidValue(str, exc);
						}
						break;
					case "family_name":
						if (!ReferenceEquals(familyName, null)) {
							throw this._duplicateKey(str);
						}
						else if (!reader.Read()) {
							throw this._prematureEnd();
						}
						familyName = reader.GetString() ?? throw this._invalidValue(str);
						break;
					case "name":
						if (!ReferenceEquals(name, null)) {
							throw this._duplicateKey(str);
						}
						else if (!reader.Read()) {
							throw this._prematureEnd();
						}
						name = reader.GetString() ?? throw this._invalidValue(str);
						break;
					case "id":
						if (id.HasValue) {
							throw this._duplicateKey(str);
						}
						else if (!reader.Read()) {
							throw this._prematureEnd();
						}
						id = reader.GetInt32();
						break;
					default:
						extensionData ??= new Dictionary<string, JsonElement>();
						if (!reader.Read()) {
							throw this._prematureEnd();
						}
						else if (!extensionData.TryAdd(str, JsonElement.ParseValue(ref reader))) {
							throw this._duplicateKey(str);
						}
						break;
				}
			}
			throw this._prematureEnd();
		}

		[Pure]
		private JsonException _prematureEnd() {
			return new JsonException("input ended before complete");
		}
		[Pure]
		private JsonException _duplicateKey(string key) {
			return new JsonException(string.Format("duplicate key \"{0}\"", key));
		}
		[Pure]
		private JsonException _invalidValue(string key, Exception? exc = null) {
			return new JsonException(string.Format("invalid value for key \"{0}\"", key), exc);
		}

		public override void Write(Utf8JsonWriter writer,
				PluginDetails? value, JsonSerializerOptions options) {
			if (value == null) {
				writer.WriteNullValue();
				return;
			}
			throw new NotImplementedException();
		}
	}
}
