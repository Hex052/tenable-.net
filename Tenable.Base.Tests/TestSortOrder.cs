using System;
using Xunit;

namespace Tenable.Base.Tests {
	public class TestSortOrder {
		[Fact]
		public void TestConstruct() {
			Assert.Throws<ArgumentNullException>("field", () => new SortOrder(null!, SortDirection.ASC));
			Assert.Throws<ArgumentOutOfRangeException>("direction", () => new SortOrder("field", (SortDirection)3));
			var sort = new SortOrder("field", SortDirection.ASC);
			Assert.Throws<ArgumentNullException>("value", () => { sort.Field = null!; });
			Assert.Throws<ArgumentOutOfRangeException>("value", () => { sort.Direction = (SortDirection)3; });
		}
		[Fact]
		public void TestEqual() {
			var first = new SortOrder("field", SortDirection.ASC);
			var second = first;
			Assert.True(first.Equals((object)second));
			Assert.True(first.Equals(second));
			Assert.True(first == second);
			Assert.False(first != second);
			Assert.True(first != null);
			Assert.True(null != first);
#pragma warning disable xUnit2000
			Assert.False(first!.Equals("null"));
#pragma warning restore xUnit2000
			second = new SortOrder("field", SortDirection.ASC);
			Assert.True(first!.Equals((object)second));
			Assert.True(first!.Equals(second));
			Assert.True(first == second);
			Assert.False(first != second);
			Assert.Equal(first!.GetHashCode(), second.GetHashCode());
			second.Direction = SortDirection.DESC;
			Assert.False(first.Equals(second));
			Assert.False(first.Equals((object)second));
			Assert.False(first == second);
			Assert.True(first != second);
			second.Field = "other";
			second.Direction = SortDirection.ASC;
			Assert.False(first.Equals(second));
			Assert.False(first == second);
			Assert.True(first != second);
		}
	}
}
