using System;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Tenable.Base.JsonConverters {
	public class UnixSecondConverter : JsonConverter<DateTime> {
		internal static readonly DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			if (reader.TryGetInt64(out long unixTime)) {
				return UNIX_EPOCH.AddSeconds(unixTime);
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options) {
			double unixTime = Math.Floor((value - UNIX_EPOCH).TotalSeconds);
			writer.WriteNumberValue(Convert.ToInt64(unixTime));
		}
	}
}
