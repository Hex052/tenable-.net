using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result {
	public class ExportRequestResult {
		[JsonPropertyName("export_uuid")]
		public Guid ExportUuid {
			get; set;
		}

		[JsonConstructor]
		public ExportRequestResult(Guid exportUuid) {
			this.ExportUuid = exportUuid;
		}
	}
}
