using RestSharp;
using RichardSzalay.MockHttp;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Tests;
using Tenable.IO.Server.Results;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.IO.Tests {
	public class TestContentTypeOctetstream {
		private readonly ITestOutputHelper output;
		public TestContentTypeOctetstream(ITestOutputHelper output) {
			this.output = output;
		}

		[Fact]
		public async Task TestServerStatus() {
			using var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Get, "https://cloud.tenable.com/server/status")
				.WithHeaders("X-ApiKeys", "accessKey=apikey;secretKey=secretkey;")
				.WithExactQueryString(string.Empty)
				.Respond("application/octet-stream", "{\"code\":200,\"status\":\"ready\"}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };

			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500);
			using ITenableIO tio = new TenableIO("apikey", "secretkey", options);

			ServerStatus status = await tio.Server.StatusAsync(tokenSource.Token);
			Assert.NotNull(status);
			Assert.Equal(200, status.Code);
			Assert.Equal("ready", status.Status);
		}
	}
}
