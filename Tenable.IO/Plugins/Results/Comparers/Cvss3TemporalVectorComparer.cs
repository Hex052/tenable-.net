using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class Cvss3TemporalVectorComparer : EqualityComparer<Cvss3TemporalVector> {
		public Cvss3TemporalVectorComparer() {
		}

		[Pure]
		public override bool Equals([AllowNull] Cvss3TemporalVector x, [AllowNull] Cvss3TemporalVector y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.Exploitability == y.Exploitability
					&& x.RemediationLevel == y.RemediationLevel
					&& x.ReportConfidence == y.ReportConfidence
					&& x.ExploitCodeMaturity == y.ExploitCodeMaturity
					&& x.Raw == y.Raw;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Cvss3TemporalVector obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (ReferenceEquals(obj.Exploitability, null) ? 0 : obj.Exploitability.GetHashCode())
						+ (ReferenceEquals(obj.RemediationLevel, null) ? 0 : obj.RemediationLevel.GetHashCode())
						+ (ReferenceEquals(obj.ReportConfidence, null) ? 0 : obj.ReportConfidence.GetHashCode())
						+ (ReferenceEquals(obj.ExploitCodeMaturity, null) ? 0 : obj.ExploitCodeMaturity.GetHashCode())
						+ (ReferenceEquals(obj.Raw, null) ? 0 : obj.Raw.GetHashCode());
				}
			}
		}
	}
}
