using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result {
	public class ExportStatus {
		/// <summary>
		/// The status of the export request
		/// </summary>
		[JsonPropertyName("status")]
		public Status Status {
			get; set;
		}

		[JsonConstructor]
		public ExportStatus(Status status) {
			this.Status = status;
		}
	}
}
