namespace Tenable.Base {
	public interface IEndpointBase<out T> where T : ITenableBase {
		T Api {
			get;
		}
	}
}
