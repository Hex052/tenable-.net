using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tenable.Base.Collections {
	/// <summary>Enumerator over some results</summary>
	/// <inheritdoc cref="ResultEnumerable{TResult, TCache}"/>
	/// <typeparam name="TEnumerable">
	/// The type of the enumerable this is an enumerator over
	/// </typeparam>
	public abstract class ResultEnumerator<TResult, TCache, TEnumerable> : IAsyncEnumerator<TResult>, IEnumerator<TResult> where TCache : class where TEnumerable : ResultEnumerable<TResult, TCache> {
		private bool disposedValue = false;

		/// <summary>The enumerable this is an enumerator over</summary>
		public TEnumerable Enumerable {
			get;
		}
		public CancellationToken CancellationToken {
			get;
		}

		/// <summary>The current page number being worked on</summary>
		/// <remarks>
		/// Because of how some endpoints work, this might not increase by 1,
		/// but rather it might be by another interval.
		/// This would be the case if the "pages" are simply a length and start at
		/// an offset.
		/// In that case, these page numbers will be the length of each page.
		/// </remarks>
		public int? PageId {
			get; protected set;
		}
		/// <summary>Items in the the current page</summary>
		public IReadOnlyList<TResult>? CurrentPage {
			get; protected set;
		}
		/// <summary>Index into <see cref="CurrentPage"/></summary>
		public int PageIndex {
			get; protected set;
		} = -1;

		/// <summary>
		/// Number of results that have been enumerated so far
		/// </summary>
		public int Count {
			get; set;
		}
		/// <summary>
		/// Page numbers that have been completed
		/// </summary>
		protected IList<int> Processed = new List<int>();

		/// <inheritdoc cref="IEnumerator.Current"/>
		public TResult Current => this.CurrentPage![this.PageIndex];
		object? IEnumerator.Current => this.Current;

		/// <inheritdoc cref="IEnumerator.MoveNext()"/>
		[Obsolete("Use MoveNextAsync")]
		public bool MoveNext() {
			return this.MoveNextAsync().GetAwaiter().GetResult();
		}
		/// <inheritdoc cref="IAsyncEnumerator{T}.MoveNextAsync()"/>
		public ValueTask<bool> MoveNextAsync() {
			if (this.PageId == null) {
				// No page is currently in use, either freshly-constructed or reset
				// this.LastPage is guarenteed to not be set
				return this.NextPageAsync();
			}
			var nextpos = this.PageIndex + 1;
			// avoid incrementing this.PageIndex in case we return false
			if (nextpos >= this.CurrentPage!.Count) {
				// Current page has been used up
				return this.LastPage ? new ValueTask<bool>(false) : this.NextPageAsync();
			}
			this.PageIndex = nextpos;
			++this.Count;
			return new ValueTask<bool>(true);
		}
		/// <summary>
		/// Part of <see cref="MoveNextAsync()"/>, handles transitions between pages
		/// </summary>
		/// <remarks>
		/// Implementers should increment <see cref="Count"/> on successfully
		/// advancing to the next page. If there is no next page to retrieve after
		/// the current one (either known in advance or if an empty page is
		/// returned, for example), implementers should also set
		/// <see cref="LastPage"/> to <see langword="true"/>.
		/// </remarks>
		/// <returns>
		/// <see langword="true"/> if this was advanced;
		/// <see langword="false"/> otherwise
		/// </returns>
		protected abstract ValueTask<bool> NextPageAsync();
		/// <summary>
		/// Indicates whether this is the last page and if any subsequent calls to
		/// <see cref="NextPage"> should be avoided
		/// </summary>
		protected bool LastPage {
			get; set;
		} = false;

		/// <summary>
		/// Reset the page back to the beginning.
		/// Should yield the pages in the same exact order, too.
		/// </summary>
		/// <remarks>
		/// Does not modify <see cref="CurrentPage"/>
		/// </remarks>
		public virtual void Reset() {
			this.PageIndex = -1;
			this.PageId = null;
			this.Count = 0;
			this.Processed.Clear();
			this.LastPage = false;
		}

		protected virtual void Dispose(bool disposing) {
			if (!this.disposedValue) {
				if (disposing) {
					// dispose managed state (managed objects)
				}
				// free unmanaged resources (unmanaged objects) and override finalizer

				// set large fields to null
				this.CurrentPage = null;
				this.PageId = null;
				this.Processed = null!;

				this.disposedValue = true;
			}
		}
		public void Dispose() {
			this.Dispose(disposing: true);
			System.GC.SuppressFinalize(this);
		}
		/// <summary>
		/// Perform the asynchronous cleanup of managed resources
		/// and/or cascading calls to DisposeAsync().
		/// </summary>
		/// <seealso href="https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-disposeasync#the-disposeasynccore-method"/>
		protected virtual ValueTask DisposeAsyncCore() {
			// No items to dispose asynchronously.
			return new ValueTask();
		}
		public async ValueTask DisposeAsync() {
			await this.DisposeAsyncCore().ConfigureAwait(false);
			this.Dispose(false);
			System.GC.SuppressFinalize(this);
		}

		protected ResultEnumerator(TEnumerable enumerable, CancellationToken cancellationToken) {
			this.Enumerable = enumerable;
			this.CancellationToken = cancellationToken;
		}
	}
}
