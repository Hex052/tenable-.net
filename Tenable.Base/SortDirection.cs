using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.Base {
	[JsonConverter(typeof(SortDirectionConverter))]
	public enum SortDirection {
		/// <summary>Sort in ascending order</summary>
		ASC = 0,
		/// <summary>Sort in descending order</summary>
		DESC = 1,
	}
}
