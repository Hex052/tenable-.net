using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class PluginAttribute {
		private string _attributeName;
		/// <exception cref="ArgumentNullException">
		/// On set, value is <see langword="null"/>
		/// </exception>
		[JsonPropertyName("attribute_name")]
		public string AttributeName {
			get => this._attributeName;
			set => this._attributeName = value ?? throw new ArgumentNullException(nameof(value));
		}

		private string _attributeValue;
		/// <exception cref="ArgumentNullException">
		/// On set, value is <see langword="null"/>
		/// </exception>
		[JsonPropertyName("attribute_value")]
		public string AttributeValue {
			get => this._attributeValue;
			set => this._attributeValue = value ?? throw new ArgumentNullException(nameof(value));
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		/// <summary>
		/// Construct a new <see cref="PluginAttribute"/>
		/// </summary>
		/// <param name="attributeName">
		/// Value to assign to <see cref="AttributeName"/>
		/// </param>
		/// <param name="attributeValue">
		/// Value to assign to <see cref="AttributeValue"/>
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="attributeName"/> and/or
		/// <paramref name="attributeValue"/> is <see langword="null"/>
		/// </exception>
		[JsonConstructor]
		public PluginAttribute(string attributeName, string attributeValue) {
			this._attributeName = attributeName ?? throw new ArgumentNullException(nameof(attributeName));
			this._attributeValue = attributeValue ?? throw new ArgumentNullException(nameof(attributeValue));
		}
	}
}
