using System;
using System.Text.Json.Serialization;

namespace Tenable.Base {
	/// <remarks>
	/// This class is reused for the filter and as part of
	/// <see cref="Results.AgentPagination">
	/// </remarks>
	public sealed class SortOrder : IEquatable<SortOrder> {
		private string _field;
		/// <summary>The field to sort by</summary>
		[JsonPropertyName("name")]
		public string Field {
			get => this._field;
			set => this._field = value ?? throw new ArgumentNullException(nameof(value));
		}

		private SortDirection _direction;

		/// <summary>The direction <see cref="Field"> is sorted in</summary>
		[JsonPropertyName("order")]
		public SortDirection Direction {
			get => this._direction;
			set {
				_verifyDirection(nameof(value), value);
				this._direction = value;
			}
		}
		private static void _verifyDirection(string paramName, SortDirection direction) {
			if (!Enum.IsDefined(typeof(SortDirection), direction)) {
				throw new ArgumentOutOfRangeException(
					paramName,
					direction,
					$"Not valid member of {nameof(SortDirection)}");
			}
		}

		[JsonConstructor]
		public SortOrder(string field, SortDirection direction) {
			this._field = field ?? throw new ArgumentNullException(nameof(field));
			_verifyDirection(nameof(direction), direction);
			this.Direction = direction;
		}

		public override string ToString() {
			return string.Format(
				"{0}:{1}",
				this.Field,
				this.Direction == SortDirection.ASC ? "asc" : "desc");
		}

		public bool Equals(SortOrder? other) {
			if (ReferenceEquals(this, other)) {
				return true;
			}
			else if (ReferenceEquals(other, null)) {
				return false;
			}
			return this.Field == other.Field
				&& this.Direction == other.Direction;
		}

		public override bool Equals(object? obj) {
			return obj is SortOrder other && this.Equals(other);
		}

		public override int GetHashCode() {
			return (
				this.Field.GetHashCode(),
				this.Direction).GetHashCode();
		}

		public static bool operator ==(SortOrder? lhs, SortOrder? rhs) {
			if (ReferenceEquals(lhs, null)) {
				return ReferenceEquals(rhs, null);
			}
			return ReferenceEquals(lhs, rhs) || lhs.Equals(rhs);
		}
		public static bool operator !=(SortOrder? lhs, SortOrder? rhs) {
			return !(lhs == rhs);
		}
	}
}
