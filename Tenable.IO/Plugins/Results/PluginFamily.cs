using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class PluginFamily {
		/// <summary>The unique ID of the family</summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}

		/// <summary>Name of the family</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}

		/// <summary>The number of plugins in the family</summary>
		[JsonPropertyName("count")]
		public int Count {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public PluginFamily(int id, string name, int count) {
			this.Id = id;
			this.Name = name;
			this.Count = count;
		}
	}
}
