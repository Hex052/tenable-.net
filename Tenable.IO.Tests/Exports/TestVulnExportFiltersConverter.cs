using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using Tenable.IO.Exports;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestVulnExportFiltersConverter {
		[Fact]
		public void TestAllNull() {
			var filters = new VulnExportFilters();

			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new VulnExportFiltersConverter();

			converter.Write(jsonWriter, filters, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected: "{}", actual: result);
		}

		[Theory]
		[InlineData(nameof(VulnExportFilters.CidrRange), "10.0.0.0/8", "cidr_range")]

		public void TestSingleStringProperty(string propertyName, string input, string expectedName) {
			var filters = new VulnExportFilters();
			typeof(VulnExportFilters).GetProperty(propertyName)!
				.SetValue(filters, input);

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			Assert.Equal(JsonValueKind.String, enumerator.Current.Value.ValueKind);
			Assert.Equal(input, enumerator.Current.Value.GetString());
			Assert.False(enumerator.MoveNext());
		}

		[Theory]
		[InlineData(nameof(VulnExportFilters.FirstFound), "1970-01-01T00:00:00", "first_found", 0)]
		[InlineData(nameof(VulnExportFilters.FirstFound), "2021-05-15T05:50:32", "first_found", 1621057832)]
		[InlineData(nameof(VulnExportFilters.LastFound), "1970-01-01T00:00:00", "last_found", 0)]
		[InlineData(nameof(VulnExportFilters.LastFixed), "1970-01-01T00:00:00", "last_fixed", 0)]
		[InlineData(nameof(VulnExportFilters.Since), "1970-01-01T00:00:00", "since", 0)]

		public void TestSingleDateTimeProperty(string propertyName, string input, string expectedName, int expectedValue) {
			var filters = new VulnExportFilters();
			typeof(VulnExportFilters).GetProperty(propertyName)!
				.SetValue(filters, DateTime.Parse(input));

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			Assert.Equal(JsonValueKind.Number, enumerator.Current.Value.ValueKind);
			Assert.Equal(expectedValue, enumerator.Current.Value.GetInt32());
			Assert.False(enumerator.MoveNext());
		}

		[Theory]
		[InlineData(nameof(VulnExportFilters.PluginFamily), "Windows;General", "plugin_family")]
		[InlineData(nameof(VulnExportFilters.Severity), "low;critical", "severity")]
		[InlineData(nameof(VulnExportFilters.Severity), "fixed;open", "severity")]
		[InlineData(nameof(VulnExportFilters.Severity), "fixed", "severity")]

		public void TestSingleEnumerableStringProperty(string propertyName, string inputs, string expectedName, string? expectedValues = null) {
			var filters = new VulnExportFilters();
			typeof(VulnExportFilters).GetProperty(propertyName)!
				.SetValue(filters, inputs.Split(';'));

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			var val = enumerator.Current.Value;
			Assert.Equal(JsonValueKind.Array, val.ValueKind);
			Assert.True(
				val.EnumerateArray()
				.All(v => v.ValueKind == JsonValueKind.String));
			Assert.True(
				val.EnumerateArray()
					.Select(f => f.GetString())
					.OrderBy(f => f)
					.SequenceEqual(
						(expectedValues ?? inputs)
						.Split(';')
						.OrderBy(f => f)));
			Assert.False(enumerator.MoveNext());
		}

		[Theory]
		[InlineData(nameof(VulnExportFilters.PluginId), "plugin_id")]
		[InlineData(nameof(VulnExportFilters.PluginId), "plugin_id", 17)]
		[InlineData(nameof(VulnExportFilters.PluginId), "plugin_id", 17, 138436854)]

		public void TestSingleEnumerableIntProperty(string propertyName, string expectedName, params int[] values) {
			var filters = new VulnExportFilters();
			values = values.OrderBy(f => f).ToArray();
			typeof(VulnExportFilters).GetProperty(propertyName)!
				.SetValue(filters, values);

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			var val = enumerator.Current.Value;
			Assert.Equal(JsonValueKind.Array, val.ValueKind);
			Assert.True(
				val.EnumerateArray()
				.All(v => v.ValueKind == JsonValueKind.Number));
			Assert.True(
				val.EnumerateArray()
					.Select(f => f.GetInt32())
					.OrderBy(f => f)
					.SequenceEqual(values));
			Assert.False(enumerator.MoveNext());
		}

		[Fact]
		public void TestTagsEmpty() {
			TestTags(Array.Empty<KeyValuePair<string, string>>());
		}

		[Theory]
		[InlineData("OS:Windows", "OS:MacOS", "Location:Dubai")]
		public void TestName(params string[] keyValuePairs) {
			var pairs = keyValuePairs.Select(f => {
				var vals = f.Split(':');
				Assert.Equal(2, vals.Length);
				return new KeyValuePair<string, string>(vals[0], vals[1]);
			})
				.ToList();
			TestTags(pairs);
		}


		protected static void TestTags(IEnumerable<KeyValuePair<string, string>> pairs) {
			var filters = new VulnExportFilters() { Tag = pairs };

			var result = Serialize(filters);
			var expected = pairs
				.GroupBy(p => p.Key)
				.ToDictionary(g => "tag." + g.Key, g => g.Select(t => t.Value).ToList());
			var actual = JsonSerializer.Deserialize<Dictionary<string, List<string>>>(result);
			Assert.Equal(expected, actual);
		}

		protected static string Serialize(VulnExportFilters filters) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new VulnExportFiltersConverter();
			converter.Write(jsonWriter, filters, new JsonSerializerOptions());
			jsonWriter.Flush();
			return Encoding.UTF8.GetString(buffer.WrittenSpan);
		}
	}
}
