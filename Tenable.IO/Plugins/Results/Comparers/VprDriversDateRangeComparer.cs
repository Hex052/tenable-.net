using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class VprDriversDateRangeComparer : EqualityComparer<VprDriversDateRange> {

		[Pure]
		public override bool Equals([AllowNull] VprDriversDateRange x, [AllowNull] VprDriversDateRange y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.LowerBound == y.LowerBound && x.UpperBound == y.UpperBound;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] VprDriversDateRange obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return obj.LowerBound
						+ (obj.UpperBound.HasValue ? obj.UpperBound.Value : 0);
				}
			}
		}
	}
}
