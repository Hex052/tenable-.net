using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using RestSharp;
using Tenable.Base;
using Tenable.Base.Tests;
using Tenable.IO.Exports;
using Tenable.IO.Exports.Result;
using Tenable.IO.Exports.Result.Vulns;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestVulnEnumerable {
		private class ExportEnumeratorSleep : ExportEnumerator<Vuln, VulnExportStatus> {
			public Func<int, Task>? SleepFunction {
				get; set;
			}
			public ExportEnumeratorSleep(ExportEnumerable<Vuln, VulnExportStatus> enumerable, CancellationToken cancellationToken) : base(enumerable, cancellationToken) {
			}
			protected override Task Sleep(int milliseconds) {
				return this.SleepFunction is null ? Task.CompletedTask : this.SleepFunction(milliseconds);
			}
		}
		[Theory]
		[InlineData("9b3e8d50-b27a-4784-af16-2e1f88d2fd60", 52)]
		[InlineData("a1f04ae8-34f8-45df-bc8f-66a872077ac9", 200)]
		public async Task TestGetPage(string uuid, int pageId) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var expectedUrl = string.Format("vulns/export/{0}/chunks/{1}", uuid, pageId);
			var result = new[] {
				new Vuln(
					new VulnAsset(Guid.Empty, Guid.Empty, true),
					new Plugin(
						string.Empty,
						string.Empty,
						2,
						true,
						string.Empty,
						DateTime.Now,
						string.Empty,
						string.Empty,
						string.Empty,
						string.Empty),
					"info",
					0,
					0,
					DateTime.Now,
					DateTime.Now,
					"OPEN"),
			};
			var api = new VulnEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(result);

			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.GetPageAsync(pageId, tokenSource.Token));
			// Second call should be cached; should only have one call to the REST API
			Assert.Same(result, await api.GetPageAsync(pageId, tokenSource.Token));

			tio.Verify(
				tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("e5890978-2372-4178-85d7-d12d9fd1df46")]
		[InlineData("717353c2-346e-446e-9a04-399dcbf23b7f")]
		public async Task TestGetStatusAsync(string uuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new VulnExportStatus(
				Status.PROCESSING,
				new[] { 1, 5, 8 },
				Guid.Empty,
				Array.Empty<int>(),
				Array.Empty<int>(),
				12,
				3,
				1,
				3,
				new EmptyJsonObject(),
				50,
				DateTime.Now);
			var expectedUrl = string.Format("vulns/export/{0}/status", uuid);
			var api = new VulnEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<VulnExportStatus>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(result);

			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.GetStatusAsync(tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<VulnExportStatus>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("20c14a9c-a96b-4565-bf21-9a37b8b1297a")]
		[InlineData("fd9a282b-56af-46e9-8522-63b44436c53b")]
		public async Task TestCancelAsync(string uuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new ExportStatus(Status.CANCELLED);
			var expectedUrl = string.Format("vulns/export/{0}/cancel", uuid);
			var api = new VulnEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<ExportStatus>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(result);

			using var tokenSource = new CancellationTokenSource();
			Assert.Equal(Status.CANCELLED, await api.CancelAsync(tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<ExportStatus>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public void TestGetEnumerator() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var expected = string.Format("vulns/export/{0}/cancel", Guid.Empty);
			var api = new VulnEnumerable(tio.Object, Guid.Empty);
			Assert.Equal(
				default(CancellationToken),
				Assert.IsAssignableFrom<ExportEnumerator<Vuln, VulnExportStatus>>(api.GetEnumerator()).CancellationToken);
			using var tokenSource = new CancellationTokenSource();
			Assert.Equal(
				tokenSource.Token,
				Assert.IsAssignableFrom<ExportEnumerator<Vuln, VulnExportStatus>>(api.GetAsyncEnumerator(tokenSource.Token)).CancellationToken);
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestGetAll() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var enumerable = new Mock<VulnEnumerable>(MockBehavior.Loose, new object[] { tio.Object, Guid.Empty });
			var status = new VulnExportStatus[] {
				new VulnExportStatus(Status.QUEUED, Array.Empty<int>(), Guid.Empty, Array.Empty<int>(), Array.Empty<int>(), 4, 0, 0, 0, new object(), 5, DateTime.MinValue),
				new VulnExportStatus(Status.PROCESSING, new int[] { 3 }, Guid.Empty, Array.Empty<int>(), Array.Empty<int>(), 4, 0, 0, 0, new object(), 5, DateTime.MinValue),
				new VulnExportStatus(Status.PROCESSING, new int[] {1, 3 }, Guid.Empty, Array.Empty<int>(), Array.Empty<int>(), 4, 0, 0, 0, new object(), 5, DateTime.MinValue),
				new VulnExportStatus(Status.FINISHED, new int[] {1, 2, 3, 4 }, Guid.Empty, Array.Empty<int>(), Array.Empty<int>(), 4, 0, 0, 0, new object(), 5, DateTime.MinValue),
			};
			var chunk1 = new Vuln[] {
				new Vuln(new VulnAsset(new Guid("4663314c-c367-4de9-959b-5ebfa49e4f81"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("dc071635-676c-4c45-ba7a-4b3e658840ce"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("04dce16d-acde-4b12-a716-065202955131"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("2d62cbf3-f261-4ffb-b13a-de12d25858a6"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("3abe53bd-6c08-4475-b405-942abeed162e"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
			};
			var chunk2 = new Vuln[] {
				new Vuln(new VulnAsset(new Guid("f6b72294-4a02-4512-bd0c-a29f92d916dc"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("606c34d0-c3d8-4731-a176-e2728e8c2ebe"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("2d7f549e-e802-4420-8501-5f11e6805ff6"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("0c202340-fb7c-4598-a2a3-68174b21789b"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("7132e6b6-8844-441b-9d67-ef6c04c0b1be"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
			};
			var chunk3 = new Vuln[] {
				new Vuln(new VulnAsset(new Guid("025884fa-944d-402a-b2d7-784659c97806"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("302ee368-b271-4d9a-a23e-a6c26bb08408"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("dfcb332a-9183-4a0f-ba97-94fba2565f31"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("b1adc95e-6b83-42ea-9a1a-e8ed6276ab39"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
				new Vuln(new VulnAsset(new Guid("7f762830-4049-4bc3-bedb-2737c2d7bf5b"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
			};
			var chunk4 = new Vuln[] {
				new Vuln(new VulnAsset(new Guid("f973f641-8875-4fdb-96c6-971eb598dbad"), Guid.Empty, true), plugin: null!, "LOW", 1, 1, DateTime.MinValue, DateTime.MaxValue, "OPEN"),
			};
			var sleepCalls = new List<int>();
			enumerable.CallBase = true;
			enumerable.Setup(e => e.GetEnumerator())
				.Returns(() => new ExportEnumeratorSleep(enumerable.Object, default) { SleepFunction = i => Task.Run(() => sleepCalls.Add(i)) });
			enumerable.Setup(e => e.GetAsyncEnumerator(It.IsAny<CancellationToken>()))
				.Returns((CancellationToken c) => new ExportEnumeratorSleep(enumerable.Object, c) { SleepFunction = i => Task.Run(() => sleepCalls.Add(i)) });
			var returnResult = Util.ItemsFromListResettable(status.Append(status[3]).Select(f => Task.FromResult<VulnExportStatus?>(f)));
			tio.Setup(
				tio => tio.RequestJsonAsync<VulnExportStatus>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/status", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.Returns(returnResult.Call);
			tio.Setup(
				tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/chunks/1", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(chunk1);
			tio.Setup(
				tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/chunks/2", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(chunk2);
			tio.Setup(
				tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/chunks/3", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(chunk3);
			tio.Setup(
				tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/chunks/4", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(chunk4);

			// synchronous to list
			var results = enumerable.Object.ToList();
			Assert.Equal(chunk3.Concat(chunk1).Concat(chunk2).Concat(chunk4), results);
			tio.Verify(
				tio => tio.RequestJsonAsync<VulnExportStatus>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/status", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == default)),
				Times.Between(4, 5, Moq.Range.Inclusive));
			for (var i = 1; i < 5; ++i) {
				tio.Verify(
					tio => tio.RequestJsonAsync<IReadOnlyList<Vuln>>(
						It.Is<string>(
							string.Format("vulns/export/00000000-0000-0000-0000-000000000000/chunks/{0}", i),
							StringComparer.InvariantCulture),
						It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
						It.Is<CancellationToken>(c => c == default)),
					Times.Once());
			}
			tio.VerifyNoOtherCalls();
			Assert.Single(sleepCalls);
			Assert.True(sleepCalls[0] > 10_000);
			enumerable.Verify(e => e.GetEnumerator(), Times.Once());

			// asynchronous to list
			returnResult.Reset();
			results.Clear();
			sleepCalls.Clear();
			using var tokenSource = new CancellationTokenSource();
			await foreach (var i in enumerable.Object.WithCancellation(tokenSource.Token)) {
				results.Add(i);
			}
			Assert.Equal(chunk3.Concat(chunk1).Concat(chunk2).Concat(chunk4), results);
			// Status is cached in the enumerator not the enumerable, so this will
			// be requested again, but chunks are cached.
			tio.Verify(
				tio => tio.RequestJsonAsync<VulnExportStatus>(
					It.Is<string>("vulns/export/00000000-0000-0000-0000-000000000000/status", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Between(4, 5, Moq.Range.Inclusive));
			tio.VerifyNoOtherCalls();
			Assert.Single(sleepCalls);
			Assert.True(sleepCalls[0] > 10_000);
			enumerable.Verify(e => e.GetAsyncEnumerator(It.Is<CancellationToken>(c => c == tokenSource.Token)), Times.Once());
		}
	}
}
