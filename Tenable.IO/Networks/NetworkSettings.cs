using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.Networks {
	public class NetworkSettings : IEquatable<NetworkSettings> {
		/// <summary>Name of the network</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}

		/// <summary>Description of the network</summary>
		[JsonPropertyName("description")]
		public string? Description {
			get; set;
		}

		/// <summary>
		/// The number of days before assets age out (not seen on any scan)
		/// and are permanently deleted
		/// </summary>
		/// <remarks>Min value: 14, max value: 365</remarks>
		[JsonPropertyName("assets_ttl_days")]
		public int? AssetsTtlDays {
			get; set;
		}

		[JsonConstructor]
		public NetworkSettings(string name) {
			this.Name = name;
		}

		/// <summary>
		/// Copy the NetworkSettings object, in case this is a more derived
		/// object that should be serialized to JSON without extra fields
		/// </summary>
		/// <returns>Copy of this <see cref="NetworkSettings"/></returns>
		internal NetworkSettings CloneSettings() {
			return new NetworkSettings(this.Name) {
				Description = this.Description,
				AssetsTtlDays = this.AssetsTtlDays,
			};
		}

		public bool Equals(NetworkSettings? other) {
			if (ReferenceEquals(this, other)) {
				return true;
			}
			else if (ReferenceEquals(other, null)) {
				return false;
			}
			return this.Name == other.Name
				&& this.Description == other.Description
				&& this.AssetsTtlDays == other.AssetsTtlDays;
		}
		public override bool Equals(object? obj) {
			return obj is NetworkSettings n && this.Equals(n);
		}
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		public static bool operator ==(NetworkSettings? lhs, NetworkSettings? rhs) {
			if (ReferenceEquals(lhs, null)) {
				return ReferenceEquals(rhs, null);
			}
			return lhs.Equals(rhs);
		}
		public static bool operator !=(NetworkSettings? lhs, NetworkSettings? rhs) {
			return !(lhs == rhs);
		}
	}
}
