using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Tenable.IO.Plugins.Results.JsonConverters;

namespace Tenable.IO.Plugins.Results {
	/// <seealso href="https://developer.tenable.com/docs/tenable-plugin-attributes"/>
	public class PluginAttributes {
		/// <summary>
		/// The date on which this plugin was last updated
		/// </summary>
		[JsonPropertyName("plugin_modification_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? ModificationDate {
			get; set;
		}
		/// <summary>
		/// The date on which this plugin was published
		/// </summary>
		[JsonPropertyName("plugin_publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? PublicationDate {
			get; set;
		}
		/// <summary>
		/// The date on which this vulnerability was published
		/// </summary>
		[JsonPropertyName("vuln_publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? VulnPublicationDate {
			get; set;
		}
		/// <summary>
		/// The date on which the vendor published a patch for the vulnerability
		/// </summary>
		[JsonPropertyName("patch_publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? PatchPublicationDate {
			get; set;
		}

		/// <summary>
		/// The version of this plugin used
		/// </summary>
		[JsonPropertyName("plugin_version")]
		[JsonConverter(typeof(VersionNumberConverter))]
		public Version? Version {
			get; set;
		}
		/// <summary>
		/// Brief description of the plugin or vulnerability
		/// </summary>
		[JsonPropertyName("synopsis")]
		public string? Synopsis {
			get; set;
		}
		/// <summary>
		/// text description of the plugin
		/// </summary>
		[JsonPropertyName("description")]
		public string Description {
			get; set;
		}
		/// <summary>
		/// Remediation information for the vulnerability
		/// </summary>
		[JsonPropertyName("solution")]
		public string? Solution {
			get; set;
		}
		/// <summary>
		/// TODO
		/// </summary>
		[JsonPropertyName("stig_severity")]
		public string? StigSeverity {
			get; set;
		}
		/// <summary>
		/// The risk factor associated with the plugin
		/// </summary>
		[JsonPropertyName("risk_factor")]
		public RiskFactor RiskFactor {
			get => this._riskFactor;
			set {
				if (Enum.IsDefined(typeof(RiskFactor), value)) {
					this._riskFactor = value;
				}
				else {
					throw new ArgumentOutOfRangeException(nameof(value), "invalid enum");
				}
			}
		}
		private RiskFactor _riskFactor;
		/// <summary>
		/// The general type of plugin check (for example, local or remote).
		/// </summary>
		[JsonPropertyName("plugin_type")]
		public string PluginType {
			get; set;
		}
		/// <summary>
		/// TODO
		/// </summary>
		[JsonPropertyName("intel_type")]
		public string IntelType {
			get; set;
		}

		/// <summary>
		/// This vulnerability is known to be exploited by malware
		/// </summary>
		[JsonPropertyName("exploited_by_malware")]
		public bool? ExploitedByMalware {
			get; set;
		}
		/// <summary>
		/// Whether the plugin targets potentially malicious files or processes
		/// </summary>
		[JsonPropertyName("malware")]
		public bool? ChecksForMalware {
			get; set;
		}

		/// <summary>
		/// Software found by this plugin is unsupported by the software's vendor
		/// </summary>
		[JsonPropertyName("unsupported_by_vendor")]
		public bool? UnsupportedByVendor {
			get; set;
		}
		/// <summary>
		/// Whether the vendor has published a patch for the vulnerability
		/// </summary>
		[JsonPropertyName("has_patch")]
		public bool HasPatch {
			get; set;
		}
		/// <summary>
		/// A value specifying whether the plugin checks for default accounts
		/// </summary>
		[JsonPropertyName("default_account")]
		public bool? ChecksForDefaultAccount {
			get; set;
		}
		/// <summary>
		/// Whether this plugin has received media attention,
		/// such as for ShellShock or Meltdown
		/// </summary>
		[JsonPropertyName("in_the_news")]
		public bool? InTheNews {
			get; set;
		}
		/// <summary>
		/// TODO
		/// </summary>
		[JsonPropertyName("always_run")]
		public bool? AlwaysRun {
			get; set;
		}
		/// <summary>
		/// TODO
		/// </summary>
		[JsonPropertyName("compliance")]
		public bool? Compliance {
			get; set;
		}
		/// <summary>
		/// TODO
		/// </summary>
		[JsonPropertyName("potential_vulnerability")]
		public bool? PotentialVulnerability {
			get; set;
		}

		/// <summary>
		/// Whether a known public exploit exists for this vulnerability
		/// </summary>
		[JsonPropertyName("exploit_available")]
		public bool? ExploitAvailable {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the Immunity CANVAS framework
		/// </summary>
		[JsonPropertyName("exploit_framework_canvas")]
		public bool? ExploitFrameworkCanvas {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the CORE impact framework
		/// </summary>
		[JsonPropertyName("exploit_framework_core")]
		public bool? ExploitFrameworkCore {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the
		/// D2 Elliot Web Exploitation framework
		/// </summary>
		[JsonPropertyName("exploit_framework_d2_elliot")]
		public bool? ExploitFrameworkD2Elliot {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the ExploitHub framework
		/// </summary>
		[JsonPropertyName("exploit_framework_exploithub")]
		public bool? ExploitFrameworkExploitHub {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the Metasploit framework
		/// </summary>
		[JsonPropertyName("exploit_framework_metasploit")]
		public bool? ExploitFrameworkMetasploit {
			get; set;
		}
		/// <summary>
		/// Whether Nessus exploited the vulnerability
		/// during the process of identification
		/// </summary>
		[JsonPropertyName("exploited_by_nessus")]
		public bool? ExploitedByNessus {
			get; set;
		}
		/// <summary>
		/// Description of how easy it is to exploit the issue
		/// </summary>
		[JsonPropertyName("exploitability_ease")]
		public string? ExploitabilityEase {
			get; set;
		}

		/// <summary>The Common Platform Enumeration number</summary>
		[JsonPropertyName("cpe")]
		public IList<string>? Cpe {
			get; set;
		}
		/// <summary>The Common Vulnerability and Exposure ID</summary>
		[JsonPropertyName("cve")]
		public IList<string>? Cve {
			get; set;
		}

		/// <summary>
		/// The CVSS2 temporal score (characteristics of a vulnerability
		/// that change over time but not among user environments)
		/// </summary>
		[JsonPropertyName("cvss_temporal_score")]
		public decimal? CvssTemporalScore {
			get; set;
		}
		/// <summary>
		/// The CVSS2 base score (intrinsic and fundamental characteristics of
		/// a vulnerability that are constant over time and user environments)
		/// </summary>
		[JsonPropertyName("cvss_base_score")]
		public decimal? CvssBaseScore {
			get; set;
		}
		/// <summary>
		/// CVSS2 temporal metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss_temporal_vector")]
		public Cvss2TemporalVector? CvssTemporalVector {
			get; set;
		}
		/// <summary>
		/// Additional CVSS2 metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss_vector")]
		public Cvss2Vector? CvssVector {
			get; set;
		}

		/// <summary>
		/// The CVSSv3 base score (intrinsic and fundamental characteristics of
		/// a vulnerability that are constant over time and user environments)
		/// </summary>
		[JsonPropertyName("cvss3_base_score")]
		public decimal? Cvss3BaseScore {
			get; set;
		}
		/// <summary>
		/// The CVSSv3 temporal score (characteristics of a vulnerability
		/// that change over time but not among user environments)
		/// </summary>
		[JsonPropertyName("cvss3_temporal_score")]
		public decimal? Cvss3TemporalScore {
			get; set;
		}
		/// <summary>
		/// CVSSv3 temporal metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss3_temporal_vector")]
		public Cvss3TemporalVector? Cvss3TemporalVector {
			get; set;
		}
		/// <summary>
		/// Additional CVSSv3 metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss3_vector")]
		public Cvss3Vector? Cvss3Vector {
			get; set;
		}

		/// <remarks>
		/// Should be the same as <see cref="Xrefs"/>, but is parsed from an
		/// array of strings (using <see cref="XrefStringListConverter"/>)
		/// rather than an array of actual objects
		/// </remarks>
		/// <inheritdoc cref="Xrefs"/>
		[JsonPropertyName("xref")]
		[JsonConverter(typeof(XrefStringListConverter<IList<Xref>, List<Xref>>))]
		public IList<Xref>? Xref {
			get; set;
		}
		/// <summary>
		/// References to third-party information about the vulnerability,
		/// exploit, or update associated with the plugin
		/// </summary>
		[JsonPropertyName("xrefs")]
		public IList<Xref>? Xrefs {
			get; set;
		}
		/// <summary>
		/// Links to external websites that contain helpful information
		/// </summary>
		[JsonPropertyName("see_also")]
		public IList<string>? SeeAlso {
			get; set;
		}
		/// <summary>
		/// Bugtraq
		/// </summary>
		[JsonPropertyName("bid")]
		public IList<int>? BugtraqId {
			get; set;
		}

		/// <summary>
		/// Information about the Vulnerability Priority Rating (VPR)
		/// for the vulnerability
		/// </summary>
		[JsonPropertyName("vpr")]
		public Vpr? Vpr {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public PluginAttributes(string description, string pluginType,
				string intelType, RiskFactor riskFactor) {
			this.Description = description;
			this.PluginType = pluginType;
			this.IntelType = intelType;
			this.RiskFactor = riskFactor;
		}
	}
}
