using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Collections.Generic;

namespace Tenable.IO.Plugins.Results.JsonConverters {
	public class XrefStringListConverter<T, TResult> : JsonConverter<T?>
			where T : class, IEnumerable<Xref>
			where TResult : class, ICollection<Xref>, T, new() {
		public override bool HandleNull => true;

		public override bool CanConvert(Type typeToConvert) {
			return typeof(T).IsAssignableFrom(typeToConvert);
		}

		public override T? Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			switch (reader.TokenType) {
				case JsonTokenType.Null:
					return null;
				case JsonTokenType.StartArray:
					break;
				default:
					throw new JsonException();
			}

			var result = new TResult();

			while (reader.Read()) {
				switch (reader.TokenType) {
					case JsonTokenType.Null:
						result.Add(null!);
						break;
					case JsonTokenType.String:
						var str = reader.GetString()!;
						var splitloc = str.IndexOf(':');
						if (splitloc == -1) {
							return null;
						}
						result.Add(
							new Xref(
								str.Substring(0, splitloc),
								str.Substring(splitloc + 1)));
						break;
					case JsonTokenType.EndArray:
						return result;
					default:
						throw new JsonException(
							string.Format(
								"Unknown token {0}, expected {1}, {2}, {3}",
								reader.TokenType,
								JsonTokenType.String,
								JsonTokenType.Null,
								JsonTokenType.EndArray));
				}
			}
			throw new JsonException("input ended before complete");
		}

		public override void Write(Utf8JsonWriter writer,
				T? value, JsonSerializerOptions options) {
			if (ReferenceEquals(value, null)) {
				writer.WriteNullValue();
			}
			else {
				writer.WriteStartArray();
				foreach (var xref in value) {
					writer.WriteStringValue(xref?.ToString());
				}
				writer.WriteEndArray();
			}
		}
	}
}
