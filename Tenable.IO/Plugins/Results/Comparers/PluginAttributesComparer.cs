using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class PluginAttributesComparer : EqualityComparer<PluginAttributes> {
		private IEqualityComparer<Vpr> _vprComparer;
		public IEqualityComparer<Vpr> VprComparer {
			get => this._vprComparer;
			set => this._vprComparer = value ?? throw new ArgumentNullException(nameof(value));
		}
		private IEqualityComparer<Xref> _xrefComparer;
		public IEqualityComparer<Xref> XrefComparer {
			get => this._xrefComparer;
			set => this._xrefComparer = value ?? throw new ArgumentNullException(nameof(value));
		}
		private IEqualityComparer<Cvss2TemporalVector> _cvss2TemporalVectorComparer;
		public IEqualityComparer<Cvss2TemporalVector> Cvss2TemporalVectorComparer {
			get => this._cvss2TemporalVectorComparer;
			set => this._cvss2TemporalVectorComparer = value ?? throw new ArgumentNullException(nameof(value));
		}
		private IEqualityComparer<Cvss2Vector> _cvss2VectorComparer;
		public IEqualityComparer<Cvss2Vector> Cvss2VectorComparer {
			get => this._cvss2VectorComparer;
			set => this._cvss2VectorComparer = value ?? throw new ArgumentNullException(nameof(value));
		}
		private IEqualityComparer<Cvss3TemporalVector> _cvss3TemporalVectorComparer;
		public IEqualityComparer<Cvss3TemporalVector> Cvss3TemporalVectorComparer {
			get => this._cvss3TemporalVectorComparer;
			set => this._cvss3TemporalVectorComparer = value ?? throw new ArgumentNullException(nameof(value));
		}
		private IEqualityComparer<Cvss3Vector> _cvss3VectorComparer;
		public IEqualityComparer<Cvss3Vector> Cvss3VectorComparer {
			get => this._cvss3VectorComparer;
			set => this._cvss3VectorComparer = value ?? throw new ArgumentNullException(nameof(value));
		}

		public PluginAttributesComparer(
				IEqualityComparer<Vpr>? vprComparer = null,
				IEqualityComparer<Xref>? xrefComparer = null,
				IEqualityComparer<Cvss2TemporalVector>? cvss2TemporalVectorComparer = null,
				IEqualityComparer<Cvss2Vector>? cvss2VectorComparer = null,
				IEqualityComparer<Cvss3TemporalVector>? cvss3TemporalVectorComparer = null,
				IEqualityComparer<Cvss3Vector>? cvss3VectorComparer = null) {
			this._vprComparer = vprComparer ?? new VprComparer();
			this._xrefComparer = xrefComparer ?? new XrefComparer();
			this._cvss2TemporalVectorComparer = cvss2TemporalVectorComparer ?? new Cvss2TemporalVectorComparer();
			this._cvss2VectorComparer = cvss2VectorComparer ?? new Cvss2VectorComparer();
			this._cvss3TemporalVectorComparer = cvss3TemporalVectorComparer ?? new Cvss3TemporalVectorComparer();
			this._cvss3VectorComparer = cvss3VectorComparer ?? new Cvss3VectorComparer();
		}

		[Pure]
		public override bool Equals([AllowNull] PluginAttributes x, [AllowNull] PluginAttributes y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.ModificationDate == y.ModificationDate
					&& x.PublicationDate == y.PublicationDate
					&& x.VulnPublicationDate == y.VulnPublicationDate
					&& x.PatchPublicationDate == y.PatchPublicationDate
					&& x.Version == y.Version
					&& x.Synopsis == y.Synopsis
					&& x.Description == y.Description
					&& x.Solution == y.Solution
					&& x.StigSeverity == y.StigSeverity
					&& x.RiskFactor == y.RiskFactor
					&& x.PluginType == y.PluginType
					&& x.IntelType == y.IntelType
					&& x.ExploitedByMalware == y.ExploitedByMalware
					&& x.ChecksForMalware == y.ChecksForMalware
					&& x.UnsupportedByVendor == y.UnsupportedByVendor
					&& x.HasPatch == y.HasPatch
					&& x.ChecksForDefaultAccount == y.ChecksForDefaultAccount
					&& x.InTheNews == y.InTheNews
					&& x.AlwaysRun == y.AlwaysRun
					&& x.Compliance == y.Compliance
					&& x.PotentialVulnerability == y.PotentialVulnerability
					&& x.ExploitAvailable == y.ExploitAvailable
					&& x.ExploitFrameworkCanvas == y.ExploitFrameworkCanvas
					&& x.ExploitFrameworkCore == y.ExploitFrameworkCore
					&& x.ExploitFrameworkD2Elliot == y.ExploitFrameworkD2Elliot
					&& x.ExploitFrameworkExploitHub == y.ExploitFrameworkExploitHub
					&& x.ExploitFrameworkMetasploit == y.ExploitFrameworkMetasploit
					&& x.ExploitedByNessus == y.ExploitedByNessus
					&& x.ExploitabilityEase == y.ExploitabilityEase
					&& (
						ReferenceEquals(x.Cpe, y.Cpe)
						|| (
							!ReferenceEquals(x.Cpe, null)
							&& !ReferenceEquals(y.Cpe, null)
							&& x.Cpe.SequenceEqual(y.Cpe)))
					&& (
						ReferenceEquals(x.Cve, y.Cve)
						|| (
							!ReferenceEquals(x.Cve, null)
							&& !ReferenceEquals(y.Cve, null)
							&& x.Cve.SequenceEqual(y.Cve)))
					&& x.CvssTemporalScore == y.CvssTemporalScore
					&& x.CvssBaseScore == y.CvssBaseScore
					&& this._cvss2TemporalVectorComparer.Equals(x.CvssTemporalVector, y.CvssTemporalVector)
					&& this._cvss2VectorComparer.Equals(x.CvssVector, y.CvssVector)
					&& x.Cvss3BaseScore == y.Cvss3BaseScore
					&& x.Cvss3TemporalScore == y.Cvss3TemporalScore
					&& this._cvss3TemporalVectorComparer.Equals(x.Cvss3TemporalVector, y.Cvss3TemporalVector)
					&& this._cvss3VectorComparer.Equals(x.Cvss3Vector, y.Cvss3Vector)
					&& (
						ReferenceEquals(x.Xref, y.Xref)
						|| (
							!ReferenceEquals(x.Xref, null)
							&& !ReferenceEquals(y.Xref, null)
							&& x.Xref.SequenceEqual(y.Xref, this._xrefComparer)))
					&& (
						ReferenceEquals(x.Xrefs, y.Xrefs)
						|| (
							!ReferenceEquals(x.Xrefs, null)
							&& !ReferenceEquals(y.Xrefs, null)
							&& x.Xrefs.SequenceEqual(y.Xrefs, this._xrefComparer)))
					&& (
						ReferenceEquals(x.SeeAlso, y.SeeAlso)
						|| (
							!ReferenceEquals(x.SeeAlso, null)
							&& !ReferenceEquals(y.SeeAlso, null)
							&& x.SeeAlso.SequenceEqual(y.SeeAlso)))
					&& (
						ReferenceEquals(x.BugtraqId, y.BugtraqId)
						|| (
							!ReferenceEquals(x.BugtraqId, null)
							&& !ReferenceEquals(y.BugtraqId, null)
							&& x.BugtraqId.SequenceEqual(y.BugtraqId)))
					&& this._vprComparer.Equals(x.Vpr, y.Vpr);
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] PluginAttributes obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (obj.ModificationDate.HasValue ? obj.ModificationDate.Value.GetHashCode() : 0)
						+ (obj.PublicationDate.HasValue ? obj.PublicationDate.Value.GetHashCode() : 0)
						+ (obj.VulnPublicationDate.HasValue ? obj.VulnPublicationDate.Value.GetHashCode() : 0)
						+ (obj.PatchPublicationDate.HasValue ? obj.PatchPublicationDate.Value.GetHashCode() : 0)
						+ (ReferenceEquals(obj.Version, null) ? 0 : obj.Version.GetHashCode())
						+ (ReferenceEquals(obj.Synopsis, null) ? 0 : obj.Synopsis.GetHashCode())
						+ (ReferenceEquals(obj.Description, null) ? 0 : obj.Description.GetHashCode())
						+ (ReferenceEquals(obj.Solution, null) ? 0 : obj.Solution.GetHashCode())
						+ (ReferenceEquals(obj.StigSeverity, null) ? 0 : obj.StigSeverity.GetHashCode())
						+ obj.RiskFactor.GetHashCode()
						+ (ReferenceEquals(obj.PluginType, null) ? 0 : obj.PluginType.GetHashCode())
						+ (ReferenceEquals(obj.IntelType, null) ? 0 : obj.IntelType.GetHashCode())
						+ (obj.ExploitedByMalware.HasValue ? obj.ExploitedByMalware.Value.GetHashCode() : 0)
						+ (obj.ChecksForMalware.HasValue ? obj.ChecksForMalware.Value.GetHashCode() : 0)
						+ (obj.UnsupportedByVendor.HasValue ? obj.UnsupportedByVendor.Value.GetHashCode() : 0)
						+ obj.HasPatch.GetHashCode()
						+ (obj.ChecksForDefaultAccount.HasValue ? obj.ChecksForDefaultAccount.Value.GetHashCode() : 0)
						+ (obj.InTheNews.HasValue ? obj.InTheNews.Value.GetHashCode() : 0)
						+ (obj.AlwaysRun.HasValue ? obj.AlwaysRun.Value.GetHashCode() : 0)
						+ (obj.Compliance.HasValue ? obj.Compliance.Value.GetHashCode() : 0)
						+ (obj.PotentialVulnerability.HasValue ? obj.PotentialVulnerability.Value.GetHashCode() : 0)
						+ (obj.ExploitAvailable.HasValue ? obj.ExploitAvailable.Value.GetHashCode() : 0)
						+ (obj.ExploitFrameworkCanvas.HasValue ? obj.ExploitFrameworkCanvas.Value.GetHashCode() : 0)
						+ (obj.ExploitFrameworkCore.HasValue ? obj.ExploitFrameworkCore.Value.GetHashCode() : 0)
						+ (obj.ExploitFrameworkD2Elliot.HasValue ? obj.ExploitFrameworkD2Elliot.Value.GetHashCode() : 0)
						+ (obj.ExploitFrameworkExploitHub.HasValue ? obj.ExploitFrameworkExploitHub.Value.GetHashCode() : 0)
						+ (obj.ExploitFrameworkMetasploit.HasValue ? obj.ExploitFrameworkMetasploit.Value.GetHashCode() : 0)
						+ (obj.ExploitedByNessus.HasValue ? obj.ExploitedByNessus.Value.GetHashCode() : 0)
						+ (ReferenceEquals(obj.ExploitabilityEase, null) ? 0 : obj.ExploitabilityEase.GetHashCode())
						+ (ReferenceEquals(obj.Cpe, null) ? 0 : (int)obj.Cpe.Select<string, long>(c => c.GetHashCode()).Sum())
						+ (ReferenceEquals(obj.Cve, null) ? 0 : (int)obj.Cve.Select<string, long>(c => c.GetHashCode()).Sum())
						+ (obj.CvssBaseScore.HasValue ? obj.CvssBaseScore.Value.GetHashCode() : 0)
						+ (obj.CvssTemporalScore.HasValue ? obj.CvssTemporalScore.Value.GetHashCode() : 0)
						+ (ReferenceEquals(obj.CvssTemporalVector, null) ? 0 : this._cvss2TemporalVectorComparer.GetHashCode(obj.CvssTemporalVector))
						+ (ReferenceEquals(obj.CvssVector, null) ? 0 : this._cvss2VectorComparer.GetHashCode(obj.CvssVector))
						+ (obj.Cvss3BaseScore.HasValue ? obj.Cvss3BaseScore.Value.GetHashCode() : 0)
						+ (obj.Cvss3TemporalScore.HasValue ? obj.Cvss3TemporalScore.Value.GetHashCode() : 0)
						+ (ReferenceEquals(obj.Cvss3TemporalVector, null) ? 0 : this._cvss3TemporalVectorComparer.GetHashCode(obj.Cvss3TemporalVector))
						+ (ReferenceEquals(obj.Cvss3Vector, null) ? 0 : this._cvss3VectorComparer.GetHashCode(obj.Cvss3Vector))
						+ (ReferenceEquals(obj.Xref, null) ? 0 : (int)obj.Xref.Select<Xref, long>(x => this._xrefComparer.GetHashCode(x)).Sum())
						+ (ReferenceEquals(obj.Xrefs, null) ? 0 : (int)obj.Xrefs.Select<Xref, long>(x => this._xrefComparer.GetHashCode(x)).Sum())
						+ (ReferenceEquals(obj.SeeAlso, null) ? 0 : (int)obj.SeeAlso.Select<string, long>(s => s.GetHashCode()).Sum())
						+ (ReferenceEquals(obj.BugtraqId, null) ? 0 : (int)obj.BugtraqId.Select<int, long>(s => s.GetHashCode()).Sum())
						+ (ReferenceEquals(obj.Vpr, null) ? 0 : this._vprComparer.GetHashCode(obj.Vpr));
				}
			}
		}
	}
}
