using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.Base;
using Tenable.IO.Agents;
using Tenable.IO.Agents.Results;
using Xunit;

namespace Tenable.IO.Tests.Agents {
	public class TestAgentEnumerator {
		private sealed class AgentenumeratorNextPageId : AgentEnumerator {
			public AgentenumeratorNextPageId(AgentEnumerable enumerable, CancellationToken cancellationToken) : base(enumerable, cancellationToken) {
			}
			public int NextPageIdPublic() => base.NextPageId();
		}

		[Fact]
		public void TestConstruct() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var filter = new AgentFilter();
			var enumerable = new Mock<AgentEnumerable>(tio.Object, filter, 1);
			using var tokenSource = new CancellationTokenSource();
			using var enumerator = new AgentEnumerator(enumerable.Object, tokenSource.Token);
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.Equal(tokenSource.Token, enumerator.CancellationToken);
			enumerable.VerifyNoOtherCalls();
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public void TestNextPageId() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var filter = new AgentFilter();
			var enumerator = new AgentenumeratorNextPageId(
				new AgentEnumerable(tio.Object, filter, 1), default);
			Assert.Equal(0, enumerator.NextPageIdPublic());
			enumerator.GetType()
				.GetProperty(nameof(AgentEnumerator.PageId))!
				.SetValue(enumerator, 89);
			Assert.Equal(89 + filter.Limit, enumerator.NextPageIdPublic());
			enumerator.GetType()
				.GetProperty(nameof(AgentEnumerator.PageId))!
				.SetValue(enumerator, 925);
			filter.Limit = 504;
			Assert.Equal(925 + filter.Limit, enumerator.NextPageIdPublic());

			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestGetAll() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var filter = new AgentFilter() { Limit = 5 };
			var enumerable = new AgentEnumerable(tio.Object, filter, 1);
			var page0 = new Agent[] {
				new Agent(0, Guid.Empty, "agent1", "asdf", "asdf", "10.50.83.5", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent2", "asdf", "asdf", "86.218.10.158", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent3", "asdf", "asdf", "48.38.218.52", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent4", "asdf", "asdf", "144.87.247.85", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent5", "asdf", "asdf", "2.3.110.116", Guid.Empty, "default"),
			};
			var page1 = new Agent[] {
				new Agent(0, Guid.Empty, "agent6", "asdf", "asdf", "219.150.175.50", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent7", "asdf", "asdf", "127.128.33.29", Guid.Empty, "default"),
			};
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "0"))),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AgentPage?>(new AgentPage(page0, new Pagination(7, 5, 0, new SortOrder[] { new SortOrder("name", SortDirection.ASC) }))));
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "5"))),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AgentPage?>(new AgentPage(page1, new Pagination(7, 5, 5, new SortOrder[] { new SortOrder("name", SortDirection.ASC) }))));

			// Test synchronous enumeration
			Assert.Equal(page0.Concat(page1), enumerable);
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "0"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == default)),
				Times.Once());
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "5"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == default)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test asynchronous enumeration
			enumerable = new AgentEnumerable(tio.Object, filter, 1);
			using var tokenSource = new CancellationTokenSource();
			var result = new List<Agent>(page0.Length + page1.Length);
			await using (var enumerator = enumerable.GetAsyncEnumerator(tokenSource.Token)) {
				while (await enumerator.MoveNextAsync()) {
					result.Add(enumerator.Current);
				}
			}
			Assert.Equal(page0.Concat(page1), result);
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "0"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "5"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestGetAllPrematureEnd() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var filter = new AgentFilter() { Limit = 5 };
			var page0 = new Agent[] {
				new Agent(0, Guid.Empty, "agent1", "asdf", "asdf", "10.50.83.5", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent2", "asdf", "asdf", "86.218.10.158", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent3", "asdf", "asdf", "48.38.218.52", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent4", "asdf", "asdf", "144.87.247.85", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent5", "asdf", "asdf", "2.3.110.116", Guid.Empty, "default"),
			};
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "0"))),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AgentPage?>(new AgentPage(page0, new Pagination(7, 5, 0, new SortOrder[] { new SortOrder("name", SortDirection.ASC) }))));
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "5"))),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AgentPage?>(new AgentPage(Array.Empty<Agent>(), new Pagination(4, 5, 5, new SortOrder[] { new SortOrder("name", SortDirection.ASC) }))));

			// Test (outdated) synchronous enumeration
			Assert.Equal(page0, new AgentEnumerable(tio.Object, filter, 1));
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "0"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == default)),
				Times.Once());
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "5"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == default)),
				Times.Once());
			tio.VerifyNoOtherCalls();


			// Test asynchronous enumeration
			using var tokenSource = new CancellationTokenSource();
			var result = new List<Agent>(page0.Length);
			await foreach (var item in new AgentEnumerable(tio.Object, filter, 1).WithCancellation(tokenSource.Token)) {
				result.Add(item);
			}
			Assert.Equal(page0, result);
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "0"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f =>
						f.Count() == 2
						&& f.Contains(new KeyValuePair<string, string>("offset", "5"))
						&& f.Contains(new KeyValuePair<string, string>("limit", "5"))),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
