using System;
using System.Collections.Generic;
using Tenable.Base;
using Tenable.IO.Agents;
using Xunit;

namespace Tenable.IO.Tests.Agents {
	public class TestAgentFilter {
		[Fact]
		public void TestEquality() {
			var first = new AgentFilter();
			var second = new AgentFilter();
			var different = new AgentFilter() {
				Filter = new List<Tuple<string, string, string>>(),
				Sort = new List<SortOrder>(),
				Wildcard = ": no",
				WildcardFields = new List<string>()
			};
			Assert.False(first.Equals(null));
			Assert.False(first!.Equals((object?)null));
			Assert.False(first!.Equals("(object?)null"));
			Assert.True(first!.Equals((object)first));
			Assert.True(first!.Equals(first));
			Assert.True(first!.Equals(second));
			Assert.False(first!.Equals(different));
			Assert.False(different!.Equals(first));
			Assert.True(first == second);
#pragma warning disable CS1718 // Comparison made to same variable
			Assert.True(first == first);
#pragma warning restore CS1718 // Comparison made to same variable
			Assert.True(first != null);
			Assert.True(null != first);
			Assert.True(first!.Equals((object?)second));
			Assert.Equal(first.GetHashCode(), second.GetHashCode());
			Assert.Equal(different.GetHashCode(), different.GetHashCode());
		}
	}
}
