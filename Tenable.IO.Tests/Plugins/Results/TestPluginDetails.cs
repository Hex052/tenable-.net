using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Tenable.IO.Plugins.Results;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results {
	public class TestPluginDetails {
		[Fact]
		public void TestDict() {
			var attributes = new PluginAttribute[] {
				new PluginAttribute("fname", "aix_IY19744.nasl"),
				new PluginAttribute("plugin_name", "AIX 5.1 : IY19744"),
				new PluginAttribute("script_version", "$Revision: 1.5 $"),
				new PluginAttribute("solution", "http://www-912.ibm.com/eserver/support/fixes/"),
				new PluginAttribute("risk_factor", "High"),
				new PluginAttribute(
					"description",
					"The remote host is missing AIX Critical Security Patch number IY19744\n(SECURITY: Buffer Overflow in xntpd).\n\nYou should install this patch for your system to be up-to-date."),
				new PluginAttribute("plugin_publication_date", "2006/09/16"),
				new PluginAttribute("synopsis", "The remote host is missing a vendor-supplied security patch"),
			};
			var result = new PluginDetails(attributes, "AIX Local Security Checks", "AIX 5.1 : IY19744", 22372);
			Assert.Same(attributes, result.Attributes);
			Assert.Null(result.ExtensionData);
			Assert.Equal(8, result.Attributes.Count);
			Assert.Equal(8, result.Count);
			Assert.Empty(result.Attributes.Where(a => ReferenceEquals(a, null)));
			Assert.Empty(result.Attributes.Where(a => !ReferenceEquals(a.ExtensionData, null)));
			Assert.Equal(result.Attributes.Select(f => f.AttributeName), result.Keys);
			Assert.Equal(result.Attributes.Select(f => f.AttributeValue), result.Values);
			Assert.Equal(result.Attributes.Select(f => new KeyValuePair<string, string>(f.AttributeName, f.AttributeValue)), result.AsEnumerable());
			Assert.False(result.ContainsKey("name"));
			Assert.True(result.TryGetValue("plugin_name", out string? val));
			Assert.Equal("AIX 5.1 : IY19744", val);
			Assert.False(result.TryGetValue("family_name", out val));
			Assert.Null(val);
			Assert.Equal("aix_IY19744.nasl", result["fname"]);
			Assert.Throws<KeyNotFoundException>(() => result["name"]);
			Assert.Throws<ArgumentNullException>(() => result[null!]);
			Assert.Throws<ArgumentNullException>(() => result.TryGetValue(null!, out string? _));
			Assert.Throws<ArgumentNullException>(() => result.ContainsKey(null!));
		}

		[Fact]
		public void TestNewPluginAttribute() {
			Assert.Throws<ArgumentNullException>(() => new PluginAttribute(string.Empty, null!));
			Assert.Throws<ArgumentNullException>(() => new PluginAttribute(null!, string.Empty));
			Assert.Throws<ArgumentNullException>(() => new PluginAttribute(null!, null!));
			var attrib = new PluginAttribute(string.Empty, string.Empty);
			Assert.Throws<ArgumentNullException>(() => attrib.AttributeName = null!);
			Assert.Throws<ArgumentNullException>(() => attrib.AttributeValue = null!);
		}
	}
}
