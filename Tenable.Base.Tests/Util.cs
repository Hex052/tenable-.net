using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.Base.Tests {
	public static class Util {
		public class ResettableCaller<T> {
			private IEnumerator<T> _enumerator;
			private IEnumerator<T>? _listEnumerator;
			private LinkedList<T> _list;
			private int _callCount;
			public ResettableCaller(IEnumerable<T> enumerable) {
				this._enumerator = enumerable.GetEnumerator();
				this._list = new LinkedList<T>();
				this._callCount = 0;
			}
			public void Reset() {
				this._listEnumerator = this._list.GetEnumerator();
			}
			public T Call() {
				if (this._listEnumerator is not null) {
					// Enumerator was reset
					if (this._listEnumerator.MoveNext()) {
						this._callCount += 1;
						return this._listEnumerator.Current;
					}
					this._listEnumerator = null;
				}
				Assert.True(this._enumerator.MoveNext(), $"IEnumerable exhausted, too many calls! {this._callCount} calls made before this one");
				this._callCount += 1;
				var result = this._enumerator.Current;
				this._list.AddLast(result);
				return result;
			}
		}

		public static Func<T> ItemsFromList<T>(IEnumerable<T> items) {
			var enumerator = items.GetEnumerator();
			int callCount = 0;
			return () => {
				Assert.True(enumerator.MoveNext(), $"IEnumerable exhausted, too many calls! {callCount} calls made before this one");
				callCount += 1;
				return enumerator.Current;
			};
		}
		public static ResettableCaller<T> ItemsFromListResettable<T>(IEnumerable<T> items) {
			return new ResettableCaller<T>(items);
		}

		public static Func<HttpRequestMessage, Task<HttpResponseMessage>> HttpMockNoResponseException(ITestOutputHelper? output) {
			return async (HttpRequestMessage request) => {
				StringBuilder msg = new StringBuilder("No matching mock handler\n");
				msg.Append(request.Method.Method);
				msg.Append(' ');
				msg.Append(request.RequestUri);
				msg.Append("\nHeaders:\n");
				foreach (var header in request.Headers) {
					foreach (var value in header.Value) {
						msg.Append("  ");
						msg.Append(header.Key);
						msg.Append(": ");
						msg.Append(value);
						msg.AppendLine();
					}
				}
				if (request.Content is not null) {
					msg.AppendLine();
					var body = Encoding.UTF8.GetString(await request.Content.ReadAsByteArrayAsync());
					msg.Append(body);
				}
				else {
					msg.Append("<<no body>>");
				}
				output?.WriteLine(msg.ToString());
				throw new InvalidOperationException(msg.ToString());
			};
		}
	}
}
