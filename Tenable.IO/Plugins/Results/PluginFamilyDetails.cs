using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class PluginFamilyDetails {
		[JsonPropertyName("plugins")]
		public IReadOnlyList<PluginBrief> Plugins {
			get; set;
		}

		/// <summary>The unique ID of the family</summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}

		/// <summary>Name of the family</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public PluginFamilyDetails(int id, string name, IReadOnlyList<PluginBrief> plugins) {
			this.Id = id;
			this.Name = name;
			this.Plugins = plugins;
		}
	}
}
