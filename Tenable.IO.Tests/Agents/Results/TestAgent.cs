using System;
using System.Text.Json;
using Tenable.IO.Agents.Results;
using Xunit;

namespace Tenable.IO.Tests.Agents.Results {
	public class TestAgent {
		[Fact]
		public void TestConvert1() {
			var json = @"{
	""id"": 14570,
	""uuid"": ""938cb466-06ea-477e-abb0-99d8da0e0f20"",
	""name"": ""example.com"",
	""platform"": ""LINUX"",
	""distro"": ""es7-x86-64"",
	""ip"": ""192.0.2.57"",
	""plugin_feed_id"": ""201805161620"",
	""core_build"": ""13"",
	""core_version"": ""7.0.3"",
	""linked_on"": 1508329886,
	""last_connect"": 1526565624,
	""status"": ""off"",
	""groups"": [
		{
			""name"": ""SC Research"",
			""id"": 1167
		}
	]
}";
			var agent = JsonSerializer.Deserialize<Agent>(json)!;
			Assert.Equal(14570, agent.Id);
			Assert.Equal(new Guid("938cb466-06ea-477e-abb0-99d8da0e0f20"), agent.Uuid);
			Assert.Equal("example.com", agent.Name);
			Assert.Equal("LINUX", agent.Platform);
			Assert.Equal("es7-x86-64", agent.Distro);
			Assert.Equal("192.0.2.57", agent.Ip);
			Assert.Equal("13", agent.CoreBuild);
			Assert.Equal(new Version(7, 0, 3), agent.CoreVersion);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1508329886), agent.LinkedOn);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1526565624), agent.LastConnect);
			Assert.Equal(Status.OFF, agent.Status);
			Assert.NotNull(agent.Groups);
			Assert.Single(agent.Groups!);
			Assert.NotNull(agent.Groups![0]);
			Assert.True(ReferenceEquals(
				agent.ExtensionData, null) || agent.ExtensionData.Count == 0);
			Assert.Equal("SC Research", agent.Groups![0].Name);
			Assert.Equal(1167, agent.Groups![0].Id);
		}

		[Fact]
		public void TestConvert2() {
			var json = @"{
	""id"": 157,
	""uuid"": ""655993d5-c131-46e8-a82f-957f6f894cac"",
	""name"": ""GRD-LPTP"",
	""platform"": ""WINDOWS"",
	""distro"": ""win-x86-64"",
	""ip"": ""192.0.2.57"",
	""last_scanned"": 1515620036,
	""plugin_feed_id"": ""201801081515"",
	""core_build"": ""106"",
	""core_version"": ""7.0.0"",
	""linked_on"": 1456775443,
	""last_connect"": 1515674073,
	""status"": ""off"",
	""groups"": [
		{
			""name"": ""CodyAgents"",
			""id"": 8
		},
		{
			""name"": ""Agent Group A"",
			""id"": 3316
		}
	]
}";
			var agent = JsonSerializer.Deserialize<Agent>(json)!;
			Assert.Equal(157, agent.Id);
			Assert.Equal(new Guid("655993d5-c131-46e8-a82f-957f6f894cac"), agent.Uuid);
			Assert.Equal("GRD-LPTP", agent.Name);
			Assert.Equal("WINDOWS", agent.Platform);
			Assert.Equal("win-x86-64", agent.Distro);
			Assert.Equal("192.0.2.57", agent.Ip);
			Assert.Equal("106", agent.CoreBuild);
			Assert.Equal(new Version(7, 0, 0), agent.CoreVersion);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1456775443), agent.LinkedOn);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1515674073), agent.LastConnect);
			Assert.Equal(Status.OFF, agent.Status);
			Assert.NotNull(agent.Groups);
			Assert.Equal(2, agent.Groups!.Count);
			Assert.NotNull(agent.Groups![0]);
			Assert.Equal("CodyAgents", agent.Groups![0].Name);
			Assert.Equal(8, agent.Groups![0].Id);
			Assert.NotNull(agent.Groups![1]);
			Assert.Equal("Agent Group A", agent.Groups![1].Name);
			Assert.Equal(3316, agent.Groups![1].Id);
		}

		[Fact]
		public void TestConvert3() {
			var json = @"{
	""id"": 14569,
	""uuid"": ""72ac6ad1-fc86-4af4-be0c-0ff3bfbfb242"",
	""name"": ""example.com"",
	""platform"": ""LINUX"",
	""distro"": ""es7-x86-64"",
	""ip"": ""192.0.2.57"",
	""plugin_feed_id"": ""201805161620"",
	""core_build"": ""13"",
	""core_version"": ""7.0.3"",
	""linked_on"": 1508329832,
	""last_connect"": 1526565530,
	""status"": ""off"",
	""groups"": [
		{
			""name"": ""SC Research"",
			""id"": 1167
		}
	]
}";
			var agent = JsonSerializer.Deserialize<Agent>(json)!;
			Assert.Equal(14569, agent.Id);
			Assert.Equal(new Guid("72ac6ad1-fc86-4af4-be0c-0ff3bfbfb242"), agent.Uuid);
			Assert.Equal("example.com", agent.Name);
			Assert.Equal("LINUX", agent.Platform);
			Assert.Equal("es7-x86-64", agent.Distro);
			Assert.Equal("192.0.2.57", agent.Ip);
			Assert.Equal("13", agent.CoreBuild);
			Assert.Equal(new Version(7, 0, 3), agent.CoreVersion);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1508329832), agent.LinkedOn);
			Assert.Equal(DateTime.UnixEpoch.AddSeconds(1526565530), agent.LastConnect);
			Assert.Equal(Status.OFF, agent.Status);
			Assert.NotNull(agent.Groups);
			Assert.Single(agent.Groups!);
			Assert.NotNull(agent.Groups![0]);
			Assert.Equal("SC Research", agent.Groups![0].Name);
			Assert.Equal(1167, agent.Groups![0].Id);
		}
	}
}
