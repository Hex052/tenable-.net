using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class VprDriversDateRange {
		/// <summary>
		/// The lower bound of the range
		/// </summary>
		[JsonPropertyName("lower_bound")]
		public int LowerBound {
			get; set;
		}
		/// <summary>
		/// The upper bound of the range
		/// </summary>
		[JsonPropertyName("upper_bound")]
		public int? UpperBound {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public VprDriversDateRange(int lowerBound) {
			this.LowerBound = lowerBound;
		}
	}
	public class VprDrivers {
		/// <summary>
		/// A range representing the number of days since the National
		/// Vulnerability Database (NVD) published the vulnerability
		/// </summary>
		/// <remarks>
		/// Ranges include:
		/// 0-7 days,
		/// 7-30 days,
		/// 30-60 days,
		/// 60-180 days,
		/// 180-365 days,
		/// 365-730 days,
		/// More than 730 days(+731)
		/// </remarks>
		[JsonPropertyName("age_of_vuln")]
		public VprDriversDateRange Age {
			get; set;
		}

		/// <summary>
		/// The relative maturity of a possible exploit for the vulnerability
		/// based on the existence, sophistication, and prevalence of exploit
		/// intelligence from internal and external sources
		/// </summary>
		/// <remarks>
		/// The possible values ('High', 'Functional', 'PoC', or 'Unproven')
		/// parallel the CVSS Exploit Code Maturity categories
		/// </remarks>
		[JsonPropertyName("exploit_code_maturity")]
		public string ExploitCodeMaturity {
			get; set;
		}

		/// <summary>
		/// whether Tenable predicted the CVSSv3 impact score for the
		/// vulnerability because NVD did not provide one (true) or
		/// used the NVD-provided CVSSv3 impact score (false) when
		/// calculating the VPR
		/// </summary>
		[JsonPropertyName("cvss_impact_score_predicted")]
		public bool CvssImpactScorePredicted {
			get; set;
		}

		/// <summary>
		/// The NVD-provided CVSSv3 impact score for the vulnerability. If the NVD
		/// did not provide a score, Tenable.io displays a Tenable-predicted score.
		/// See <see cref="VprDrivers.CvssImpactScorePredicted"/>
		/// </summary>
		[JsonPropertyName("cvss3_impact_score")]
		public decimal Cvss3ImpactScore {
			get; set;
		}

		/// <summary>
		/// The relative intensity based on the number and frequency of
		/// recently observed threat events related to this vulnerability
		/// </summary>
		/// <remarks>
		/// The possible values: Very Low, Low, Medium, High, or Very High
		/// </remarks>
		[JsonPropertyName("threat_intensity_last28")]
		public string? ThreatIntensityLast28 {
			get; set;
		}

		/// <summary>
		/// A range representing the number of days since a threat
		/// event occurred for the vulnerability
		/// </summary>
		[JsonPropertyName("threat_recency")]
		public VprDriversDateRange? ThreatRecency {
			get; set;
		}

		/// <summary>
		/// A list of all sources (for example, social media channels,
		/// the dark web, etc.) where threat events related to this
		/// vulnerability occurred
		/// </summary>
		[JsonPropertyName("threat_sources_last28")]
		public IList<string>? ThreatSourcesLast28 {
			get; set;
		}

		/// <summary>
		/// The relative number of unique products affected by the vulnerability
		/// </summary>
		/// <remarks>
		/// The possible values: Very Low, Low, Medium, High, or Very High
		/// </remarks>
		[JsonPropertyName("product_coverage")]
		public string? ProductCoverage {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public VprDrivers(VprDriversDateRange age, string exploitCodeMaturity,
				bool cvssImpactScorePredicted) {
			this.Age = age;
			this.ExploitCodeMaturity = exploitCodeMaturity;
			this.CvssImpactScorePredicted = cvssImpactScorePredicted;
		}
	}
}
