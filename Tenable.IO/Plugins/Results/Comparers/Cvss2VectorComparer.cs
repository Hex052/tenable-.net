using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class Cvss2VectorComparer : EqualityComparer<Cvss2Vector> {
		public Cvss2VectorComparer() {
		}

		[Pure]
		public override bool Equals([AllowNull] Cvss2Vector x, [AllowNull] Cvss2Vector y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.AccessComplexity == y.AccessComplexity
					&& x.AccessVector == y.AccessVector
					&& x.Authentication == y.Authentication
					&& x.ConfidentialityImpact == y.ConfidentialityImpact
					&& x.IntegrityImpact == y.IntegrityImpact
					&& x.AvailabilityImpact == y.AvailabilityImpact
					&& x.Raw == y.Raw;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Cvss2Vector obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (ReferenceEquals(obj.AccessComplexity, null) ? 0 : obj.AccessComplexity.GetHashCode())
						+ (ReferenceEquals(obj.AccessVector, null) ? 0 : obj.AccessVector.GetHashCode())
						+ (ReferenceEquals(obj.Authentication, null) ? 0 : obj.Authentication.GetHashCode())
						+ (ReferenceEquals(obj.ConfidentialityImpact, null) ? 0 : obj.ConfidentialityImpact.GetHashCode())
						+ (ReferenceEquals(obj.IntegrityImpact, null) ? 0 : obj.IntegrityImpact.GetHashCode())
						+ (ReferenceEquals(obj.AvailabilityImpact, null) ? 0 : obj.AvailabilityImpact.GetHashCode())
						+ (ReferenceEquals(obj.Raw, null) ? 0 : obj.Raw.GetHashCode());
				}
			}
		}
	}
}
