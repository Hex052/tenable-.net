#!/bin/sh

rm -f coverage/*

## Fails to collect after a failure
# dotnet test --blame -p:CollectCoverage=true -p:MergeWith="../coverage/coverage.json" -p:CoverletOutput="../coverage/" -p:CoverletOutputFormat=\"json,opencover,lcov\"

## Doesn't fail to collect after a failure but needs output combined
## (also can't combine to the right format)
# rm -rf ./results/
# dotnet test --blame --collect:"XPlat Code Coverage" -r ./results/
# # | tee /dev/stderr | grep Attachments
# dotnet tool install --global dotnet-reportgenerator-globaltool
# # reportgenerator -reports:"*/TestResults/*/coverage.cobertura.xml"
# results="$(ls ./results/*/coverage.cobertura.xml | tr -s '[:space:]' ';' | sed 's/.$//')"
# reportgenerator \
# 	-reports:"${results}" \
# 	-targetdir:"./coverage" \
# 	-reporttypes:"lcov;Html;SonarQube" \

## Works, but kinda jank with the for loop
dotnet tool install --global coverlet.console || true
dotnet build || return $?
ERROR=0
for dir in Tenable.Base.Tests Tenable.IO.Tests; do
	for framework in net5.0; do
		coverlet "${dir}/bin/Debug/${framework}/" \
			--target dotnet \
			--targetargs "test --no-build --no-restore ${dir}" \
			--output "./coverage/" \
			--format lcov --format opencover --format json --format cobertura \
			--merge-with "./coverage/coverage.json" \
			--exclude "[Tenable.IO.Tests]*" \
			--exclude "[Tenable.Base.Tests]*" \
			|| ERROR=1
	done
done

return ${ERROR}
