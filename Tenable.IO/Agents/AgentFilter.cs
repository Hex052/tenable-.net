using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Tenable.Base;

namespace Tenable.IO.Agents {
	public class AgentFilter : IEquatable<AgentFilter> {
		public List<Tuple<string, string, string>>? Filter {
			get; set;
		}

		public FilterType FilterType {
			get; set;
		} = FilterType.AND;

		public List<SortOrder>? Sort {
			get; set;
		}

		public string? Wildcard {
			get; set;
		}
		public List<string>? WildcardFields {
			get; set;
		}

		/// <summary>The number of records to retrieve in each page</summary>
		/// <remarks>The minimum is 1, and the maximum is 5000</remarks>
		public int Limit {
			get; set;
		} = 100;

		/// <summary>The starting record to retrieve</summary>
		public int Offset {
			get; set;
		} = 0;

		public bool Equals([AllowNull] AgentFilter? other) {
			if (ReferenceEquals(this, other)) {
				return true;
			}
			else if (ReferenceEquals(other, null)) {
				return false;
			}
			return this.Filter == other.Filter
				&& this.FilterType == other.FilterType
				&& this.Sort == other.Sort
				&& this.Wildcard == other.Wildcard
				&& this.WildcardFields == other.WildcardFields
				&& this.Limit == other.Limit
				&& this.Offset == other.Offset;
		}

		public override bool Equals(object? obj) {
			if (obj is AgentFilter other) {
				return this.Equals(other);
			}
			return false;
		}

		public override int GetHashCode() {
			return (
				this.Filter?.GetHashCode() ?? 0,
				this.FilterType.GetHashCode(),
				this.Sort?.GetHashCode() ?? 0,
				this.Wildcard?.GetHashCode() ?? 0,
				this.WildcardFields?.GetHashCode() ?? 0,
				this.Limit,
				this.Offset).GetHashCode();
		}

		public static bool operator ==(AgentFilter? lhs, AgentFilter? rhs) {
			if (ReferenceEquals(lhs, null)) {
				return ReferenceEquals(rhs, null);
			}
			return lhs.Equals(rhs);
		}
		public static bool operator !=(AgentFilter? lhs, AgentFilter? rhs) {
			return !(lhs == rhs);
		}
	}
}
