using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Collections;
using Xunit;

namespace Tenable.Base.Tests.Collections {
	public class TestPageEnumerable {
		private sealed class Page : IPage<EmptyJsonObject, Pagination> {
			public IReadOnlyList<EmptyJsonObject> Items {
				get; set;
			}
			public Pagination Pagination {
				get; set;
			}
			public Page(IReadOnlyList<EmptyJsonObject> items, Pagination pagination) {
				this.Items = items;
				this.Pagination = pagination;
			}
		}
		private sealed class EnumerableTested : PageEnumerable<EmptyJsonObject, Page, Pagination> {
			public EnumerableTested(ITenableBase api) : base(api) {
			}

			public override IAsyncEnumerator<EmptyJsonObject> GetAsyncEnumerator(CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public override IEnumerator<EmptyJsonObject> GetEnumerator() => throw new NotImplementedException();

			public override string RelativePageUri(int pageId) => string.Format("asdf/1234/{0}", pageId);
			protected override IEnumerable<KeyValuePair<string, string>> QueryParameters(int pageId) {
				return new KeyValuePair<string, string>[] {
					new KeyValuePair<string, string>("pageId", pageId.ToString()),
					new KeyValuePair<string, string>("key", "value"),
				};
			}

			/// <summary>
			/// As if <see cref="GC.Collect"/> were called and now all the
			/// weakrefs are invalid
			/// </summary>
			public void Collect() {
				foreach (var item in this.PageCache.Values) {
					item.SetTarget(null!);
				}
			}
		}

		private sealed class MockBaseApi : ITenableBase {
			private string partialUrl;
			private Method method;
			private ICollection<KeyValuePair<string, string>> queryParameters;
			private CancellationToken cancellationToken;
			private int callCount = 0;
			public int CallCount => this.callCount;
			public int DefaultRetryLimit => throw new NotImplementedException();
			public RestClient RestClient => throw new NotImplementedException();

			public MockBaseApi(string partialUrl, Method method, ICollection<KeyValuePair<string, string>> queryParameters, CancellationToken cancellationToken) {
				if (string.IsNullOrWhiteSpace(partialUrl)) {
					throw new ArgumentException($"'{nameof(partialUrl)}' cannot be null or whitespace.", nameof(partialUrl));
				}

				this.partialUrl = partialUrl;
				this.method = method;
				this.queryParameters = queryParameters ?? throw new ArgumentNullException(nameof(queryParameters));
				this.cancellationToken = cancellationToken;
			}

			public RestRequest CreateRequest(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody) => throw new NotImplementedException();
			public void Dispose() => GC.SuppressFinalize(this);
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken = default) {
				++this.callCount;
				Assert.Equal(this.partialUrl, partialUrl);
				Assert.Equal(this.method, method);
				Assert.Equal(this.queryParameters, queryParameters);
				Assert.Equal(this.cancellationToken, cancellationToken);

				return Task.FromResult<T?>(Assert.IsAssignableFrom<T>(new Page(Array.Empty<EmptyJsonObject>(), new Pagination(0, 50, 0, Array.Empty<SortOrder>()))));
			}
			public Task<RestResponse> RetryRequestAsync(RestRequest request, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse> RetryRequestAsync(RestRequest request, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request, CancellationToken cancellationToken = default) => throw new NotImplementedException();
		}

		[Fact]
		public async Task TestGetFullPageAsyncGcCollected() {
			using var tokenSource = new CancellationTokenSource();
			var api = new MockBaseApi("asdf/1234/24", Method.Get, new[] { new KeyValuePair<string, string>("pageId", "24"), new KeyValuePair<string, string>("key", "value") }, tokenSource.Token);
			EnumerableTested enumerable = new EnumerableTested(api);

			Page? result = await enumerable.GetFullPageAsync(24, tokenSource.Token);
			Assert.Equal(1, api.CallCount);
			Assert.Same(result, await enumerable.GetFullPageAsync(24, tokenSource.Token));
			Assert.Equal(1, api.CallCount);
			enumerable.Collect();// As if GC.Collect(2, GCCollectionMode.Forced, true, false);
			result = await enumerable.GetFullPageAsync(24, tokenSource.Token);
			Assert.Equal(2, api.CallCount);
			Assert.Same(result, await enumerable.GetFullPageAsync(24, tokenSource.Token));
			Assert.Equal(2, api.CallCount);
		}
	}
}
