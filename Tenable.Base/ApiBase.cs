using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace Tenable.Base {
	public abstract class ApiBase : ITenableBase {
		private bool _disposed;

		public int DefaultRetryLimit {
			get; set;
		}

		public RestClient RestClient {
			get; protected set;
		}

		protected ApiBase(string url) : this(new Uri(url ?? throw new ArgumentNullException(nameof(url)))) {
		}
		protected ApiBase(Uri url) : this(new RestClientOptions(url ?? throw new ArgumentNullException(nameof(url)))) {
		}
		protected ApiBase(RestClientOptions options) {
			if (options is null) {
				throw new ArgumentNullException(nameof(options));
			}

			this.DefaultRetryLimit = 5;
			this.RestClient = new RestClient(options);
			this.RestClient.UseSerializer<JsonConverters.CustomJsonSerializer>();
		}

		public async Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, int retryLimit,
				CancellationToken cancellationToken = default) {
			RestResponse<T> response = await this.RetryRequestJsonAsync<T>(
				this.CreateRequest(partialUrl, method, queryParameters, jsonBody),
				retryLimit,
				cancellationToken);
			return response.Data;
		}
		public async Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, CancellationToken cancellationToken = default) {
			RestResponse<T> response = await this.RetryRequestJsonAsync<T>(
				this.CreateRequest(partialUrl, method, queryParameters, jsonBody),
				cancellationToken);
			return response.Data;
		}
		public async Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				CancellationToken cancellationToken = default) {
			RestResponse<T> response = await this.RetryRequestJsonAsync<T>(
				this.CreateRequest(partialUrl, method, queryParameters, null),
				cancellationToken);
			return response.Data;
		}
		public async Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				object? jsonBody, CancellationToken cancellationToken = default) {
			RestResponse<T> response = await this.RetryRequestJsonAsync<T>(
				this.CreateRequest(partialUrl, method, null, jsonBody),
				cancellationToken);
			return response.Data;
		}
		public async Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				CancellationToken cancellationToken = default) {
			RestResponse<T> response = await this.RetryRequestJsonAsync<T>(
				this.CreateRequest(partialUrl, method, null, null),
				cancellationToken);
			return response.Data;
		}

		public async Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, int retryLimit,
				CancellationToken cancellationToken = default) {
			RestResponse response = await this.RetryRequestAsync(
				this.CreateRequest(partialUrl, method, queryParameters, jsonBody),
				retryLimit,
				cancellationToken);
			return response.Content;
		}
		public async Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, CancellationToken cancellationToken = default) {
			RestResponse response = await this.RetryRequestAsync(
				this.CreateRequest(partialUrl, method, queryParameters, jsonBody),
				cancellationToken);
			return response.Content;
		}
		public async Task<string?> RequestAsync(string partialUrl, Method method,
				object? jsonBody, CancellationToken cancellationToken = default) {
			RestResponse response = await this.RetryRequestAsync(
				this.CreateRequest(partialUrl, method, null, jsonBody),
				cancellationToken);
			return response.Content;
		}
		public async Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				CancellationToken cancellationToken = default) {
			RestResponse response = await this.RetryRequestAsync(
				this.CreateRequest(partialUrl, method, queryParameters, null),
				cancellationToken);
			return response.Content;
		}
		public async Task<string?> RequestAsync(string partialUrl, Method method,
				CancellationToken cancellationToken = default) {
			RestResponse response = await this.RetryRequestAsync(
				this.CreateRequest(partialUrl, method, null, null),
				cancellationToken);
			return response.Content;
		}

		public RestRequest CreateRequest(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody) {
			if (partialUrl is null) {
				throw new ArgumentNullException(nameof(partialUrl));
			}

			RestRequest request = new RestRequest(partialUrl, method);
			if (jsonBody != null) {
				request.RequestFormat = DataFormat.Json;
				request.AddJsonBody(jsonBody);
			}
			if (!ReferenceEquals(queryParameters, null)) {
				foreach (var item in queryParameters) {
					request.AddQueryParameter(item.Key, item.Value);
				}
			}
			return request;
		}

		public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request,
				int retryLimit, CancellationToken cancellationToken = default) {
			return this._retryAsync<RestResponse<T>>(
				request ?? throw new ArgumentNullException(nameof(request)),
				this.RestClient.ExecuteAsync<T>,
				retryLimit,
				cancellationToken);
		}
		public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request,
				CancellationToken cancellationToken = default) {
			return this._retryAsync<RestResponse<T>>(
				request ?? throw new ArgumentNullException(nameof(request)),
				this.RestClient.ExecuteAsync<T>,
				this.DefaultRetryLimit,
				cancellationToken);
		}

		public Task<RestResponse> RetryRequestAsync(RestRequest request,
				int retryLimit, CancellationToken cancellationToken = default) {
			return this._retryAsync<RestResponse>(
				request ?? throw new ArgumentNullException(nameof(request)),
				this.RestClient.ExecuteAsync,
				retryLimit,
				cancellationToken);
		}
		public Task<RestResponse> RetryRequestAsync(RestRequest request,
				CancellationToken cancellationToken = default) {
			return this._retryAsync<RestResponse>(
				request ?? throw new ArgumentNullException(nameof(request)),
				this.RestClient.ExecuteAsync,
				this.DefaultRetryLimit,
				cancellationToken);
		}

		/// <summary>
		/// Handles the logic of retrying, rather than duplicating it in both
		/// <see cref="RetryRequestAsync(RestRequest, int)"/> and
		/// <see cref="RetryRequestJsonAsync{T}(RestRequest, int)"/>
		/// </summary>
		/// <remarks>Will retry on both 429 and 5xx errors</remarks>
		/// <typeparam name="T">Result type</typeparam>
		/// <param name="request">Request to execute</param>
		/// <param name="executeAsync">
		/// A function that executes the request.
		/// It's only argument will always be <paramref name="request"/>.
		/// </param>
		/// <param name="retryLimit">
		/// The number of times to retry the request before giving
		/// up and raising an exception
		/// </param>
		/// <returns>
		/// Response from request, after retrying
		/// </returns>
		/// <exception cref="Exceptions.BadRequestException"/>
		/// <exception cref="Exceptions.NoPermissionException"/>
		/// <exception cref="Exceptions.NotFoundException"/>
		/// <exception cref="Exceptions.HttpStatusException"/>
		/// <exception cref="Exceptions.RateLimitException"/>
		/// <exception cref="Exceptions.RetryException"/>
		/// <exception cref="Exceptions.InvalidCredentialsException"/>
		/// <exception cref="Exceptions.ConflictException"/>
		private async Task<T> _retryAsync<T>(RestRequest request,
				Func<RestRequest, CancellationToken, Task<T>> executeAsync, int retryLimit,
				CancellationToken cancellationToken) where T : RestResponse {
			uint retryAfterSec;
			int tryCount = 1;
			Exceptions.TenableException? exception;
			while (true) {
				if (this._disposed) {
					throw new ObjectDisposedException(this.GetType().AssemblyQualifiedName);
				}
				T response = await executeAsync(request, cancellationToken);
				exception = _throwException(response, out retryAfterSec);
				if (exception is null) {
					return response;
				}
				else if (tryCount >= retryLimit) {
					throw exception;
				}
				else if (tryCount == 1) {
					await Task.Yield();
				}
				await this.SleepAsync(retryAfterSec * 1000, cancellationToken);
				++tryCount;
			}
		}

		protected virtual Task SleepAsync(uint milliseconds, CancellationToken cancellationToken) {
			if (milliseconds > 0) {
				return Task.Delay((int)milliseconds, cancellationToken);
			}
			return Task.CompletedTask;
		}

		/// <summary>
		/// Informs the caller if they should return the response, or retry.
		/// Alternately, this function throws an exception if the request is an
		/// error that is not transient.
		/// </summary>
		/// <param name="response">
		/// The response to use to determine what should be done
		/// </param>
		/// <param name="retryAfterSec">
		/// How long the caller should wait before retrying
		/// </param>
		/// <returns>
		/// An exception if it should continue retrying (
		/// <see cref="Exceptions.RetryException"/> or something more derived),
		/// or <see langword="null"/> if the response was fine.
		/// </returns>
		/// <exception cref="Exceptions.BadRequestException"/>
		/// <exception cref="Exceptions.NoPermissionException"/>
		/// <exception cref="Exceptions.NotFoundException"/>
		/// <exception cref="Exceptions.HttpStatusException"/>
		/// <exception cref="Exceptions.InvalidCredentialsException"/>
		/// <exception cref="Exceptions.ConflictException"/>
		private static Exceptions.RetryException? _throwException(
				RestResponse response, out uint retryAfterSec) {
			switch (response.StatusCode) {
				case HttpStatusCode.OK:
					if (response.ErrorException is not null) {
						throw new Exceptions.UnexpectedNullResultException("Parser error", response.ErrorException)
					}
					break;
				case HttpStatusCode.NotFound:
					throw new Exceptions.NotFoundException(
						response.ResponseUri!.ToString(),
						response.Content);
				case HttpStatusCode.TooManyRequests:
				case HttpStatusCode.InternalServerError:
				case HttpStatusCode.ServiceUnavailable:
					var header = response.Headers
						.FirstOrDefault(
							h => "retry-after".Equals(h.Name, StringComparison.InvariantCultureIgnoreCase)
								&& h.Type == ParameterType.HttpHeader)
						?.Value;
					if (header is null || !uint.TryParse(header.ToString(), out retryAfterSec)) {
						retryAfterSec = 20;
					}
					if (response.StatusCode == HttpStatusCode.TooManyRequests) {
						return new Exceptions.RateLimitException(
							response.StatusCode,
							retryAfterSec,
							response.ResponseUri!.ToString(),
							response.Content);
					}
					else {
						return new Exceptions.RetryException(
							response.StatusCode,
							retryAfterSec,
							response.ResponseUri!.ToString(),
							response.Content);
					}
				case HttpStatusCode.BadRequest:
					throw new Exceptions.BadRequestException(
						response.ResponseUri!.ToString(),
						response.Content);
				case HttpStatusCode.Forbidden:
					throw new Exceptions.NoPermissionException(
						response.ResponseUri!.ToString(),
						response.Content);
				case HttpStatusCode.Unauthorized:
					throw new Exceptions.InvalidCredentialsException(
						response.ResponseUri!.ToString(),
						response.Content);
				case HttpStatusCode.Conflict:
					throw new Exceptions.ConflictException(
						response.ResponseUri!.ToString(),
						response.Content);
				default:
					if (response.StatusCode == default(HttpStatusCode)) {
						// default (0) is returned if there was some error in
						// RestClient making the call
						retryAfterSec = 20;
						return new Exceptions.RetryException(
							statusCode: response.StatusCode,
							retryAfter: retryAfterSec,
							message: response.ErrorMessage!,
							body: string.Empty,
							innerException: response.ErrorException!);
					}
					else if ((int)response.StatusCode >= 500) {
						// Upper bound does not need to be checked,
						// there are no status codes above 600
						retryAfterSec = 20;
						return new Exceptions.RetryException(
							response.StatusCode,
							retryAfterSec,
							response.ResponseUri!.ToString(),
							response.Content);
					}
					else if ((int)response.StatusCode >= 400) {
						// Upper bound does not need to be checked,
						// 500 codes are already checked
						throw new Exceptions.HttpStatusException(
							response.StatusCode,
							response.ResponseUri!.ToString(),
							response.Content);
					}
					break;
			}
			retryAfterSec = 0;
			return null;
		}

		protected virtual void Dispose(bool disposing) {
			if (!_disposed) {
				if (disposing) {
					this.RestClient.Dispose();
				}
				_disposed = true;
			}
		}

		public void Dispose() {
			// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}
	}
}
