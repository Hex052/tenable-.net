using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Globalization;

namespace Tenable.Base.JsonConverters {
	public class NullableDateJsonConverter : JsonConverter<DateTime?> {
		public override DateTime? Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			if (DateTime.TryParseExact(reader.GetString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result)) {
				return result;
			}
			return null;
		}

		public override void Write(Utf8JsonWriter writer, DateTime? value,
				JsonSerializerOptions options) {
			writer.WriteStringValue(
				value.HasValue ? value.Value.ToString("yyyy-MM-dd") : string.Empty);
		}
	}
}
