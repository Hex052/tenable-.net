using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Networks.Results {
	public class Network : NetworkSettings, IEquatable<Network> {
		/// <summary>The UUID of the network owner</summary>
		/// <remarks>
		/// The owner of the network object does not have any
		/// additional permissions above administrator permissions
		/// </remarks>
		[JsonPropertyName("owner_uuid")]
		public Guid OwnerUuid {
			get; set;
		}

		/// <summary>
		/// Timestamp when the network was created
		/// (parsed from milliseconds)
		/// </summary>
		[JsonPropertyName("created")]
		[JsonConverter(typeof(UnixMillisecondConverter))]
		public DateTime Created {
			get; set;
		}

		/// <summary>
		/// Timestamp when the network was last modified
		/// (parsed from milliseconds)
		/// </summary>
		[JsonPropertyName("modified")]
		[JsonConverter(typeof(UnixMillisecondConverter))]
		public DateTime Modified {
			get; set;
		}

		/// <summary>Number of scanners in the network</summary>
		[JsonPropertyName("scanner_count")]
		public int? ScannerCount {
			get; set;
		}

		/// <summary>Uuid of the network</summary>
		[JsonPropertyName("uuid")]
		public Guid Uuid {
			get; set;
		}

		/// <summary>Description of the network</summary>
		[JsonPropertyName("is_default")]
		public bool IsDefault {
			get; set;
		}

		/// <summary>The UUID of the user who created the network</summary>
		[JsonPropertyName("created_by")]
		public Guid CreatedBy {
			get; set;
		}

		/// <summary>The UUID of the user who modified the network</summary>
		[JsonPropertyName("modified_by")]
		public Guid ModifiedBy {
			get; set;
		}

		/// <summary>Timestamp when the network was deleted</summary>
		/// <remarks>
		/// To view deleted networks, set
		/// <see cref="NetworkFilter.IncludeDeleted"/> to <see langword="true"/>
		/// when requesting the networks
		/// </remarks>
		[JsonPropertyName("deleted")]
		[JsonConverter(typeof(UnixMillisecondConverter))]
		public DateTime? Deleted {
			get; set;
		}
		/// <summary>The UUID of the user who deleted the network</summary>
		/// <inheritdoc cref="Deleted" path="/remarks"/>
		[JsonPropertyName("deleted_by")]
		public Guid? DeletedBy {
			get; set;
		}

		/// <summary>
		/// Timestamp when the network was created (parsed from seconds)
		/// </summary>
		[JsonPropertyName("created_in_seconds")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime CreatedInSeconds {
			get; set;
		}

		/// <summary>
		/// Timestamp when the network was last modified (parsed from seconds)
		/// </summary>
		[JsonPropertyName("modified_in_seconds")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime ModifiedInSeconds {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Network(Guid ownerUuid, DateTime created, DateTime modified,
				Guid uuid, string name, bool isDefault, Guid createdBy,
				Guid modifiedBy, DateTime createdInSeconds,
				DateTime modifiedInSeconds) : base(name) {
			this.OwnerUuid = ownerUuid;
			this.Created = created;
			this.Modified = modified;
			this.Uuid = uuid;
			this.IsDefault = isDefault;
			this.CreatedBy = createdBy;
			this.ModifiedBy = modifiedBy;
			this.CreatedInSeconds = createdInSeconds;
			this.ModifiedInSeconds = modifiedInSeconds;
		}

		public bool Equals(Network? other) {
			return base.Equals(other)
				&& this.OwnerUuid == other.OwnerUuid
				&& this.Created == other.Created
				&& this.Modified == other.Modified
				&& this.Uuid == other.Uuid
				&& this.IsDefault == other.IsDefault
				&& this.CreatedBy == other.CreatedBy
				&& this.ModifiedBy == other.ModifiedBy
				&& this.Deleted == other.Deleted
				&& this.DeletedBy == other.DeletedBy
				&& this.CreatedInSeconds == other.CreatedInSeconds
				&& this.ModifiedInSeconds == other.ModifiedInSeconds
				&& ((IEnumerable<KeyValuePair<string, JsonElement>>?)this.ExtensionData ?? Array.Empty<KeyValuePair<string, JsonElement>>())
					.SequenceEqual((IEnumerable<KeyValuePair<string, JsonElement>>?)other.ExtensionData ?? Array.Empty<KeyValuePair<string, JsonElement>>());
		}
		public override bool Equals(object? obj) {
			return obj is Network n && this.Equals(n);
		}
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		public static bool operator ==(Network? lhs, Network? rhs) {
			if (ReferenceEquals(lhs, null)) {
				return ReferenceEquals(rhs, null);
			}
			return lhs.Equals(rhs);
		}
		public static bool operator !=(Network? lhs, Network? rhs) {
			return !(lhs == rhs);
		}
	}
}
