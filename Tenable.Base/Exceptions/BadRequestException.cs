using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// Used when Tenable returns a 400 error, likely because there was a
	/// bad filter parameter
	/// </summary>
	public class BadRequestException : HttpStatusException {
		public BadRequestException() : base(HttpStatusCode.BadRequest) {
		}
		public BadRequestException(string message) : base(HttpStatusCode.BadRequest, message) {
		}
		public BadRequestException(string message, string? body) : base(HttpStatusCode.BadRequest, message, body) {
		}
		public BadRequestException(string message, Exception innerException) : base(HttpStatusCode.BadRequest, message, innerException) {
		}
		public BadRequestException(string message, string? body, Exception innerException) : base(HttpStatusCode.BadRequest, message, body, innerException) {
		}
	}
}
