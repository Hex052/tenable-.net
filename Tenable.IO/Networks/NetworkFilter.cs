using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Tenable.Base;

namespace Tenable.IO.Networks {
	public sealed class NetworkFilter : IEquatable<NetworkFilter> {
		public IList<Tuple<string, string, string>>? Filter {
			get; set;
		}

		public FilterType FilterType {
			get; set;
		} = FilterType.OR;

		public IList<SortOrder>? Sort {
			get; set;
		}

		/// <summary>The number of records to retrieve in each page</summary>
		/// <remarks>The minimum is 1, and the maximum is 5000</remarks>
		public int Limit {
			get; set;
		} = 100;

		/// <summary>The starting record to retrieve</summary>
		public int Offset {
			get; set;
		} = 0;

		/// <summary>
		/// Whether to include deleted
		/// </summary>
		public bool? IncludeDeleted {
			get; set;
		}

		public bool Equals([AllowNull] NetworkFilter? other) {
			if (ReferenceEquals(this, other)) {
				return true;
			}
			else if (ReferenceEquals(other, null)) {
				return false;
			}
			return (this.Filter ?? Array.Empty<Tuple<string, string, string>>()).SequenceEqual(other.Filter ?? Array.Empty<Tuple<string, string, string>>())
				&& this.FilterType == other.FilterType
				&& (this.Sort ?? Array.Empty<SortOrder>()).SequenceEqual(other.Sort ?? Array.Empty<SortOrder>())
				&& this.IncludeDeleted == other.IncludeDeleted
				&& this.Limit == other.Limit
				&& this.Offset == other.Offset;
		}

		public override bool Equals(object? obj) {
			return obj is NetworkFilter other && this.Equals(other);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}

		public static bool operator ==(NetworkFilter? lhs, NetworkFilter? rhs) {
			if (ReferenceEquals(lhs, null)) {
				return ReferenceEquals(rhs, null);
			}
			return lhs.Equals(rhs);
		}
		public static bool operator !=(NetworkFilter? lhs, NetworkFilter? rhs) {
			return !(lhs == rhs);
		}
	}
}
