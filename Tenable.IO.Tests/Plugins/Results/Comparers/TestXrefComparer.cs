using System;
using Tenable.IO.Plugins.Results;
using Tenable.IO.Plugins.Results.Comparers;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results.Comparers {
	public class TestXrefComparer {
		[Fact]
		public void TestSameAndNull() {
			var xref = new Xref("CWE", "87");
			var comparer = new XrefComparer();
			Assert.True(comparer.Equals(xref, xref));
			Assert.True(comparer.Equals(null, null));
			Assert.False(comparer.Equals(xref, null));
			Assert.False(comparer.Equals(null, xref));
		}

		[Fact]
		public void TestGetHashCodeException() {
			var comparer = new XrefComparer();
			Assert.Throws<ArgumentNullException>(() => {
				comparer.GetHashCode(null!);
			});
		}

		[Theory]
		[InlineData("CWE", "271")]
		[InlineData("CVE", "CVE-2021-1048")]
		public void TestEqual(string type, string id) {
			var first = new Xref(type, id);
			var second = new Xref(type, id);
			var comparer = new XrefComparer();
			Assert.Equal(comparer.GetHashCode(first), comparer.GetHashCode(second));
			Assert.True(comparer.Equals(first, second));
		}
		[Fact]
		public void TestEqualNullProperties() {
			var first = new Xref(string.Empty, string.Empty) { Id = null!, Type = null! };
			var second = new Xref(string.Empty, string.Empty) { Id = null!, Type = null! };
			var comparer = new XrefComparer();
			Assert.Equal(comparer.GetHashCode(first), comparer.GetHashCode(second));
			Assert.True(comparer.Equals(first, second));
		}
		[Theory]
		[InlineData("CWE", "271", "CWE", "270")]
		[InlineData("CVE", "CVE-2021-1048", "CWE", "270")]
		public void TestUnequal(string type1, string id1, string type2, string id2) {
			var first = new Xref(type1, id1);
			var second = new Xref(type2, id2);
			var comparer = new XrefComparer();
			Assert.False(comparer.Equals(first, second));
		}
	}
}
