using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Collections.Generic;

namespace Tenable.IO.Plugins.Results.JsonConverters {
	public class DateConverter : JsonConverter<DateTime?> {
		public override bool HandleNull => true;

		public override bool CanConvert(Type typeToConvert) {
			return typeToConvert == typeof(DateTime)
				|| typeToConvert == typeof(DateTime?);
		}

		public override DateTime? Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			var str = reader.GetString();
			if (string.IsNullOrEmpty(str)) {
				return null;
			}
			try {
				return DateTime.ParseExact(str, "yyyy-MM-dd", null);
			}
			catch (FormatException exc) {
				throw new JsonException(null, exc);
			}
		}

		public override void Write(Utf8JsonWriter writer,
				DateTime? value, JsonSerializerOptions options) {
			writer.WriteStringValue(value.HasValue ? value.Value.ToString("yyyy-MM-dd") : string.Empty);
		}
	}
}
