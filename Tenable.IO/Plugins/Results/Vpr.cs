using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Plugins.Results {
	public class Vpr {
		/// <summary>
		/// The Vulnerability Priority Rating (VPR) for the vulnerability
		/// </summary>
		/// <remarks>
		/// If a plugin is designed to detect multiple
		/// vulnerabilities, the VPR represents the highest value
		/// calculated for a vulnerability associated with the plugin
		/// </remarks>
		[JsonPropertyName("score")]
		public decimal Score {
			get; set;
		}
		/// <summary>
		/// The key drivers Tenable uses to calculate a vulnerability's VPR
		/// </summary>
		[JsonPropertyName("drivers")]
		public VprDrivers? Drivers {
			get; set;
		}
		/// <summary>
		/// The timestamp when Tenable.io last imported the VPR
		/// for this vulnerability
		/// </summary>
		/// <remarks>
		/// Tenable.io imports a VPR value the first time you scan a
		/// vulnerability on your network. Then, Tenable.io automatically
		/// re-imports new and updated VPR values daily.
		/// </remarks>
		[JsonPropertyName("updated")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? Updated {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}
	}
}
