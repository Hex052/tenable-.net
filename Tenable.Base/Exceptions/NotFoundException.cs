using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// The specified item could not be found
	/// </summary>
	public class NotFoundException : HttpStatusException {
		public NotFoundException() : base(HttpStatusCode.NotFound) {
		}
		public NotFoundException(string message) : base(HttpStatusCode.NotFound, message) {
		}
		public NotFoundException(string message, string? body) : base(HttpStatusCode.NotFound, message, body) {
		}
		public NotFoundException(string message, Exception innerException) : base(HttpStatusCode.NotFound, message, innerException) {
		}
		public NotFoundException(string message, string? body, Exception innerException) : base(HttpStatusCode.NotFound, message, body, innerException) {
		}
	}
}
