using Moq;
using RestSharp;
using RichardSzalay.MockHttp;
using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.Base.Tests;
using Tenable.IO.Networks;
using Tenable.IO.Networks.Results;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.IO.Tests.Networks {
	public class TestNetworksApi {
		private readonly ITestOutputHelper output;
		public TestNetworksApi(ITestOutputHelper output) {
			this.output = output;
		}

		[Fact]
		public void TestFromTenableIO() {
			var tio = new TenableIO("testKey", "testSecret", "https://example.com/");
			var networksApi = tio.Networks;
			Assert.NotNull(networksApi);
			Assert.IsType<NetworksApi>(networksApi);
			Assert.Same(tio, networksApi.Api);
			Assert.Same(networksApi, tio.Networks);
		}

		[Fact]
		public void TestListNetworks() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new NetworksApi(tio.Object);
			var filters = new NetworkFilter() {
				Filter = new[] {
					new Tuple<string, string, string>("name", "eq", "default"),
					new Tuple<string, string, string>("name", "eq", "Default"),
				},
				FilterType = FilterType.OR,
				IncludeDeleted = false,
				Limit = 72,
				Sort = new[] { new SortOrder("name", SortDirection.ASC) },
			};

			NetworkEnumerable enumerable = api.ListNetworks(filters);
			Assert.Equal(filters, enumerable.Filter);
			Assert.Same(tio.Object, enumerable.Api);
			NetworkEnumerable enumerable2 = api.ListNetworks();
			Assert.Equal(new NetworkFilter(), enumerable2.Filter);
			Assert.Same(tio.Object, enumerable2.Api);
			Assert.NotSame(enumerable, enumerable2);

			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("c5b0d19d-ebed-4ecc-a420-3f6faa36e563", "bc10655d-dad1-4a43-90e7-7c309de38cf5")]
		[InlineData("1a909868-e0ae-4abd-8886-28b07c7f5d58", "99b465ac-6394-45a5-9b0e-5972084ed134")]
		public async Task TestGetNetworkDetailsAsync(string uuidStr, string uuidNullStr) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var validUuid = new Guid(uuidStr);
			var throwUuid = new Guid(uuidNullStr);
			using var tokenSource = new CancellationTokenSource();
			Expression<Func<ITenableIO, Task<Network?>>> valid = tio => tio.RequestJsonAsync<Network>(
				It.Is<string>(string.Format("networks/{0}", validUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Get),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Expression<Func<ITenableIO, Task<Network?>>> throws = tio => tio.RequestJsonAsync<Network>(
				It.Is<string>(string.Format("networks/{0}", throwUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Get),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Network result = new Network(Guid.Empty, DateTime.MinValue, DateTime.MaxValue, Guid.Empty, string.Empty, true, Guid.Empty, Guid.Empty, DateTime.MaxValue, DateTime.MaxValue);
			tio.Setup(valid).ReturnsAsync(result);
			tio.Setup(throws).ReturnsAsync(default(Network));
			var api = new NetworksApi(tio.Object);


			var exc = await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetNetworkDetailsAsync(throwUuid, tokenSource.Token));
			tio.Verify(throws, Times.Once());
			tio.VerifyNoOtherCalls();
			Assert.Same(result, await api.GetNetworkDetailsAsync(validUuid, tokenSource.Token));
			tio.Verify(valid, Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("e24d09ec-3224-471e-8769-7c09accd4564", "fa93b8dc-0bed-45ba-953c-4712b2a5319d")]
		public async Task TestUpdateNetworkAsync(string uuidStr, string uuidNullStr) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var validUuid = new Guid(uuidStr);
			var throwUuid = new Guid(uuidNullStr);
			using var tokenSource = new CancellationTokenSource();
			NetworkSettings settings = new NetworkSettings("default");
			NetworkSettings derivedSettings = new Network(Guid.Empty, DateTime.MinValue, DateTime.MaxValue, Guid.Empty, "Default", true, Guid.Empty, Guid.Empty, DateTime.MaxValue, DateTime.MaxValue) { Description = "Default network", AssetsTtlDays = 90 };
			Expression<Func<ITenableIO, Task<Network?>>> valid = tio => tio.RequestJsonAsync<Network>(
				It.Is<string>(string.Format("networks/{0}", validUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Put),
				It.Is<NetworkSettings>(s => ReferenceEquals(settings, s)),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Expression<Func<ITenableIO, Task<Network?>>> throws = tio => tio.RequestJsonAsync<Network>(
				It.Is<string>(string.Format("networks/{0}", throwUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Put),
				It.Is<NetworkSettings>(s => !ReferenceEquals(settings, s) && s.GetType() == typeof(NetworkSettings) && s.Name == "Default" && s.Description == "Default network" && s.AssetsTtlDays == 90),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			tio.Setup(valid).ReturnsAsync((Network)derivedSettings);
			tio.Setup(throws).ReturnsAsync(default(Network));
			var api = new NetworksApi(tio.Object);


			var exc = await Assert.ThrowsAsync<UnexpectedNullResultException>(async () => await api.UpdateNetworkAsync(throwUuid, derivedSettings, tokenSource.Token));
			tio.Verify(throws, Times.Once());
			tio.VerifyNoOtherCalls();
			Assert.Same((Network)derivedSettings, await api.UpdateNetworkAsync(validUuid, settings, tokenSource.Token));
			tio.Verify(valid, Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("15579c76-7b33-4420-8a97-3d8092e7a85a", "84312e15-33ff-408d-9b62-1755ba5f664d")]
		public async Task TestDeleteNetworkAsync(string uuidStr, string uuidNullStr) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var validUuid = new Guid(uuidStr);
			var throwUuid = new Guid(uuidNullStr);
			using var tokenSource = new CancellationTokenSource();
			Expression<Func<ITenableIO, Task<EmptyJsonObject?>>> valid = tio => tio.RequestJsonAsync<EmptyJsonObject>(
				It.Is<string>(string.Format("networks/{0}", validUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Delete),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Expression<Func<ITenableIO, Task<EmptyJsonObject?>>> throws = tio => tio.RequestJsonAsync<EmptyJsonObject>(
				It.Is<string>(string.Format("networks/{0}", throwUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Delete),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			tio.Setup(valid).ReturnsAsync(new EmptyJsonObject());
			tio.Setup(throws).ReturnsAsync(default(EmptyJsonObject));
			var api = new NetworksApi(tio.Object);


			var result = await api.DeleteNetworkAsync(throwUuid, tokenSource.Token);
			Assert.NotNull(result);
			tio.Verify(throws, Times.Once());
			tio.VerifyNoOtherCalls();
			result = await api.DeleteNetworkAsync(validUuid, tokenSource.Token);
			Assert.NotNull(result);
			tio.Verify(valid, Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("5debbf49-cf55-4de9-ba12-b9e226580b16", "f6753076-5d97-4460-83f5-659c649ee2f3", 30)]
		[InlineData("1c54abdc-18c0-4251-98cf-b92ff4854a18", "7a605e32-4ee0-4a43-868a-197613bbc9ec", 72)]
		public async Task TestGetNetworkAssetCountAsync(string uuidStr, string uuidNullStr, int numDays) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var validUuid = new Guid(uuidStr);
			var throwUuid = new Guid(uuidNullStr);
			using var tokenSource = new CancellationTokenSource();
			Expression<Func<ITenableIO, Task<AssetCount?>>> valid = tio => tio.RequestJsonAsync<AssetCount>(
				It.Is<string>(string.Format("networks/{0}/counts/assets-not-seen-in/{1}", validUuid, numDays), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Get),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Expression<Func<ITenableIO, Task<AssetCount?>>> throws = tio => tio.RequestJsonAsync<AssetCount>(
				It.Is<string>(string.Format("networks/{0}/counts/assets-not-seen-in/{1}", throwUuid, numDays), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Get),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			var result = new AssetCount(470, 5);
			tio.Setup(valid).ReturnsAsync(result);
			tio.Setup(throws).ReturnsAsync(default(AssetCount));
			var api = new NetworksApi(tio.Object);


			var exc = await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetNetworkAssetCountAsync(throwUuid, numDays, tokenSource.Token));
			tio.Verify(throws, Times.Once());
			tio.VerifyNoOtherCalls();
			Assert.Same(result, await api.GetNetworkAssetCountAsync(validUuid, numDays, tokenSource.Token));
			tio.Verify(valid, Times.Once());
			tio.VerifyNoOtherCalls();
		}


		[Theory]
		[InlineData("15579c76-7b33-4420-8a97-3d8092e7a85a", "84312e15-33ff-408d-9b62-1755ba5f664d", "7a57f5b0-6ba0-428c-aa34-fa934189ae63")]
		[InlineData("0b5550be-e96f-4b7b-b6eb-dc6d3849d220", "df06a26c-f193-4918-a8e3-00364b95e9c9", "660b66a0-b6b4-4912-b397-c594e1e9ad4c")]
		public async Task TestAssignScannerAsync(string uuidStr, string uuidNullStr, string assignUuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var validUuid = new Guid(uuidStr);
			var throwUuid = new Guid(uuidNullStr);
			using var tokenSource = new CancellationTokenSource();
			Expression<Func<ITenableIO, Task<EmptyJsonObject?>>> valid = tio => tio.RequestJsonAsync<EmptyJsonObject>(
				It.Is<string>(string.Format("networks/{0}/scanners/{1}", validUuid, assignUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Post),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			Expression<Func<ITenableIO, Task<EmptyJsonObject?>>> throws = tio => tio.RequestJsonAsync<EmptyJsonObject>(
				It.Is<string>(string.Format("networks/{0}/scanners/{1}", throwUuid, assignUuid), StringComparer.InvariantCulture),
				It.Is<Method>(m => m == Method.Post),
				It.Is<CancellationToken>(c => c == tokenSource.Token));
			tio.Setup(valid).ReturnsAsync(new EmptyJsonObject());
			tio.Setup(throws).ReturnsAsync(default(EmptyJsonObject));
			var api = new NetworksApi(tio.Object);


			var result = await api.AssignScannerAsync(throwUuid, assignUuid, tokenSource.Token);
			Assert.NotNull(result);
			tio.Verify(throws, Times.Once());
			tio.VerifyNoOtherCalls();
			result = await api.AssignScannerAsync(validUuid, assignUuid, tokenSource.Token);
			Assert.NotNull(result);
			tio.Verify(valid, Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("15579c76-7b33-4420-8a97-3d8092e7a85a", false, "84312e15-33ff-408d-9b62-1755ba5f664d", "7a57f5b0-6ba0-428c-aa34-fa934189ae63")]
		[InlineData("cdb41b64-0bb8-48ad-8272-315e65fe31ad", false)]
		[InlineData("0b5550be-e96f-4b7b-b6eb-dc6d3849d220", false, "df06a26c-f193-4918-a8e3-00364b95e9c9", "660b66a0-b6b4-4912-b397-c594e1e9ad4c", "b3a7389d-f07a-4e1a-aaf6-4a788cc374fc", "bdc9569f-f594-485a-90bb-2c868202ab29")]
		[InlineData("188b7cc5-6d6d-4058-86c2-d08c3d7af9e1", true)]
		[InlineData("188b7cc5-6d6d-4058-86c2-d08c3d7af9e1", true, "de8b6213-61f8-4087-9771-1952f34da32b")]
		public async Task TestAssignScannersAsync(string uuidStr, bool throws, params string[] assigns) {
			var exptectedBody = JsonSerializer.Serialize(new {
				scanner_uuids = assigns,
			});
			var networkUuid = new Guid(uuidStr);
			using var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Post, string.Format("https://cloud.tenable.com/networks/{0}/scanners", networkUuid))
				.WithContent(exptectedBody)
				.Respond("text/json", throws ? string.Empty : "{}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var restOptions = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };

			using var tio = new TenableIO("accesskey", "secretkey", restOptions);
			using var tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500);

			var result = await tio.Networks.AssignScannersAsync(networkUuid, assigns, tokenSource.Token);
			Assert.Null(Assert.IsType<EmptyJsonObject>(result).ExtensionData);
			Assert.Equal(1, mockHttp.GetMatchCount(call));
		}

		[Theory]
		[InlineData("dee91c51-de8e-43f9-a29a-26e8f6d0df9c", true, true)]
		[InlineData("edc7764b-033c-45b3-bf18-751ffb4afc2c", true, false)]
		[InlineData("8b213b81-0bae-43dd-b41a-1e9156f84c9a", false, true)]
		[InlineData("f59dc134-7a21-4729-8e8a-8fb0991de653", false, false)]
		public async Task TestListScannersAsync(string uuidStr, bool throws, bool listAssignable) {
			var networkUuid = new Guid(uuidStr);
			var url = string.Concat("https://cloud.tenable.com/networks/", uuidStr, (listAssignable ? "/assignable-scanners" : "/scanners"));
			using var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Get, url)
				.WithExactQueryString(string.Empty)
				.Respond("text/json", throws ? "null" : "{\"scanners\": []}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var restOptions = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };

			using var tio = new TenableIO("accesskey", "secretkey", restOptions);
			using var tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500);

			var networks = tio.Networks;
			Func<Guid, CancellationToken, Task<Scanners>> f = listAssignable ? networks.ListAssignableScannersAsync : networks.ListScannersAsync;
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(async () => await f(networkUuid, tokenSource.Token));
			}
			else {
				var result = Assert.IsType<Scanners>(await f(networkUuid, tokenSource.Token));
				Assert.Null(result.ExtensionData);
				Assert.Empty(result.Items);
			}
			Assert.Equal(1, mockHttp.GetMatchCount(call));
		}
	}
}
