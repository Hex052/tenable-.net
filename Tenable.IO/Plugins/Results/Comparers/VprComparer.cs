using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class VprComparer : EqualityComparer<Vpr> {
		private IEqualityComparer<VprDrivers> _driverComparer;
		public IEqualityComparer<VprDrivers> DriverComparer {
			get => this._driverComparer;
			set => this._driverComparer = value ?? throw new ArgumentNullException(nameof(value));
		}

		public VprComparer(IEqualityComparer<VprDrivers>? driverComparer = null) {
			this._driverComparer = driverComparer ?? new VprDriversComparer();
		}

		[Pure]
		public override bool Equals([AllowNull] Vpr x, [AllowNull] Vpr y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.Score == y.Score
					&& this._driverComparer.Equals(x.Drivers, y.Drivers)
					&& x.Updated == y.Updated;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Vpr obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return obj.Score.GetHashCode()
						+ (obj.Drivers == null ? 0 : this._driverComparer.GetHashCode(obj.Drivers))
						+ (obj.Updated.HasValue ? obj.Updated.Value.GetHashCode() : 0);
				}
			}
		}
	}
}
