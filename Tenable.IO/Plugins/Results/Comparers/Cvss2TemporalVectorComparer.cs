using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class Cvss2TemporalVectorComparer : EqualityComparer<Cvss2TemporalVector> {
		public Cvss2TemporalVectorComparer() {
		}

		[Pure]
		public override bool Equals([AllowNull] Cvss2TemporalVector x, [AllowNull] Cvss2TemporalVector y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.Exploitability == y.Exploitability
					&& x.RemediationLevel == y.RemediationLevel
					&& x.ReportConfidence == y.ReportConfidence
					&& x.Raw == y.Raw;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Cvss2TemporalVector obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (ReferenceEquals(obj.Exploitability, null) ? 0 : obj.Exploitability.GetHashCode())
						+ (ReferenceEquals(obj.RemediationLevel, null) ? 0 : obj.RemediationLevel.GetHashCode())
						+ (ReferenceEquals(obj.ReportConfidence, null) ? 0 : obj.ReportConfidence.GetHashCode())
						+ (ReferenceEquals(obj.Raw, null) ? 0 : obj.Raw.GetHashCode());
				}
			}
		}
	}
}
