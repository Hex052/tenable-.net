using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.IO.Plugins;
using Tenable.IO.Plugins.Results;
using Xunit;

namespace Tenable.IO.Tests.Plugins {
	public class TestPluginsApi {
		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public async Task TestGetAllFiltered(bool withDateFilter) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var pages = new PluginPage[] {
				// I just needed names for all these plugins, so this works.
				new PluginPage(
					new Plugin[] {
						new Plugin(24601, "En lo profundo", null!),
						new Plugin(131040, "Comienzo el truco en la cuerda floja que me marca el rumbo", null!),
						new Plugin(262144, "En lo profundo", null!),
						new Plugin(9670, "¿Alguna vez Hércules dijo: ¡No quiero, renuncio!", null!),
						new Plugin(24601, "En lo profundo", null!),
						new Plugin(32967, "Creo que mi esfuerzo es nulo, suento que no ayudo", null!),
					},
					size: 6,
					totalCount: 1291, // 215 * 6 + 1
					new PluginFilter() { Page = 214, Size = 6 }),
				new PluginPage(
					new Plugin[] {
						new Plugin(153813, "Echuzo un crack, comienza a tronar", null!),
						new Plugin(129825, "Se va a desmoronar, comienza a derrumbarse", null!),
						new Plugin(148321, "Es peso que con gota a gota lo reventó, whoa", null!),
						new Plugin(143808, "Peso como un tip-tip-tip hasta que haces pop, whoa, oh, oh", null!),
						new Plugin(152816, "Dáselo a tu hermana, pon en sus manos", null!),
						new Plugin(39933, "Todas las tareas que no aguantamos", null!),
					},
					size: 6,
					totalCount: 1291, // 215 * 6 + 1
				new PluginFilter() { Page = 215, Size = 6 }),
				new PluginPage(
					new Plugin[] {
						new Plugin(128694, "¿Quién soy yo si pierdo con el balón?", null!),
					},
					size: 1,
					totalCount: 1291, // 215 * 6 + 1
					new PluginFilter() { Page = 216, Size = 6 }),
			};
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.IsAny<CancellationToken>()
					))
				.Returns((string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken) => {
					cancellationToken.ThrowIfCancellationRequested();
					Assert.NotNull(queryParameters);
					var qparams = queryParameters!.ToList();
					Assert.Equal(withDateFilter ? 3 : 2, qparams.Count);
					Assert.Contains(new KeyValuePair<string, string>("size", "6"), qparams);
					if (withDateFilter) {
						Assert.Contains(new KeyValuePair<string, string>("last_updated", "2021-12-04"), qparams);
					}
					var page = int.Parse(qparams.First(f => f.Key == "page").Value);
					Assert.InRange(page, 214, 216);
					return Task.FromResult<PluginPage?>(pages[page - 214]);
				});
			var api = new PluginsApi(tio.Object);
			var filter = new PluginFilter() { Page = 214, Size = 6 };
			if (withDateFilter) {
				filter.LastUpdated = new DateTime(2021, 12, 4);
				foreach (var page in pages) {
					page.Parameters.LastUpdated = new DateTime(2021, 12, 4);
				}
			}
			var list = api.List(filter);
			var results = list.ToList();

			// Synchronous enumeration
			Assert.Equal(pages.SelectMany(p => p.Items), results);

			tio.Verify(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == default)),
				Times.Exactly(3));
			tio.VerifyNoOtherCalls();

			// Asynchronous enumeration
			list = api.List(filter);
			results.Clear();
			using var tokenSource = new CancellationTokenSource();
			await foreach (var item in list.WithCancellation(tokenSource.Token)) {
				results.Add(item);
			}
			Assert.Equal(pages.SelectMany(p => p.Items), results);

			tio.Verify(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Exactly(3));
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestGetAllEmpty() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var defaultSize = new PluginFilter().Size;
			var defaultSizeStr = defaultSize.ToString();
			var page = new PluginPage(
				Array.Empty<Plugin>(),
				size: 0,
				totalCount: 0,
				new PluginFilter() { Page = 1, Size = defaultSize, });
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.IsAny<CancellationToken>()))
				.Callback((string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken) => {
					cancellationToken.ThrowIfCancellationRequested();
					Assert.NotNull(queryParameters);
					var qparams = queryParameters!.ToList();
					Assert.Equal(2, qparams.Count);
					Assert.Contains(new KeyValuePair<string, string>("size", defaultSizeStr), qparams);
					Assert.Contains(new KeyValuePair<string, string>("page", "1"), qparams);
				})
				.Returns(Task.FromResult<PluginPage?>(page));
			var api = new PluginsApi(tio.Object);

			// Synchronous enumeraiton
			var list = api.List();
			Assert.Empty(list);
			tio.Verify(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == default)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Asynchronous enumeraiton
			list = api.List();
			using var tokenSource = new CancellationTokenSource();
			await using var enumerator = list.GetAsyncEnumerator(tokenSource.Token);
			Assert.False(await enumerator.MoveNextAsync());
			tio.Verify(
				tio => tio.RequestJsonAsync<PluginPage>(
					It.Is<string>("plugins/plugin", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		[InlineData(null)]
		public async Task TestGetFamilies(bool? all) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new PluginFamilyList(new PluginFamily[] {
				new PluginFamily(1, "test", 405),
				new PluginFamily(14, "test again", 225),
			});
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginFamilyList>(
					It.Is<string>("plugins/families", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.IsAny<CancellationToken>()))
				.Callback((string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken) => {
					cancellationToken.ThrowIfCancellationRequested();
					Assert.NotNull(queryParameters);
					var qparams = queryParameters!.ToList();
					if (all.HasValue) {
						Assert.Single(qparams);
						Assert.Equal("all", qparams[0].Key);
						Assert.Equal(all.Value ? "true" : "false", qparams[0].Value);
					}
					else if (qparams.Count != 0) {
						Assert.Single(qparams);
						Assert.Equal("all", qparams[0].Key);
						Assert.Contains(qparams[0].Value, new string[] { "true", "false" });
					}
				})
				.Returns(Task.FromResult<PluginFamilyList?>(result));
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginFamilyList>(
					It.Is<string>("plugins/families", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<PluginFamilyList?>(result));

			var api = new PluginsApi(tio.Object);
			using var tokenSource = new CancellationTokenSource();
			if (!all.HasValue) {
				Assert.Same(result, await api.FamiliesAsync(tokenSource.Token));
				tio.Verify(
					tio => tio.RequestJsonAsync<PluginFamilyList>(
						It.Is<string>("plugins/families", StringComparer.InvariantCulture),
						It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			else {
				Assert.Same(result, await api.FamiliesAsync(all.Value, tokenSource.Token));
				tio.Verify(
					tio => tio.RequestJsonAsync<PluginFamilyList>(
						It.Is<string>("plugins/families", StringComparer.InvariantCulture),
						It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
						It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(1)]
		[InlineData(23)]
		public async Task TestFamilyPlugins(int id) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var json = @"{
	""plugins"": [
		{
			""id"": 22372,
			""name"": ""AIX 5.1 : IY19744""
		},
		{
			""id"": 22373,
			""name"": ""AIX 5.1 : IY20486""
		},
		{
			""id"": 22374,
			""name"": ""AIX 5.1 : IY21309""
		}
	],
	""name"": ""AIX Local Security Checks"",
	""id"": 1
}";
			var result = JsonSerializer.Deserialize<PluginFamilyDetails>(json)!;
			result.Id = id;
			var destUrl = string.Format("plugins/families/{0}", id);
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginFamilyDetails>(
					It.Is<string>(destUrl, StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<PluginFamilyDetails?>(result));

			var api = new PluginsApi(tio.Object);
			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.FamilyPluginsAsync(id, tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<PluginFamilyDetails>(
					It.Is<string>(destUrl, StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(72186)]
		[InlineData(22372)]
		public async Task TestDetailsAsync(int id) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var attributes = new PluginAttribute[] {
				new PluginAttribute("fname", "aix_IY19744.nasl"),
				new PluginAttribute("plugin_name", "AIX 5.1 : IY19744"),
				new PluginAttribute("script_version", "$Revision: 1.5 $"),
				new PluginAttribute("solution", "http://www-912.ibm.com/eserver/support/fixes/"),
				new PluginAttribute("risk_factor", "High"),
				new PluginAttribute(
					"description",
					"The remote host is missing AIX Critical Security Patch number IY19744\n(SECURITY: Buffer Overflow in xntpd).\n\nYou should install this patch for your system to be up-to-date."),
				new PluginAttribute("plugin_publication_date", "2006/09/16"),
				new PluginAttribute("synopsis", "The remote host is missing a vendor-supplied security patch"),
			};
			var result = new PluginDetails(attributes, "AIX Local Security Checks", "AIX 5.1 : IY19744", 22372);
			result.Id = id;
			var destUrl = string.Format("plugins/plugin/{0}", id);
			tio.Setup(
				tio => tio.RequestJsonAsync<PluginDetails>(
					It.Is<string>(destUrl, StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<PluginDetails?>(result));

			var api = new PluginsApi(tio.Object);
			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.DetailsAsync(id, tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<PluginDetails>(
					It.Is<string>(destUrl, StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
