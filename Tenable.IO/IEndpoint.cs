using Tenable.Base;

namespace Tenable.IO {
	public interface IEndpoint : IEndpointBase<ITenableIO> {
	}
}
