using RestSharp;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Exceptions;
using Tenable.IO.Plugins.Results;

namespace Tenable.IO.Plugins {
	public class PluginsApi : IPluginsApi {
		public const string FAMILIES_PARTIAL_URL = "plugins/families";

		public ITenableIO Api {
			get;
		}

		internal PluginsApi(ITenableIO api) {
			this.Api = api;
		}

		public PluginEnumerable List(PluginFilter filter) {
			return new PluginEnumerable(this.Api, filter);
		}
		public PluginEnumerable List() {
			return new PluginEnumerable(this.Api, new PluginFilter());
		}

		public async Task<PluginFamilyList> FamiliesAsync(bool all, CancellationToken cancellationToken = default) {
			var result = await this.Api.RequestJsonAsync<PluginFamilyList>(
				FAMILIES_PARTIAL_URL,
				Method.Get,
				queryParameters: new[] { new KeyValuePair<string, string>("all", all ? "true" : "false") },
				cancellationToken);
			return result ?? throw new UnexpectedNullResultException(FAMILIES_PARTIAL_URL);
		}
		public async Task<PluginFamilyList> FamiliesAsync(CancellationToken cancellationToken = default) {
			var result = await this.Api.RequestJsonAsync<PluginFamilyList>(
				FAMILIES_PARTIAL_URL, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(FAMILIES_PARTIAL_URL);
		}

		public async Task<PluginDetails> DetailsAsync(int id, CancellationToken cancellationToken = default) {
			var partialUrl = string.Format("plugins/plugin/{0}", id);
			var result = await this.Api.RequestJsonAsync<PluginDetails>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}

		public async Task<PluginFamilyDetails> FamilyPluginsAsync(int id, CancellationToken cancellationToken = default) {
			var partialUrl = string.Format("plugins/families/{0}", id);
			var result = await this.Api.RequestJsonAsync<PluginFamilyDetails>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}
	}
}
