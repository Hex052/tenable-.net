using Tenable.Base;

namespace Tenable.IO.Plugins.Results {
	public class PluginPagination : IPagination {
		private readonly PluginPage _page;

		internal PluginPagination(PluginPage page) {
			this._page = page;
		}

		public int Total => this._page.TotalCount;

		public int Limit => this._page.Parameters.Size;

		public int Offset => (this._page.Parameters.Page - 1) * this._page.Parameters.Size;
	}
}
