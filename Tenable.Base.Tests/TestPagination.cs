using Xunit;
using System.Text.Json;

namespace Tenable.Base.Tests {
	public class TestPagination {
		[Fact]
		public void TestDeserialize() {
			var json = @"{""total"":3,""limit"":50,""offset"":0,""sort"":[{""name"":""name"",""order"":""asc""}]}";
			var page = JsonSerializer.Deserialize<Pagination>(json)!;
			Assert.Equal(3, page.Total);
			Assert.Equal(50, page.Limit);
			Assert.Equal(0, page.Offset);
			Assert.NotNull(page.Sort);
			Assert.Single(page.Sort);
			Assert.Equal("name", page.Sort[0].Field);
			Assert.Equal(SortDirection.ASC, page.Sort[0].Direction);
		}
	}
}
