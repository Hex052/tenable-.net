using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class XrefComparer : EqualityComparer<Xref> {
		[Pure]
		public override bool Equals([AllowNull] Xref x, [AllowNull] Xref y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.Id == y.Id && x.Type == y.Type;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Xref obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (ReferenceEquals(null, obj.Id) ? 0 : obj.Id.GetHashCode())
						+ (ReferenceEquals(null, obj.Type) ? 0 : obj.Type.GetHashCode());
				}
			}
		}
	}
}
