using System.Threading;
using System.Threading.Tasks;

namespace Tenable.Base.Collections {
	/// <summary>
	/// Base enumerator for anything where the results are returned in pages,
	/// where the pages include how many there are and the current offset
	/// </summary>
	/// <typeparam name="TPage">
	/// Type of page that is returned from the API
	/// </typeparam>
	/// <typeparam name="TPagination">
	/// Parameter to <see cref="IPage{TItem, TPagination}"/>
	/// </typeparam>
	/// <inheritdoc cref="ResultEnumerator{TResult, TCache, TEnumerable}"/>
	public abstract class PageEnumerator<TResult, TEnumerable, TPage, TPagination> : ResultEnumerator<TResult, TPage, TEnumerable> where TEnumerable : PageEnumerable<TResult, TPage, TPagination> where TPage : class, IPage<TResult, TPagination> where TPagination : IPagination {
		protected PageEnumerator(TEnumerable enumerable,
				CancellationToken cancellationToken) :
				base(enumerable, cancellationToken) {
		}

		/// <summary>
		/// Should simply return whatever the next page id is,
		/// with no side effects.
		/// If <see cref="ResultEnumerator{TResult, TCache, TEnumerable}.PageId"/>
		/// is null, then this should return whatever the first page id is.
		/// </summary>
		/// <returns>The next page id after the current one</returns>
		protected abstract int NextPageId();

		protected override async ValueTask<bool> NextPageAsync() {
			// not using this.PageId since it shouldn't change if this fails
			int nextPageId = this.NextPageId();

			TPage page = await this.Enumerable.GetFullPageAsync(nextPageId, this.CancellationToken);
			if (this.PageId.HasValue) {
				// Whether we reached the last page or not, we can do this
				this.Processed.Add(this.PageId.Value);
			}
			if (page.Items.Count == 0) {
				// Apparently we had already reached the last page
				this.LastPage = true;
				return false;
			}
			this.PageId = nextPageId;
			this.CurrentPage = page.Items;
			this.PageIndex = 0;

			// Check if we don't have any more pages after this one
			var pagination = page.Pagination;
			if (pagination.Total <= pagination.Offset + pagination.Limit) {
				this.LastPage = true;
			}
			return true;
		}
	}
}
