using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Plugins.Results {
	// TODO https://developer.tenable.com/reference/io-plugins-list
	public class Plugin {
		/// <summary>The ID of the plugin</summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}

		/// <summary>The name of the plugin</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}

		/// <summary>The plugin attributes</summary>
		/// <seealso href="https://developer.tenable.com/docs/tenable-plugin-attributes"/>
		[JsonPropertyName("attributes")]
		public PluginAttributes Attributes {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Plugin(int id, string name, PluginAttributes attributes) {
			this.Id = id;
			this.Name = name;
			this.Attributes = attributes;
		}
	}
}
