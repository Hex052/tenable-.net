using Xunit;
using System.Text.Json;
using Tenable.IO.Agents.Results;

namespace Tenable.IO.Tests.Agents.Results {
	public class TestAgentPage {
		[Fact]
		public void TestDeserialize() {
			var json = @"{""agents"":[{""id"":157,""uuid"":""655993d5-c131-46e8-a82f-957f6f894cac"",""name"":""GRD-LPTP"",""platform"":""WINDOWS"",""distro"":""win-x86-64"",""ip"":""192.0.2.57"",""last_scanned"":1515620036,""plugin_feed_id"":""201801081515"",""core_build"":""106"",""core_version"":""7.0.0"",""linked_on"":1456775443,""last_connect"":1515674073,""status"":""off"",""groups"":[{""name"":""CodyAgents"",""id"":8},{""name"":""AgentGroupA"",""id"":3316}]},{""id"":14569,""uuid"":""72ac6ad1-fc86-4af4-be0c-0ff3bfbfb242"",""name"":""example.com"",""platform"":""LINUX"",""distro"":""es7-x86-64"",""ip"":""192.0.2.57"",""plugin_feed_id"":""201805161620"",""core_build"":""13"",""core_version"":""7.0.3"",""linked_on"":1508329832,""last_connect"":1526565530,""status"":""off"",""groups"":[{""name"":""SCResearch"",""id"":1167}]},{""id"":14570,""uuid"":""938cb466-06ea-477e-abb0-99d8da0e0f20"",""name"":""example.com"",""platform"":""LINUX"",""distro"":""es7-x86-64"",""ip"":""192.0.2.57"",""plugin_feed_id"":""201805161620"",""core_build"":""13"",""core_version"":""7.0.3"",""linked_on"":1508329886,""last_connect"":1526565624,""status"":""off"",""groups"":[{""name"":""SCResearch"",""id"":1167}]}],""pagination"":{""total"":3,""limit"":50,""offset"":0,""sort"":[{""name"":""name"",""order"":""asc""}]}}";
			var page = JsonSerializer.Deserialize<AgentPage>(json)!;
			Assert.NotNull(page.Items);
			Assert.NotNull(page.Pagination);
			Assert.Equal(3, page.Items.Count);
		}
	}
}
