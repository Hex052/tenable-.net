using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestNullableDateJsonConverter {
		public class TestObject {
			[JsonConverter(typeof(NullableDateJsonConverter))]
			public DateTime? Time {
				get; set;
			}
		}

		[Theory]
		[InlineData("\"1970-01-01\"", "1970-01-01")]
		[InlineData("\"2021-05-15\"", "2021-05-15")]
		[InlineData("\"2017-12-31\"", "2017-12-31")]
		[InlineData("\"2017-12-31T15:57:02.890\"", null)]
		[InlineData("\"\"", null)]
		[InlineData("null", null)]
		public void TestDeserialize(string jsonTime, string? time) {
			var expected = time is null ? default(DateTime?) : DateTime.Parse(time);
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.Time), jsonTime);
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Time;
			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("false")]
		[InlineData(0)]
		public void TestDeserializeException(object badData) {
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.Time), badData);
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("1970-01-01T00:00:00.000", "\"1970-01-01\"")]
		[InlineData("2021-05-15T05:50:32.853", "\"2021-05-15\"")]
		[InlineData("2017-12-31T20:40:23.447", "\"2017-12-31\"")]
		[InlineData(null, "\"\"")]
		public void TestSerialize(string? time, string expected) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new NullableDateJsonConverter();

			converter.Write(jsonWriter, time is null ? default(DateTime?) : DateTime.Parse(time), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
	}
}
