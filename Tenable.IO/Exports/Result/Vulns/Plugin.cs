using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Tenable.IO.Plugins.Results;

namespace Tenable.IO.Exports.Result.Vulns {
	public class Plugin {
		/// <summary>The plugin Bugtraq ID</summary>
		[JsonPropertyName("bid")]
		public IList<int>? BugtraqId {
			get; set;
		}

		/// <summary>
		/// The name of the CANVAS exploit pack that includes the vulnerability
		/// </summary>
		[JsonPropertyName("canvas_package")]
		public string? CanvasPackage {
			get; set;
		}

		/// <summary>
		/// A value specifying whether the plugin checks for default accounts
		/// </summary>
		[JsonPropertyName("checks_for_default_account")]
		public bool? ChecksForDefaultAccount {
			get; set;
		}
		/// <summary>
		/// A value specifying whether the plugin checks for malware
		/// </summary>
		[JsonPropertyName("checks_for_malware")]
		public bool? ChecksForMalware {
			get; set;
		}

		/// <summary>The Common Platform Enumeration number</summary>
		[JsonPropertyName("cpe")]
		public List<string>? Cpe {
			get; set;
		}
		/// <summary>The Common Vulnerability and Exposure ID</summary>
		[JsonPropertyName("cve")]
		public List<string>? Cve {
			get; set;
		}

		/// <summary>
		/// The CVSS base score (intrinsic and fundamental characteristics of
		/// a vulnerability that are constant over time and user environments)
		/// </summary>
		[JsonPropertyName("cvss_base_score")]
		public decimal? CvssBaseScore {
			get; set;
		}
		/// <summary>
		/// The CVSS temporal score (characteristics of a vulnerability
		/// that change over time but not among user environments)
		/// </summary>
		[JsonPropertyName("cvss_temporal_score")]
		public decimal? CvssTemporalScore {
			get; set;
		}
		/// <summary>
		/// CVSS temporal metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss_temporal_vector")]
		public CvssTemporalVector? CvssTemporalVector {
			get; set;
		}
		/// <summary>
		/// Additional CVSS metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss_vector")]
		public CvssVector? CvssVector {
			get; set;
		}
		/// <summary>
		/// The CVSSv3 base score (intrinsic and fundamental characteristics of
		/// a vulnerability that are constant over time and user environments)
		/// </summary>
		[JsonPropertyName("cvss3_base_score")]
		public decimal? Cvss3BaseScore {
			get; set;
		}
		/// <summary>
		/// The CVSSv3 temporal score (characteristics of a vulnerability
		/// that change over time but not among user environments)
		/// </summary>
		[JsonPropertyName("cvss3_emporal_score")]
		public decimal? Cvss3TemporalScore {
			get; set;
		}
		/// <summary>
		/// CVSSv3 temporal metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss3_temporal_vector")]
		public CvssTemporalVector? Cvss3TemporalVector {
			get; set;
		}
		/// <summary>
		/// Additional CVSSv3 metrics for the vulnerability
		/// </summary>
		[JsonPropertyName("cvss3_vector")]
		public CvssVector? Cvss3Vector {
			get; set;
		}

		/// <summary>
		/// The name of the exploit in the D2 Elliot Web Exploitation framework
		/// </summary>
		[JsonPropertyName("d2_elliot_name")]
		public string? D2ElliotName {
			get; set;
		}

		/// <summary>
		/// text description of the vulnerability
		/// </summary>
		[JsonPropertyName("description")]
		public string Description {
			get; set;
		}

		/// <summary>
		/// Whether a public exploit exists
		/// </summary>
		[JsonPropertyName("exploit_available")]
		public bool? ExploitAvailable {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the Immunity CANVAS framework
		/// </summary>
		[JsonPropertyName("exploit_framework_canvas")]
		public bool? ExploitFrameworkCanvas {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the CORE impact framework
		/// </summary>
		[JsonPropertyName("exploit_framework_core")]
		public bool? ExploitFrameworkCore {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the
		/// D2 Elliot Web Exploitation framework
		/// </summary>
		[JsonPropertyName("exploit_framework_d2_elliot")]
		public bool? ExploitFrameworkD2Elliot {
			get; set;
		}
		/// <summary>
		/// Indicates if an exploit exists in the Metasploit framework
		/// </summary>
		[JsonPropertyName("exploit_framework_metasploit")]
		public bool? ExploitFrameworkMetasploit {
			get; set;
		}
		/// <summary>
		/// Description of how easy it is to exploit the issue
		/// </summary>
		[JsonPropertyName("exploitability_ease")]
		public string? ExploitabilityEase {
			get; set;
		}
		/// <summary>
		/// This vulnerability is known to be exploited by malware
		/// </summary>
		[JsonPropertyName("exploited_by_malware")]
		public bool? ExploitedByMalware {
			get; set;
		}
		/// <summary>
		/// Whether Nessus exploited the vulnerability
		/// during the process of identification
		/// </summary>
		[JsonPropertyName("exploited_by_nessus")]
		public bool? ExploitedByNessus {
			get; set;
		}
		/// <summary>
		/// The SKU number of the exploit in the ExploitHub framework
		/// </summary>
		[JsonPropertyName("exploithub_sku")]
		public string? ExploithubSku {
			get; set;
		}

		/// <summary>
		/// The family to which plugin belongs
		/// </summary>
		[JsonPropertyName("family")]
		public string Family {
			get; set;
		}
		/// <summary>
		/// The ID of the plugin family
		/// </summary>
		[JsonPropertyName("family_id")]
		public int FamilyId {
			get; set;
		}
		/// <summary>
		/// Whether the vendor has published a patch for the vulnerability
		/// </summary>
		[JsonPropertyName("has_patch")]
		public bool HasPatch {
			get; set;
		}
		/// <summary>
		/// The ID of the plugin
		/// </summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}

		/// <summary>
		/// Whether this plugin has received media attention,
		/// such as for ShellShock or Meltdown
		/// </summary>
		[JsonPropertyName("in_the_news")]
		public bool? InTheNews {
			get; set;
		}
		/// <summary>
		/// The name of the related exploit in the Metasploit framework
		/// </summary>
		[JsonPropertyName("metasploit_name")]
		public string? MetasploitName {
			get; set;
		}
		/// <summary>
		/// The Microsoft security bulletin that this plugin covers
		/// </summary>
		[JsonPropertyName("ms_bulletin")]
		public string? MicrosoftBulletin {
			get; set;
		}

		/// <summary>
		/// The name of the plugin
		/// </summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}
		/// <summary>
		/// The date on which the vendor published a patch for the vulnerability
		/// </summary>
		[JsonPropertyName("patch_publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? PatchPublicationDate {
			get; set;
		}
		/// <summary>
		/// The date on which this plugin was last modified
		/// </summary>
		[JsonPropertyName("modification_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? ModificationDate {
			get; set;
		}
		/// <summary>
		/// The date on which this plugin was published
		/// </summary>
		[JsonPropertyName("publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime PublicationDate {
			get; set;
		}

		/// <summary>
		/// The risk factor associated with the plugin
		/// </summary>
		/// <remarks>
		/// Possible values are: None,
		/// Low (The vulnerability has a CVSS score between 0.1 and 3.9),
		/// Medium (The vulnerability has a CVSS score between 4.0 and 6.9),
		/// High (The vulnerability has a CVSS score between 7.0 and 9.9),
		/// or Critical (The vulnerability has a CVSS score of 10.0).
		/// </remarks>
		[JsonPropertyName("risk_factor")]
		public string RiskFactor {
			get; set;
		}
		/// <summary>
		/// Links to external websites that contain helpful information
		/// </summary>
		[JsonPropertyName("see_also")]
		public List<string>? SeeAlso {
			get; set;
		}
		/// <summary>
		/// Remediation information for the vulnerability
		/// </summary>
		[JsonPropertyName("solution")]
		public string? Solution {
			get; set;
		}
		/// <summary>
		/// Security Technical Implementation Guide severity code
		/// for this vulnerability
		/// </summary>
		[JsonPropertyName("stig_severity")]
		public string? StigSeverity {
			get; set;
		}
		/// <summary>
		/// Brief description of the plugin or vulnerability
		/// </summary>
		[JsonPropertyName("synopsis")]
		public string Synopsis {
			get; set;
		}
		/// <summary>
		/// The general type of plugin check (for example, local or remote).
		/// </summary>
		[JsonPropertyName("type")]
		public string Type {
			get; set;
		}
		/// <summary>
		/// Software found by this plugin is unsupported by the software's vendor
		/// </summary>
		[JsonPropertyName("unsupported_by_vendor")]
		public bool? UnsupportedByVendor {
			get; set;
		}

		/// <summary>
		/// Ubuntu security notice that this plugin covers
		/// </summary>
		[JsonPropertyName("usn")]
		public string? UbuntuSecurityNotice {
			get; set;
		}

		/// <summary>
		/// The version of this plugin used
		/// </summary>
		[JsonPropertyName("version")]
		public string Version {
			get; set;
		}
		/// <summary>
		/// Information about the Vulnerability Priority Rating (VPR)
		/// for the vulnerability
		/// </summary>
		[JsonPropertyName("vpr")]
		public Vpr? Vpr {
			get; set;
		}

		/// <summary>
		/// The date on which this vulnerability was published
		/// </summary>
		[JsonPropertyName("vuln_publication_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? VulnPublicationDate {
			get; set;
		}
		/// <summary>
		/// References to third-party information about the vulnerability,
		/// exploit, or update associated with the plugin
		/// </summary>
		[JsonPropertyName("xrefs")]
		public List<Xref>? Xrefs {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Plugin(string description, string family, int familyId,
				bool hasPatch, string name, DateTime publicationDate,
				string riskFactor, string synopsis, string type, string version) {
			this.Description = description;
			this.Family = family;
			this.FamilyId = familyId;
			this.HasPatch = hasPatch;
			this.Name = name;
			this.Synopsis = synopsis;
			this.Type = type;
			this.PublicationDate = publicationDate;
			this.RiskFactor = riskFactor;
			this.Version = version;
		}
	}
}
