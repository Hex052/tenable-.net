using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports.Result.Vulns {
	public class VulnExportStatus : RequestExportStatus {
		/// <summary>
		/// Export uuid
		/// </summary>
		[JsonPropertyName("uuid")]
		[JsonInclude]
		public Guid Uuid {
			get; set;
		}

		/// <summary>
		/// A list of chunks for which the export process failed
		/// </summary>
		[JsonPropertyName("chunks_failed")]
		public IReadOnlyList<int> ChunksFailed {
			get; set;
		}

		/// <summary>
		/// A list of chunks for which the export process was cancelled
		/// </summary>
		/// <remarks>
		/// If a chunk fails processing, Tenable.io automatically cancels all
		/// subsequent chunks queued for export in the same request
		/// </remarks>
		[JsonPropertyName("chunks_cancelled")]
		public IReadOnlyList<int> ChunksCancelled {
			get; set;
		}

		/// <summary>
		/// The total number of chunks associated with the export as a whole
		/// </summary>
		[JsonPropertyName("total_chunks")]
		public int TotalChunks {
			get; set;
		}

		/// <summary>
		/// The total number of completed chunks available for download
		/// </summary>
		[JsonPropertyName("chunks_available_count")]
		public int ChunksAvailableCount {
			get; set;
		}

		/// <summary>
		/// The total number of empty chunks
		/// </summary>
		[JsonPropertyName("empty_chunks_count")]
		public int ChunksEmptyCount {
			get; set;
		}

		/// <summary>
		/// The number of chunks that have been processed
		/// and are available for download
		/// </summary>
		[JsonPropertyName("finished_chunks")]
		public int FinishedChunks {
			get; set;
		}

		// TODO make this not just type Object
		/// <summary>
		/// The filters used in the export request
		/// </summary>
		[JsonPropertyName("filters")]
		public object Filters {
			get; set;
		}

		/// <summary>
		/// The number of assets contained in each export chunk
		/// </summary>
		[JsonPropertyName("num_assets_per_chunk")]
		public int NumAssetsPerChunk {
			get; set;
		}

		/// <summary>
		/// When the export job was created
		/// </summary>
		[JsonPropertyName("created")]
		[JsonConverter(typeof(UnixMillisecondConverter))]
		public DateTime Created {
			get; set;
		}

		/// <remarks>
		/// Probably too many parameters, but whatever. You shouldn't be creating
		/// any by hand, System.Text.Json should take care of that for you.
		/// </remarks>
		/// <param name="status"></param>
		/// <param name="chunksAvailable"></param>
		/// <param name="uuid"></param>
		/// <param name="chunksFailed"></param>
		/// <param name="chunksCancelled"></param>
		/// <param name="totalChunks"></param>
		/// <param name="chunksAvailableCount"></param>
		/// <param name="chunksEmptyCount"></param>
		/// <param name="finishedChunks"></param>
		/// <param name="filters"></param>
		/// <param name="numAssetsPerChunk"></param>
		/// <param name="created"></param>
		[JsonConstructor]
		public VulnExportStatus(Status status, IReadOnlyList<int> chunksAvailable,
				Guid uuid, IReadOnlyList<int> chunksFailed,
				IReadOnlyList<int> chunksCancelled, int totalChunks,
				int chunksAvailableCount, int chunksEmptyCount, int finishedChunks,
				object filters, int numAssetsPerChunk, DateTime created) : base(status, chunksAvailable) {
			this.Uuid = uuid;
			this.ChunksFailed = chunksFailed;
			this.ChunksCancelled = chunksCancelled;
			this.TotalChunks = totalChunks;
			this.ChunksAvailableCount = chunksAvailableCount;
			this.ChunksEmptyCount = chunksEmptyCount;
			this.FinishedChunks = finishedChunks;
			this.Filters = filters;
			this.NumAssetsPerChunk = numAssetsPerChunk;
			this.Created = created;
		}
	}
}
