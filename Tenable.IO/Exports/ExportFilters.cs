using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public abstract class ExportFilters {
		/// <summary>
		/// The ID of the network object associated with scanners that
		/// detected what you want to export
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("network_id")]
		public Guid? NetworkId {
			get; set;
		}

		/// <summary>Tuples of tag categories and values</summary>
		/// <remarks>Case sensitive</remarks>
		[JsonIgnore(Condition = JsonIgnoreCondition.Always)]
		[JsonPropertyName("tag")]
		public IEnumerable<KeyValuePair<string, string>>? Tag {
			get; set;
		}
	}
}
