using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports {
	public abstract class ExportFiltersConverter<T> : JsonConverter<T> where T : ExportFilters {
		protected UnixSecondConverter _dateConverter = new UnixSecondConverter();

		public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			throw new NotSupportedException(nameof(T) + " cannot be read from JSON");
		}

		public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options) {
			if (value.NetworkId != null) {
				writer.WriteString("network_id", value.NetworkId.Value);
			}

			// And now the whole reason for writing this custom converter: tags
			if (value.Tag != null) {
				foreach (var grouped in value.Tag.GroupBy(t => t.Key)) {
					writer.WritePropertyName("tag." + grouped.Key);
					JsonSerializer.Serialize<IEnumerable<string>>(writer, grouped.Select(t => t.Value), options);
				}
			}
		}
	}
}
