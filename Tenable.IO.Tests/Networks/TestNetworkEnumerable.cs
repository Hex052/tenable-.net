using RestSharp;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Tests;
using Tenable.IO.Networks;
using Tenable.IO.Networks.Results;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.IO.Tests.Networks {
	public class TestNetworkEnumerable {
		private readonly ITestOutputHelper output;
		public TestNetworkEnumerable(ITestOutputHelper output) {
			this.output = output;
		}

		[Fact]
		public async Task TestGetAllAsync() {
			var page1response = @"
{
	""networks"": [
		{
			""owner_uuid"": ""ddbd3e11-3311-4682-9912-8e81805fd8a9"",
			""created"": 1544744085270,
			""modified"": 1544744085270,
			""scanner_count"": 10,
			""uuid"": ""00000000-0000-0000-0000-000000000000"",
			""name"": ""Default"",
			""is_default"": true,
			""created_by"": ""ddbd3e11-3311-4682-9912-8e81805fd8a9"",
			""modified_by"": ""ddbd3e11-3311-4682-9912-8e81805fd8a9"",
			""created_in_seconds"": 1544744085,
			""modified_in_seconds"": 1544744085
		},
		{
			""owner_uuid"": ""0e67b283-07a4-464c-a5e4-7b42576962fd"",
			""created"": 1557526802865,
			""modified"": 1557526802865,
			""scanner_count"": 1,
			""uuid"": ""42475f11-5e6b-4d6a-a53d-63fe494961df"",
			""name"": ""Headquarters"",
			""description"": ""Network devices at Columbia, MD location"",
			""is_default"": false,
			""created_by"": ""0f403df2-3b35-4339-9f74-1574805de203"",
			""modified_by"": ""0f403df2-3b35-4339-9f74-1574805de203"",
			""assets_ttl_days"": 91,
			""created_in_seconds"": 1557526802,
			""modified_in_seconds"": 1557526802
		}
	],
	""pagination"": {
		""total"": 3,
		""limit"": 2,
		""offset"": 0,
		""sort"": [
			{
				""name"": ""name"",
				""order"": ""asc""
			}
		]
	}
}";
			var page2response = @"
{
	""networks"": [
		{
			""owner_uuid"": ""0e67b283-07a4-464c-a5e4-7b42576962fd"",
			""created"": 1557526809861,
			""modified"": 1557526809861,
			""scanner_count"": 2,
			""uuid"": ""42475f11-5e6b-4d6a-a53d-63fe494961df"",
			""name"": ""HQ2"",
			""description"": ""Network devices at Denver, CO location"",
			""is_default"": false,
			""created_by"": ""0f403df2-3b35-4339-9f74-1574805de203"",
			""modified_by"": ""0f403df2-3b35-4339-9f74-1574805de203"",
			""assets_ttl_days"": 91,
			""created_in_seconds"": 1557526809,
			""modified_in_seconds"": 1557526809
		}
	],
	""pagination"": {
		""total"": 3,
		""limit"": 2,
		""offset"": 2,
		""sort"": [
			{
				""name"": ""name"",
				""order"": ""asc""
			}
		]
	}
}";
			var expectedResults = new Network[] {
				new Network(
					ownerUuid: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
					created: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
					modified: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
					uuid: Guid.Empty,
					name: "Default",
					isDefault: true,
					createdBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
					modifiedBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
					createdInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc),
					modifiedInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc)) {
						ScannerCount = 10
					},
				new Network(
					ownerUuid: new Guid("0e67b283-07a4-464c-a5e4-7b42576962fd"),
					created: new DateTime(2019, 5, 10, 22, 20, 2, 865, DateTimeKind.Utc),
					modified: new DateTime(2019, 5, 10, 22, 20, 2, 865, DateTimeKind.Utc),
					uuid: new Guid("42475f11-5e6b-4d6a-a53d-63fe494961df"),
					name: "Headquarters",
					isDefault: false,
					createdBy: new Guid("0f403df2-3b35-4339-9f74-1574805de203"),
					modifiedBy: new Guid("0f403df2-3b35-4339-9f74-1574805de203"),
					createdInSeconds: new DateTime(2019, 5, 10, 22, 20, 2, DateTimeKind.Utc),
					modifiedInSeconds: new DateTime(2019, 5, 10, 22, 20, 2, DateTimeKind.Utc)) {
						ScannerCount = 1,
						AssetsTtlDays = 91,
						Description = "Network devices at Columbia, MD location",
					},
				new Network(
					ownerUuid: new Guid("0e67b283-07a4-464c-a5e4-7b42576962fd"),
					created: new DateTime(2019, 5, 10, 22, 20, 9, 861, DateTimeKind.Utc),
					modified: new DateTime(2019, 5, 10, 22, 20, 9, 861, DateTimeKind.Utc),
					uuid: new Guid("42475f11-5e6b-4d6a-a53d-63fe494961df"),
					name: "HQ2",
					isDefault: false,
					createdBy: new Guid("0f403df2-3b35-4339-9f74-1574805de203"),
					modifiedBy: new Guid("0f403df2-3b35-4339-9f74-1574805de203"),
					createdInSeconds: new DateTime(2019, 5, 10, 22, 20, 9, DateTimeKind.Utc),
					modifiedInSeconds: new DateTime(2019, 5, 10, 22, 20, 9, DateTimeKind.Utc)) {
						ScannerCount = 2,
						AssetsTtlDays = 91,
						Description = "Network devices at Denver, CO location",
					},
			};
			var filter = new NetworkFilter() {
				Filter = new Tuple<string, string, string>[] {
					new Tuple<string, string, string>("name", "eq", "Default"),
					new Tuple<string, string, string>("name", "eq", "HQ2"),
					new Tuple<string, string, string>("name", "eq", "Headquarters"),
				},
				FilterType = FilterType.OR,
				Limit = 2,
				Sort = new SortOrder[] { new SortOrder("name", SortDirection.ASC) },
				IncludeDeleted = false,
			};
			var query1 = new KeyValuePair<string, string>[] {
				new KeyValuePair<string, string>("f", "name:eq:Default"),
				new KeyValuePair<string, string>("f", "name:eq:HQ2"),
				new KeyValuePair<string, string>("f", "name:eq:Headquarters"),
				new KeyValuePair<string, string>("ft", "or"),
				new KeyValuePair<string, string>("limit", "2"),
				new KeyValuePair<string, string>("offset", "0"),
				new KeyValuePair<string, string>("sort", "name:asc"),
				new KeyValuePair<string, string>("includeDeleted", "false"),
			};
			var query2 = new KeyValuePair<string, string>[] {
				new KeyValuePair<string, string>("f", "name:eq:Default"),
				new KeyValuePair<string, string>("f", "name:eq:HQ2"),
				new KeyValuePair<string, string>("f", "name:eq:Headquarters"),
				new KeyValuePair<string, string>("ft", "or"),
				new KeyValuePair<string, string>("limit", "2"),
				new KeyValuePair<string, string>("offset", "2"),
				new KeyValuePair<string, string>("sort", "name:asc"),
				new KeyValuePair<string, string>("includeDeleted", "false"),
			};
			using var mockHttp = new MockHttpMessageHandler();

			var noresponse = Util.HttpMockNoResponseException(this.output);
			var call1 = mockHttp.When(HttpMethod.Get, "https://cloud.tenable.com/networks")
				.WithQueryString(query1)
				.Respond("application/json", page1response);
			var call2 = mockHttp.When(HttpMethod.Get, "https://cloud.tenable.com/networks")
				.WithQueryString(query2)
				.Respond("application/json", page2response);
			mockHttp.Fallback.Respond(noresponse);
			var restOptions = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };

			using var tio = new TenableIO("accesskey", "secretkey", restOptions);
			using var tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500);

			var results = new List<Network>(3);
			await foreach (var item in tio.Networks.ListNetworks(filter).WithCancellation(tokenSource.Token)) {
				results.Add(item);
			}
			Assert.Equal(1, mockHttp.GetMatchCount(call1));
			Assert.Equal(1, mockHttp.GetMatchCount(call2));
			Assert.Equal(expectedResults.Length, results.Count);
			for (int i = 0; i < expectedResults.Length; ++i) {
				try {
					Assert.Equal(expectedResults[i], results[i]);
				}
				catch {
					output.WriteLine("at pos {0}", i);
					throw;
				}
			}
			Assert.All(results, r => Assert.Null(r.ExtensionData));
		}
	}
}
