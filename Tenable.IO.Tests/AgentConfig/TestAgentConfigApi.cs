using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.Base.Exceptions;
using Tenable.IO.AgentConfig;
using Xunit;

namespace Tenable.IO.Tests.AgentConfig {
	public class TestAgentConfigApi {
		[Fact]
		public void TestFromTenableIO() {
			var tio = new TenableIO(
				"testKey",
				"testSecret",
				"https://example.com/");
			var agentConfig = tio.AgentConfig;
			Assert.NotNull(agentConfig);
			Assert.IsType<AgentConfigApi>(agentConfig);
			Assert.Same(tio, agentConfig.Api);
			Assert.Same(agentConfig, tio.AgentConfig);
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public async Task TestGetAsync(bool throws) {
			// Setup
			var result = new IO.AgentConfig.Results.AgentConfig(
				true,
				new IO.AgentConfig.Results.AutoUnlink(90, false));
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			tio.Setup(f => f.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<IO.AgentConfig.Results.AgentConfig?>(throws ? null : result));
			var api = new AgentConfigApi(tio.Object);

			// Test
			using var tokenSource = new CancellationTokenSource();
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetAsync(tokenSource.Token));
			}
			else {
				Assert.Equal(result, await api.GetAsync(tokenSource.Token));
			}

			// Assert
			tio.Verify(
				t => t.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(
					It.Is<string>("scanners/1/agents/config", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with explicit scanner ID
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetAsync(25, tokenSource.Token));
			}
			else {
				Assert.Equal(result, await api.GetAsync(25, tokenSource.Token));
			}

			// Assert
			tio.Verify(
				t => t.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(
					It.Is<string>("scanners/25/agents/config", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public async Task TestSetAsync(bool throws) {
			// Setup
			var result = new IO.AgentConfig.Results.AgentConfig(
				true,
				new IO.AgentConfig.Results.AutoUnlink(90, false));
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new AgentConfigApi(tio.Object);
			var postJson = new IO.AgentConfig.Results.AgentConfig() {
				AutoUnlink = new IO.AgentConfig.Results.AutoUnlink() { Enabled = false },
				SoftwareUpdate = true,
			};
			tio.Setup(
					f => f.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(
						It.IsAny<string>(),
						It.IsAny<Method>(),
						It.IsAny<IO.AgentConfig.Results.AgentConfig>(),
						It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<IO.AgentConfig.Results.AgentConfig?>(throws ? null : result));

			// Test
			using var tokenSource = new CancellationTokenSource();
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.SetAsync(postJson, tokenSource.Token));
			}
			else {
				Assert.Equal(result, await api.SetAsync(postJson, tokenSource.Token));
			}

			// Assert
			tio.Verify(
				t => t.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(
					It.Is<string>("scanners/1/agents/config", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Put, EqualityComparer<Method>.Default),
					It.Is<IO.AgentConfig.Results.AgentConfig>(j => ReferenceEquals(postJson, j)),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with explicit scanner ID
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.SetAsync(postJson, 25, tokenSource.Token));
			}
			else {
				Assert.Equal(result, await api.SetAsync(postJson, 25, tokenSource.Token));
			}
			tio.Verify(
				t => t.RequestJsonAsync<IO.AgentConfig.Results.AgentConfig>(
					It.Is<string>("scanners/25/agents/config", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Put, EqualityComparer<Method>.Default),
					It.Is<IO.AgentConfig.Results.AgentConfig>(j => ReferenceEquals(postJson, j)),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
