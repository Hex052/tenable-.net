using System;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Tenable.Base.JsonConverters {
	public class UnixMillisecondConverter : JsonConverter<DateTime> {
		public override DateTime Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			if (reader.TryGetInt64(out long unixTime)) {
				return UnixSecondConverter.UNIX_EPOCH.AddMilliseconds(unixTime);
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, DateTime value,
				JsonSerializerOptions options) {
			double unixTime = Math.Floor(value.Subtract(UnixSecondConverter.UNIX_EPOCH).TotalMilliseconds);
			writer.WriteNumberValue(Convert.ToInt64(unixTime));
		}
	}
}
