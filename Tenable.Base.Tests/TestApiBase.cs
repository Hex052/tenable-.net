using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using RestSharp;
using RichardSzalay.MockHttp;
using Tenable.Base.Exceptions;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.Base.Tests {
	public class TestApiBase {
		private readonly ITestOutputHelper output;
		public TestApiBase(ITestOutputHelper output) {
			this.output = output;
		}

		private const string BASE_URL = "https://example.com/";
		public class ApiBaseConstructor : ApiBase {
			public ApiBaseConstructor(string url) : base(url) {
			}
			public ApiBaseConstructor(Uri url) : base(url) {
			}
			public ApiBaseConstructor(RestClientOptions options) : base(options) {
			}

			public Task Sleep(uint milliseconds, CancellationToken cancellationToken = default) {
				return this.SleepAsync(milliseconds, cancellationToken);
			}
		}

		[Fact]
		public void TestConstructString() {
			// Mock<ApiBase> required due to private constructor
			var api = new Mock<ApiBase>(BASE_URL);
			_testConstructVerify(api.Object, new Uri(BASE_URL));
			Assert.Throws<ArgumentNullException>(() => new ApiBaseConstructor(default(string)!));
		}
		[Fact]
		public void TestConstructUri() {
			// Construct ApiBase with a Uri and not a string
			var uri = new Uri(BASE_URL);
			var api = new Mock<ApiBase>(uri);
			_testConstructVerify(api.Object, uri);
			Assert.Throws<ArgumentNullException>(() => new ApiBaseConstructor(default(Uri)!));
		}
		[Fact]
		public void TestConstructOptions() {
			// Construct ApiBase with RestClientOptions and not a string
			var uri = new Uri(BASE_URL);
			RestClientOptions options = new RestClientOptions() { BaseUrl = uri };
			var api = new Mock<ApiBase>(options);
			_testConstructVerify(api.Object, uri);
			Assert.Throws<ArgumentNullException>(() => new ApiBaseConstructor(default(RestClientOptions)!));
		}
		private static void _testConstructVerify(ApiBase api, Uri baseUri) {
			Assert.Equal(baseUri, api.RestClient.BuildUri(new RestRequest("/")));
			Assert.Empty(
				api.RestClient.DefaultParameters
				.Where(p => p.Type == ParameterType.HttpHeader && p.Name != "Accept"));
		}

		[Theory]
		[InlineData("asdf", Method.Get)]
		[InlineData("v1/test/whitelist", Method.Delete)]
		public async Task TestRequestJson(string partialUrl, Method method) {
			// Setup
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.Respond("text/json", "{}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			using var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			var result = await api.RequestJsonAsync<EmptyJsonObject>(partialUrl, method, tokenSource.Token);
			Assert.NotNull(result);
			Assert.Null(result!.ExtensionData);
			var stringResult = await api.RequestAsync(partialUrl, method, tokenSource.Token);
			Assert.NotNull(stringResult);
			Assert.Equal("{}", stringResult);

			// Verify
			Assert.Equal(2, mockHttp.GetMatchCount(call));
		}
		[Theory]
		[InlineData("asdf", Method.Get, "{}", "\"{}\"")]
		[InlineData("v1/test/whitelist", Method.Delete, 156, "156")]
		public async Task TestRequestJson_body(string partialUrl, Method method,
				object? jsonBody, string? exptectedBody) {
			// Setup
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.WithContent(exptectedBody)
				.Respond("text/json", "{}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			using var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			var result = await api.RequestJsonAsync<EmptyJsonObject>(partialUrl, method, jsonBody, tokenSource.Token);
			Assert.NotNull(result);
			Assert.Null(result!.ExtensionData);
			var stringResult = await api.RequestAsync(partialUrl, method, jsonBody, tokenSource.Token);
			Assert.NotNull(stringResult);
			Assert.Equal("{}", stringResult);

			// Verify
			Assert.Equal(2, mockHttp.GetMatchCount(call));
			mockApi.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("oranges/for/dinner", Method.Get, "[{\"Item1\":\"1\",\"Item2\":\"2\"},{\"Item1\":\"a\",\"Item2\":\"b\"},{\"Item1\":\"a\",\"Item2\":\"c\"}]")]
		[InlineData("writing/foods/makes/you/hungry", Method.Delete, "[]")]
		public async Task TestRequestJson_parameters(string partialUrl,
				Method method, string parameters) {
			// Setup
			var queryParameters = JsonSerializer.Deserialize<IReadOnlyList<Tuple<string, string>>>(parameters)!
				.Select(f => KeyValuePair.Create(f.Item1, f.Item2))
				.ToList();
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.WithQueryString(queryParameters)
				.Respond("text/json", "{}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			using var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			var result = await api.RequestJsonAsync<EmptyJsonObject>(partialUrl, method, queryParameters, tokenSource.Token);
			Assert.NotNull(result);
			Assert.Null(result!.ExtensionData);
			var stringResult = await api.RequestAsync(partialUrl, method, queryParameters, tokenSource.Token);
			Assert.NotNull(stringResult);
			Assert.Equal("{}", stringResult);

			// Verify
			Assert.Equal(2, mockHttp.GetMatchCount(call));
			mockApi.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("path/to/endpoint", Method.Get, "[{\"Item1\":\"1\",\"Item2\":\"2\"},{\"Item1\":\"a\",\"Item2\":\"b\"},{\"Item1\":\"a\",\"Item2\":\"c\"}]", false, "false")]
		[InlineData("trial", Method.Delete, "[]", 85.8, "85.8")]
		public async Task TestRequestJson_parameters_body(string partialUrl,
				Method method, string parameters, object? jsonBody, string? exptectedBody) {
			// Setup
			var queryParameters = JsonSerializer.Deserialize<IReadOnlyList<Tuple<string, string>>>(parameters)!
				.Select(f => KeyValuePair.Create(f.Item1, f.Item2))
				.ToList();
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.WithQueryString(queryParameters)
				.WithContent(exptectedBody)
				.Respond("text/json", "{}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			using var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			var result = await api.RequestJsonAsync<EmptyJsonObject>(partialUrl, method, queryParameters: queryParameters, jsonBody: jsonBody, cancellationToken: tokenSource.Token);
			Assert.NotNull(result);
			Assert.Null(result!.ExtensionData);
			var stringResult = await api.RequestAsync(partialUrl, method, queryParameters: queryParameters, jsonBody: jsonBody, cancellationToken: tokenSource.Token);
			Assert.NotNull(stringResult);
			Assert.Equal("{}", stringResult);

			// Verify
			Assert.Equal(2, mockHttp.GetMatchCount(call));
			mockApi.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("path/to/endpoint", Method.Get, "[{\"Item1\":\"1\",\"Item2\":\"2\"},{\"Item1\":\"a\",\"Item2\":\"b\"},{\"Item1\":\"a\",\"Item2\":\"c\"}]", false, "false", 5)]
		[InlineData("trial", Method.Delete, "[]", 85, "85", 4)]
		public async Task TestRequestJson_parameters_body_limit(
				string partialUrl, Method method, string parameters,
				object? jsonBody, string? exptectedBody, int retryLimit) {
			// Setup
			var queryParameters = JsonSerializer.Deserialize<IReadOnlyList<Tuple<string, string>>>(parameters)!
				.Select(f => KeyValuePair.Create(f.Item1, f.Item2))
				.ToList();
			var mockHttp = new MockHttpMessageHandler();
			int callCount = 0;
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.WithQueryString(queryParameters)
				.WithContent(exptectedBody)
				.Respond(_ => {
					++callCount;
					if (callCount == retryLimit) {
						return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage() { Content = JsonContent.Create<object>(new object()) });
					}
					var retryAfterResponse = new HttpResponseMessage() { StatusCode = HttpStatusCode.TooManyRequests };
					retryAfterResponse.Headers.Add("Retry-After", "2");
					return Task.FromResult<HttpResponseMessage>(retryAfterResponse);
				});
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			mockApi.Protected()
				.Setup<Task>("SleepAsync", ItExpr.IsAny<uint>(), ItExpr.IsAny<CancellationToken>())
				.Returns(Task.CompletedTask);
			var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			var result = await api.RequestJsonAsync<EmptyJsonObject>(partialUrl, method, queryParameters: queryParameters, jsonBody: jsonBody, retryLimit: retryLimit, cancellationToken: tokenSource.Token);
			Assert.NotNull(result);
			Assert.Null(result!.ExtensionData);

			// Verify
			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Exactly(retryLimit - 1),
				ItExpr.Is<uint>(i => i == 2_000),
				ItExpr.Is<CancellationToken>(c => c == tokenSource.Token));
			mockApi.VerifyNoOtherCalls();
			Assert.Equal(retryLimit, mockHttp.GetMatchCount(call));
		}
		[Theory]
		[InlineData("path/to/endpoint", Method.Get, "[{\"Item1\":\"1\",\"Item2\":\"2\"},{\"Item1\":\"a\",\"Item2\":\"b\"},{\"Item1\":\"a\",\"Item2\":\"c\"}]", true, "true", 12)]
		// [InlineData("trial", Method.Delete, "[]", null, null, 2)] // Doesn't work because of https://github.com/richardszalay/mockhttp/pull/95
		[InlineData("trial", Method.Delete, "[]", "", "\"\"", 2)]
		public async Task TestRequest_parameters_body_limit(
				string partialUrl, Method method, string parameters,
				object? jsonBody, string? exptectedBody, int retryLimit) {
			// Setup
			var queryParameters = JsonSerializer.Deserialize<IReadOnlyList<Tuple<string, string>>>(parameters)!
				.Select(f => KeyValuePair.Create(f.Item1, f.Item2))
				.ToList();
			var mockHttp = new MockHttpMessageHandler();
			int callCount = 0;
			var call = mockHttp.When(new HttpMethod(method.ToString()), string.Concat(BASE_URL, partialUrl))
				.WithQueryString(queryParameters)
				.WithContent(exptectedBody)
				.Respond(_ => {
					++callCount;
					if (callCount == retryLimit) {
						return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage() { Content = new ByteArrayContent(new byte[] { 97, 115, 100, 102 }) });
					}
					var retryAfterResponse = new HttpResponseMessage() { StatusCode = HttpStatusCode.TooManyRequests };
					retryAfterResponse.Headers.Add("Retry-After", "12");
					return Task.FromResult<HttpResponseMessage>(retryAfterResponse);
				});
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp };
			var mockApi = new Mock<ApiBase>(options);
			mockApi.Protected()
				.Setup<Task>("SleepAsync", ItExpr.IsAny<uint>(), ItExpr.IsAny<CancellationToken>())
				.Returns(Task.CompletedTask);
			using var api = mockApi.Object;

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2_000); // If it hasn't finished in that time, it isn't going to.
			string? result = await api.RequestAsync(partialUrl, method, queryParameters: queryParameters, jsonBody: jsonBody, retryLimit: retryLimit, cancellationToken: tokenSource.Token);
			Assert.Equal("asdf", result);

			// Verify
			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Exactly(retryLimit - 1),
				ItExpr.Is<uint>(i => i == 12000),
				ItExpr.Is<CancellationToken>(c => c == tokenSource.Token));
			mockApi.VerifyNoOtherCalls();
			Assert.Equal(retryLimit, mockHttp.GetMatchCount(call));
		}


		[Fact]
		public void TestCreateRequest() {
			var partialUrl = "scanners/4";
			var mockHttp = new MockHttpMessageHandler();
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			using var api = mockApi.Object;
			var request = api.CreateRequest(partialUrl, Method.Get, null, null);
			Assert.Equal(partialUrl, request.Resource);
			Assert.Equal(Method.Get, request.Method);
			Assert.Empty(request.Parameters);

			// With query parameters
			var parameters = new List<KeyValuePair<string, string>>() {
				new KeyValuePair<string, string>("foo", "bar"),
				new KeyValuePair<string, string>("foo", "bars"),
				new KeyValuePair<string, string>("foo", "biff"),
				new KeyValuePair<string, string>("data", "5"),
			};
			request = api.CreateRequest(partialUrl, Method.Delete, parameters, null);
			Assert.Equal(partialUrl, request.Resource);
			Assert.Equal(Method.Delete, request.Method);
			Assert.True(
				request.Parameters
				.Where(p => p.Type == ParameterType.QueryString)
				.Select(p => new KeyValuePair<string, string>(p.Name!, (p.Value as string)!))
				.SequenceEqual(parameters));

			// With JSON body
			var body = parameters;
			request = api.CreateRequest(partialUrl, Method.Post, null, body);
			Assert.Equal(partialUrl, request.Resource);
			Assert.Equal(Method.Post, request.Method);
			Assert.Single(request.Parameters);
			var param = request.Parameters.First();
			Assert.Equal(ParameterType.RequestBody, param.Type);
			Assert.Same(body, param.Value);

			mockApi.VerifyNoOtherCalls();
		}
		[Fact]
		public void TestCreateRequestThrow() {
			var mockHttp = new MockHttpMessageHandler();
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			using var api = mockApi.Object;
			Assert.Throws<ArgumentNullException>(() => {
				return api.CreateRequest(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>(), new object());
			});
		}

		[Fact]
		public async Task TestRequestThrowUrlArgumentNull() {
			var mockHttp = new MockHttpMessageHandler();
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			using var api = mockApi.Object;
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestJsonAsync<EmptyJsonObject>(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>(), new object(), 4);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestAsync(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>(), new object(), 4);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestJsonAsync<EmptyJsonObject>(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>(), new object());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestAsync(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>(), new object());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestJsonAsync<EmptyJsonObject>(null!, Method.Get, new object());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestAsync(null!, Method.Get, new object());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestJsonAsync<EmptyJsonObject>(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestAsync(null!, Method.Get, Array.Empty<KeyValuePair<string, string>>());
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestJsonAsync<EmptyJsonObject>(null!, Method.Get);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RequestAsync(null!, Method.Get);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RetryRequestJsonAsync<EmptyJsonObject>(null!, 7);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RetryRequestAsync(null!, 7);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RetryRequestJsonAsync<EmptyJsonObject>(null!);
			});
			await Assert.ThrowsAsync<ArgumentNullException>(() => {
				return api.RetryRequestAsync(null!);
			});
		}

		[Theory]
		[InlineData(HttpStatusCode.OK, null, false, 0)]
		[InlineData(HttpStatusCode.BadRequest, typeof(BadRequestException), true, 0)]
		[InlineData(HttpStatusCode.Forbidden, typeof(NoPermissionException), true, 0)]
		[InlineData(HttpStatusCode.InternalServerError, typeof(RetryException), false, 20)]
		[InlineData(HttpStatusCode.NotFound, typeof(NotFoundException), true, 0)]
		[InlineData(HttpStatusCode.MethodNotAllowed, typeof(HttpStatusException), true, 0)]
		[InlineData(HttpStatusCode.NoContent, null, false, 0)]
		[InlineData(HttpStatusCode.Gone, typeof(HttpStatusException), true, 0)]
		[InlineData(HttpStatusCode.Unauthorized, typeof(InvalidCredentialsException), true, 0)]
		[InlineData(HttpStatusCode.Conflict, typeof(ConflictException), true, 0)]
		[InlineData(HttpStatusCode.BadGateway, typeof(RetryException), false, 20)]
		[InlineData(HttpStatusCode.ServiceUnavailable, typeof(RetryException), false, 20)]
		[InlineData((HttpStatusCode)599 /* There is no status code with this number */, typeof(RetryException), false, 20)]
		public async Task TestThrowRetryException(HttpStatusCode statusCode,
				Type? exceptionType, bool thrownQuick, uint expectedRetryAfterSec) {
			var partialUrl = "throw/retry";
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Get, string.Concat(BASE_URL, partialUrl))
				.Respond(_ => {
					var response = new HttpResponseMessage();
					response.StatusCode = statusCode;
					response.Content = new ByteArrayContent(Array.Empty<byte>());
					return response;
				});
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			mockApi.Protected()
				.Setup<Task>("SleepAsync", ItExpr.IsAny<uint>(), ItExpr.IsAny<CancellationToken>())
				.Returns(Task.CompletedTask);
			using var api = mockApi.Object;

			if (exceptionType is null) {
				Assert.True(string.IsNullOrEmpty(await api.RequestAsync(partialUrl, Method.Get)));
				Assert.Equal(1, mockHttp.GetMatchCount(call));
			}
			else {
				HttpStatusException exception;
				if (thrownQuick) {
					exception = Assert.IsAssignableFrom<HttpStatusException>(
						await Assert.ThrowsAsync(
							exceptionType,
							() => api.RequestAsync(partialUrl, Method.Get)));
					Assert.Equal(1, mockHttp.GetMatchCount(call));
				}
				else {
					var source = new CancellationTokenSource();
					exception = Assert.IsAssignableFrom<HttpStatusException>(
						await Assert.ThrowsAsync(
							exceptionType,
							() => api.RequestAsync(partialUrl, Method.Get, null, null, 2, source.Token)));
					mockApi.Protected().Verify<Task>(
						"SleepAsync",
						Times.Once(),
						ItExpr.Is<uint>(i => i == expectedRetryAfterSec * 1000),
						ItExpr.Is<CancellationToken>(c => c == source.Token));
					Assert.Equal(2, mockHttp.GetMatchCount(call));
				}
				Assert.IsType(exceptionType, exception);
				Assert.Equal(statusCode, exception.StatusCode);
				if (exception is RetryException retryException) {
					Assert.Equal(expectedRetryAfterSec, retryException.RetryAfter);
				}
			}
			mockApi.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(null, 20)]
		[InlineData("null", 20)]
		[InlineData("", 20)]
		[InlineData("0", 0)]
		[InlineData("-1", 20)]
		[InlineData("53", 53)]
		[InlineData("17", 17)]
		public async Task TestRateLimitException(
				string? headerValue, uint expectedRetryAfterSec) {
			var partialUrl = "intern/4";
			var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Get, string.Concat(BASE_URL, partialUrl))
				.Respond(_ => {
					var response = new HttpResponseMessage();
					response.StatusCode = HttpStatusCode.TooManyRequests;
					response.Headers.Add("Server", "Apache");
					if (headerValue is not null) {
						Assert.True(response.Headers.TryAddWithoutValidation("retry-after", headerValue));
					}
					response.Content = new ByteArrayContent(Array.Empty<byte>());
					return response;
				});
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			mockApi.Protected()
				.Setup<Task>("SleepAsync", ItExpr.IsAny<uint>(), ItExpr.IsAny<CancellationToken>())
				.Returns(Task.CompletedTask);
			using var api = mockApi.Object;

			// Test without waiting
			var exc = await Assert.ThrowsAsync<RateLimitException>(
				() => api.RequestAsync(partialUrl, Method.Get, null, null, 1));
			// verify
			Assert.Equal(expectedRetryAfterSec, exc.RetryAfter);
			Assert.Equal(1, mockHttp.GetMatchCount(call));
			mockApi.VerifyNoOtherCalls();

			// Test with waiting
			exc = await Assert.ThrowsAsync<RateLimitException>(
				() => api.RequestAsync(partialUrl, Method.Get, null, null, 2));
			// verify
			Assert.Equal(expectedRetryAfterSec, exc.RetryAfter);
			Assert.Equal(3, mockHttp.GetMatchCount(call));
			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Once(),
				ItExpr.Is<uint>(i => i == expectedRetryAfterSec * 1000),
				ItExpr.Is<CancellationToken>(c => c == default));
			mockApi.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestSleep() {
			var mockHttp = new MockHttpMessageHandler();
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBaseConstructor>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			mockApi.Protected()
				.Setup<Task>("SleepAsync", ItExpr.IsAny<uint>(), ItExpr.IsAny<CancellationToken>())
				.CallBase();
			using var api = mockApi.Object;

			var tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(2000);
			Assert.Equal(Task.CompletedTask, api.Sleep(0, tokenSource.Token));
			var before = DateTime.UtcNow;
			await api.Sleep(50, tokenSource.Token);
			Assert.True(DateTime.UtcNow.Subtract(before).TotalMilliseconds > 45);
			Assert.True(DateTime.UtcNow.Subtract(before).TotalMilliseconds < 500);
			var cancelledTokenSource = new CancellationTokenSource();
			cancelledTokenSource.Cancel();
			before = DateTime.UtcNow;
			await Assert.ThrowsAnyAsync<OperationCanceledException>(() => api.Sleep(50, cancelledTokenSource.Token));
			Assert.True(DateTime.UtcNow.Subtract(before).TotalMilliseconds < 50);


			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Once(),
				ItExpr.Is<uint>(i => i == 0),
				ItExpr.Is<CancellationToken>(c => c == tokenSource.Token));
			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Once(),
				ItExpr.Is<uint>(i => i == 50),
				ItExpr.Is<CancellationToken>(c => c == tokenSource.Token));
			mockApi.Protected().Verify<Task>(
				"SleepAsync",
				Times.Once(),
				ItExpr.Is<uint>(i => i == 50),
				ItExpr.Is<CancellationToken>(c => c == cancelledTokenSource.Token));
			mockApi.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestDispose() {
			var mockMockHttp = new Mock<MockHttpMessageHandler>(() => new MockHttpMessageHandler(default));
			mockMockHttp.CallBase = true;
			MockHttpMessageHandler mockHttp = mockMockHttp.Object;
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var mockApi = new Mock<ApiBase>(
				new RestClientOptions(BASE_URL) { ConfigureMessageHandler = _ => mockHttp });
			mockApi.CallBase = true;
			var api = mockApi.Object;

			for (int i = 1; i < 3; i++) {
				// Test disposal is idempotent
				api.Dispose();
				mockApi.Protected().Verify("Dispose", Times.Exactly(i), ItExpr.Is<bool>(b => b));
				mockMockHttp.Protected().Verify("Dispose", Times.AtMostOnce(), ItExpr.IsAny<bool>());

				using CancellationTokenSource tokenSource = new CancellationTokenSource();
				tokenSource.CancelAfter(millisecondsDelay: 250); // If it hasn't finished in that time, it isn't going to.
				await Assert.ThrowsAnyAsync<ObjectDisposedException>(() => api.RequestAsync("asdf", Method.Get, tokenSource.Token));
			}

			mockMockHttp.VerifyNoOtherCalls();
			mockApi.VerifyNoOtherCalls();
		}
	}
}
