using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.AgentConfig.Results {
	public class AgentConfig {
		/// <summary>
		/// If true, software updates are only affected by agent exclusions.
		/// If false, software updates are disabled for all agents, regardless
		/// of any exclusions that are set.
		/// </summary>
		[JsonPropertyName("software_update")]
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public bool? SoftwareUpdate {
			get; set;
		}

		/// <summary>Agent auto unlink settings</summary>
		[JsonPropertyName("auto_unlink")]
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public AutoUnlink? AutoUnlink {
			get; set;
		}

		public AgentConfig() {
		}
		/// <summary>
		/// This is really only for System.Text.Json! If you're creating
		/// objects to call <see cref="AgentConfigApi.Set(AgentConfig, int)"/>,
		/// you should probably call the other constructor
		/// <see cref="AgentConfig.AgentConfig()"/>
		/// </summary>
		[JsonConstructor]
		public AgentConfig(bool? softwareUpdate, AutoUnlink? autoUnlink) {
			if (!softwareUpdate.HasValue) {
				throw new ArgumentNullException(nameof(softwareUpdate));
			}
			if (ReferenceEquals(autoUnlink, null)) {
				throw new ArgumentNullException(nameof(autoUnlink));
			}
			this.SoftwareUpdate = softwareUpdate;
			this.AutoUnlink = autoUnlink;
		}
	}
}
