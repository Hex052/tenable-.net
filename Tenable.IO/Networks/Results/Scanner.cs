using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Networks.Results {
	public class Scanner {
		/// <summary>
		/// The date on which the scanner was linked to Tenable
		/// </summary>
		[JsonPropertyName("creation_date")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime Scanners {
			get; set;
		}

		/// <summary>The scanner software distribution</summary>
		[JsonPropertyName("distro")]
		public string? Distro {
			get; set;
		}
		/// <summary>The scanner platform</summary>
		[JsonPropertyName("platform")]
		public string? Platform {
			get; set;
		}

		/// <summary>The build of the engine running on the scanner</summary>
		[JsonPropertyName("engine_build")]
		public string? EngineBuild {
			get; set;
		}
		/// <summary>The version of the engine running on the scanner</summary>
		[JsonPropertyName("engine_version")]
		public string? EngineVersion {
			get; set;
		}
		/// <summary>The current loaded set of plugins</summary>
		[JsonPropertyName("loaded_plugin_set")]
		public string? LoadedPluginSet {
			get; set;
		}

		/// <summary>Key used when linking the scanner to Tenable</summary>
		[JsonPropertyName("key")]
		public string Key {
			get; set;
		}

		/// <summary>When the scanner last connected to Tenable</summary>
		[JsonPropertyName("last_connect")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime? LastConnect {
			get; set;
		}
		/// <summary>When the scanner was last updated</summary>
		[JsonPropertyName("last_modification_date")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime LastModificationDate {
			get; set;
		}
		/// <summary>The frequency at which the scanner polls Tenable</summary>
		[JsonPropertyName("report_frequency")]
		[JsonConverter(typeof(TimeSpanSecondConverter))]
		public TimeSpan? ReportFrequency {
			get; set;
		}

		/// <summary>Whether the scanner is enabled or not</summary>
		[JsonPropertyName("linked")]
		public bool Linked {
			get; set;
		}
		/// <summary>Whether the scanner is part of a scanner group</summary>
		[JsonPropertyName("pool")]
		public bool Pool {
			get; set;
		}
		/// <summary>Whether or not this scanner is actually a scanner group</summary>
		[JsonPropertyName("group")]
		public bool Group {
			get; set;
		}
		/// <summary>The name of the scanner</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}
		/// <summary>ID of this scanner</summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}
		/// <summary>Type of scanner</summary>
		/// <remarks>Possible values: <c>"local"</c> and <c>"remote"</c></remarks>
		[JsonPropertyName("type")]
		public string Type {
			get; set;
		}
		/// <summary>UUID of the scanner</summary>
		[JsonPropertyName("uuid")]
		public string Uuid {
			get; set;
		}
		/// <summary>UUID of the Nessus install on the scanner</summary>
		[JsonPropertyName("remote_uuid")]
		public string RemoteUuid {
			get; set;
		}
		/// <summary>Whether the scanner supports remote logging</summary>
		[JsonPropertyName("supports_remote_logs")]
		public bool SupportsRemoteLogs {
			get; set;
		}

		/// <summary>
		/// The number of hosts that the scanner has discovered
		/// </summary>
		[JsonPropertyName("num_hosts")]
		public int? NumHosts {
			get; set;
		}
		/// <summary>
		/// The number of scans that the scanner is currently running
		/// </summary>
		/// <remarks>
		/// It is unclear how this is different from <see cref="ScanCount"/>
		/// </remarks>
		[JsonPropertyName("num_scans")]
		public int NumScans {
			get; set;
		}
		/// <summary>
		/// The number of active sessions between the scanner and hosts
		/// </summary>
		[JsonPropertyName("num_sessions")]
		public int? NumSessions {
			get; set;
		}
		/// <summary>
		/// The number of active TCP sessions between the scanner and hosts
		/// </summary>
		[JsonPropertyName("num_tcp_sessions")]
		public int? NumTcpSessions {
			get; set;
		}
		/// <summary>
		/// The current number of scans currently running on the scanner
		/// </summary>
		/// <remarks>
		/// It is unclear how this is different from <see cref="NumScans"/>
		/// </remarks>
		[JsonPropertyName("scan_count")]
		public int ScanCount {
			get; set;
		}

		/// <summary>The scanner owner</summary>
		[JsonPropertyName("owner")]
		public string Owner {
			get; set;
		}
		/// <summary>ID of the scanner owner</summary>
		[JsonPropertyName("owner_id")]
		public int OwnerId {
			get; set;
		}
		/// <summary>ID of the scanner owner</summary>
		[JsonPropertyName("owner_name")]
		public string OwnerName {
			get; set;
		}
		/// <summary>UUID of the scanner owner</summary>
		[JsonPropertyName("owner_Uuid")]
		public string OwnerUuid {
			get; set;
		}

		/// <summary>Historical. Always <c>"service"</c></summary>
		[Obsolete("Always \"service\"")]
		[JsonPropertyName("source")]
		public string Source {
			get; set;
		} = "service";
		/// <summary>See <see cref="LastModificationDate"/></summary>
		[Obsolete("Equivalent to " + nameof(LastModificationDate))]
		[JsonPropertyName("timestamp")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime Timestamp {
			get; set;
		}

		/// <summary>Current status of the scanner</summary>
		/// <remarks>Possible values: <c>"on"</c> and <c>"off"</c></remarks>
		[JsonPropertyName("status")]
		public string Status {
			get; set;
		}



		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Scanner(DateTime scanners, string key,
				DateTime lastModificationDate, bool linked, bool pool,
				bool group, string name, int id, string type, string uuid,
				string remoteUuid, bool supportsRemoteLogs, int numScans,
				int scanCount, string owner, int ownerId, string ownerName,
				string ownerUuid,
				string status) {
			this.Scanners = scanners;
			this.Key = key;
			this.LastModificationDate = lastModificationDate;
			this.Linked = linked;
			this.Pool = pool;
			this.Group = group;
			this.Name = name;
			this.Id = id;
			this.Type = type;
			this.Uuid = uuid;
			this.RemoteUuid = remoteUuid;
			this.SupportsRemoteLogs = supportsRemoteLogs;
			this.NumScans = numScans;
			this.ScanCount = scanCount;
			this.Owner = owner;
			this.OwnerId = ownerId;
			this.OwnerName = ownerName;
			this.OwnerUuid = ownerUuid;
			this.Status = status;
		}
	}
}
