using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.IO.AgentConfig;
using Tenable.IO.Agents;
using Tenable.IO.Exports;
using Tenable.IO.Exports.Result.Assets;
using Tenable.IO.Networks;
using Tenable.IO.Plugins;
using Tenable.IO.Server;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestPageEnumerable {
		private sealed class EnumerableTested : ExportEnumerable<Asset, AssetExportStatus> {
			public EnumerableTested(ITenableIO api, Guid uuid) : base(api, uuid) {
			}

			public override IAsyncEnumerator<Asset> GetAsyncEnumerator(CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public override IEnumerator<Asset> GetEnumerator() => throw new NotImplementedException();

			public override string RelativePageUri(int pageId) => string.Format("asdf/1234/{0}", pageId);

			public override string RelativeCancelUri() => throw new NotImplementedException();
			public override string RelativeStatusUri() => throw new NotImplementedException();

			/// <summary>
			/// As if <see cref="GC.Collect"/> were called and now all the
			/// weakrefs are invalid
			/// </summary>
			public void Collect() {
				foreach (var item in this.PageCache.Values) {
					item.SetTarget(null!);
				}
			}

			public override ValueTask<IReadOnlyList<Asset>> GetPageAsync(int pageId, CancellationToken cancellationToken = default) {
				return base.GetPageAsync(pageId, cancellationToken);
			}
		}

		private sealed class MockBaseApi : ITenableIO {
			private string partialUrl;
			private Method method;
			private CancellationToken cancellationToken;
			private int callCount = 0;
			public int CallCount => this.callCount;

			public int DefaultRetryLimit => throw new NotImplementedException();
			public RestClient RestClient => throw new NotImplementedException();
			public string AccessKey => throw new NotImplementedException();
			public string SecretKey => throw new NotImplementedException();
			public string HeaderValue => throw new NotImplementedException();
			public object AccessGroups => throw new NotImplementedException();
			public IAgentConfigApi AgentConfig => throw new NotImplementedException();
			public object AgentGroups => throw new NotImplementedException();
			public object AgentExclusions => throw new NotImplementedException();
			public IAgentsApi Agents => throw new NotImplementedException();
			public object Assets => throw new NotImplementedException();
			public object AuditLog => throw new NotImplementedException();
			public object Credentials => throw new NotImplementedException();
			public object Editor => throw new NotImplementedException();
			public object Exclusions => throw new NotImplementedException();
			public IExportsApi Exports => throw new NotImplementedException();
			public object Files => throw new NotImplementedException();
			public object Filters => throw new NotImplementedException();
			public object Folders => throw new NotImplementedException();
			public object Groups => throw new NotImplementedException();
			public INetworksApi Networks => throw new NotImplementedException();
			public object Permissions => throw new NotImplementedException();
			public IPluginsApi Plugins => throw new NotImplementedException();
			public object Policies => throw new NotImplementedException();
			public object ScannerGroups => throw new NotImplementedException();
			public object Scanners => throw new NotImplementedException();
			public object Scans => throw new NotImplementedException();
			public IServerApi Server => throw new NotImplementedException();
			public object Session => throw new NotImplementedException();
			public object Tags => throw new NotImplementedException();
			public object TargetGroups => throw new NotImplementedException();
			public object Users => throw new NotImplementedException();
			public object Workbenches => throw new NotImplementedException();

			public MockBaseApi(string partialUrl, Method method, CancellationToken cancellationToken) {
				if (string.IsNullOrWhiteSpace(partialUrl)) {
					throw new ArgumentException($"'{nameof(partialUrl)}' cannot be null or whitespace.", nameof(partialUrl));
				}

				this.partialUrl = partialUrl;
				this.method = method;
				this.cancellationToken = cancellationToken;
			}

			public RestRequest CreateRequest(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody) => throw new NotImplementedException();
			public void Dispose() => GC.SuppressFinalize(this);
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<string?> RequestAsync(string partialUrl, Method method, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, CancellationToken cancellationToken = default) {
				++this.callCount;
				Assert.Equal(this.partialUrl, partialUrl);
				Assert.Equal(this.method, method);
				Assert.Equal(this.cancellationToken, cancellationToken);

				return Task.FromResult<T?>(Assert.IsAssignableFrom<T>(new List<Asset>()));
			}
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, object? jsonBody, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<T?> RequestJsonAsync<T>(string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse> RetryRequestAsync(RestRequest request, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse> RetryRequestAsync(RestRequest request, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request, int retryLimit, CancellationToken cancellationToken = default) => throw new NotImplementedException();
			public Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request, CancellationToken cancellationToken = default) => throw new NotImplementedException();
		}

		[Fact]
		public async Task TestGetFullPageAsyncGcCollected() {
			using var tokenSource = new CancellationTokenSource();
			var api = new MockBaseApi("asdf/1234/24", Method.Get, tokenSource.Token);
			EnumerableTested enumerable = new EnumerableTested(api, Guid.Empty);

			IReadOnlyList<Asset>? result = await enumerable.GetPageAsync(24, tokenSource.Token);
			Assert.Equal(1, api.CallCount);
			Assert.Same(result, await enumerable.GetPageAsync(24, tokenSource.Token));
			Assert.Equal(1, api.CallCount);
			enumerable.Collect();// As if GC.Collect(2, GCCollectionMode.Forced, true, false);
			result = await enumerable.GetPageAsync(24, tokenSource.Token);
			Assert.Equal(2, api.CallCount);
			Assert.Same(result, await enumerable.GetPageAsync(24, tokenSource.Token));
			Assert.Equal(2, api.CallCount);
		}
	}
}
