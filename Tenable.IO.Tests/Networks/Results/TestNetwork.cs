using System;
using System.Collections.Generic;
using System.Text.Json;
using Tenable.IO.Networks;
using Tenable.IO.Networks.Results;
using Xunit;

namespace Tenable.IO.Tests.Networks.Results {
	public class TestNetwork {
		[Fact]
		public void TestEquality() {
			var first = new Network(
				ownerUuid: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				created: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
				modified: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
				uuid: Guid.Empty,
				name: "Default",
				isDefault: true,
				createdBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				modifiedBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				createdInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc),
				modifiedInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc)) {
				ScannerCount = 10,
			};
			var second = new Network(
				ownerUuid: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				created: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
				modified: new DateTime(2018, 12, 13, 23, 34, 45, 270, DateTimeKind.Utc),
				uuid: Guid.Empty,
				name: "Default",
				isDefault: true,
				createdBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				modifiedBy: new Guid("ddbd3e11-3311-4682-9912-8e81805fd8a9"),
				createdInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc),
				modifiedInSeconds: new DateTime(2018, 12, 13, 23, 34, 45, DateTimeKind.Utc)) {
				ScannerCount = 10,
				ExtensionData = new Dictionary<string, JsonElement>(),
			};
			var different = new Network(
				ownerUuid: new Guid("0e67b283-07a4-464c-a5e4-7b42576962fd"),
				created: new DateTime(2019, 5, 10, 22, 20, 2, 270, DateTimeKind.Utc),
				modified: new DateTime(2019, 5, 10, 22, 20, 2, 270, DateTimeKind.Utc),
				uuid: new Guid("42475f11-5e6b-4d6a-a53d-63fe494961df"),
				name: "Headquarters",
				isDefault: false,
				createdBy: new Guid("0e67b283-07a4-464c-a5e4-7b42576962fd"),
				modifiedBy: new Guid("0e67b283-07a4-464c-a5e4-7b42576962fd"),
				createdInSeconds: new DateTime(2019, 5, 10, 22, 20, 2, DateTimeKind.Utc),
				modifiedInSeconds: new DateTime(2019, 5, 10, 22, 20, 2, DateTimeKind.Utc)) {
				ScannerCount = 1,
				AssetsTtlDays = 91,
			};
			var firstCloned = first.CloneSettings();
			var secondCloned = second.CloneSettings();
			Assert.False(first.Equals(default(Network)));
			Assert.False(first!.Equals(default(object)));
			Assert.False(first!.Equals("(object?)null"));
			Assert.True(first!.Equals((object)first));
			Assert.True(firstCloned.Equals((object)firstCloned));
			Assert.True(first!.Equals(first));
			Assert.True(first!.Equals(second));
			Assert.True(second!.Equals(first));
			Assert.True(secondCloned.Equals((object)firstCloned));
			Assert.True(firstCloned.Equals((object)secondCloned));
			Assert.False(firstCloned.Equals(default(object)));
			Assert.False(firstCloned!.Equals("(object?)null"));
			Assert.True(second!.Equals((object)first));
			Assert.True(first!.Equals((object)second));
			Assert.False(first!.Equals(different));
			Assert.False(different!.Equals(first));
			Assert.False(first!.Equals(different));
			Assert.True(first == second);
			Assert.True((NetworkSettings)first == (NetworkSettings)second);
#pragma warning disable CS1718 // Comparison made to same variable
			Assert.True(first == first);
			Assert.True((NetworkSettings)first == (NetworkSettings)first);
#pragma warning restore CS1718 // Comparison made to same variable
			Assert.True(first != default(Network));
			Assert.True(first != default(NetworkSettings));
			Assert.True(default(Network) != first);
			Assert.True(default(NetworkSettings) != first);
			Assert.True(((object)first!).Equals((object?)second));
			Assert.NotEqual(first.GetHashCode(), second.GetHashCode());
			Assert.Equal(different.GetHashCode(), different.GetHashCode());
		}
	}
}
