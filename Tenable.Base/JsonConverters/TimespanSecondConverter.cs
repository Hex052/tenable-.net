using System;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Tenable.Base.JsonConverters {
	public class TimeSpanSecondConverter : JsonConverter<TimeSpan> {
		public override TimeSpan Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			if (reader.TryGetInt64(out long seconds)) {
				return TimeSpan.FromSeconds(seconds);
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options) {
			writer.WriteNumberValue(Convert.ToInt64(Math.Floor(value.TotalSeconds)));
		}
	}
}
