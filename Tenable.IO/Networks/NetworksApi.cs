using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.IO.Networks.Results;

namespace Tenable.IO.Networks {
	public class NetworksApi : INetworksApi {
		public ITenableIO Api {
			get;
		}
		internal NetworksApi(ITenableIO api) {
			this.Api = api;
		}

		public NetworkEnumerable ListNetworks(NetworkFilter filter) {
			return new NetworkEnumerable(this.Api, filter);
		}
		public NetworkEnumerable ListNetworks() {
			return new NetworkEnumerable(this.Api, new NetworkFilter());
		}

		public async Task<Network> GetNetworkDetailsAsync(Guid networkId,
				CancellationToken cancellationToken = default) {
			string partialUrl = string.Format("networks/{0}", networkId);
			var result = await this.Api.RequestJsonAsync<Network>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}

		public async Task<Network> UpdateNetworkAsync(Guid networkId,
				NetworkSettings settings,
				CancellationToken cancellationToken = default) {
			if (settings.GetType() != typeof(NetworkSettings)) {
				settings = settings.CloneSettings();
			}
			string partialUrl = string.Format("networks/{0}", networkId);
			var result = await this.Api.RequestJsonAsync<Network>(
				partialUrl, Method.Put, jsonBody: settings, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}

		public async Task<EmptyJsonObject> DeleteNetworkAsync(Guid networkId,
				CancellationToken cancellationToken = default) {
			return await this.Api.RequestJsonAsync<EmptyJsonObject>(
				string.Format("networks/{0}", networkId),
				Method.Delete,
				cancellationToken)
				?? new EmptyJsonObject();
		}

		public async Task<AssetCount> GetNetworkAssetCountAsync(Guid networkId,
				int numDays, CancellationToken cancellationToken = default) {
			var partialUrl = string.Format("networks/{0}/counts/assets-not-seen-in/{1}", networkId, numDays);
			var result = await this.Api.RequestJsonAsync<AssetCount>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}

		public async Task<EmptyJsonObject> AssignScannerAsync(Guid networkId,
				string scannerUuid, CancellationToken cancellationToken = default) {
			return await this.Api.RequestJsonAsync<EmptyJsonObject>(
				string.Format("networks/{0}/scanners/{1}", networkId, scannerUuid),
				Method.Post,
				cancellationToken)
				?? new EmptyJsonObject();
		}
		public async Task<EmptyJsonObject> AssignScannersAsync(Guid networkId,
				IEnumerable<string> scannerUuids,
				CancellationToken cancellationToken = default) {
			return await this.Api.RequestJsonAsync<EmptyJsonObject>(
				string.Format("networks/{0}/scanners", networkId),
				Method.Post,
				jsonBody: new {
					scanner_uuids = scannerUuids
				},
				cancellationToken)
				?? new EmptyJsonObject();
		}

		public async Task<Scanners> ListScannersAsync(Guid networkId, CancellationToken cancellationToken = default) {
			var partialUrl = string.Format("networks/{0}/scanners", networkId);
			var result = await this.Api.RequestJsonAsync<Scanners>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}
		public async Task<Scanners> ListAssignableScannersAsync(Guid networkId, CancellationToken cancellationToken = default) {
			var partialUrl = string.Format("networks/{0}/assignable-scanners", networkId);
			var result = await this.Api.RequestJsonAsync<Scanners>(
				partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}
	}
}
