using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class VprDriversComparer : EqualityComparer<VprDrivers> {
		private IEqualityComparer<VprDriversDateRange> _dateRangeComparer;
		public IEqualityComparer<VprDriversDateRange> DateRangeComparer {
			get => this._dateRangeComparer;
			set => this._dateRangeComparer = value ?? throw new ArgumentNullException(nameof(value));
		}

		public VprDriversComparer(IEqualityComparer<VprDriversDateRange>? driverComparer = null) {
			this._dateRangeComparer = driverComparer ?? new VprDriversDateRangeComparer();
		}

		[Pure]
		public override bool Equals([AllowNull] VprDrivers x, [AllowNull] VprDrivers y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return this._dateRangeComparer.Equals(x.Age, y.Age)
					&& x.ExploitCodeMaturity == y.ExploitCodeMaturity
					&& x.CvssImpactScorePredicted == y.CvssImpactScorePredicted
					&& x.Cvss3ImpactScore == y.Cvss3ImpactScore
					&& x.ThreatIntensityLast28 == y.ThreatIntensityLast28
					&& this._dateRangeComparer.Equals(x.ThreatRecency, y.ThreatRecency)
					&& (
						ReferenceEquals(x.ThreatSourcesLast28, y.ThreatSourcesLast28)
						|| (
							!ReferenceEquals(x.ThreatSourcesLast28, null)
							&& !ReferenceEquals(y.ThreatSourcesLast28, null)
							&& x.ThreatSourcesLast28.SequenceEqual(y.ThreatSourcesLast28)))
					&& x.ProductCoverage == y.ProductCoverage;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] VprDrivers obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return (obj.Age == null ? 0 : this._dateRangeComparer.GetHashCode(obj.Age))
						+ (obj.ExploitCodeMaturity == null ? 0 : obj.ExploitCodeMaturity.GetHashCode())
						+ obj.CvssImpactScorePredicted.GetHashCode()
						+ obj.Cvss3ImpactScore.GetHashCode()
						+ (obj.ThreatIntensityLast28 == null ? 0 : obj.ThreatIntensityLast28.GetHashCode())
						+ (obj.ThreatRecency == null ? 0 : this._dateRangeComparer.GetHashCode(obj.ThreatRecency))
						+ (obj.ThreatSourcesLast28 == null ? 0 : (int)obj.ThreatSourcesLast28.Select<string, long>(s => s.GetHashCode()).Sum())
						+ (obj.ProductCoverage == null ? 0 : obj.ProductCoverage.GetHashCode());
				}
			}
		}
	}
}
