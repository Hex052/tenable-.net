using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.Base.Tests;
using Tenable.IO.Exports;
using Tenable.IO.Exports.Result;
using Tenable.IO.Exports.Result.Assets;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class ExportEnumeratorOverride<TResult, TStatus> : ExportEnumerator<TResult, TStatus> where TStatus : RequestExportStatus {
		public bool LastPagePublic => base.LastPage;
		public IList<int> ProcessedPublic {
			get => base.Processed;
			set => base.Processed = value;
		}
		public IList<int> IncompletePublic {
			get => base.Incomplete;
			set => base.Incomplete = value;
		}
		public int? PageIdPublic {
			set => base.PageId = value;
		}
		public int PageIndexPublic {
			set => base.PageIndex = value;
		}
		public IReadOnlyList<TResult> CurrentPagePublic {
			set => base.CurrentPage = value;
		}

		public int SleepCount {
			get; private set;
		}
		protected override Task Sleep(int milliseconds) {
			++this.SleepCount;
			return Task.CompletedTask;
		}
		public ExportEnumeratorOverride(ExportEnumerable<TResult, TStatus> enumerable, CancellationToken cancellationToken) : base(enumerable, cancellationToken) {
			this.SleepCount = 0;
		}
		public ValueTask<bool> NextPageAsyncPublic() {
			return base.NextPageAsync();
		}
	}
	public class TestExportEnumerator {
		[Fact]
		public async Task TestNextPageFromStartAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			tio.Setup(t => t.RequestJsonAsync<AssetExportStatus>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AssetExportStatus?>(new AssetExportStatus(Status.FINISHED, new[] { 4, 11 })));
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			var page = new[] {
				new Asset(
					Guid.Empty,
					DateTime.MinValue,
					DateTime.MaxValue,
					DateTime.MinValue,
					DateTime.MaxValue,
					Guid.Empty,
					"default")};
			enumerable.Setup(e => e.GetPageAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
				.Returns(new ValueTask<IReadOnlyList<Asset>>(page));
			enumerable.Setup(e => e.RelativeStatusUri()).Returns("prettyplease");

			// Test
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.True(await enumerator.NextPageAsyncPublic());

			// Validate
			Assert.Equal(0, enumerator.PageIndex);
			Assert.Equal(4, enumerator.PageId);
			Assert.Same(page, enumerator.CurrentPage);
			Assert.Equal(1, enumerator.Count); // Listed as required side effect
			Assert.False(enumerator.LastPagePublic); // Listed as required side effect
			Assert.Empty(enumerator.ProcessedPublic);
			Assert.Equal(new[] { 11 }, enumerator.IncompletePublic);
			enumerable.Verify(
				e => e.GetPageAsync(It.Is<int>(i => i == 4), It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			enumerable.Verify(e => e.RelativeStatusUri(), Times.Once());
			enumerable.VerifyNoOtherCalls();
			tio.Verify(
				t => t.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(s => s.Equals("prettyplease")),
					It.Is<Method>(m => m == Method.Get),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestNextPageMidwayAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			tio.Setup(
					t => t.RequestJsonAsync<AssetExportStatus>(
						It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AssetExportStatus?>(new AssetExportStatus(
					Status.FINISHED,
					new[] { 4, 5, 7, 13 })));
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			var asset = new Asset(
				Guid.Empty,
				DateTime.MinValue,
				DateTime.MaxValue,
				DateTime.MinValue,
				DateTime.MaxValue,
				Guid.Empty,
				"default");
			var page = new[] { asset };
			enumerable.Setup(e => e.GetPageAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
				.Returns(new ValueTask<IReadOnlyList<Asset>>(page));
			enumerable.Setup(e => e.RelativeStatusUri()).Returns("sertup");
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			enumerator.ProcessedPublic = new List<int>() { 4 };
			enumerator.IncompletePublic = new List<int>() { };
			enumerator.PageIdPublic = 7;
			enumerator.Count = 125;

			// Test
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.True(await enumerator.NextPageAsyncPublic());

			// Validate
			Assert.Equal(0, enumerator.PageIndex);
			Assert.Equal(5, enumerator.PageId);
			Assert.Same(page, enumerator.CurrentPage);
			Assert.Same(asset, enumerator.Current);
			Assert.Equal(126, enumerator.Count); // Listed as required side effect
			Assert.False(enumerator.LastPagePublic); // Listed as required side effect
			Assert.Equal(new[] { 4, 7 }, enumerator.ProcessedPublic);
			Assert.Equal(new[] { 13 }, enumerator.IncompletePublic);

			enumerable.Verify(
				e => e.GetPageAsync(
					It.Is<int>(i => i == 5),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			enumerable.Verify(e => e.RelativeStatusUri(), Times.Once());
			enumerable.VerifyNoOtherCalls();
			tio.Verify(
				t => t.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(s => s.Equals("sertup")),
					It.Is<Method>(m => m == Method.Get),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestNextPageHasIncompleteAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			var asset = new Asset(
				Guid.Empty,
				DateTime.MinValue,
				DateTime.MaxValue,
				DateTime.MinValue,
				DateTime.MaxValue,
				Guid.Empty,
				"default");
			var page = new[] { asset };
			enumerable.Setup(e => e.GetPageAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
				.Returns(new ValueTask<IReadOnlyList<Asset>>(page));
			enumerable.Setup(e => e.RelativeStatusUri()).Returns("sertup");
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			enumerator.ProcessedPublic = new List<int>() { 1 };
			enumerator.IncompletePublic = new List<int>() { 8 };
			enumerator.PageIndexPublic = 152;
			enumerator.PageIdPublic = 5;
			enumerator.Count = 310;

			// Test
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.True(await enumerator.NextPageAsyncPublic());

			// Validate
			Assert.Equal(0, enumerator.PageIndex);
			Assert.Equal(8, enumerator.PageId);
			Assert.Same(page, enumerator.CurrentPage);
			Assert.Same(asset, enumerator.Current);
			Assert.Equal(311, enumerator.Count); // Listed as required side effect
			Assert.False(enumerator.LastPagePublic); // Listed as required side effect
			Assert.Equal(new[] { 1, 5 }, enumerator.ProcessedPublic);
			Assert.Empty(enumerator.IncompletePublic);

			enumerable.Verify(
				e => e.GetPageAsync(
					It.Is<int>(i => i == 8),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			enumerable.VerifyNoOtherCalls();
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestNextPageWaitForNextAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			var result = new[] {
				new AssetExportStatus(
					Status.PROCESSING,
					new[] { 1, 2, 6, 9 }),
				new AssetExportStatus(
					Status.PROCESSING,
					new[] { 1, 2, 6, 9 }),
				new AssetExportStatus(
					Status.PROCESSING,
					new[] { 1, 2, 6, 9 }),
				new AssetExportStatus(
					Status.PROCESSING,
					new[] { 1, 2, 6, 8, 9 }),
			};
			tio.Setup(t => t.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(s => s.Equals("retrystatus")),
					It.Is<Method>(m => m == Method.Get),
					It.IsAny<CancellationToken>()))
				.Returns(Util.ItemsFromList(result.Select(Task.FromResult<AssetExportStatus?>)));
			var asset = new Asset(
				Guid.Empty,
				DateTime.MinValue,
				DateTime.MaxValue,
				DateTime.MinValue,
				DateTime.MaxValue,
				Guid.Empty,
				"default");
			var page = new[] { asset };
			enumerable.Setup(e => e.GetPageAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
				.Returns(new ValueTask<IReadOnlyList<Asset>>(page));
			enumerable.Setup(e => e.RelativeStatusUri()).Returns("retrystatus");
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			enumerator.ProcessedPublic = new List<int>() { 1, 2, 9 };
			enumerator.IncompletePublic = new List<int>() { };
			enumerator.PageIdPublic = 6;
			enumerator.Count = 62;

			// Test
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.True(await enumerator.NextPageAsyncPublic());

			// Validate
			Assert.Equal(0, enumerator.PageIndex);
			Assert.Equal(8, enumerator.PageId);
			Assert.Same(page, enumerator.CurrentPage);
			Assert.Same(asset, enumerator.Current);
			Assert.Equal(63, enumerator.Count); // Listed as required side effect
			Assert.False(enumerator.LastPagePublic); // Listed as required side effect
			Assert.Equal<int>(new[] { 1, 2, 9, 6 }, enumerator.ProcessedPublic);
			Assert.Equal<int>(Array.Empty<int>(), enumerator.IncompletePublic);
			Assert.Equal(8, enumerator.PageId);
			Assert.Equal(3, enumerator.SleepCount);

			enumerable.Verify(
				e => e.GetPageAsync(
					It.Is<int>(i => i == 8),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			enumerable.Verify(
				e => e.RelativeStatusUri(),
				// Times.Between(1, 2, Moq.Range.Inclusive));
				Times.Exactly(4));
			enumerable.VerifyNoOtherCalls();
			tio.Verify(
				t => t.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(s => s.Equals("retrystatus")),
					It.Is<Method>(m => m == Method.Get),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Exactly(4));
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestNextPageCompletedAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			var asset = new Asset(
				Guid.Empty,
				DateTime.MinValue,
				DateTime.MaxValue,
				DateTime.MinValue,
				DateTime.MaxValue,
				Guid.Empty,
				"default");
			var page = new[] { asset };
			tio.Setup(t => t.RequestJsonAsync<AssetExportStatus>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<AssetExportStatus?>(new AssetExportStatus(
					Status.FINISHED,
					new[] { 1, 2, 3, 4, 5, 6, 7 })));
			enumerable.Setup(e => e.GetPageAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
				.Returns(new ValueTask<IReadOnlyList<Asset>>(page));
			enumerable.Setup(e => e.RelativeStatusUri()).Returns("ending");
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			enumerator.ProcessedPublic = new List<int>() { 1, 2, 3, 4, 5, 7 };
			enumerator.IncompletePublic = new List<int>() { };
			enumerator.CurrentPagePublic = page;
			enumerator.PageIndexPublic = 0;
			Assert.Same(asset, enumerator.Current);
			enumerator.PageIdPublic = 6;
			enumerator.Count = 835;

			// Test
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			Assert.False(await enumerator.NextPageAsyncPublic());

			// Validate
			Assert.Equal(0, enumerator.PageIndex);
			Assert.Equal(6, enumerator.PageId);
			Assert.Same(page, enumerator.CurrentPage);
			Assert.Same(asset, enumerator.Current);
			Assert.Equal(835, enumerator.Count); // Listed as required side effect
			Assert.True(enumerator.LastPagePublic); // Listed as required side effect
			Assert.Equal(new[] { 1, 2, 3, 4, 5, 7 }, enumerator.ProcessedPublic);
			Assert.Empty(enumerator.IncompletePublic);

			enumerable.Verify(e => e.RelativeStatusUri(), Times.Once());
			enumerable.VerifyNoOtherCalls();
			tio.Verify(
				t => t.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(s => "ending".Equals(s)),
					It.Is<Method>(m => m == Method.Get),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}



		[Fact]
		public void TestReset() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var enumerable = new Mock<ExportEnumerable<Asset, AssetExportStatus>>(
				tio.Object, Guid.Empty);
			using var tokenSource = new CancellationTokenSource();
			var enumerator = new ExportEnumeratorOverride<Asset, AssetExportStatus>(
				enumerable.Object, tokenSource.Token);
			enumerator.ProcessedPublic = new List<int>() { 7, 25 };
			enumerator.IncompletePublic = new List<int>() { 6, 11 };
			enumerator.GetType()
				.GetProperty(
					nameof(enumerator.PageId),
					BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)!
				.SetValue(enumerator, 3);

			// Test
			Assert.Same(enumerable.Object, enumerator.Enumerable);
			enumerator.Reset();

			// Validate
			enumerable.VerifyNoOtherCalls();
			tio.VerifyNoOtherCalls();
			Assert.Empty(enumerator.ProcessedPublic);
			Assert.Equal(new[] { 7, 25, 3, 6, 11 }, enumerator.IncompletePublic);
			Assert.Null(enumerator.PageId);
			Assert.True(enumerator.PageIndex < 0);
			Assert.Equal(0, enumerator.Count);
			Assert.False(enumerator.LastPagePublic);
		}
	}
}
