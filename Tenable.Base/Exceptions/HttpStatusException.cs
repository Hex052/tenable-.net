using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// Used as a base class for all exceptions thrown by Tenable where an
	/// abnormal HTTP status code was the cause
	/// </summary>
	/// <remarks>
	/// If this exception is thrown and not anything more derived,
	/// an unexpected status code was returned from the server
	/// </remarks>
	public class HttpStatusException : TenableException {
		public HttpStatusException(HttpStatusCode statusCode) : base() {
			this.StatusCode = statusCode;
		}
		public HttpStatusException(HttpStatusCode statusCode, string message) : base(message) {
			this.StatusCode = statusCode;
		}
		public HttpStatusException(HttpStatusCode statusCode, string message, string? body) : base(message) {
			this.StatusCode = statusCode;
			this.Body = body;
		}
		public HttpStatusException(HttpStatusCode statusCode, string message, Exception innerException) : base(message, innerException) {
			this.StatusCode = statusCode;
		}
		public HttpStatusException(HttpStatusCode statusCode, string message, string? body, Exception innerException) : base(message, innerException) {
			this.StatusCode = statusCode;
			this.Body = body;
		}

		/// <summary>
		/// The status code returned
		/// </summary>
		public HttpStatusCode StatusCode {
			get; protected set;
		}

		/// <summary>
		/// The body of the HTTP response
		/// </summary>
		public string? Body {
			get; protected set;
		}

		public override string Message {
			get {
				if (string.IsNullOrEmpty(this.Body)) {
					return string.Format(
						"{0} ({1}) {2}",
						(int)this.StatusCode,
						this.StatusCode,
						base.Message);
				}
				else {
					return string.Format(
						"{0} ({1}) {2} {{{3}}}",
						(int)this.StatusCode,
						this.StatusCode,
						base.Message,
						this.Body);
				}
			}
		}
	}
}
