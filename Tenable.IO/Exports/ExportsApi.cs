using RestSharp;
using System;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Exceptions;

namespace Tenable.IO.Exports {
	public class ExportsApi : IExportsApi {
		public ITenableIO Api {
			get;
		}

		internal ExportsApi(ITenableIO api) {
			this.Api = api;
		}

		/// <summary>Relative location of the vuln export endpoint</summary>
		public static readonly string VULN_EXPORT_ENDPOINT = "vulns/export";
		/// <summary>Relative location of the asset export endpoint</summary>
		public static readonly string ASSET_EXPORT_ENDPOINT = "assets/export";

		public Task<VulnEnumerable> VulnsAsync(
					CancellationToken cancellationToken = default) {
			return this.VulnsAsync(null!, cancellationToken);
		}
		public async Task<VulnEnumerable> VulnsAsync(VulnExportOptions options,
				CancellationToken cancellationToken = default) {
			var export = await this.Api.RequestJsonAsync<Result.ExportRequestResult>(
				VULN_EXPORT_ENDPOINT,
				Method.Post,
				jsonBody: options,
				cancellationToken);
			if (export is null) {
				throw new UnexpectedNullResultException(VULN_EXPORT_ENDPOINT);
			}
			return this.Vulns(export.ExportUuid);
		}
		public VulnEnumerable Vulns(Guid uuid) {
			return new VulnEnumerable(Api, uuid);
		}

		public Task<AssetEnumerable> AssetsAsync(
			CancellationToken cancellationToken = default) {
			return this.AssetsAsync(null!, cancellationToken);
		}
		public async Task<AssetEnumerable> AssetsAsync(AssetExportOptions options,
				CancellationToken cancellationToken = default) {
			var export = await this.Api.RequestJsonAsync<Result.ExportRequestResult>(
				ASSET_EXPORT_ENDPOINT,
				Method.Post,
				jsonBody: options,
				cancellationToken);
			if (export is null) {
				throw new UnexpectedNullResultException(ASSET_EXPORT_ENDPOINT);
			}
			return this.Assets(export.ExportUuid);
		}
		public AssetEnumerable Assets(Guid uuid) {
			return new AssetEnumerable(Api, uuid);
		}
	}
}
