using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using Tenable.IO.Exports;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestAssetExportFiltersConverter {
		[Fact]
		public void TestAllNull() {
			var filters = new AssetExportFilters();

			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new AssetExportFiltersConverter();

			converter.Write(jsonWriter, filters, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected: "{}", actual: result);
		}

		[Theory]
		[InlineData(nameof(AssetExportFilters.FirstScanTime), "1970-01-01T00:00:00", "first_scan_time", 0)]
		[InlineData(nameof(AssetExportFilters.LastAuthenticatedScanTime), "2021-05-15T05:50:32", "last_authenticated_scan_time", 1621057832)]
		[InlineData(nameof(AssetExportFilters.LastAssessed), "1970-01-01T00:00:00", "last_assessed", 0)]
		[InlineData(nameof(AssetExportFilters.DeletedAt), "1970-01-01T00:00:00", "deleted_at", 0)]
		[InlineData(nameof(AssetExportFilters.TerminatedAt), "2014-06-21T21:19:53", "terminated_at", 1403385593)]
		[InlineData(nameof(AssetExportFilters.UpdatedAt), "1970-01-01T00:00:00", "updated_at", 0)]
		[InlineData(nameof(AssetExportFilters.CreatedAt), "2019-10-27T10:19:33", "created_at", 1572171573)]
		public void TestSingleDateTimeProperty(string propertyName, string input, string expectedName, int expectedValue) {
			var filters = new AssetExportFilters();
			typeof(AssetExportFilters).GetProperty(propertyName)!
				.SetValue(filters, DateTime.Parse(input));

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			Assert.Equal(JsonValueKind.Number, enumerator.Current.Value.ValueKind);
			Assert.Equal(expectedValue, enumerator.Current.Value.GetInt32());
			Assert.False(enumerator.MoveNext());
		}

		[Theory]
		[InlineData(nameof(AssetExportFilters.IsDeleted), true, "is_deleted")]
		[InlineData(nameof(AssetExportFilters.IsDeleted), false, "is_deleted")]
		[InlineData(nameof(AssetExportFilters.IsLicensed), false, "is_licensed")]
		[InlineData(nameof(AssetExportFilters.IsLicensed), true, "is_licensed")]
		[InlineData(nameof(AssetExportFilters.ServiceNowSysId), true, "servicenow_sysid")]
		[InlineData(nameof(AssetExportFilters.ServiceNowSysId), false, "servicenow_sysid")]
		[InlineData(nameof(AssetExportFilters.HasPluginResults), false, "has_plugin_results")]
		[InlineData(nameof(AssetExportFilters.HasPluginResults), true, "has_plugin_results")]
		public void TestSingleBoolProperty(string propertyName, bool value, string expectedName) {
			var filters = new AssetExportFilters();
			typeof(AssetExportFilters).GetProperty(propertyName)!
				.SetValue(filters, value);

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			Assert.Equal(
				value ? JsonValueKind.True : JsonValueKind.False,
				enumerator.Current.Value.ValueKind);
			Assert.Equal(value, enumerator.Current.Value.GetBoolean());
			Assert.False(enumerator.MoveNext());
		}

		[Theory]
		[InlineData(nameof(AssetExportFilters.Sources), "AWS;NESSUS", "sources")]
		public void TestSingleEnumerableStringProperty(string propertyName, string inputs, string expectedName, string? expectedValues = null) {
			var filters = new AssetExportFilters();
			typeof(AssetExportFilters).GetProperty(propertyName)!
				.SetValue(filters, inputs.Split(';'));

			var result = Serialize(filters);
			var doc = JsonSerializer.Deserialize<JsonElement>(result);
			using var enumerator = doc.EnumerateObject();
			Assert.True(enumerator.MoveNext());
			Assert.Equal(expectedName, enumerator.Current.Name);
			var val = enumerator.Current.Value;
			Assert.Equal(JsonValueKind.Array, val.ValueKind);
			Assert.True(
				val.EnumerateArray()
				.All(v => v.ValueKind == JsonValueKind.String));
			Assert.True(
				val.EnumerateArray()
					.Select(f => f.GetString())
					.OrderBy(f => f)
					.SequenceEqual(
						(expectedValues ?? inputs)
						.Split(';')
						.OrderBy(f => f)));
			Assert.False(enumerator.MoveNext());
		}

		[Fact]
		public void TestTagsEmpty() {
			TestTags(Array.Empty<KeyValuePair<string, string>>());
		}
		[Theory]
		[InlineData("OS:Windows", "OS:MacOS", "Location:Dubai")]
		[InlineData("State:OLD", "Owner:Service Desk")]
		public void TestName(params string[] keyValuePairs) {
			var pairs = keyValuePairs.Select(f => {
				var vals = f.Split(':');
				Assert.Equal(2, vals.Length);
				return new KeyValuePair<string, string>(vals[0], vals[1]);
			})
				.ToList();
			TestTags(pairs);
		}
		protected static void TestTags(IEnumerable<KeyValuePair<string, string>> pairs) {
			var filters = new AssetExportFilters() { Tag = pairs };

			var result = Serialize(filters);
			var expected = pairs
				.GroupBy(p => p.Key)
				.ToDictionary(g => "tag." + g.Key, g => g.Select(t => t.Value).ToList());
			var actual = JsonSerializer.Deserialize<Dictionary<string, List<string>>>(result);
			Assert.Equal(expected, actual);
		}

		private static string Serialize(AssetExportFilters filters) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new AssetExportFiltersConverter();
			converter.Write(jsonWriter, filters, new JsonSerializerOptions());
			jsonWriter.Flush();
			return Encoding.UTF8.GetString(buffer.WrittenSpan);
		}
	}
}
