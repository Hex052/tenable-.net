using System.Text.Json.Serialization;

namespace Tenable.IO.Agents.Results{

	[JsonConverter(typeof(JsonStringEnumConverter))]
	public enum Status {
		/// <summary>
		/// The agent has connected recently and is therefore likely ready to scan
		/// </summary>
		ON,
		/// <summary>
		/// The agent has not been seen recently and should be considered offline
		/// </summary>
		OFF,
		/// <summary>
		/// The agent is online,
		/// but it is still processing plugin updates and is not ready to scan
		/// </summary>
		INIT
	}
}
