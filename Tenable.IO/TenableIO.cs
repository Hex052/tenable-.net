using RestSharp;
using System;
using Tenable.Base;

namespace Tenable.IO {
	/// <summary>
	/// This object allows interfacing with the Tenable.io APIs
	/// </summary>
	public class TenableIO : ApiBase, ITenableIO {
		/// <summary>The default base URL of tenable.io</summary>
		public const string CLOUD_URL = "https://cloud.tenable.com";
		public const string HEADER_NAME = "X-APIKeys";

		public string AccessKey {
			get;
		}
		public string SecretKey {
			get;
		}
		private string? _headerValue;
		public string HeaderValue {
			get => this._headerValue ??= string.Format(
				"accessKey={0};secretKey={1};",
				this.AccessKey,
				this.SecretKey);
		}

		/// <summary>
		/// Construct a new <see cref="TenableIO" /> object
		/// </summary>
		/// <param name="accessKey">User's access key for Tenable.io</param>
		/// <param name="secretKey">User's secret key for Tenable.io</param>
		/// <param name="url">
		/// Url for Tenable.io, such as https://cloud.tenable.com
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// Thrown if any parameter is <see langword="null"/>
		/// </exception>
		public TenableIO(string accessKey, string secretKey, string url) : this(accessKey, secretKey, new RestClientOptions(url ?? throw new ArgumentNullException(nameof(url)))) {
		}
		public TenableIO(string accessKey, string secretKey, RestClientOptions options) : base(options ?? throw new ArgumentNullException(nameof(options))) {
			this.AccessKey = accessKey ?? throw new ArgumentNullException(nameof(accessKey));
			this.SecretKey = secretKey ?? throw new ArgumentNullException(nameof(secretKey));
			this.RestClient.AddDefaultHeader(HEADER_NAME, this.HeaderValue);
		}
		/// <inheritdoc cref="TenableIO.TenableIO(string, string, string)"/>
		/// <remarks>Url will be set to <see cref="CLOUD_URL"/></remarks>
		public TenableIO(string accessKey, string secretKey) : this(accessKey, secretKey, new RestClientOptions(CLOUD_URL)) {
		}

		public object AccessGroups {
			get => throw new NotImplementedException();
		}
		private readonly WeakReference<AgentConfig.IAgentConfigApi> _agentConfig = new WeakReference<AgentConfig.IAgentConfigApi>(null!, false);
		public AgentConfig.IAgentConfigApi AgentConfig {
			get {
				AgentConfig.IAgentConfigApi? result;
				if (!this._agentConfig.TryGetTarget(out result)) {
					result = new AgentConfig.AgentConfigApi(this);
					this._agentConfig.SetTarget(result);
				}
				return result;
			}
		}
		public object AgentGroups {
			get => throw new NotImplementedException();
		}
		public object AgentExclusions {
			get => throw new NotImplementedException();
		}
		public Agents.IAgentsApi Agents {
			get => new Agents.AgentsApi(this);
		}
		public object Assets {
			get => throw new NotImplementedException();
		}
		public object AuditLog {
			get => throw new NotImplementedException();
		}
		public object Credentials {
			get => throw new NotImplementedException();
		}
		public object Editor {
			get => throw new NotImplementedException();
		}
		public object Exclusions {
			get => throw new NotImplementedException();
		}
		private readonly WeakReference<Exports.IExportsApi> _exports = new WeakReference<Exports.IExportsApi>(null!, false);
		public Exports.IExportsApi Exports {
			get {
				Exports.IExportsApi? result;
				if (!this._exports.TryGetTarget(out result)) {
					result = new Exports.ExportsApi(this);
					this._exports.SetTarget(result);
				}
				return result;
			}
		}
		public object Files {
			get => throw new NotImplementedException();
		}
		public object Filters {
			get => throw new NotImplementedException();
		}
		public object Folders {
			get => throw new NotImplementedException();
		}
		public object Groups {
			get => throw new NotImplementedException();
		}
		private readonly WeakReference<Networks.INetworksApi> _networks = new WeakReference<Networks.INetworksApi>(null!, false);
		public Networks.INetworksApi Networks {
			get {
				Networks.INetworksApi? result;
				if (!this._networks.TryGetTarget(out result)) {
					result = new Networks.NetworksApi(this);
					this._networks.SetTarget(result);
				}
				return result;
			}
		}
		public object Permissions {
			get => throw new NotImplementedException();
		}
		private readonly WeakReference<Plugins.IPluginsApi> _plugins = new WeakReference<Plugins.IPluginsApi>(null!, false);
		public Plugins.IPluginsApi Plugins {
			get {
				Plugins.IPluginsApi? result;
				if (!this._plugins.TryGetTarget(out result)) {
					result = new Plugins.PluginsApi(this);
					this._plugins.SetTarget(result);
				}
				return result;
			}
		}
		public object Policies {
			get => throw new NotImplementedException();
		}
		public object ScannerGroups {
			get => throw new NotImplementedException();
		}
		public object Scanners {
			get => throw new NotImplementedException();
		}
		public object Scans {
			get => throw new NotImplementedException();
		}
		private readonly WeakReference<Server.IServerApi> _server = new WeakReference<Server.IServerApi>(null!, false);
		public Server.IServerApi Server {
			get {
				Server.IServerApi? result;
				if (!this._server.TryGetTarget(out result)) {
					result = new Server.ServerApi(this);
					this._server.SetTarget(result);
				}
				return result;
			}
		}
		public object Session {
			get => throw new NotImplementedException();
		}
		public object Tags {
			get => throw new NotImplementedException();
		}
		public object TargetGroups {
			get => throw new NotImplementedException();
		}
		public object Users {
			get => throw new NotImplementedException();
		}
		public object Workbenches {
			get => throw new NotImplementedException();
		}
	}
}
