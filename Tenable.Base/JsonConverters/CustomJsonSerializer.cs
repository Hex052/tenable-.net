using System;
using System.Text.Json;
using RestSharp;
using RestSharp.Serializers;
using RestSharp.Serializers.Json;

namespace Tenable.Base.JsonConverters {
	/// <summary>
	/// This class overrides the content types to add "application/octet-stream"
	/// as a supported Content-Type, since Tenable sometimes returns that
	/// instead of "text/json"
	/// </summary>
	public class CustomJsonSerializer : IRestSerializer {
		private readonly SystemTextJsonSerializer _serializer;

		public CustomJsonSerializer() {
			this._serializer = new SystemTextJsonSerializer();
		}

		public ISerializer Serializer => ((IRestSerializer)this._serializer).Serializer;

		public IDeserializer Deserializer => ((IRestSerializer)this._serializer).Deserializer;

		public string[] AcceptedContentTypes => ((IRestSerializer)this._serializer).AcceptedContentTypes;

		public SupportsContentType SupportsContentType => this._supportsContentType;
		private bool _supportsContentType(string contentType) {
			return ((IRestSerializer)this._serializer).SupportsContentType(contentType)
				|| "application/octet-stream".Equals(contentType, StringComparison.InvariantCultureIgnoreCase);
		}

		public DataFormat DataFormat => ((IRestSerializer)this._serializer).DataFormat;

		public string? Serialize(Parameter parameter) {
			return ((IRestSerializer)this._serializer).Serialize(parameter);
		}
	}
}
