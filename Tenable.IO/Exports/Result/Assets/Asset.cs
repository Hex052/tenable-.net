using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports.Result.Assets {
	public class Asset {
		/// <summary>The UUID of the asset</summary>
		[JsonPropertyName("id")]
		public Guid Id {
			get; set;
		}

		/// <summary>Whether an agent identified this asset</summary>
		[JsonPropertyName("has_agent")]
		public bool HasAgent {
			get; set;
		} = false;
		/// <summary>Whether this asset has plugin results</summary>
		[JsonPropertyName("has_plugin_results")]
		public bool HasPluginResults {
			get; set;
		} = false;

		/// <summary>The time and date the asset was created</summary>
		[JsonPropertyName("created_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime CreatedAt {
			get; set;
		}
		/// <summary>The time and date the asset was terminated in AWS</summary>
		[JsonPropertyName("terminated_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? TerminatedAt {
			get; set;
		}
		/// <summary>The user who terminated the AWS instance of the asset</summary>
		[JsonPropertyName("terminated_by")]
		public string? TerminatedBy {
			get; set;
		}
		/// <summary>The time and date the asset record was last updated</summary>
		[JsonPropertyName("updated_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime UpdatedAt {
			get; set;
		}
		/// <summary>The time and date the asset record was deleted</summary>
		[JsonPropertyName("deleted_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? DeletedAt {
			get; set;
		}
		/// <summary>The user who deleted asset</summary>
		[JsonPropertyName("deleted_by")]
		public string? DeletedBy {
			get; set;
		}
		/// <summary>The time and date the asset was first seen</summary>
		[JsonPropertyName("first_seen")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime FirstSeen {
			get; set;
		}
		/// <summary>The time and date the asset was last seen</summary>
		[JsonPropertyName("last_seen")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime LastSeen {
			get; set;
		}
		/// <summary>
		/// The time of the first scan of this asset
		/// </summary>
		[JsonPropertyName("first_scan_time")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? FirstScanTime {
			get; set;
		}
		/// <summary>The time of the last scan of this asset</summary>
		[JsonPropertyName("last_scan_time")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastScanTime {
			get; set;
		}
		/// <summary>The time of the last credentialed scan of this asset</summary>
		[JsonPropertyName("last_authenticated_scan_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastAuthenticatedScanDate {
			get; set;
		}
		[JsonPropertyName("last_licensed_scan_date")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastLicensedScanDate {
			get; set;
		}
		/// <summary>The UUID of this asset's last scan</summary>
		/// <remarks>
		/// <see cref="string"/> used because <see cref="Guid"/> isn't long enough
		/// </remarks>
		[JsonPropertyName("last_scan_id")]
		public string? LastScanId {
			get; set;
		}
		/// <summary>The schedule uuid of this asset's last scan</summary>
		/// <remarks>
		/// <see cref="string"/> used because <see cref="Guid"/> isn't long enough
		/// </remarks>
		[JsonPropertyName("last_schedule_id")]
		public string? LastScheduleId {
			get; set;
		}
		/// <summary>The sources of the scans that identified this asset</summary>
		[JsonPropertyName("sources")]
		public List<Source>? Sources {
			get; set;
		}

		/// <summary>Unique ID of the VM in Azure</summary>
		[JsonPropertyName("azure_vm_id")]
		public string? AzureVmId {
			get; set;
		}
		/// <summary>Unique ID of the VM in Azure Resource Manager</summary>
		[JsonPropertyName("azure_resource_id")]
		public string? AzureResourceId {
			get; set;
		}
		/// <summary>Unique ID of the VM in Google Cloud Platform</summary>
		[JsonPropertyName("gcp_project_id")]
		public string? GcpProjectId {
			get; set;
		}
		/// <summary>Name of the project the VM belongs to</summary>
		[JsonPropertyName("gcp_zone")]
		public string? GcpZone {
			get; set;
		}
		/// <summary>Name of the zone the VM belongs to</summary>
		[JsonPropertyName("gcp_instance_id")]
		public string? GcpInstanceId {
			get; set;
		}
		/// <summary>The unique identifier of the AMI image in Amazon EC2</summary>
		[JsonPropertyName("aws_ec2_instance_ami_id")]
		public string? AwsEc2InstanceAmiId {
			get; set;
		}
		/// <summary>Unique id of the Linux instance in AWS EC2</summary>
		[JsonPropertyName("aws_ec2_instance_id")]
		public string? AwsEc2InstanceId {
			get; set;
		}
		/// <summary>
		/// Unique id of the user associated with the VM instance in AWS
		/// </summary>
		[JsonPropertyName("aws_owner_id")]
		public string? AwsOwnerId {
			get; set;
		}
		/// <summary>AWS avalability zone this asset is in</summary>
		[JsonPropertyName("aws_availability_zone")]
		public string? AwsAvailabilityZone {
			get; set;
		}
		/// <summary>AWS region this asset is in</summary>
		[JsonPropertyName("aws_region")]
		public string? AwsRegion {
			get; set;
		}
		/// <summary>unique ID for the VPC that hosts this VM</summary>
		[JsonPropertyName("aws_vpc_id")]
		public string? AwsVpcId {
			get; set;
		}
		/// <summary>Name of the AWS instance group</summary>
		[JsonPropertyName("aws_ec2_instance_group_name")]
		public string? AwsEc2InstanceGroupName {
			get; set;
		}
		/// <summary>State of the VM in AWS at the time of the last scan</summary>
		[JsonPropertyName("aws_ec2_instance_state_name")]
		public string? AwsEc2InstanceStateName {
			get; set;
		}
		/// <summary>Type of the AWS EC2 instance</summary>
		[JsonPropertyName("aws_ec2_instance_type")]
		public string? AwsEc2InstanceType {
			get; set;
		}
		/// <summary>Subnet the VM is inside in AWS</summary>
		[JsonPropertyName("aws_subnet_id")]
		public string? AwsSubnetId {
			get; set;
		}
		/// <summary>
		/// The product code associated with the AMI used to launch the
		/// virtual machine instance in AWS EC2
		/// </summary>
		[JsonPropertyName("aws_ec2_product_code")]
		public string? AwsEc2ProductCode {
			get; set;
		}
		/// <summary>Name of the AWS EC2 instance</summary>
		[JsonPropertyName("aws_ec2_name")]
		public string? AwsEc2Name {
			get; set;
		}

		/// <summary>
		/// Unique id of the asset in McAfee ePolicy Orchestrator
		/// </summary>
		[JsonPropertyName("mcafee_epo_guid")]
		public Guid? McafeeEpoGuid {
			get; set;
		}
		/// <summary>
		/// Unique id of the ePO agent that identified this asset
		/// </summary>
		[JsonPropertyName("mcafee_epo_agent_guid")]
		public Guid? McafeeEpoAgentGuid {
			get; set;
		}
		/// <summary>Unique ID of the asset in ServiceNow</summary>
		[JsonPropertyName("servicenow_sysid")]
		public string? ServiceNowSysId {
			get; set;
		}
		/// <summary>Unique ID of the asset in BigFix</summary>
		[JsonPropertyName("bigfix_asset_id")]
		public string? BigFixAssetId {
			get; set;
		}
		/// <summary>The asset id in Qualys</summary>
		[JsonPropertyName("qualys_asset_ids")]
		public List<string>? QualysAssetIds {
			get; set;
		}
		/// <summary>The host ids of the asset in Qualys</summary>
		[JsonPropertyName("qualys_host_ids")]
		public List<string>? QualysHostIds {
			get; set;
		}
		/// <summary>
		/// The hardware keys for the asset in Symantec Endpoint Protection
		/// </summary>
		[JsonPropertyName("symantec_ep_hardware_keys")]
		public List<string>? SymantecEpHardwareKeys {
			get; set;
		}

		/// <summary>The BIOS UUID of the asset</summary>
		[JsonPropertyName("bios_uuid")]
		[JsonConverter(typeof(GuidNoDashConverter))]
		public Guid? BiosUuid {
			get; set;
		}
		/// <summary>The BIOS UUID of the asset</summary>
		[JsonPropertyName("manufacturer_tpm_ids")]
		public List<string>? ManufacturerTpmIds {
			get; set;
		}
		/// <summary>
		/// The ID of the network associated with scanners that identified the asset
		/// </summary>
		[JsonPropertyName("network_id")]
		public Guid NetworkId {
			get; set;
		}
		/// <summary>
		/// Name of the network <see cref="NetworkId"/> refers to
		/// </summary>
		[JsonPropertyName("network_name")]
		public string NetworkName {
			get; set;
		}
		/// <summary>The UUID of the agent on the device</summary>
		[JsonPropertyName("agent_uuid")]
		[JsonConverter(typeof(GuidNoDashConverter))]
		public Guid? AgentUuid {
			get; set;
		}
		/// <summary>
		/// Names of any Nessus agents that identified this asset
		/// </summary>
		[JsonPropertyName("agent_names")]
		public List<string>? AgentNames {
			get; set;
		}
		/// <summary>All detected fully-qualified domain names</summary>
		[JsonPropertyName("fqdns")]
		public List<string>? Fqdns {
			get; set;
		}
		/// <summary>All detected host names of the asset</summary>
		[JsonPropertyName("hostnames")]
		public List<string>? Hostnames {
			get; set;
		}
		/// <summary>All detected NETBIOS names</summary>
		[JsonPropertyName("netbios_names")]
		public List<string>? NetbiosNames {
			get; set;
		}
		/// <summary>All detected IPv4 addresses</summary>
		[JsonPropertyName("ipv4s")]
		public List<string>? IPv4s {
			get; set;
		}
		/// <summary>The IPv6 address of the device</summary>
		[JsonPropertyName("ipv6s")]
		public List<string>? IPv6s {
			get; set;
		}
		/// <summary>The detected MAC addresses of the device</summary>
		[JsonPropertyName("mac_addresses")]
		public List<string>? MacAddresses {
			get; set;
		}
		/// <summary>The asset's network interfaces</summary>
		[JsonPropertyName("network_interfaces")]
		public List<NetworkInterface>? NetworkInterfaces {
			get; set;
		}
		/// <summary>The type of asset according to plugin 54615</summary>
		[JsonPropertyName("system_types")]
		public List<string>? SystemTypes {
			get; set;
		}
		/// <summary>The device's operating system</summary>
		[JsonPropertyName("operating_systems")]
		public List<string>? OperatingSystems {
			get; set;
		}
		/// <summary>
		/// CPE values for all the software identified on an asset
		/// </summary>
		[JsonPropertyName("installed_software")]
		public List<string>? InstalledSoftware {
			get; set;
		}
		/// <summary>The device's ssh fingerprints</summary>
		[JsonPropertyName("ssh_fingerprints")]
		public List<string>? SshFingerprints {
			get; set;
		}
		/// <summary>The asset's tags in Tenable</summary>
		[JsonPropertyName("tags")]
		public List<Tag>? Tags {
			get; set;
		}

		/// <summary>Lumin asset criticality rating</summary>
		[JsonPropertyName("acr_score")]
		public string? AcrScore {
			get; set;
		}
		/// <summary>Lumin asset exposure score</summary>
		[JsonPropertyName("exposure_score")]
		public string? ExposureScore {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Asset(Guid id, DateTime createdAt, DateTime updatedAt, DateTime firstSeen, DateTime lastSeen, Guid networkId, string networkName) {
			this.Id = id;
			this.CreatedAt = createdAt;
			this.UpdatedAt = updatedAt;
			this.FirstSeen = firstSeen;
			this.LastSeen = lastSeen;
			this.NetworkId = networkId;
			this.NetworkName = networkName;
		}
	}
}
