using System.Collections.Generic;
using System.Text.Json.Serialization;
using Tenable.Base;

namespace Tenable.IO.Plugins.Results {
	public class PluginPage : IPage<Plugin, PluginPagination> {
		/// <summary>The Plugins from the API</summary>
		[JsonPropertyName("plugins")]
		public IReadOnlyList<Plugin> Items {
			get; set;
		}

		/// <summary>
		/// The number of records in the returned results (<see cref="Items"/>)
		/// </summary>
		[JsonPropertyName("size")]
		public int Size {
			get; set;
		}

		/// <summary>
		/// The total number of available plugin records after Tenable
		/// applies the <see cref="PluginFilter.LastUpdated"/> filter
		/// </summary>
		[JsonPropertyName("total_count")]
		public int TotalCount {
			get; set;
		}

		/// <summary>The pagination information</summary>
		[JsonPropertyName("params")]
		public PluginFilter Parameters {
			get; set;
		}

		public PluginPagination Pagination {
			get;
		}

		[JsonConstructor]
		public PluginPage(IReadOnlyList<Plugin> items, int size,
				int totalCount, PluginFilter parameters) {
			this.Items = items;
			this.Size = size;
			this.TotalCount = totalCount;
			this.Parameters = parameters;
			this.Pagination = new PluginPagination(this);
		}
	}
}
