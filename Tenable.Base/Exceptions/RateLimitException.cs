using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// When too many requests have been issued and you are being rate-limited
	/// </summary>
	/// <remarks>
	/// <see cref="HttpStatusException"/>
	/// </remarks>
	public class RateLimitException : RetryException {
		public RateLimitException(HttpStatusCode statusCode, uint retryAfter) : base(statusCode, retryAfter) {
		}
		public RateLimitException(HttpStatusCode statusCode, uint retryAfter, string message) : base(statusCode, retryAfter, message) {
		}
		public RateLimitException(HttpStatusCode statusCode, uint retryAfter, string message, string? body) : base(statusCode, retryAfter, message, body) {
		}
		public RateLimitException(HttpStatusCode statusCode, uint retryAfter, string message, Exception innerException) : base(statusCode, retryAfter, message, innerException) {
		}
		public RateLimitException(HttpStatusCode statusCode, uint retryAfter, string message, string? body, Exception innerException) : base(statusCode, retryAfter, message, body, innerException) {
		}
	}
}
