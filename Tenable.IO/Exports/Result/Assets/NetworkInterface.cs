using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result.Assets {
	public class NetworkInterface {
		/// <summary>Name of the interface</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}

		/// <summary>MAC addresses of the interface</summary>
		[JsonPropertyName("mac_address")]
		public List<string> MacAddress {
			get; set;
		}

		/// <summary>IPv4 addresses of the interface</summary>
		[JsonPropertyName("ipv4")]
		public List<string>? Ipv4 {
			get; set;
		}
		/// <summary>IPv6 addresses of the interface</summary>
		[JsonPropertyName("ipv6")]
		public List<string>? Ipv6 {
			get; set;
		}
		/// <summary>FQDNs of the interface</summary>
		[JsonPropertyName("fqdn")]
		public List<string>? Fqdn {
			get; set;
		}

		[JsonConstructor]
		public NetworkInterface(string name, List<string> macAddress) {
			this.Name = name;
			this.MacAddress = macAddress;
		}
	}
}
