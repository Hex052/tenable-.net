using System.Collections.Generic;
using System.Text.Json.Serialization;
using Tenable.Base;

namespace Tenable.IO.Plugins.Results {
	[JsonConverter(typeof(JsonStringEnumConverter))]
	public enum RiskFactor {
		/// <summary>
		/// The vulnerability has a CVSS score of 0.0 (an informational plugin)
		/// </summary>
		None,
		Info = None,
		/// <summary>
		/// The vulnerability has a CVSS score between 0.1 and 3.9
		/// </summary>
		Low,
		/// <summary>
		/// The vulnerability has a CVSS score between 4.0 and 6.9
		/// </summary>
		Medium,
		/// <summary>
		/// The vulnerability has a CVSS score between 7.0 and 9.9
		/// </summary>
		High,
		/// <summary>
		/// The vulnerability has a CVSS score of 10.0
		/// </summary>
		Critical
	}
}
