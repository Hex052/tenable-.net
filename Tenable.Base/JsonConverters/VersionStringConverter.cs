using System;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Tenable.Base.JsonConverters {
	public class VersionStringConverter : JsonConverter<Version?> {
		public override bool HandleNull => true;

		public override Version? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			if (reader.TokenType == JsonTokenType.Null) {
				return null;
			}
			else if (reader.TokenType == JsonTokenType.String) {
				var val = reader.GetString();
				if (Version.TryParse(val, out Version? result)) {
					return result!;
				}
				else if (int.TryParse(val, out int intresult)) {
					return new Version(intresult, 0);
				}
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, Version? value, JsonSerializerOptions options) {
			writer.WriteStringValue(value?.ToString());
		}
	}
}
