using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.IO.Plugins.Results;

namespace Tenable.IO.Plugins {
	public interface IPluginsApi : IEndpoint {
		/// <summary>
		/// List plugins, applying a filter to
		/// </summary>
		/// <param name="filter">Filter to apply to the results</param>
		/// <returns>Enumerable over all the results</returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		PluginEnumerable List(PluginFilter filter);
		/// <inheritdoc cref="List(PluginFilter)"/>
		PluginEnumerable List();

		/// <summary>
		/// Lists plugin families
		/// </summary>
		/// <param name="all">
		/// Specifies whether to return all plugin families.
		/// If true, the plugin families hidden in Tenable.io UI,
		/// for example, Port Scanners, are included in the list.
		/// </param>
		/// <returns>
		/// </returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<PluginFamilyList> FamiliesAsync(bool all, CancellationToken cancellationToken = default);
		/// <summary>
		/// Lists plugin families visible in the Tenable.io UI
		/// </summary>
		/// <remarks>
		/// The same as calling <see cref="FamiliesAsync(bool, CancellationToken)"/> with <see langword="false"/>
		/// </remarks>
		/// <inheritdoc cref="FamiliesAsync(bool, CancellationToken)"/>
		Task<PluginFamilyList> FamiliesAsync(CancellationToken cancellationToken = default);


		/// <summary>
		/// Get details for a plugin
		/// </summary>
		/// <param name="id">Plugin id to get details for</param>
		/// <returns>
		/// Details for a particular plugin
		/// </returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<PluginDetails> DetailsAsync(int id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Get plugins in a family
		/// </summary>
		/// <param name="id">Family id to get details for</param>
		/// <returns>
		/// Plugins in a particular plugin family
		/// </returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<PluginFamilyDetails> FamilyPluginsAsync(int id, CancellationToken cancellationToken = default);
	}
}
