using System;
using Tenable.IO.Exports.Result.Assets;

namespace Tenable.IO.Exports {
	public class AssetEnumerable : ExportEnumerable<Asset, AssetExportStatus> {
		internal protected AssetEnumerable(ITenableIO api, Guid uuid) : base(api, uuid) {
		}

		public override string RelativePageUri(int pageId) {
			return string.Format("assets/export/{0}/chunks/{1}", this.Uuid, pageId);
		}
		public override string RelativeStatusUri() {
			return string.Format("assets/export/{0}/status", this.Uuid);
		}
		public override string RelativeCancelUri() {
			return string.Format("assets/export/{0}/cancel", this.Uuid);
		}
	}
}
