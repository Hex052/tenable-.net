using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base;

namespace Tenable.IO.Exports {
	public class VulnExportFiltersConverter : ExportFiltersConverter<VulnExportFilters> {

		public sealed override void Write(Utf8JsonWriter writer, VulnExportFilters value, JsonSerializerOptions options) {
			writer.WriteStartObject();
			if (value.CidrRange != null) {
				writer.WriteString("cidr_range", value.CidrRange);
			}
			if (value.FirstFound != null) {
				writer.WritePropertyName("first_found");
				this._dateConverter.Write(writer, value.FirstFound.Value, options);
			}
			if (value.LastFound != null) {
				writer.WritePropertyName("last_found");
				this._dateConverter.Write(writer, value.LastFound.Value, options);
			}
			if (value.LastFixed != null) {
				writer.WritePropertyName("last_fixed");
				this._dateConverter.Write(writer, value.LastFixed.Value, options);
			}
			if (value.PluginFamily != null) {
				writer.WritePropertyName("plugin_family");
				JsonSerializer.Serialize<IEnumerable<string>>(writer, value.PluginFamily, options);
			}
			if (value.PluginId != null) {
				writer.WritePropertyName("plugin_id");
				JsonSerializer.Serialize<IEnumerable<int>>(writer, value.PluginId, options);
			}
			if (value.Severity != null) {
				writer.WritePropertyName("severity");
				JsonSerializer.Serialize<IEnumerable<string>>(writer, value.Severity, options);
			}
			if (value.Since != null) {
				writer.WritePropertyName("since");
				this._dateConverter.Write(writer, value.Since.Value, options);
			}
			if (value.State != null) {
				writer.WritePropertyName("state");
				JsonSerializer.Serialize<IEnumerable<string>>(writer, value.State, options);
			}
			if (value.VprScore != null) {
				writer.WritePropertyName("vpr_score");
				JsonSerializer.Serialize<VulnExportVprScore>(writer, value.VprScore, options);
			}

			base.Write(writer, value, options);
			writer.WriteEndObject();
		}
	}
}
