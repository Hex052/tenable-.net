using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestUtcDateTimeConverter {
		public class TestObject {
			[JsonConverter(typeof(UtcDateTimeConverter))]
			public DateTime Time {
				get; set;
			}
		}

		[Theory]
		[InlineData("1970-01-01T00:00:00.000", "1970-01-01T00:00:00.000")]
		[InlineData("2021-05-15T05:50:32.853", "2021-05-15T05:50:32.853")]
		[InlineData("2017-12-31T20:40:23.447", "2017-12-31T20:40:23.447")]
		public void TestDeserialize(string jsonTime, string time) {
			var expected = DateTime.Parse(time);
			var json = string.Format("{{ \"{0}\": \"{1}\" }}", nameof(TestObject.Time), jsonTime);
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Time;
			Assert.Equal(DateTimeKind.Unspecified, result.Kind);
			Assert.Equal(expected: expected, actual: DateTime.SpecifyKind(result, DateTimeKind.Unspecified));
			json = string.Format("{{ \"{0}\": \"{1}Z\" }}", nameof(TestObject.Time), jsonTime);
			result = JsonSerializer.Deserialize<TestObject>(json)!.Time;
			Assert.Equal(DateTimeKind.Utc, result.Kind);
			Assert.Equal(expected: expected, actual: DateTime.SpecifyKind(result, DateTimeKind.Utc));
		}

		[Theory]
		[InlineData("\"\"")]
		[InlineData("\"a;sfogihad;sgih\"")]
		[InlineData(0)]
		[InlineData("\"1621057832\"")]
		public void TestDeserializeException(object badData) {
			var json = string.Format(
				"{{ \"{0}\": {1} }}",
				nameof(TestObject.Time),
				badData);
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("1970-01-01T00:00:00.000", "\"1970-01-01T00:00:00.000Z\"")]
		[InlineData("2021-05-15T05:50:32.853", "\"2021-05-15T05:50:32.853Z\"")]
		[InlineData("2017-12-31T20:40:23.447", "\"2017-12-31T20:40:23.447Z\"")]
		public void ToInt(string time, string expected) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new UtcDateTimeConverter();

			converter.Write(jsonWriter, DateTime.Parse(time), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
	}
}
