using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Collections;
using Tenable.Base.Exceptions;
using Tenable.IO.Exports.Result;

namespace Tenable.IO.Exports {
	/// <summary>
	/// Enumerator over exports
	/// </summary>
	/// <typeparam name="TResult">Enumerator result type</typeparam>
	/// <typeparam name="TStatus">Type of the export status object</typeparam>
	public class ExportEnumerator<TResult, TStatus> : ResultEnumerator<TResult, IReadOnlyList<TResult>, ExportEnumerable<TResult, TStatus>> where TStatus : RequestExportStatus {
		private bool disposedValue = false;

		/// <summary>
		/// Page numbers that have yet to be completed
		/// </summary>
		protected IList<int> Incomplete = new List<int>();

		internal protected ExportEnumerator(ExportEnumerable<TResult, TStatus> enumerable,
				CancellationToken cancellationToken) : base(enumerable, cancellationToken) {
		}

		/// <summary>Refreshes <see cref="Incomplete"/></summary>
		/// <param name="status">
		/// The status that is received from
		/// <see cref="ExportEnumerable{TResult, TStatus}.GetStatus"/>
		/// </param>
		/// <returns>
		/// <see langword="true"/> if new chunks are available;
		/// <see langword="false"/> if not.
		/// The status as retrieved from
		/// <see cref="ExportEnumerable{TResult, TStatus}.GetStatusAsync"/>.
		/// </returns>
		/// <inheritdoc cref="ExportEnumerable{TResult, TStatus}.GetStatusAsync" path="/exception"/>
		public async ValueTask<ValueTuple<bool, TStatus>> RefreshAsync() {
			TStatus status = await this.Enumerable.GetStatusAsync(this.CancellationToken);
			HashSet<int> setProcessed = new HashSet<int>(this.Processed);
			if (this.PageId.HasValue) {
				setProcessed.Add(this.PageId.Value);
			}
			var result = status.ChunksAvailable
				.Where(i => !setProcessed.Contains(i))
				.ToList();
			var oldCount = this.Incomplete.Count;
			this.Incomplete = result;
			return (result.Count > oldCount, status);
		}

		private static bool _endStatus(Status status) {
			return status == Status.FINISHED
				|| status == Status.CANCELLED
				|| status == Status.ERROR;
		}
		protected virtual Task Sleep(int milliseconds) {
			return milliseconds == 0 ? Task.CompletedTask : Task.Delay(milliseconds, this.CancellationToken);
		}
		protected override async ValueTask<bool> NextPageAsync() {
			// Check if there is more pages to get
			if (this.Incomplete.Count == 0) {
				(bool newAvailable, TStatus status) = await this.RefreshAsync();
				while (!newAvailable) {
					if (_endStatus(status.Status)) {
						// There is nothing new and we finished.
						this.LastPage = true;
						return false;
					}
					// There is nothing new, but there will be as more chunks are
					// processed. We should wait for that to happen.
					await this.Sleep(milliseconds: 30000);
					(newAvailable, status) = await this.RefreshAsync();
				}
			}
			// Clear the current page being worked on
			if (this.PageId != null) {
				this.Processed.Add(this.PageId.Value);
				this.PageId = null;
				this.CurrentPage = null;
			}
			// We did get things, so update what is available
			this.PageId = this.Incomplete[0];
			var page = await this.Enumerable.GetPageAsync(this.PageId.Value, this.CancellationToken);
			this.CurrentPage = page;
			this.Incomplete.RemoveAt(0);
			this.PageIndex = 0;
			++this.Count;
			return true;
		}

		/// <summary>
		/// Reset the page back to the beginning, but is unlikely
		/// to enumerate the pages in the same exact order.
		/// </summary>
		/// <inheritdoc cref="ResultEnumerator{TResult, TEnumerable}.Reset"/>
		public override void Reset() {
			this.Incomplete = this.Processed
				.Append(this.PageId!.Value)
				.Concat(this.Incomplete)
				.ToList();
			base.Reset();
		}

		protected override void Dispose(bool disposing) {
			if (!this.disposedValue) {
				if (disposing) {
					// dispose managed state (managed objects)
				}
				// free unmanaged resources (unmanaged objects) and override finalizer

				// set large fields to null
				this.Incomplete = null!;

				this.disposedValue = true;
			}
			base.Dispose(disposing);
		}
	}
}
