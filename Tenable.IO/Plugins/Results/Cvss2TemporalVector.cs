using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class Cvss2TemporalVector {
		/// <summary>
		/// The CVSS Exploit Maturity Code (E) for the
		/// vulnerability the plugin covers
		/// </summary>
		[JsonPropertyName("Exploitability")]
		public string? Exploitability {
			get; set;
		}
		/// <summary>
		/// The CVSS Remediation Level (RL) temporal metric
		/// for the vulnerability the plugin covers
		/// </summary>
		[JsonPropertyName("RemediationLevel")]
		public string? RemediationLevel {
			get; set;
		}
		/// <summary>
		/// The CVSS Report Confidence (RC) temporal metric
		/// for the vulnerability the plugin covers
		/// </summary>
		[JsonPropertyName("ReportConfidence")]
		public string? ReportConfidence {
			get; set;
		}
		/// <summary>
		/// The complete metrics and result values for the vulnerability
		/// in a condensed and coded format
		/// </summary>
		/// <example><code>"E:U/RL:OF/RC:C"</code></example>
		[JsonPropertyName("raw")]
		public string Raw {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Cvss2TemporalVector(string raw) {
			this.Raw = raw;
		}
	}
}
