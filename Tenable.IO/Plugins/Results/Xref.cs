using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class Xref {
		/// <summary>The ID of the Xref</summary>
		[JsonPropertyName("id")]
		public string Id {
			get; set;
		}

		/// <summary>The type of Xref</summary>
		[JsonPropertyName("type")]
		public string Type {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Xref(string type, string id) {
			this.Id = id ?? throw new ArgumentNullException(nameof(id));
			this.Type = type ?? throw new ArgumentNullException(nameof(type));
		}

		[Pure]
		public override string ToString() {
			return this.Type + ":" + this.Id;
		}
	}
}
