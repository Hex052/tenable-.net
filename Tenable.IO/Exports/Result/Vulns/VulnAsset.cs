using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports.Result.Vulns {
	public class VulnAsset {
		/// <summary>The UUID of the asset</summary>
		[JsonPropertyName("uuid")]
		public Guid Uuid {
			get; set;
		}
		/// <summary>The UUID of the agent on the device</summary>
		[JsonPropertyName("agent_uuid")]
		[JsonConverter(typeof(GuidNoDashConverter))]
		public Guid? AgentUuid {
			get; set;
		}
		/// <summary>The BIOS UUID of the asset</summary>
		[JsonPropertyName("bios_uuid")]
		[JsonConverter(typeof(GuidNoDashConverter))]
		public Guid? BiosUuid {
			get; set;
		}

		/// <summary>The asset's fully-qualified domain name</summary>
		[JsonPropertyName("fqdn")]
		public string? Fqdn {
			get; set;
		}
		/// <summary>The host name of the asset</summary>
		[JsonPropertyName("hostname")]
		public string? Hostname {
			get; set;
		}
		/// <summary>The device's NETBIOS name</summary>
		[JsonPropertyName("netbios_name")]
		public string? NetbiosName {
			get; set;
		}
		/// <summary>The device's NETBIOS workgroup/domain</summary>
		[JsonPropertyName("netbios_workgroup")]
		public List<string>? NetbiosWorkgroup {
			get; set;
		}

		/// <summary>The IPv4 address of the device</summary>
		[JsonPropertyName("ipv4")]
		public string? IPv4 {
			get; set;
		}
		/// <summary>The IPv6 address of the device</summary>
		[JsonPropertyName("ipv6")]
		public string? IPv6 {
			get; set;
		}
		/// <summary>The MAC address of the device</summary>
		[JsonPropertyName("mac_address")]
		public string? MacAddress {
			get; set;
		}

		/// <summary>The type of asset</summary>
		[JsonPropertyName("device_type")]
		public string? DeviceType {
			get; set;
		}
		/// <summary>The device's operating system</summary>
		[JsonPropertyName("operating_system")]
		public List<string>? OperatingSystem {
			get; set;
		}

		/// <summary>
		/// A value specifying whether Tenable.io tracks the asset
		/// in the asset management system. Untracked assets have
		/// identifiers that change with each scan.
		/// </summary>
		/// <remarks>
		/// This parameter is relevant to PCI-type scans and in certain
		/// cases where there is not enough information in a scan to
		/// identify the asset. Untracked assets appear in the scan
		/// history, but do not appear in workbenches or reports.
		/// </remarks>
		[JsonPropertyName("tracked")]
		public bool Tracked {
			get; set;
		}
		/// <summary>
		/// The ID of the network associated with scanners that identified the asset
		/// </summary>
		[JsonPropertyName("network_id")]
		public Guid NetworkId {
			get; set;
		}

		/// <summary>
		/// The last time the device was scanned without using credentials
		/// </summary>
		[JsonPropertyName("last_unauthenticated_results")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastUnauthenticatedResults {
			get; set;
		}
		/// <summary>
		/// The last time the device was successfully scanned with credentials
		/// </summary>
		[JsonPropertyName("last_authenticated_results")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastAuthenticatedResults {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public VulnAsset(Guid uuid, Guid networkId, bool tracked) {
			this.Uuid = uuid;
			this.NetworkId = networkId;
			this.Tracked = tracked;
		}
	}
}
