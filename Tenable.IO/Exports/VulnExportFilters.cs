using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public class VulnExportFilters : ExportFilters {
		/// <summary>
		/// Restricts search for vulnerabilities to assets assigned an IP
		/// address within the specified CIDR range.
		/// For example, <c>0.0.0.0/0</c> restricts the search to
		/// <c>0.0.0.1</c> and <c>255.255.255.254</c>.
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("cidr_range")]
		public string? CidrRange {
			get; set;
		}

		/// <summary>
		/// Specifies the earliest time for a vulnerability to have been discovered
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("first_found")]
		public DateTime? FirstFound {
			get; set;
		}

		/// <summary>
		/// Specifies the earliest time for a vulnerability to have been last seen
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("last_found")]
		public DateTime? LastFound {
			get; set;
		}

		/// <summary>
		/// Specifies the earliest time that vulnerabilities may have been fixed
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("last_fixed")]
		public DateTime? LastFixed {
			get; set;
		}

		/// <summary>
		/// A case-sensitive list of plugin families for which you want to filter
		/// the vulnerabilities returned in the vulnerability export
		/// </summary>
		/// <remarks>
		/// Call <see cref="Tenable.IO.Plugins.PluginsApi.Families(bool)"/>
		/// to get all the plugin families available
		/// </remarks>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("plugin_family")]
		public IEnumerable<string>? PluginFamily {
			get; set;
		}

		/// <summary>
		/// A list of plugin families for which you want to filter
		/// the vulnerabilities returned in the vulnerability export
		/// </summary>
		/// <remarks>
		/// Call <see cref="Tenable.IO.Plugins.PluginsApi.List(int, int)"/>
		/// to get all the plugins available
		/// </remarks>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("plugin_id")]
		public IEnumerable<int>? PluginId {
			get; set;
		}

		/// <summary>
		/// The severity of the vulnerabilities to include in the export
		/// </summary>
		/// <remarks>
		/// The severity of a vulnerability is defined using the
		/// Common Vulnerability Scoring System (CVSS) base score
		/// </remarks>
		/// <example>
		/// Severity = new List&lt;string&gt;{"info", "low", "medium", "high", "critical"}
		/// </example>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("severity")]
		public IEnumerable<string>? Severity {
			get; set;
		}

		/// <summary>
		/// If <see cref="VulnExportFilters.State"> has "open" or "reopened",
		/// the export includes data for vulnerabilities that were seen on or
		/// after the since date you specify.
		/// If <see cref="VulnExportFilters.State"> has "fixed", the export
		/// includes data for vulnerabilities that were fixed on or
		/// after the since date you specify.
		/// If <see cref="VulnExportFilters.State"> is null or empty, both apply.
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("since")]
		public DateTime? Since {
			get; set;
		}

		/// <summary>
		/// The state of the vulnerabilities you want the export to include.
		/// </summary>
		/// <remarks>Case insensitive.</remarks>
		/// <example>
		/// Severity = new List&lt;string&gt;{"open", "reopened", "fixed"}
		/// </example>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("state")]
		public IEnumerable<string>? State {
			get; set;
		}

		/// <summary>
		/// Returns vulnerabilities with the specified
		/// Vulnerability Priority Rating (VPR) score or scores
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("vpr_score")]
		public VulnExportVprScore? VprScore {
			get; set;
		}
	}
}
