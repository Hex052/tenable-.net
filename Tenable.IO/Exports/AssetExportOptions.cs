using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public class AssetExportOptions {
		/// <summary>
		/// Specifies the number of assets per exported chunk
		/// </summary>
		/// <remarks>
		/// Range 100-10000 (others return a 400 error). Smaller numbers may
		/// improve performance, at the cost of sending more requests
		/// </remarks>
		[JsonPropertyName("chunk_size")]
		public int ChunkSize {
			get; set;
		} = 500;

		/// <summary>Specifies filters for exported assets</summary>
		/// <remarks>
		/// If multiple filters are used, they will be joined by AND
		/// </remarks>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
		[JsonPropertyName("filters")]
		[JsonConverter(typeof(AssetExportFiltersConverter))]
		public AssetExportFilters? Filters {
			get; set;
		}
	}
}
