using RestSharp;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Exceptions;
using Tenable.IO.Server.Results;

namespace Tenable.IO.Server {
	public class ServerApi : IServerApi {
		public const string STATUS_PARTIAL_URL = "server/status";
		public const string PROPERTIES_PARTIAL_URL = "server/properties";

		public ITenableIO Api {
			get;
		}

		internal ServerApi(ITenableIO api) {
			this.Api = api;
		}

		public async Task<ServerStatus> StatusAsync(CancellationToken cancellationToken = default) {
			var result = await this.Api.RequestJsonAsync<ServerStatus>(
				STATUS_PARTIAL_URL, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(STATUS_PARTIAL_URL);
		}
		public async Task<object> PropertiesAsync(CancellationToken cancellationToken = default) {
			var result = await this.Api.RequestJsonAsync<object>(
				PROPERTIES_PARTIAL_URL, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(PROPERTIES_PARTIAL_URL);
		}
	}
}
