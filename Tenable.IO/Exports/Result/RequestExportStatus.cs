using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result {
	public abstract class RequestExportStatus : ExportStatus {
		/// <summary>
		/// A list of chunks for which the export process failed
		/// </summary>
		[JsonPropertyName("chunks_available")]
		public IReadOnlyList<int> ChunksAvailable {
			get; set;
		}

		[JsonConstructor]
		protected RequestExportStatus(Status status, IReadOnlyList<int> chunksAvailable) : base(status) {
			this.ChunksAvailable = chunksAvailable;
		}
	}
}
