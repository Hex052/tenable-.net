using System.Collections.Generic;
using System.Text.Json.Serialization;
using Tenable.Base;

namespace Tenable.IO.Agents.Results {
	public class AgentPage : IPage<Agent, Pagination> {
		/// <summary>The agents from the API</summary>
		[JsonPropertyName("agents")]
		public IReadOnlyList<Agent> Items {
			get; set;
		}

		/// <summary>The pagination information</summary>
		[JsonPropertyName("pagination")]
		public Pagination Pagination {
			get; set;
		}

		[JsonConstructor]
		public AgentPage(IReadOnlyList<Agent> items, Pagination pagination) {
			this.Items = items;
			this.Pagination = pagination;
		}
	}
}
