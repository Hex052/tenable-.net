using System.Buffers;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.IO.Plugins.Results;
using Tenable.IO.Plugins.Results.Comparers;
using Tenable.IO.Plugins.Results.JsonConverters;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results.JsonConverters {
	public class TestPluginDetailsConverter {
		private class TestObject {
			[JsonConverter(typeof(PluginDetailsConverter))]
			public PluginDetails? PluginDetails {
				get; set;
			}
		}

		[Fact]
		public void TestDeserialize() {
			var jsonValue = @"{
	""attributes"": [
		{
			""attribute_value"": ""aix_IY19744.nasl"",
			""attribute_name"": ""fname""
		},
		{
			""attribute_value"": ""AIX 5.1 : IY19744"",
			""attribute_name"": ""plugin_name""
		},
		{
			""attribute_value"": ""$Revision: 1.5 $"",
			""attribute_name"": ""script_version""
		},
		{
			""attribute_value"": ""http://www-912.ibm.com/eserver/support/fixes/"",
			""attribute_name"": ""solution""
		},
		{
			""attribute_value"": ""High"",
			""attribute_name"": ""risk_factor""
		},
		{
			""attribute_value"": ""The remote host is missing AIX Critical Security Patch number IY19744\n(SECURITY: Buffer Overflow in xntpd).\n\nYou should install this patch for your system to be up-to-date."",
			""attribute_name"": ""description""
		},
		{
			""attribute_value"": ""2006/09/16"",
			""attribute_name"": ""plugin_publication_date""
		},
		{
			""attribute_value"": ""The remote host is missing a vendor-supplied security patch"",
			""attribute_name"": ""synopsis""
		}
	],
	""family_name"": ""AIX Local Security Checks"",
	""name"": ""AIX 5.1 : IY19744"",
	""id"": 22372
}";
			var json = $"{{ \"{nameof(TestObject.PluginDetails)}\": {jsonValue} }}";
			var result = JsonSerializer.Deserialize<TestObject>(json)!.PluginDetails!;
			var expectedAttributes = new PluginAttribute[] {
				new PluginAttribute("fname", "aix_IY19744.nasl"),
				new PluginAttribute("plugin_name", "AIX 5.1 : IY19744"),
				new PluginAttribute("script_version", "$Revision: 1.5 $"),
				new PluginAttribute("solution", "http://www-912.ibm.com/eserver/support/fixes/"),
				new PluginAttribute("risk_factor", "High"),
				new PluginAttribute(
					"description",
					"The remote host is missing AIX Critical Security Patch number IY19744\n(SECURITY: Buffer Overflow in xntpd).\n\nYou should install this patch for your system to be up-to-date."),
				new PluginAttribute("plugin_publication_date", "2006/09/16"),
				new PluginAttribute("synopsis", "The remote host is missing a vendor-supplied security patch"),
			};
			var comparer = new PluginAttributeComparer();
			Assert.NotNull(result);
			Assert.Null(result.ExtensionData);
			Assert.Equal(expectedAttributes, result.Attributes, comparer);
			Assert.Equal(
				expectedAttributes.Select(a => comparer.GetHashCode(a)),
				result.Attributes.Select(a => comparer.GetHashCode(a)));
			Assert.Equal(22372, result.Id);
			Assert.Equal("AIX 5.1 : IY19744", result.Name);
			Assert.Equal("AIX Local Security Checks", result.FamilyName);
		}

		[Fact]
		public void TestDeserializeNull() {
			var json = $"{{ \"{nameof(TestObject.PluginDetails)}\": null }}";
			var result = JsonSerializer.Deserialize<TestObject>(json)!.PluginDetails;
			Assert.Null(result);
		}


		[Theory]
		[InlineData("\"\"")]
		[InlineData("\"a;sfogihad;sgih\"")]
		[InlineData("-10")]
		[InlineData("\"CWE:79\"")]
		[InlineData("\"2017-12-31T20:40:23.447Z\"")]
		[InlineData("false")]
		[InlineData("[true]")]
		[InlineData("{}")]
		[InlineData("{\"fake\": 5, \"fake\": 5}")]
		[InlineData("{\"fake\": 5, \"category\": null, \"fake\": 5}")]
		[InlineData("{\"id\": 22372}")]
		[InlineData("{\"id\": 22372, \"name\": \"AIX 5.1 : IY19744\"}")]
		[InlineData("{\"id\": 22372, \"name\": \"AIX 5.1 : IY19744\", \"id\": \"asdf\"}")]
		[InlineData("{\"id\": 22372, \"name\": \"AIX 5.1 : IY19744\", \"name\": 0}")]
		[InlineData("{\"id\": 22372, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"family_name\": \"AIX Local Security Checks\", \"name\": \"AIX 5.1 : IY19744\", \"family_name\": false}")]
		[InlineData("{\"id\": 22372, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": false}")]
		[InlineData("{\"attributes\": null, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"attributes\": {}, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": [{}], \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": \"22372\", \"attributes\": {\"attribute_value\": \"\", \"attribute_name\": \"fname\"}, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": {\"attribute_value\": 0, \"attribute_name\": \"fname\"}, \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": [{\"attribute_value\": \"\", \"attribute_name\": \"fname\"}], \"attributes\": [{}], \"name\": \"AIX 5.1 : IY19744\", \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": [], \"name\": null, \"family_name\": \"AIX Local Security Checks\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": [], \"family_name\": \"AIX Local Security Checks\"")]
		[InlineData("{\"id\": 22372, \"attributes\": [], \"name\": \"AIX 5.1 : IY19744\"}")]
		[InlineData("{\"id\": 22372, \"attributes\": [], \"name\": \"AIX 5.1 : IY19744\", \"family_name\": null}")]
		public void TestDeserializeException(string badData) {
			Assert.Throws<JsonException>(() => {
				var json = $"{{ \"{nameof(TestObject.PluginDetails)}\": {badData} }}";
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}

		[Fact]
		public void TestWriteNull() {
			var buffer = new ArrayBufferWriter<byte>();
			using var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new PluginDetailsConverter();

			converter.Write(jsonWriter, null, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal("null", result);
		}
	}
}
