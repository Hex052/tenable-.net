using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace Tenable.Base {
	public interface ITenableBase : IDisposable {
		/// <summary>
		/// Used in functions where <c>retryLimit</c> is not provided
		/// </summary>
		int DefaultRetryLimit {
			get;
		}

		/// <summary>
		/// The RestClient that will be
		/// </summary>
		RestClient RestClient {
			get;
		}

		/// <summary>Request a JSON object from a particular REST endpoint</summary>
		/// <typeparam name="T">Type to retrieve from the endpoint</typeparam>
		/// <returns>
		/// Object parsed from the body of the response as JSON using
		/// <see cref="System.Text.Json"/>
		/// </returns>
		/// <inheritdoc cref="RequestAsync(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, int, CancellationToken)" path="/param"/>
		/// <inheritdoc cref="RetryRequestJsonAsync{T}(RestRequest, int, CancellationToken)"/>
		Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, int retryLimit,
				CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RetryRequestJsonAsync{T}(RestRequest)" path="/remarks"/>
		/// <inheritdoc cref="RequestJsonAsync{T}(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, int, CancellationToken)"/>
		Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestJsonAsync{T}(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestJsonAsync{T}(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				object? jsonBody, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestJsonAsync{T}(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<T?> RequestJsonAsync<T>(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				CancellationToken cancellationToken = default);

		/// <summary>Send a HTTP request to a particular endpoint</summary>
		/// <inheritdoc cref="CreateRequest(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?)" path="/param"/>
		/// <inheritdoc cref="RetryRequestAsync(RestRequest, int, CancellationToken)"/>
		Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, int retryLimit,
				CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RetryRequestAsync(RestRequest)" path="/remarks"/>
		/// <inheritdoc cref="RequestAsync(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, int, CancellationToken)"/>
		Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestAsync(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<string?> RequestAsync(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestAsync(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<string?> RequestAsync(string partialUrl, Method method,
				object? jsonBody, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RequestAsync(string, Method, IEnumerable{KeyValuePair{string, string}}?, object?, CancellationToken)"/>
		Task<string?> RequestAsync(string partialUrl, Method method,
				CancellationToken cancellationToken = default);

		/// <summary>
		/// Create a <see cref="RestRequest"/> from the given parameters,
		/// in one function, rather than duplicating it in all the functions.
		/// </summary>
		/// <param name="partialUrl">
		/// The partial URL path to the endpoint, which will be joined
		/// with <see cref="BaseUrl"/> in the request
		/// </param>
		/// <param name="method">
		/// The HTTP method to use to retrieve the data
		/// </param>
		/// <param name="queryParameters">
		/// The parameters to use in the query, which for some requests
		/// (like POST) are sent as form results.
		/// </param>
		/// <param name="jsonBody">
		/// The body of the JSON request, which will be converted to text
		/// using <see cref="System.Text.Json"/>
		/// </param>
		/// <returns>
		/// An <see cref="RestRequest"/> from the given paramters
		/// </returns>
		RestRequest CreateRequest(string partialUrl, Method method,
				IEnumerable<KeyValuePair<string, string>>? queryParameters,
				object? jsonBody);

		/// <typeparam name="T">
		/// Type of <see cref="RestResponse{T}.Data"/>
		/// </typeparam>
		/// <inheritdoc cref="RetryRequestAsync(RestRequest, int, CancellationToken)"/>
		Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request,
				int retryLimit, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="RetryRequestAsync(RestRequest)" path="/remarks"/>
		/// <inheritdoc cref="RetryRequestJsonAsync{T}(RestRequest, int, CancellationToken)"/>
		Task<RestResponse<T>> RetryRequestJsonAsync<T>(RestRequest request,
				CancellationToken cancellationToken = default);

		/// <summary>
		/// Try to execute a request, retrying up to <paramref name="retryLimit"/>
		/// times in case there was a temporary error
		/// </summary>
		/// <remarks>
		/// Will retry on both 429 and 5xx errors, up to
		/// <paramref name="retryLimit"/> times
		/// </remarks>
		/// <param name="request">Request to execute</param>
		/// <param name="execute">
		/// A function that executes the request.
		/// It's only argument will always be <paramref name="request"/>.
		/// </param>
		/// <param name="retryLimit">
		/// The number of times to retry the request before giving
		/// up and raising an exception
		/// </param>
		/// <returns>
		/// Response from request, after retrying
		/// </returns>
		/// <exception cref="Exceptions.BadRequestException">
		/// Something in the request could not be understood
		/// and the server returned a 400 error
		/// </exception>
		/// <exception cref="Exceptions.NoPermissionException">
		/// The user does not have permission to perform the requested action
		/// and the server returned a 403 error
		/// </exception>
		/// <exception cref="Exceptions.NotFoundException">
		/// The requested item was not found
		/// and the server returned a 404 error
		/// </exception>
		/// <exception cref="Exceptions.HttpStatusException">
		/// An unexpected status code was returned by the server
		/// </exception>
		/// <exception cref="Exceptions.RateLimitException">
		/// You are rate-limited and <paramref name="retryLimit"/> was exceeded
		/// </exception>
		/// <exception cref="Exceptions.InvalidCredentialsException">
		/// The API key is invalid
		/// and the server returned a 401 error
		/// </exception>
		/// <exception cref="Exceptions.ConflictException">
		/// The action would create a conflict
		/// and the server returned a 409 error
		/// </exception>
		/// <exception cref="Exceptions.RetryException">
		/// Some server-side error occured that is likely temporary, but
		/// <paramref name="retryLimit"/> was exceeded while reattempting
		/// </exception>
		/// <exception cref="ArgumentOutOfRangeException">
		/// <paramref name="retryLimit"/> must be greater than zero
		/// </exception>
		/// <exception cref="OperationCanceledException">
		/// <paramref name="cancellationToken"/> was canceled.
		/// </exception>
		Task<RestResponse> RetryRequestAsync(RestRequest request,
				int retryLimit, CancellationToken cancellationToken = default);
		/// <summary>
		/// Try to execute a request, retrying several
		/// times in case there was a temporary error
		/// </summary>
		/// <remarks>
		/// Will retry on both 429 and 5xx errors
		/// </remarks>
		/// <inheritdoc cref="RetryRequestAsync(RestRequest, int, CancellationToken)"/>
		Task<RestResponse> RetryRequestAsync(RestRequest request,
				CancellationToken cancellationToken = default);
	}
}
