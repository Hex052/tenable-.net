using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Tenable.IO.Agents {
	internal sealed class BulkItems {
		/// <summary>
		/// The agent ids in the bulk operation
		/// </summary>
		/// <remarks>
		/// I'm perfectly happy with this being the non-generic one, this
		/// shouldn't have to be deserialized
		/// <remarks>
		[JsonPropertyName("items")]
		private readonly IEnumerable _items;
		public IEnumerable Items {
			get => this._items;
		}

		[JsonConstructor]
		public BulkItems(IEnumerable items) {
			if (items is null) {
				throw new ArgumentNullException(nameof(items));
			}
			else if (items is ICollection || items is ICollection<object>) {
				this._items = items;
			}
			else {
				this._items = items.Cast<object>().ToArray();
			}
		}

		public override bool Equals(object? obj) {
			return ReferenceEquals(this, obj) || (obj is BulkItems other && Enumerable.SequenceEqual(this.Items.Cast<object>(), other.Items.Cast<object>()));
		}
		public override int GetHashCode() {
			unchecked {
				int hash = 17;
				foreach (var item in this._items) {
					hash = (hash * 23) + (item == null ? 0 : item.GetHashCode());
				}
				return hash;
			}
		}
	}
}
