using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.Base.JsonConverters {
	public class VersionNumberConverter : JsonConverter<Version?> {
		public override bool HandleNull => true;
		public override Version? Read(ref Utf8JsonReader reader,
				Type typeToConvert, JsonSerializerOptions options) {
			ReadOnlySpan<byte> raw;
			switch (reader.TokenType) {
				case JsonTokenType.Null:
					return null;
				case JsonTokenType.Number:
					raw = reader.ValueSpan;
					break;
				case JsonTokenType.String:
					if (reader.ValueSpan.IsEmpty) {
						throw this._formatException();
					}
					raw = reader.ValueSpan;
					break;
				default:
					throw new JsonException(
						string.Format("Token not type number, null, or string; is {0}", reader.TokenType));
			}

			if (raw[0] == ((byte)'-')) {
				throw new JsonException("Invalid format; version was negative");
			}
			for (int i = 0; i < raw.Length; i++) {
				switch (raw[i]) {
					case ((byte)'.'): {
						var second = raw.Slice(i + 1);
						var enumerator = second.GetEnumerator();
						while (enumerator.MoveNext()) {
							if (enumerator.Current == ((byte)'E') || enumerator.Current == ((byte)'e')) {
								throw this._formatException();
							}
						}
						return new Version(
							this._toInt(raw.Slice(0, i)),
							this._toInt(second));
					}
					case ((byte)'0'):
					case ((byte)'1'):
					case ((byte)'2'):
					case ((byte)'3'):
					case ((byte)'4'):
					case ((byte)'5'):
					case ((byte)'6'):
					case ((byte)'7'):
					case ((byte)'8'):
					case ((byte)'9'):
						// Is number
						break;
					default: // Also covers `((byte)'E')` and `((byte)'e')`
						throw this._formatException();
				}
			}
			// Was only numbers
			return new Version(this._toInt(raw), 0);
		}

		private JsonException _formatException() {
			return new JsonException(
				"Invalid format; number must be only digits and a single decimal");
		}

		private int _toInt(ReadOnlySpan<byte> span) {
			var result = 0;
			var enumerator = span.GetEnumerator();
			while (enumerator.MoveNext()) {
				result = (result * 10) + enumerator.Current - ((byte)'0');
			}
			return result;
		}

		public override void Write(Utf8JsonWriter writer, Version? value,
				JsonSerializerOptions options) {
			if (ReferenceEquals(value, null)) {
				writer.WriteNullValue();
			}
			else if (value.Revision >= 0 || value.Build >= 0) {
				throw new JsonException(
					string.Format("Value must be only major.minor, is {0}", value));
			}
			else {
				writer.WriteRawValue(value.ToString());
			}
		}
	}
}
