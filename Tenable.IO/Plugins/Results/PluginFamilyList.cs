using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class PluginFamilyList {
		[JsonPropertyName("families")]
		public IReadOnlyList<PluginFamily> Families {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public PluginFamilyList(IReadOnlyList<PluginFamily> families) {
			this.Families = families;
		}
	}
}
