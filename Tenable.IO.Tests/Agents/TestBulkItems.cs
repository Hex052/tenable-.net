using System;
using System.Linq;
using Tenable.IO.Agents;
using Xunit;

namespace Tenable.IO.Tests.Agents {
	public class TestBulkItems {
		[Fact]
		public void TestEquals() {
			var first = new BulkItems(new string?[] { "asdf", "1234", "third", null, "last" });
			var second = first;
			Assert.True(first.Equals(second));
			second = new BulkItems((new string?[] { "asdf", "1234", "third", null, "last" }).Select(o => o));
			Assert.True(first.Equals(second));
			Assert.Equal(first.GetHashCode(), second.GetHashCode());
			second = new BulkItems(new object?[] { "asdf", "1234", "third", null, "last" });
			Assert.True(first.Equals(second));
			Assert.Equal(first.GetHashCode(), second.GetHashCode());
			Assert.False(first.Equals("asdf"));
			Assert.False(first.Equals(null));
		}
		[Fact]
		public void TestThrows() {
			Assert.Throws<ArgumentNullException>("items", () => new BulkItems(null!));
		}
	}
}
