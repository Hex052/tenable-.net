using System.Collections.Generic;
using System.Text.Json;

namespace Tenable.IO.Exports {
	public class AssetExportFiltersConverter : ExportFiltersConverter<AssetExportFilters> {

		public sealed override void Write(Utf8JsonWriter writer, AssetExportFilters value, JsonSerializerOptions options) {
			writer.WriteStartObject();
			// dates
			if (value.CreatedAt != null) {
				writer.WritePropertyName("created_at");
				this._dateConverter.Write(writer, value.CreatedAt.Value, options);
			}
			if (value.UpdatedAt != null) {
				writer.WritePropertyName("updated_at");
				this._dateConverter.Write(writer, value.UpdatedAt.Value, options);
			}
			if (value.TerminatedAt != null) {
				writer.WritePropertyName("terminated_at");
				this._dateConverter.Write(writer, value.TerminatedAt.Value, options);
			}
			if (value.DeletedAt != null) {
				writer.WritePropertyName("deleted_at");
				this._dateConverter.Write(writer, value.DeletedAt.Value, options);
			}
			if (value.FirstScanTime != null) {
				writer.WritePropertyName("first_scan_time");
				this._dateConverter.Write(writer, value.FirstScanTime.Value, options);
			}
			if (value.LastAuthenticatedScanTime != null) {
				writer.WritePropertyName("last_authenticated_scan_time");
				this._dateConverter.Write(writer, value.LastAuthenticatedScanTime.Value, options);
			}
			if (value.LastAssessed != null) {
				writer.WritePropertyName("last_assessed");
				this._dateConverter.Write(writer, value.LastAssessed.Value, options);
			}

			// bools
			if (value.IsDeleted != null) {
				writer.WritePropertyName("is_deleted");
				JsonSerializer.Serialize<bool>(writer, value.IsDeleted.Value, options);
			}
			if (value.IsLicensed != null) {
				writer.WritePropertyName("is_licensed");
				JsonSerializer.Serialize<bool>(writer, value.IsLicensed.Value, options);
			}
			if (value.ServiceNowSysId != null) {
				writer.WritePropertyName("servicenow_sysid");
				JsonSerializer.Serialize<bool>(writer, value.ServiceNowSysId.Value, options);
			}
			if (value.HasPluginResults != null) {
				writer.WritePropertyName("has_plugin_results");
				JsonSerializer.Serialize<bool>(writer, value.HasPluginResults.Value, options);
			}

			//List
			if (value.Sources != null) {
				writer.WritePropertyName("sources");
				JsonSerializer.Serialize<IEnumerable<string>>(writer, value.Sources, options);
			}

			base.Write(writer, value, options);
			writer.WriteEndObject();
		}
	}
}
