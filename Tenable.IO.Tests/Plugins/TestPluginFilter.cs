using System;
using System.Text.Json;
using Tenable.IO.Plugins;
using Xunit;

namespace Tenable.IO.Tests.Plugins {
	public class TestPluginFilter {
		[Fact]
		public void TestDeserialize() {
			var json = @"{
	""page"": 1,

	""size"": 2,
	""last_updated"": """"
}";
			var pluginFilter = JsonSerializer.Deserialize<PluginFilter>(json)!;
			Assert.NotNull(pluginFilter);
			Assert.Null(pluginFilter.ExtensionData);
			Assert.Equal(1, pluginFilter.Page);
			Assert.Equal(2, pluginFilter.Size);
			Assert.False(pluginFilter.LastUpdated.HasValue);
		}
		[Fact]
		public void TestDeserializeWithDate() {
			var json = @"{
	""page"": 56,

	""size"": 150,
	""last_updated"": ""2021-12-04""
}";
			var pluginFilter = JsonSerializer.Deserialize<PluginFilter>(json);
			Assert.NotNull(pluginFilter);
			Assert.Equal(56, pluginFilter!.Page);
			Assert.Equal(150, pluginFilter.Size);
			Assert.Equal(new DateTime(2021, 12, 4), pluginFilter.LastUpdated);
		}

		[Fact]
		public void TestOutOfRangeException() {
			var filter = new PluginFilter();

			var old = filter.Size;
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Size = -1);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Size = 0);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Size = int.MinValue);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Size = int.MaxValue);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Size = 10000000);
			Assert.Equal(old, filter.Size);

			old = filter.Page;
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Page = 0);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Page = -1);
			Assert.Throws<ArgumentOutOfRangeException>(() => filter.Page = int.MinValue);
			Assert.Equal(old, filter.Page);
		}
	}
}
