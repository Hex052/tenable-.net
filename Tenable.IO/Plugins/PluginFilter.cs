using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.IO.Plugins.Results.JsonConverters;

namespace Tenable.IO.Plugins {
	public class PluginFilter {
		/// <summary>
		/// The last updated date to filter on;
		/// only plugins after the this date will be returned.
		/// </summary>
		[JsonPropertyName("last_updated")]
		[JsonConverter(typeof(DateConverter))]
		public DateTime? LastUpdated {
			get; set;
		}

		/// <summary>
		/// The number of records to include in the result set per page
		/// </summary>
		[JsonPropertyName("size")]
		public int Size {
			get => this._size;
			set {
				if (value < 1 || value > 10000) {
					throw new ArgumentOutOfRangeException(nameof(value), "Must be positive and max is 10,000");
				}
				else {
					this._size = value;
				}
			}
		}
		private int _size;

		/// <summary>
		/// The page number of the first page of the results to return
		/// </summary>
		[JsonPropertyName("page")]
		public int Page {
			get => this._page;
			set {
				if (value < 1) {
					throw new ArgumentOutOfRangeException(nameof(value), "Must be positive");
				}
				else {
					this._page = value;
				}
			}
		}
		private int _page;

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		public PluginFilter() {
			this.Page = 1;
			this.Size = 500;
		}
	}
}
