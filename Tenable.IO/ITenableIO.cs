using Tenable.Base;

namespace Tenable.IO {
	/// <summary>
	/// This object allows interfacing with the Tenable.io APIs
	/// </summary>
	public interface ITenableIO : ITenableBase {
		/// <summary>
		/// The access key to the tenable.io API
		/// </summary>
		string AccessKey {
			get;
		}
		/// <summary>
		/// The secret key to the tenable.io API
		/// </summary>
		string SecretKey {
			get;
		}
		/// <summary>
		/// Returns the value that will be used in the X-APIKeys header
		/// </summary>
		string HeaderValue {
			get;
		}

		object AccessGroups {
			get;
		}
		AgentConfig.IAgentConfigApi AgentConfig {
			get;
		}
		object AgentGroups {
			get;
		}
		object AgentExclusions {
			get;
		}
		Agents.IAgentsApi Agents {
			get;
		}
		object Assets {
			get;
		}
		object AuditLog {
			get;
		}
		object Credentials {
			get;
		}
		object Editor {
			get;
		}
		object Exclusions {
			get;
		}
		Exports.IExportsApi Exports {
			get;
		}
		object Files {
			get;
		}
		object Filters {
			get;
		}
		object Folders {
			get;
		}
		object Groups {
			get;
		}
		Networks.INetworksApi Networks {
			get;
		}
		object Permissions {
			get;
		}
		Plugins.IPluginsApi Plugins {
			get;
		}
		object Policies {
			get;
		}
		object ScannerGroups {
			get;
		}
		object Scanners {
			get;
		}
		object Scans {
			get;
		}
		Server.IServerApi Server {
			get;
		}
		object Session {
			get;
		}
		object Tags {
			get;
		}
		object TargetGroups {
			get;
		}
		object Users {
			get;
		}
		object Workbenches {
			get;
		}
	}
}
