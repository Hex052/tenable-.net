using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.Base {
	public class Pagination : IPagination {
		/// <summary>The total number of items</summary>
		[JsonPropertyName("total")]
		public int Total {
			get; set;
		}

		/// <summary>The number of items requested in this page</summary>
		[JsonPropertyName("limit")]
		public int Limit {
			get; set;
		}

		/// <summary>The number of items before the start of this page</summary>
		[JsonPropertyName("offset")]
		public int Offset {
			get; set;
		}

		/// <summary>The fields and sort order used in the request</summary>
		[JsonPropertyName("sort")]
		public IReadOnlyList<SortOrder> Sort {
			get; set;
		}

		[JsonConstructor]
		public Pagination(int total, int limit, int offset,
				IReadOnlyList<SortOrder> sort) {
			this.Total = total;
			this.Limit = limit;
			this.Offset = offset;
			this.Sort = sort;
		}
	}
}
