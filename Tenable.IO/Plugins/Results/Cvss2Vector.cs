using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Plugins.Results {
	public class Cvss2Vector {
		/// <summary>
		/// The CVSS Access Complexity (AC) metric for the vulnerability
		/// </summary>
		[JsonPropertyName("AccessComplexity")]
		public string? AccessComplexity {
			get; set;
		}
		/// <summary>
		/// The CVSS Attack Vector (AV) metric for the vulnerability
		/// </summary>
		[JsonPropertyName("AccessVector")]
		public string? AccessVector {
			get; set;
		}
		/// <summary>
		/// The CVSS Authentication (Au) metric for the vulnerability
		/// </summary>
		[JsonPropertyName("Authentication")]
		public string? Authentication {
			get; set;
		}
		/// <summary>
		/// The CVSS confidentiality impact metric of the
		/// vulnerability to the vulnerable component
		/// </summary>
		[JsonPropertyName("Confidentiality-Impact")]
		public string? ConfidentialityImpact {
			get; set;
		}
		/// <summary>
		/// The CVSSv3 integrity impact metric for the vulnerability
		/// </summary>
		[JsonPropertyName("Integrity-Impact")]
		public string? IntegrityImpact {
			get; set;
		}
		/// <summary>
		/// The CVSSv2 availability impact metric for the vulnerability
		/// </summary>
		[JsonPropertyName("Availability-Impact")]
		public string? AvailabilityImpact {
			get; set;
		}
		/// <summary>
		/// The complete metrics and result values for the
		/// vulnerability in a condensed and coded format
		/// </summary>
		/// <example><code>"AV:N/AC:M/Au:N/C:C/I:C/A:C"</code></example>
		[JsonPropertyName("raw")]
		public string Raw {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Cvss2Vector(string raw) {
			this.Raw = raw;
		}
	}
}
