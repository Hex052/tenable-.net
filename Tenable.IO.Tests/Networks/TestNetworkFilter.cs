using System;
using System.Collections.Generic;
using Tenable.Base;
using Tenable.IO.Networks;
using Xunit;

namespace Tenable.IO.Tests.Networks {
	public class TestNetworkFilter {
		[Fact]
		public void TestEquality() {
			var first = new NetworkFilter();
			var second = new NetworkFilter() {
				Filter = new List<Tuple<string, string, string>>(),
				Sort = new List<SortOrder>(),
			};
			var different = new NetworkFilter() {
				Filter = new List<Tuple<string, string, string>>() {
					new Tuple<string, string, string>("name", "eq", "default"),
					new Tuple<string, string, string>("name", "eq", "Default"),
					new Tuple<string, string, string>("name", "eq", "DEFAULT"),
				},
				FilterType = FilterType.OR,
			};
			Assert.False(first.Equals(default(NetworkFilter)));
			Assert.False(first!.Equals(default(object)));
			Assert.False(first!.Equals("(object?)null"));
			Assert.True(first!.Equals((object)first));
			Assert.True(first!.Equals(first));
			Assert.True(first!.Equals(second));
			Assert.True(second!.Equals(first));
			Assert.False(first!.Equals(different));
			Assert.False(different!.Equals(first));
			Assert.False(first!.Equals(different));
			Assert.True(first == second);
#pragma warning disable CS1718 // Comparison made to same variable
			Assert.True(first == first);
#pragma warning restore CS1718 // Comparison made to same variable
			Assert.True(first != default(NetworkFilter));
			Assert.True(default(NetworkFilter) != first);
			Assert.True(((object)first!).Equals((object?)second));
			Assert.NotEqual(new NetworkFilter().GetHashCode(), new NetworkFilter().GetHashCode());
			Assert.NotEqual(first.GetHashCode(), second.GetHashCode());
			Assert.Equal(different.GetHashCode(), different.GetHashCode());
		}
	}
}
