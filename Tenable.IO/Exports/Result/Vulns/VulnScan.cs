using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports.Result.Vulns {
	public class VulnScan {
		/// <summary>
		/// When the scan completed
		/// </summary>
		[JsonPropertyName("completed_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime CompletedAt {
			get; set;
		}

		/// <summary>
		/// The schedule UUID for the scan
		/// </summary>
		/// <remarks>
		/// The type is <see cref="System.String"/> because
		/// I'm unaware of any type in .NET that is designed for 48 hex digits,
		/// since <see cref="System.Guid"/> is only for 128 bits (32 hex digits)
		/// </remarks>
		[JsonPropertyName("schedule_uuid")]
		public string ScheduleUuid {
			get; set;
		}

		/// <summary>
		/// When the scan started
		/// </summary>
		[JsonPropertyName("started_at")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime StartedAt {
			get; set;
		}

		/// <summary>
		/// The UUID of the scan
		/// </summary>
		[JsonPropertyName("uuid")]
		public string Uuid {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public VulnScan(DateTime completedAt, string scheduleUuid,
				DateTime startedAt, string uuid) {
			this.CompletedAt = completedAt;
			this.ScheduleUuid = scheduleUuid;
			this.StartedAt = startedAt;
			this.Uuid = uuid;
		}
	}
}
