using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public class VulnExportVprScore {
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("eq")]
		public List<float>? Equal {
			get; set;
		}
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("neq")]
		public List<float>? NotEqual {
			get; set;
		}

		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("gt")]
		public float? GreaterThan {
			get; set;
		}
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("gte")]
		public float? GreaterThanOrEqual {
			get; set;
		}
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("lt")]
		public float? LessThan {
			get; set;
		}
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("lte")]
		public float? LessThanOrEqual {
			get; set;
		}
	}
}
