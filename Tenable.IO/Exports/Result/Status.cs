using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result {
	[JsonConverter(typeof(JsonStringEnumConverter))]
	public enum Status {
		/// <summary>
		/// The export request has been queued until
		/// other requests currently in process are completed
		/// </summary>
		QUEUED,
		/// <summary>
		/// The export request is being processed,
		/// not all results (possibly no results) are available yet
		/// </summary>
		PROCESSING,
		/// <summary>
		/// The export request is finished, the results are available and
		/// <see cref="RequestExportStatus.ChunksAvailable"/> is complete
		/// </summary>
		FINISHED,
		/// <summary>
		/// The export request was canceled
		/// </summary>
		CANCELLED,
		/// <summary>
		/// An error was encountered, the request should likely be retried
		/// </summary>
		/// <remarks>
		/// <see href="https://developer.tenable.com/reference/exports-assets-export-status"/>
		/// recommends you contact support if errors continue
		/// </remarks>
		ERROR,
	}
}
