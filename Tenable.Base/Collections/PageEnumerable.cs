using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;
using Tenable.Base.Exceptions;

namespace Tenable.Base.Collections {
	/// <summary>
	///
	/// </summary>
	/// <typeparam name="TResult">Results of the enumeration</typeparam>
	/// <typeparam name="TPage">The full page that gets returned</typeparam>
	/// <typeparam name="TPagination"></typeparam>
	public abstract class PageEnumerable<TResult, TPage, TPagination> : ResultEnumerable<TResult, TPage> where TPage : class, IPage<TResult, TPagination> where TPagination : IPagination {
		protected PageEnumerable(ITenableBase api) : base(api) {
		}

		/// <summary>
		/// The query parameters that are needed to get a page
		/// </summary>
		protected abstract IEnumerable<KeyValuePair<string, string>> QueryParameters(int pageId);

		/// <summary>
		/// Unlike <see cref="GetPage"/>, this returns not just the items in a page
		/// </summary>
		/// <param name="pageId">Page number to retrieve</param>
		/// <returns>
		/// The full <see cref="IPage{TItem, TPagination}"/> from the API
		/// </returns>
		public async ValueTask<TPage> GetFullPageAsync(int pageId,
				CancellationToken cancellationToken = default) {
			WeakReference<TPage>? listref;
			TPage? result;
			if (this.PageCache.TryGetValue(pageId, out listref)
					&& listref.TryGetTarget(out result!)) {
				return result;
			}
			string url = this.RelativePageUri(pageId);
			result = await this.Api.RequestJsonAsync<TPage>(
				url,
				Method.Get,
				queryParameters: this.QueryParameters(pageId),
				cancellationToken);
			if (result is null) {
				throw new UnexpectedNullResultException(url);
			}
			else if (listref is null) {
				this.PageCache[pageId] = new WeakReference<TPage>(result);
			}
			else {
				listref.SetTarget(result);
			}
			return result;
		}

		public override async ValueTask<IReadOnlyList<TResult>> GetPageAsync(int pageId,
				CancellationToken cancellationToken = default) {
			return (await this.GetFullPageAsync(pageId, cancellationToken)).Items;
		}
	}
}
