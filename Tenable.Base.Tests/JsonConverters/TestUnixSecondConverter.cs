using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestUnixSecondConverter {
		public class TestObject {
			[JsonConverter(typeof(UnixSecondConverter))]
			public DateTime Time {
				get; set;
			}
		}

		[Theory]
		[InlineData(0, "1970-01-01T00:00:00")]
		[InlineData(1621057832, "2021-05-15T05:50:32")]
		public void FromInt(int timestamp, string time) {
			var expected = DateTime.Parse(time);
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.Time), timestamp);
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Time;
			Assert.Equal(expected: expected, actual: result);
		}

		[Theory]
		[InlineData("\"\"")]
		[InlineData("\"a;sfogihad;sgih\"")]
		[InlineData("\"2021-05-15T05:50:32\"")]
		[InlineData("\"1621057832\"")]
		[InlineData("false")]
		[InlineData("{}")]
		[InlineData("[]")]
		[InlineData("null")]
		[InlineData("999999999999999999999999999999999999999999999999999999")]
		public void FromIntException(string badData) {
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.Time), badData);
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}

		[Theory]
		[InlineData(0, "1970-01-01T00:00:00")]
		[InlineData(1621057832, "2021-05-15T05:50:32")]
		public void ToInt(int timestamp, string time) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new UnixSecondConverter();

			converter.Write(jsonWriter, DateTime.Parse(time), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected: timestamp.ToString(), actual: result);
		}
	}
}
