using RestSharp;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.IO.Agents.Results;

namespace Tenable.IO.Agents {
	public class AgentsApi : IAgentsApi {
		public ITenableIO Api {
			get;
		}

		internal AgentsApi(ITenableIO api) {
			this.Api = api;
		}

		public AgentEnumerable List(AgentFilter filter, int scannerId) {
			return new AgentEnumerable(this.Api, filter, scannerId);
		}
		public AgentEnumerable List(AgentFilter filter) {
			return this.List(filter, 1);
		}
		public AgentEnumerable List(int scannerId) {
			return this.List(new AgentFilter(), scannerId);
		}
		public AgentEnumerable List() {
			return this.List(new AgentFilter(), 1);
		}

		public async Task<Agent> DetailsAsync(int agentId, int scannerId,
				CancellationToken cancellationToken = default) {
			string partialUrl = string.Format("scanners/{0}/agents/{1}", scannerId, agentId);
			return await this.Api.RequestJsonAsync<Agent>(
				partialUrl,
				Method.Get,
				cancellationToken)
				?? throw new UnexpectedNullResultException(partialUrl);
		}
		public Task<Agent> DetailsAsync(int agentId,
				CancellationToken cancellationToken = default) {
			return this.DetailsAsync(agentId, 1, cancellationToken);
		}

		public async Task<EmptyJsonObject> UnlinkAsync(int agentId, int scannerId, CancellationToken cancellationToken = default) {
			return await this.Api.RequestJsonAsync<EmptyJsonObject>(
				string.Format("scanners/{0}/agents/{1}", scannerId, agentId),
				Method.Delete,
				cancellationToken)
				?? new EmptyJsonObject();
		}
		public Task<EmptyJsonObject> UnlinkAsync(int agentId, CancellationToken cancellationToken = default) {
			return this.UnlinkAsync(agentId, 1, cancellationToken);
		}
		// TODO replace these to corrospond to the updated API
		public Task<EmptyJsonObject> UnlinkAsync(IEnumerable<int> agentIds,
				int scannerId, CancellationToken cancellationToken = default) {
			return this._unlinkAsync(agentIds, scannerId, cancellationToken);
		}
		public Task<EmptyJsonObject> UnlinkAsync(IEnumerable<int> agentIds,
				CancellationToken cancellationToken = default) {
			return this._unlinkAsync(agentIds, 1, cancellationToken);
		}
		public Task<EmptyJsonObject> UnlinkAsync(IEnumerable<string> agentIds,
				int scannerId, CancellationToken cancellationToken = default) {
			return this._unlinkAsync(agentIds, scannerId, cancellationToken);
		}
		public Task<EmptyJsonObject> UnlinkAsync(IEnumerable<string> agentIds, CancellationToken cancellationToken = default) {
			return this._unlinkAsync(agentIds, 1, cancellationToken);
		}
		private async Task<EmptyJsonObject> _unlinkAsync(IEnumerable agentIds,
				int scannerId, CancellationToken cancellationToken = default) {
			return await this.Api.RequestJsonAsync<EmptyJsonObject>(
				string.Format("scanners/{0}/agents/_bulk/unlink", scannerId),
				Method.Post,
				new BulkItems(agentIds),
				cancellationToken)
				?? new EmptyJsonObject();
		}
	}
}
