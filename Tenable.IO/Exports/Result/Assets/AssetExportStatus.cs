using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result.Assets {
	public class AssetExportStatus : RequestExportStatus {
		[JsonConstructor]
		public AssetExportStatus(Status status, IReadOnlyList<int> chunksAvailable) : base(status, chunksAvailable) {
		}
	}
}
