using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public class VulnExportOptions {
		/// <summary>
		/// Specifies the number of assets used to chunk the vulnerabilities.
		/// The vulnerabilities export is split up by number of
		/// asset IDs in a chunk.
		/// </summary>
		[JsonPropertyName("num_assets")]
		public int AssetsPerChunk {
			get; set;
		} = 50;

		/// <summary>
		/// Specifies whether or not to include unlicensed assets.
		/// </summary>
		[JsonPropertyName("include_unlicensed")]
		public bool IncludeUnlicensed {
			get; set;
		} = false;

		/// <summary>
		/// Specifies filters for exported vulnerabilities.
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
		[JsonPropertyName("filters")]
		[JsonConverter(typeof(VulnExportFiltersConverter))]
		public VulnExportFilters? Filters {
			get; set;
		}
	}
}
