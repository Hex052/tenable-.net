using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Agents.Results {
	public class Group {
		/// <summary>unique name of the agent group</summary>
		[JsonPropertyName("name")]
		public string Name {
			get;set;
		}

		/// <summary>unique ID of the agent group</summary>
		[JsonPropertyName("id")]
		public int Id {
			get;set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Group(string name, int id) {
			this.Name = name ?? throw new ArgumentNullException(nameof(name));
			this.Id = id;
		}
	}
}
