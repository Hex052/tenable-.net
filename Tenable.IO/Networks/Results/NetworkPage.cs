using System.Collections.Generic;
using System.Text.Json.Serialization;
using Tenable.Base;

namespace Tenable.IO.Networks.Results {
	public class NetworkPage : IPage<Network, Pagination> {
		/// <summary>The Networks from the API</summary>
		[JsonPropertyName("networks")]
		public IReadOnlyList<Network> Items {
			get; set;
		}

		/// <summary>The pagination information</summary>
		[JsonPropertyName("pagination")]
		public Pagination Pagination {
			get; set;
		}

		[JsonConstructor]
		public NetworkPage(IReadOnlyList<Network> items, Pagination pagination) {
			this.Items = items;
			this.Pagination = pagination;
		}
	}
}
