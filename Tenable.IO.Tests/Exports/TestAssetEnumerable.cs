using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Exceptions;
using Tenable.IO.Exports;
using Tenable.IO.Exports.Result;
using Tenable.IO.Exports.Result.Assets;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestAssetEnumerable {
		[Theory]
		[InlineData("a33797d3-863f-4175-aab8-7a07d24dbe16", 88)]
		[InlineData("0c10bf2a-0ff6-4a8f-9a27-5c4c5c879350", 5)]
		public async Task TestGetPageAsync(string uuid, int pageId) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var expectedUrl = string.Format("assets/export/{0}/chunks/{1}", uuid, pageId);
			var result = new[] {
				new Asset(
					Guid.Empty,
					DateTime.MinValue,
					DateTime.MaxValue,
					DateTime.MinValue,
					DateTime.MaxValue,
					Guid.Empty,
					"default"),
			};
			var api = new AssetEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<IReadOnlyList<Asset>>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<IReadOnlyList<Asset>?>(result));

			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.GetPageAsync(pageId, tokenSource.Token));
			// Second call should be cached
			Assert.Same(result, await api.GetPageAsync(pageId, tokenSource.Token));

			tio.Verify(
				tio => tio.RequestJsonAsync<IReadOnlyList<Asset>>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestGetPageAsyncThrow() {
			Guid uuid = new Guid("970e9334-b08c-41b4-8235-0ffd6fe36d0d");
			int pageId = 4;
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var expectedUrl = string.Format("assets/export/{0}/chunks/{1}", uuid, pageId);
			var result = new[] {
				new Asset(
					Guid.Empty,
					DateTime.MinValue,
					DateTime.MaxValue,
					DateTime.MinValue,
					DateTime.MaxValue,
					Guid.Empty,
					"default"),
			};
			var api = new AssetEnumerable(tio.Object, uuid);
			tio.Setup(tio => tio.RequestJsonAsync<IReadOnlyList<Asset>>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<IReadOnlyList<Asset>?>(null));

			using var tokenSource = new CancellationTokenSource();
			await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetPageAsync(pageId, tokenSource.Token).AsTask());

			tio.Verify(
				tio => tio.RequestJsonAsync<IReadOnlyList<Asset>>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("9485f311-70ea-495d-9c72-d5675d1875a8")]
		[InlineData("9135ef00-366d-47ca-9ad7-0e98d827922b")]
		public async Task TestGetStatusAsync(string uuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new AssetExportStatus(Status.QUEUED, Array.Empty<int>());
			var expectedUrl = string.Format("assets/export/{0}/status", uuid);
			var api = new AssetEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<AssetExportStatus>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(result);

			using var tokenSource = new CancellationTokenSource();
			Assert.Same(result, await api.GetStatusAsync(tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("f5d237e5-e4c6-41bc-8d29-912383cd408e")]
		[InlineData("9072afcd-817c-434d-a660-1642887b3fdf")]
		public async Task TestGetStatusAsyncThrow(string uuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new AssetExportStatus(Status.QUEUED, Array.Empty<int>());
			var expectedUrl = string.Format("assets/export/{0}/status", uuid);
			var api = new AssetEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<AssetExportStatus>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(default(AssetExportStatus));

			using var tokenSource = new CancellationTokenSource();
			await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.GetStatusAsync(tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<AssetExportStatus>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Theory]
		[InlineData("8cdfb673-4f61-48e4-a509-d171f5caa230")]
		[InlineData("698f7c3d-2be9-4437-a4b8-c954461c8580")]
		public async Task TestCancel(string uuid) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var result = new ExportStatus(Status.CANCELLED);
			var expectedUrl = string.Format("assets/export/{0}/cancel", uuid);
			var api = new AssetEnumerable(tio.Object, new Guid(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<ExportStatus>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<ExportStatus?>(result));

			using var tokenSource = new CancellationTokenSource();
			Assert.Equal(Status.CANCELLED, await api.CancelAsync(tokenSource.Token));
			tio.Verify(
				tio => tio.RequestJsonAsync<ExportStatus>(
					It.Is<string>(expectedUrl, StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public void TestGetEnumerator() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var expected = string.Format("assets/export/{0}/cancel", Guid.Empty);
			var api = new AssetEnumerable(tio.Object, Guid.Empty);
			Assert.Equal(
				default(CancellationToken),
				Assert.IsAssignableFrom<ExportEnumerator<Asset, AssetExportStatus>>(api.GetEnumerator()).CancellationToken);
			using var tokenSource = new CancellationTokenSource();
			Assert.Equal(
				tokenSource.Token,
				Assert.IsAssignableFrom<ExportEnumerator<Asset, AssetExportStatus>>(api.GetAsyncEnumerator(tokenSource.Token)).CancellationToken);
			tio.VerifyNoOtherCalls();
		}
	}
}
