using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.IO.Plugins.Results.JsonConverters;

namespace Tenable.IO.Plugins.Results {
	[JsonConverter(typeof(PluginDetailsConverter))]
	public class PluginDetails : IReadOnlyDictionary<string, string> {
		[JsonPropertyName("attributes")]
		public IReadOnlyList<PluginAttribute> Attributes {
			get; set;
		}

		[JsonPropertyName("family_name")]
		public string FamilyName {
			get; set;
		}
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}

		[JsonIgnore]
		public IEnumerable<string> Keys => this.Attributes.Select(a => a.AttributeName);

		[JsonIgnore]
		public IEnumerable<string> Values => this.Attributes.Select(a => a.AttributeValue);

		[JsonIgnore]
		public int Count => this.Attributes.Count;

		[JsonIgnore]
		public string this[string key] {
			get {
				if (key == null) {
					throw new ArgumentNullException(nameof(key));
				}
				try {
					return this.Attributes.First(a => a.AttributeName == key).AttributeValue;
				}
				catch (InvalidOperationException exc) {
					throw new KeyNotFoundException(string.Format("Key {0} not found", key), exc);
				}
			}
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public PluginDetails(IReadOnlyList<PluginAttribute> attributes,
				string familyName, string name, int id) {
			this.Attributes = attributes ?? throw new ArgumentNullException(nameof(attributes));
			this.FamilyName = familyName ?? throw new ArgumentNullException(nameof(familyName));
			this.Name = name ?? throw new ArgumentNullException(nameof(name));
			this.Id = id;
		}

		public bool ContainsKey(string key) {
			return this.Keys.Contains(key ?? throw new ArgumentNullException(nameof(key)));
		}

		public bool TryGetValue(string key, [MaybeNullWhen(false)] out string value) {
			if (key == null) {
				throw new ArgumentNullException(nameof(key));
			}
			value = this.Attributes.FirstOrDefault(a => a.AttributeName == key)?.AttributeValue;
			return value != null;
		}

		public IEnumerator<KeyValuePair<string, string>> GetEnumerator() {
			IList<KeyValuePair<string, string>> result = new KeyValuePair<string, string>[this.Attributes.Count];
			for (int i = 0; i < this.Attributes.Count; i++) {
				result[i] = new KeyValuePair<string, string>(
					this.Attributes[i].AttributeName,
					this.Attributes[i].AttributeValue);
			}
			return result.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
	}
}
