using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result.Assets {
	public class Tag {
		/// <summary>The UUID of the tag</summary>
		[JsonPropertyName("uuid")]

		public Guid Uuid {
			get; set;
		}

		/// <summary>
		/// The tag category (the first half of the category:value pair)
		/// </summary>
		[JsonPropertyName("key")]
		public string Key {
			get; set;
		}

		/// <summary>
		/// The tag value (the second half of the category:value pair)
		/// </summary>
		[JsonPropertyName("value")]
		public string Value {
			get; set;
		}

		/// <summary>
		/// UUID of the user who added the tag to the asset
		/// </summary>
		[JsonPropertyName("added_by")]
		public Guid AddedBy {
			get; set;
		}

		/// <summary>Time tag was added</summary>
		[JsonPropertyName("added_at")]
		public DateTime AddedAt {
			get; set;
		}

		[JsonConstructor]
		public Tag(Guid uuid, string key, string value,
				Guid addedBy, DateTime addedAt) {
			this.Uuid = uuid;
			this.Key = key;
			this.Value = value;
			this.AddedBy = addedBy;
			this.AddedAt = addedAt;
		}
	}
}
