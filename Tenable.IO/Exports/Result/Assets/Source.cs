using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result.Assets {
	public class Source {
		/// <summary>
		/// The name of the entity that reported the asset details.
		/// Sources can include sensors, connectors, and API imports.
		/// </summary>
		/// <remarks>
		/// Source names can be customized by your organization
		/// (for example, you specify a name when you import asset records).
		/// If your organization does not customize source names,
		/// system-generated names include:
		/// <c>"AWS"</c>, <c>"NESSUS_AGENT"</c>,
		/// <c>PVS</c> (Nessus Network monitor), <c>NESSUS_SCAN</c>,
		/// <c>WAS</c> (Web Application Scanning)
		/// </remarks>
		[JsonPropertyName("name")]

		public string Name {
			get; set;
		}
		/// <summary>Time the source first reported the asset</summary>
		[JsonPropertyName("first_seen")]
		public DateTime FirstSeen {
			get; set;
		}
		/// <summary>Time the source last reported the asset</summary>
		[JsonPropertyName("last_seen")]
		public DateTime LastSeen {
			get; set;
		}

		[JsonConstructor]
		public Source(string name, DateTime firstSeen, DateTime lastSeen) {
			this.Name = name;
			this.FirstSeen = firstSeen;
			this.LastSeen = lastSeen;
		}
	}
}
