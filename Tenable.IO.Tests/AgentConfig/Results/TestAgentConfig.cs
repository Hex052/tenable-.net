using System;
using System.Text.Json;
using Xunit;

namespace Tenable.IO.Tests.AgentConfig.Results {
	public class TestAgentConfig {
		[Theory]
		[InlineData(true, 250, false)]
		[InlineData(false, 1, false)]
		[InlineData(false, 1, true)]
		public void TestDeserialize(bool softwareUpdate, int expiration, bool enabled) {
			var json = string.Format(
				"{{ \"software_update\": {0}, \"auto_unlink\": "
				+ "{{ \"expiration\": {1}, \"enabled\": {2} }} }}",
				softwareUpdate ? "true" : "false",
				expiration,
				enabled ? "true" : "false");
			var result = JsonSerializer
				.Deserialize<Tenable.IO.AgentConfig.Results.AgentConfig>(json)!;
			Assert.Equal(softwareUpdate, result.SoftwareUpdate);
			Assert.Equal(expiration, result.AutoUnlink!.Expiration);
			Assert.Equal(enabled, result.AutoUnlink!.Enabled);
		}
		[Theory]
		[InlineData("{}")]
		[InlineData("{\"software_update\": null}")]
		[InlineData("{\"software_update\": true, \"auto_unlink\": null}")]
		[InlineData("{\"auto_unlink\": { \"expiration\": null} }")]
		[InlineData("{\"auto_unlink\": { \"enabled\": null } }")]
		public void TestDeserializeFail(string data) {
			Assert.Throws<ArgumentNullException>(() => {
				return JsonSerializer
					.Deserialize<Tenable.IO.AgentConfig.Results.AgentConfig>(data);
			});
		}

		[Fact]
		public void TestConstruct() {
			var config = new IO.AgentConfig.Results.AgentConfig();
			Assert.Null(config.SoftwareUpdate);
			Assert.Null(config.AutoUnlink);
		}
	}
}
