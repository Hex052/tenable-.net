using System;
using System.Collections.Generic;
using System.Text.Json;
using Tenable.IO.Plugins.Results;
using Tenable.IO.Plugins.Results.Comparers;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results {
	public class TestPlugin {
		[Fact]
		public void TestDeserialize() {
			var plugins = JsonSerializer.Deserialize<IList<Plugin>>(Json);
			Assert.NotNull(plugins);
			var comparer = new PluginComparer();
			// Assert.Equal(Expected, plugins, comparer);
			var pluginsEnumerator = plugins!.GetEnumerator();
			var expectedEnumerator = Expected.GetEnumerator();
			var count = 0;
			while (true) {
				var pnext = pluginsEnumerator.MoveNext();
				if (pnext != expectedEnumerator.MoveNext()) {
					throw new Exception($"at pos {count}");
				}
				else if (!pnext) {
					return;
				}
				try {
					Assert.Equal(
						expectedEnumerator.Current,
						pluginsEnumerator.Current,
						comparer);
					Assert.Equal(
						comparer.GetHashCode(expectedEnumerator.Current),
						comparer.GetHashCode(pluginsEnumerator.Current));
				}
				catch (Exception exc) {
					throw new Exception($"at pos {count}", exc);
				}
				++count;
			}
		}

		private static IList<Plugin> Expected = new List<Plugin>() {
			new Plugin(
				52668,
				"F-Secure Anti-Virus Detection and Status",
				new PluginAttributes(
					"F-Secure Anti-Virus is installed on the remote host. However, there is a problem with the installation; either its services are not running or its engine and/or virus definitions are out of date. Note that the status was obtained by querying 'polutil.exe'.",
					"LOCAL",
					"SENSOR",
					RiskFactor.Critical){
						Synopsis = "An antivirus application is installed on the remote host, but it is not working properly.",
						Solution = "Make sure that updates are working and the associated services are running.",
						SeeAlso = new List<string>(1) { "http://www.nessus.org/u?e1b65d56" },
						PublicationDate = new DateTime(2011, 3, 14, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitAvailable = false,
						Cpe = new List<string>(1) {"cpe:/a:f-secure:anti-virus" },
						ModificationDate = new DateTime(2021, 12, 31, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.532"),
						AlwaysRun = false,
						Compliance = false,
						CvssVector = new Cvss2Vector("AV:N/AC:L/Au:N/C:C/I:C/A:C") {
							AccessVector= "Network",
							AccessComplexity= "Low",
							Authentication= "None required",
							ConfidentialityImpact= "Complete",
							IntegrityImpact= "Complete",
							AvailabilityImpact= "Complete"
						},
						CvssBaseScore = 10,
						Cvss3Vector = new Cvss3Vector("AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H") {
							AttackVector= "Network",
							AttackComplexity= "Low",
							PrivilegesRequired= "None",
							UserInteraction= "None",
							Scope= "Unchanged",
							ConfidentialityImpact= "High",
							IntegrityImpact= "High",
							AvailabilityImpact= "High"
						},
						Cvss3BaseScore = new decimal(9.8),
						Cve = new List<string>(0),
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(0),
						Xrefs = new List<Xref>(0),
						Vpr = new Vpr(),
					}),
			new Plugin(
				106757,
				"CylancePROTECT Detection",
				new PluginAttributes(
					"CylancePROTECT, an antivirus application, is installed on the   Windows host.",
					"LOCAL",
					"SENSOR",
					RiskFactor.None) {
						Synopsis = "The remote Windows host contains an antivirus application.",
						SeeAlso = new List<string>(1) { "https://www.cylance.com/en-us/platform/products/index.html" },
						PublicationDate = new DateTime(2018, 2, 12, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitAvailable = false,
						Cpe = new List<string>(1) {"cpe:/a:cylance:cylanceprotect" },
						ModificationDate = new DateTime(2021, 12, 31, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.378"),
						AlwaysRun = false,
						Compliance = false,
						Cve = new List<string>(0),
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(0),
						Xrefs = new List<Xref>(0),
						Vpr = new Vpr(),
					}),
			new Plugin(
				10815,
				"Web Server Generic XSS",
				new PluginAttributes(
					"The remote host is running a web server that fails to adequately sanitize request strings of malicious JavaScript. A remote attacker can exploit this issue, via a specially crafted request, to execute arbitrary HTML and script code in a user's browser within the security context of the affected site.",
					"REMOTE",
					"SENSOR",
					RiskFactor.Medium) {
						Synopsis = "The remote web server is affected by a cross-site scripting vulnerability.",
						Solution = "Contact the vendor for a patch or upgrade.",
						SeeAlso = new List<string>(1) { "https://en.wikipedia.org/wiki/Cross-site_scripting" },
						PublicationDate = new DateTime(2001, 11, 30, 0, 0, 0, DateTimeKind.Utc),
						VulnPublicationDate = new DateTime(2004, 04, 09, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitabilityEase = "NOT_REQUIRED",
						ExploitAvailable = false,
						ModificationDate = new DateTime(2021, 12, 2, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.92"),
						AlwaysRun = false,
						Compliance = false,
						CvssVector = new Cvss2Vector("AV:N/AC:M/Au:N/C:N/I:P/A:N") {
							AccessVector = "Network",
							AccessComplexity = "Medium",
							Authentication = "None required",
							ConfidentialityImpact = "None",
							IntegrityImpact = "Partial",
							AvailabilityImpact = "None",
						},
						CvssTemporalVector = new Cvss2TemporalVector("E:H/RL:OF/RC:C") {
							Exploitability = "High",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						CvssTemporalScore = new decimal(3.7),
						CvssBaseScore = new decimal(4.3),
						Cvss3Vector = new Cvss3Vector("AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N") {
							AttackVector = "Network",
							AttackComplexity = "Low",
							PrivilegesRequired = "None",
							UserInteraction = "Required",
							Scope = "Changed",
							ConfidentialityImpact = "Low",
							IntegrityImpact = "Low",
							AvailabilityImpact = "None",
						},
						Cvss3BaseScore = new decimal(6.1),
						Cve = new List<string>(6) {
							"CVE-2002-1060",
							"CVE-2002-1700",
							"CVE-2003-1543",
							"CVE-2005-2453",
							"CVE-2006-1681",
							"CVE-2012-3382",
						},
						BugtraqId = new List<int>(8) {
							54344,
							7344,
							5305,
							17408,
							14473,
							8037,
							5011,
							7353,
						},
						Xref = new List<Xref>(1) {
							new Xref("CWE", "79"),
						},
						Xrefs = new List<Xref>(1) {
							new Xref("CWE", "79"),
						},
						Vpr = new Vpr() {
							Score = new decimal(3.8),
							Drivers = new VprDrivers(new VprDriversDateRange(731), "PROOF_OF_CONCEPT", true) {
								ThreatSourcesLast28 = new List<string>(1) { "No recorded events" },
								ProductCoverage = "LOW",
								ThreatIntensityLast28 = "VERY_LOW",
							},
							Updated = new DateTime(2021, 5, 10, 5, 21, 18, DateTimeKind.Utc),
						},
					}),
			new Plugin(
				124366,
				"McAfee Endpoint Security and Module Detection",
				new PluginAttributes(
					"McAfee Endpoint Security, a security application with multiple optional modules, is installed on the remote Windows host.",
					"LOCAL",
					"SENSOR",
					RiskFactor.None) {
						Synopsis = "A security application is installed on the remote Windows host.",
						SeeAlso = new List<string>(1) { "http://www.nessus.org/u?fc52ef2d" },
						PublicationDate = new DateTime(2019, 4, 29, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitAvailable = false,
						Cpe = new List<string>(1) { "cpe:/a:mcafee:endpoint_security" },
						ModificationDate = new DateTime(2021, 12, 31, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.687"),
						AlwaysRun = false,
						Compliance = false,
						Cve = new List<string>(0),
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(1) { new Xref("IAVT", "0001-T-0878") },
						Xrefs = new List<Xref>(1) { new Xref("IAVT", "0001-T-0878") },
						Vpr = new Vpr(),
				}),
			new Plugin(
				131725,
				"Sophos Anti-Virus Installed (Windows)",
				new PluginAttributes(
					"Sophos Anti-Virus, a commercial antivirus software package for Windows, is installed on the remote host.",
					"LOCAL",
					"SENSOR",
					RiskFactor.None) {
						Synopsis = "An antivirus application is installed on the remote Windows host.",
						Solution = "n/a.",
						SeeAlso = new List<string>(4) {
							"https://www.sophos.com/en-us.aspx",
							"https://community.sophos.com/kb/en-us/121984",
							"https://community.sophos.com/kb/en-us/120189",
							"https://downloads.sophos.com/downloads/info/latest_IDE.xml",
						},
						PublicationDate = new DateTime(2019, 12, 5, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitAvailable = false,
						Cpe = new List<string>(1) { "cpe:/a:sophos:sophos_anti-virus" },
						ModificationDate = new DateTime(2021, 12, 31, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.531"),
						AlwaysRun = false,
						Compliance = false,
						Cve = new List<string>(0),
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(0),
						Xrefs = new List<Xref>(0),
						Vpr = new Vpr(),
					}),
			new Plugin(
				133843,
				"VMware Carbon Black Cloud Endpoint Standard Installed (Windows)",
				new PluginAttributes(
					"VMware Carbon Black Cloud Endpoint Standard, formerly Cb Defense and Confer, is installed on the remote Windows host.",
					"LOCAL",
					"SENSOR",
					RiskFactor.None) {
						Synopsis = "VMware Carbon Black Cloud Endpoint Standard is installed on the remote Windows host.",
						SeeAlso = new List<string>(1) { "https://www.carbonblack.com/products/endpoint-standard/" },
						PublicationDate = new DateTime(2020, 2, 20, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = false,
						ExploitAvailable = false,
						Cpe = new List<string>(4) {
							"cpe:/a:carbonblack:carbon_black",
							"x-cpe:/a:vmware:carbon_black_endpoint_standard",
							"x-cpe:/a:carbonblack:cb_defense",
							"x-cpe:/a:confer:confer",
						},
						ModificationDate = new DateTime(2021, 12, 31, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.486"),
						AlwaysRun = false,
						Compliance = false,
						Cve = new List<string>(0),
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(0),
						Xrefs = new List<Xref>(0),
						Vpr = new Vpr(),
					}),
			new Plugin(
				139239,
				"Windows Security Feature Bypass in Secure Boot (BootHole)",
				new PluginAttributes(
					"The remote Windows host is missing an update to the Secure Boot DBX. It is, therefore, affected by multiple vulnerabilities:\n\n  - A flaw was found in grub2 in versions prior to 2.06. The rmmod implementation allows     the unloading of a module used as a dependency without checking if any other dependent     module is still loaded leading to a use-after-free scenario. This could allow arbitrary     code to be executed or a bypass of Secure Boot protections. The highest threat from this     vulnerability is to data confidentiality and integrity as well as system availability.\n    (CVE-2020-25632)\n\n  - A flaw was found in grub2 in versions prior to 2.06. Setparam_prefix() in the menu     rendering code performs a length calculation on the assumption that expressing a quoted     single quote will require 3 characters, while it actually requires 4 characters which     allows an attacker to corrupt memory by one byte for each quote in the input. The     highest threat from this vulnerability is to data confidentiality and integrity as     well as system availability. (CVE-2021-20233)\n\n\nAdditionally, the host is affected by several other security feature bypasses in Secure Boot.\n\nNote: Tenable is testing for the presence of the expected signatures added in the March 2021 DBX update referenced in the vendor advisory.",
					"LOCAL",
					"SENSOR",
					RiskFactor.High) {
						Synopsis = "The remote Windows host is affected by multiple vulnerabilities.",
						Solution = "Refer to the vendor advisory for guidance.",
						SeeAlso = new List<string>(2) {
							"http://www.nessus.org/u?6f75665a",
							"http://www.nessus.org/u?840ba26f",
						},
						PublicationDate = new DateTime(2020, 7, 31, 0, 0, 0, DateTimeKind.Utc),
						VulnPublicationDate = new DateTime(2020, 7, 29, 0, 0, 0, DateTimeKind.Utc),
						PatchPublicationDate = new DateTime(2020, 7, 29, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = true,
						ExploitabilityEase = "NOT_AVAILABLE",
						ExploitAvailable = false,
						RiskFactor = RiskFactor.High,
						StigSeverity = "II",
						Cpe = new List<string>(1) { "cpe:/o:microsoft:windows" },
						ModificationDate = new DateTime(2021, 12, 30, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.47"),
						AlwaysRun = false,
						Compliance= false,
						CvssVector = new Cvss2Vector("AV:L/AC:L/Au:N/C:C/I:C/A:C") {
							AccessVector = "Local",
							AccessComplexity = "Low",
							Authentication = "None required",
							ConfidentialityImpact = "Complete",
							IntegrityImpact = "Complete",
							AvailabilityImpact = "Complete",
						},
						CvssTemporalVector = new Cvss2TemporalVector("E:U/RL:OF/RC:C") {
							Exploitability = "Unproven",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						CvssTemporalScore = new decimal(5.3),
						CvssBaseScore = new decimal(7.2),
						Cvss3Vector = new Cvss3Vector("AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H") {
							AttackVector = "Local",
							AttackComplexity = "Low",
							PrivilegesRequired = "None",
							UserInteraction = "None",
							Scope = "Changed",
							ConfidentialityImpact = "High",
							IntegrityImpact = "High",
							AvailabilityImpact = "High",
						},
						Cvss3TemporalVector = new Cvss3TemporalVector("E:U/RL:O/RC:C") {
							ExploitCodeMaturity = "Unproven",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						Cvss3TemporalScore = new decimal(8.1),
						Cvss3BaseScore = new decimal(9.3),
						Cve = new List<string>(16) {
							"CVE-2020-10713",
							"CVE-2020-14308",
							"CVE-2020-14309",
							"CVE-2020-14310",
							"CVE-2020-14311",
							"CVE-2020-14372",
							"CVE-2020-15705",
							"CVE-2020-15706",
							"CVE-2020-15707",
							"CVE-2020-25632",
							"CVE-2020-25647",
							"CVE-2020-27749",
							"CVE-2020-27779",
							"CVE-2021-20225",
							"CVE-2021-20233",
							"CVE-2021-3418",
						},
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(1) { new Xref("IAVA", "2020-A-0349") },
						Xrefs = new List<Xref>(1) { new Xref("IAVA", "2020-A-0349") },
						Vpr = new Vpr() {
							Score = 10,
							Drivers = new VprDrivers(
								new VprDriversDateRange(366) { UpperBound = 730 },
								"UNPROVEN",
								false) {
									ThreatIntensityLast28 = "LOW",
									ThreatRecency = new VprDriversDateRange(8) { UpperBound = 30 },
									ThreatSourcesLast28 = new List<string>(1) { "Mainstream Media" },
									ProductCoverage = "LOW",
								},
							Updated = new DateTime(2021, 12, 31, 5, 26, 28, DateTimeKind.Utc),
						},
					}),
			new Plugin(
				151461,
				"F5 Networks BIG-IP : Linux kernel vulnerability (K07020416)",
				new PluginAttributes(
					"The timer_create syscall implementation in kernel/time/posix-timers.c in the Linux kernel before 4.14.8 doesn't properly validate the sigevent->sigev_notify field, which leads to out-of-bounds access in the show_timer function (called when /proc/$PID/timers is read). This allows userspace applications to read arbitrary kernel memory (on a kernel built with CONFIG_POSIX_TIMERS and CONFIG_CHECKPOINT_RESTORE).\n(CVE-2017-18344)\n\nImpact\n\nA local attacker may use this vulnerability to expose sensitive information or cause a denial of service (DoS), makingthe system unresponsive.",
					"LOCAL",
					"SENSOR",
					RiskFactor.Low) {
						Synopsis = "The remote device is missing a vendor-supplied security patch.",
						Solution = "Upgrade to one of the non-vulnerable versions listed in the F5 Solution K07020416.",
						SeeAlso = new List<string>(1) { "https://support.f5.com/csp/article/K07020416" },
						PublicationDate = new DateTime(2021, 7, 8, 0,0,0,DateTimeKind.Utc),
						VulnPublicationDate = new DateTime(2018, 7, 26, 0,0,0,DateTimeKind.Utc),
						PatchPublicationDate = new DateTime(2020, 12, 7, 0,0,0,DateTimeKind.Utc),
						HasPatch = true,
						ExploitabilityEase = "AVAILABLE",
						ExploitAvailable = true,
						Cpe = new List<string>(10) {
							"cpe:/a:f5:big-ip_access_policy_manager",
							"cpe:/a:f5:big-ip_advanced_firewall_manager",
							"cpe:/a:f5:big-ip_application_acceleration_manager",
							"cpe:/a:f5:big-ip_application_security_manager",
							"cpe:/a:f5:big-ip_application_visibility_and_reporting",
							"cpe:/a:f5:big-ip_global_traffic_manager",
							"cpe:/a:f5:big-ip_link_controller",
							"cpe:/a:f5:big-ip_local_traffic_manager",
							"cpe:/a:f5:big-ip_policy_enforcement_manager",
							"cpe:/h:f5:big-ip",
						},
						ModificationDate = new DateTime(2021, 12, 30, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.3"),
						AlwaysRun = false,
						Compliance = false,
						ExploitedByMalware = true,
						PotentialVulnerability = true,
						ExploitFrameworkCanvas = true,
						ExploitFrameworkExploitHub = false,
						ExploitFrameworkCore = false,
						ExploitFrameworkD2Elliot = false,
						ExploitFrameworkMetasploit = false,
						CvssVector = new Cvss2Vector("AV:L/AC:L/Au:N/C:P/I:N/A:N") {
							AccessVector=  "Local",
							AccessComplexity=  "Low",
							Authentication=  "None required",
							ConfidentialityImpact=  "Partial",
							IntegrityImpact=  "None",
							AvailabilityImpact=  "None",
						},
						CvssTemporalVector = new Cvss2TemporalVector("E:H/RL:OF/RC:C") {
							Exploitability = "High",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						CvssTemporalScore = new decimal(1.8),
						CvssBaseScore = new decimal(2.1),
						Cvss3Vector = new Cvss3Vector("AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N") {
							AttackVector = "Local",
							AttackComplexity = "Low",
							PrivilegesRequired = "Low",
							UserInteraction = "None",
							Scope = "Unchanged",
							ConfidentialityImpact = "High",
							IntegrityImpact = "None",
							AvailabilityImpact = "None",
						},
						Cvss3TemporalVector = new Cvss3TemporalVector("E:H/RL:O/RC:C") {
							ExploitCodeMaturity = "High",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						Cvss3TemporalScore = new decimal(5.3),
						Cvss3BaseScore = new decimal(5.5),
						Cve = new List<string>(1) { "CVE-2017-18344" },
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(0),
						Xrefs = new List<Xref>(0),
						Vpr = new Vpr() {
							Score = new decimal(6.9),
							Drivers = new VprDrivers(
								new VprDriversDateRange(731),
								"HIGH",
								false) {
									ThreatIntensityLast28 = "VERY_LOW",
									ThreatRecency = new VprDriversDateRange(8) { UpperBound = 30 },
									ThreatSourcesLast28 = new List<string>(1) { "Security Research" },
									ProductCoverage = "HIGH",
								},
							Updated = new DateTime(2022, 1, 2, 5, 27, 38, DateTimeKind.Utc),
						}
					}),
			new Plugin(
				152780,
				"OpenSSL 1.0.2 < 1.0.2za Vulnerability",
				new PluginAttributes(
					"The version of OpenSSL installed on the remote host is prior to 1.0.2za. It is, therefore, affected by a vulnerability as referenced in the 1.0.2za advisory.\n\n  - ASN.1 strings are represented internally within OpenSSL as an ASN1_STRING structure which contains a     buffer holding the string data and a field holding the buffer length. This contrasts with normal C strings     which are repesented as a buffer for the string data which is terminated with a NUL (0) byte. Although not     a strict requirement, ASN.1 strings that are parsed using OpenSSL's own d2i functions (and other similar     parsing functions) as well as any string whose value has been set with the ASN1_STRING_set() function will     additionally NUL terminate the byte array in the ASN1_STRING structure. However, it is possible for     applications to directly construct valid ASN1_STRING structures which do not NUL terminate the byte array     by directly setting the data and length fields in the ASN1_STRING array. This can also happen by using     the ASN1_STRING_set0() function. Numerous OpenSSL functions that print ASN.1 data have been found to     assume that the ASN1_STRING byte array will be NUL terminated, even though this is not guaranteed for     strings that have been directly constructed. Where an application requests an ASN.1 structure to be     printed, and where that ASN.1 structure contains ASN1_STRINGs that have been directly constructed by the     application without NUL terminating the data field, then a read buffer overrun can occur. The same thing     can also occur during name constraints processing of certificates (for example if a certificate has been     directly constructed by the application instead of loading it via the OpenSSL parsing functions, and the     certificate contains non NUL terminated ASN1_STRING structures). It can also occur in the     X509_get1_email(), X509_REQ_get1_email() and X509_get1_ocsp() functions. If a malicious actor can cause an     application to directly construct an ASN1_STRING and then process it through one of the affected OpenSSL     functions then this issue could be hit. This might result in a crash (causing a Denial of Service attack).\n    It could also result in the disclosure of private memory contents (such as private keys, or sensitive     plaintext). Fixed in OpenSSL 1.0.2za (Affected 1.0.2-1.0.2y). (CVE-2021-3712)\n\nNote that Nessus has not tested for this issue but has instead relied only on the application's self-reported version number.",
					"REMOTE",
					"SENSOR",
					RiskFactor.Medium) {
						Synopsis = "The remote service is affected by a vulnerability.",
						Solution = "Upgrade to OpenSSL version 1.0.2za or later.",
						SeeAlso = new List<string>(2) {
							"https://www.openssl.org/news/secadv/20210824.txt",
							"http://www.nessus.org/u?6c9379ce",
						},
						PublicationDate = new DateTime(2021, 8, 24, 0, 0, 0, DateTimeKind.Utc),
						VulnPublicationDate = new DateTime(2021, 8, 24, 0, 0, 0, DateTimeKind.Utc),
						PatchPublicationDate = new DateTime(2021, 8, 24, 0, 0, 0, DateTimeKind.Utc),
						HasPatch = true,
						ExploitabilityEase = "NOT_AVAILABLE",
						ExploitAvailable = false,
						StigSeverity = "I",
						Cpe = new List<string>(1) { "cpe:/a:openssl:openssl" },
						ModificationDate = new DateTime(2021, 12, 30, 0, 0, 0, DateTimeKind.Utc),
						Version = new Version("1.6"),
						AlwaysRun = false,
						Compliance = false,
						CvssVector = new Cvss2Vector("AV:N/AC:M/Au:N/C:P/I:N/A:P") {
							AccessVector = "Network",
							AccessComplexity = "Medium",
							Authentication = "None required",
							ConfidentialityImpact = "Partial",
							IntegrityImpact = "None",
							AvailabilityImpact = "Partial",
						},
						CvssTemporalVector = new Cvss2TemporalVector("E:U/RL:OF/RC:C") {
							Exploitability = "Unproven",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						CvssTemporalScore = new decimal(4.3),
						CvssBaseScore = new decimal(5.8),
						Cvss3Vector = new Cvss3Vector("AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:N/A:H") {
							AttackVector = "Network",
							AttackComplexity = "High",
							PrivilegesRequired = "None",
							UserInteraction = "None",
							Scope = "Unchanged",
							ConfidentialityImpact = "High",
							IntegrityImpact = "None",
							AvailabilityImpact = "High",
						},
						Cvss3TemporalVector = new Cvss3TemporalVector("E:U/RL:O/RC:C") {
							ExploitCodeMaturity = "Unproven",
							RemediationLevel = "Official Fix",
							ReportConfidence = "Confirmed",
						},
						Cvss3TemporalScore = new decimal(6.4),
						Cvss3BaseScore = new decimal(7.4),
						Cve = new List<string>(1) { "CVE-2021-3712" },
						BugtraqId = new List<int>(0),
						Xref = new List<Xref>(1) { new Xref("IAVA", "2021-A-0395-S") },
						Xrefs = new List<Xref>(1) { new Xref("IAVA", "2021-A-0395-S") },
						Vpr = new Vpr() {
							Score = new decimal(7.7),
							Drivers = new VprDrivers(
								new VprDriversDateRange(61) { UpperBound = 180 },
								"UNPROVEN",
								false) {
									ThreatIntensityLast28 = "LOW",
									ThreatRecency = new VprDriversDateRange(8) { UpperBound = 30 },
									ThreatSourcesLast28 = new List<string>(1) { "Social Media" },
									ProductCoverage = "VERY_HIGH",
								},
							Updated = new DateTime(2021, 12, 22, 5, 29, 55, DateTimeKind.Utc),
						},
					}
			)
		};
		private static string Json = @"
[
	{
		""id"": 52668,
		""name"": ""F-Secure Anti-Virus Detection and Status"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""An antivirus application is installed on the remote host, but it is not working properly."",
			""description"": ""F-Secure Anti-Virus is installed on the remote host. However, there is a problem with the installation; either its services are not running or its engine and/or virus definitions are out of date. Note that the status was obtained by querying 'polutil.exe'."",
			""solution"": ""Make sure that updates are working and the associated services are running."",
			""see_also"": [
				""http://www.nessus.org/u?e1b65d56""
			],
			""plugin_publication_date"": ""2011-03-14T00:00:00Z"",
			""has_patch"": false,
			""exploit_available"": false,
			""risk_factor"": ""CRITICAL"",
			""cpe"": [
				""cpe:/a:f-secure:anti-virus""
			],
			""plugin_modification_date"": ""2021-12-31T00:00:00Z"",
			""plugin_version"": 1.532,
			""always_run"": false,
			""compliance"": false,
			""cvss_vector"": {
				""raw"": ""AV:N/AC:L/Au:N/C:C/I:C/A:C"",
				""AccessVector"": ""Network"",
				""AccessComplexity"": ""Low"",
				""Authentication"": ""None required"",
				""Confidentiality-Impact"": ""Complete"",
				""Integrity-Impact"": ""Complete"",
				""Availability-Impact"": ""Complete""
			},
			""cvss_base_score"": 10,
			""cvss3_vector"": {
				""raw"": ""AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"",
				""AttackVector"": ""Network"",
				""AttackComplexity"": ""Low"",
				""PrivilegesRequired"": ""None"",
				""UserInteraction"": ""None"",
				""Scope"": ""Unchanged"",
				""Confidentiality-Impact"": ""High"",
				""Integrity-Impact"": ""High"",
				""Availability-Impact"": ""High""
			},
			""cvss3_base_score"": 9.8,
			""cve"": [],
			""bid"": [],
			""xref"": [],
			""xrefs"": [],
			""vpr"": {}
		}
	},
	{
		""id"": 106757,
		""name"": ""CylancePROTECT Detection"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""The remote Windows host contains an antivirus application."",
			""description"": ""CylancePROTECT, an antivirus application, is installed on the   Windows host."",
			""see_also"": [
				""https://www.cylance.com/en-us/platform/products/index.html""
			],
			""plugin_publication_date"": ""2018-02-12T00:00:00Z"",
			""has_patch"": false,
			""exploit_available"": false,
			""risk_factor"": ""NONE"",
			""cpe"": [
				""cpe:/a:cylance:cylanceprotect""
			],
			""plugin_modification_date"": ""2021-12-31T00:00:00Z"",
			""plugin_version"": 1.378,
			""always_run"": false,
			""compliance"": false,
			""cve"": [],
			""bid"": [],
			""xref"": [],
			""xrefs"": [],
			""vpr"": {}
		}
	},
	{
		""id"": 10815,
		""name"": ""Web Server Generic XSS"",
		""attributes"": {
			""plugin_type"": ""REMOTE"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""The remote web server is affected by a cross-site scripting vulnerability."",
			""description"": ""The remote host is running a web server that fails to adequately sanitize request strings of malicious JavaScript. A remote attacker can exploit this issue, via a specially crafted request, to execute arbitrary HTML and script code in a user's browser within the security context of the affected site."",
			""solution"": ""Contact the vendor for a patch or upgrade."",
			""see_also"": [
				""https://en.wikipedia.org/wiki/Cross-site_scripting""
			],
			""plugin_publication_date"": ""2001-11-30T00:00:00Z"",
			""vuln_publication_date"": ""2004-04-09T00:00:00Z"",
			""has_patch"": false,
			""exploitability_ease"": ""NOT_REQUIRED"",
			""exploit_available"": false,
			""risk_factor"": ""MEDIUM"",
			""plugin_modification_date"": ""2021-12-02T00:00:00Z"",
			""plugin_version"": 1.92,
			""always_run"": false,
			""compliance"": false,
			""cvss_vector"": {
				""raw"": ""AV:N/AC:M/Au:N/C:N/I:P/A:N"",
				""AccessVector"": ""Network"",
				""AccessComplexity"": ""Medium"",
				""Authentication"": ""None required"",
				""Confidentiality-Impact"": ""None"",
				""Integrity-Impact"": ""Partial"",
				""Availability-Impact"": ""None""
			},
			""cvss_temporal_vector"": {
				""raw"": ""E:H/RL:OF/RC:C"",
				""Exploitability"": ""High"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss_temporal_score"": 3.7,
			""cvss_base_score"": 4.3,
			""cvss3_vector"": {
				""raw"": ""AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N"",
				""AttackVector"": ""Network"",
				""AttackComplexity"": ""Low"",
				""PrivilegesRequired"": ""None"",
				""UserInteraction"": ""Required"",
				""Scope"": ""Changed"",
				""Confidentiality-Impact"": ""Low"",
				""Integrity-Impact"": ""Low"",
				""Availability-Impact"": ""None""
			},
			""cvss3_base_score"": 6.1,
			""cve"": [
				""CVE-2002-1060"",
				""CVE-2002-1700"",
				""CVE-2003-1543"",
				""CVE-2005-2453"",
				""CVE-2006-1681"",
				""CVE-2012-3382""
			],
			""bid"": [
				54344,
				7344,
				5305,
				17408,
				14473,
				8037,
				5011,
				7353
			],
			""xref"": [
				""CWE:79""
			],
			""xrefs"": [
				{
					""type"": ""CWE"",
					""id"": ""79""
				}
			],
			""vpr"": {
				""score"": 3.8,
				""drivers"": {
					""age_of_vuln"": {
						""lower_bound"": 731
					},
					""exploit_code_maturity"": ""PROOF_OF_CONCEPT"",
					""cvss_impact_score_predicted"": true,
					""threat_intensity_last28"": ""VERY_LOW"",
					""threat_sources_last28"": [
						""No recorded events""
					],
					""product_coverage"": ""LOW""
				},
				""updated"": ""2021-05-10T05:21:18Z""
			}
		}
	},
	{
		""id"": 124366,
		""name"": ""McAfee Endpoint Security and Module Detection"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""A security application is installed on the remote Windows host."",
			""description"": ""McAfee Endpoint Security, a security application with multiple optional modules, is installed on the remote Windows host."",
			""see_also"": [
				""http://www.nessus.org/u?fc52ef2d""
			],
			""plugin_publication_date"": ""2019-04-29T00:00:00Z"",
			""has_patch"": false,
			""exploit_available"": false,
			""risk_factor"": ""NONE"",
			""cpe"": [
				""cpe:/a:mcafee:endpoint_security""
			],
			""plugin_modification_date"": ""2021-12-31T00:00:00Z"",
			""plugin_version"": 1.687,
			""always_run"": false,
			""compliance"": false,
			""cve"": [],
			""bid"": [],
			""xref"": [
				""IAVT:0001-T-0878""
			],
			""xrefs"": [
				{
					""type"": ""IAVT"",
					""id"": ""0001-T-0878""
				}
			],
			""vpr"": {}
		}
	},
	{
		""id"": 131725,
		""name"": ""Sophos Anti-Virus Installed (Windows)"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""An antivirus application is installed on the remote Windows host."",
			""description"": ""Sophos Anti-Virus, a commercial antivirus software package for Windows, is installed on the remote host."",
			""solution"": ""n/a."",
			""see_also"": [
				""https://www.sophos.com/en-us.aspx"",
				""https://community.sophos.com/kb/en-us/121984"",
				""https://community.sophos.com/kb/en-us/120189"",
				""https://downloads.sophos.com/downloads/info/latest_IDE.xml""
			],
			""plugin_publication_date"": ""2019-12-05T00:00:00Z"",
			""has_patch"": false,
			""exploit_available"": false,
			""risk_factor"": ""NONE"",
			""cpe"": [
				""cpe:/a:sophos:sophos_anti-virus""
			],
			""plugin_modification_date"": ""2021-12-31T00:00:00Z"",
			""plugin_version"": 1.531,
			""always_run"": false,
			""compliance"": false,
			""cve"": [],
			""bid"": [],
			""xref"": [],
			""xrefs"": [],
			""vpr"": {}
		}
	},
	{
		""id"": 133843,
		""name"": ""VMware Carbon Black Cloud Endpoint Standard Installed (Windows)"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""VMware Carbon Black Cloud Endpoint Standard is installed on the remote Windows host."",
			""description"": ""VMware Carbon Black Cloud Endpoint Standard, formerly Cb Defense and Confer, is installed on the remote Windows host."",
			""see_also"": [
				""https://www.carbonblack.com/products/endpoint-standard/""
			],
			""plugin_publication_date"": ""2020-02-20T00:00:00Z"",
			""has_patch"": false,
			""exploit_available"": false,
			""risk_factor"": ""NONE"",
			""cpe"": [
				""cpe:/a:carbonblack:carbon_black"",
				""x-cpe:/a:vmware:carbon_black_endpoint_standard"",
				""x-cpe:/a:carbonblack:cb_defense"",
				""x-cpe:/a:confer:confer""
			],
			""plugin_modification_date"": ""2021-12-31T00:00:00Z"",
			""plugin_version"": 1.486,
			""always_run"": false,
			""compliance"": false,
			""cve"": [],
			""bid"": [],
			""xref"": [],
			""xrefs"": [],
			""vpr"": {}
		}
	},
	{
		""id"": 139239,
		""name"": ""Windows Security Feature Bypass in Secure Boot (BootHole)"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""The remote Windows host is affected by multiple vulnerabilities."",
			""description"": ""The remote Windows host is missing an update to the Secure Boot DBX. It is, therefore, affected by multiple vulnerabilities:\n\n  - A flaw was found in grub2 in versions prior to 2.06. The rmmod implementation allows     the unloading of a module used as a dependency without checking if any other dependent     module is still loaded leading to a use-after-free scenario. This could allow arbitrary     code to be executed or a bypass of Secure Boot protections. The highest threat from this     vulnerability is to data confidentiality and integrity as well as system availability.\n    (CVE-2020-25632)\n\n  - A flaw was found in grub2 in versions prior to 2.06. Setparam_prefix() in the menu     rendering code performs a length calculation on the assumption that expressing a quoted     single quote will require 3 characters, while it actually requires 4 characters which     allows an attacker to corrupt memory by one byte for each quote in the input. The     highest threat from this vulnerability is to data confidentiality and integrity as     well as system availability. (CVE-2021-20233)\n\n\nAdditionally, the host is affected by several other security feature bypasses in Secure Boot.\n\nNote: Tenable is testing for the presence of the expected signatures added in the March 2021 DBX update referenced in the vendor advisory."",
			""solution"": ""Refer to the vendor advisory for guidance."",
			""see_also"": [
				""http://www.nessus.org/u?6f75665a"",
				""http://www.nessus.org/u?840ba26f""
			],
			""plugin_publication_date"": ""2020-07-31T00:00:00Z"",
			""vuln_publication_date"": ""2020-07-29T00:00:00Z"",
			""patch_publication_date"": ""2020-07-29T00:00:00Z"",
			""has_patch"": true,
			""exploitability_ease"": ""NOT_AVAILABLE"",
			""exploit_available"": false,
			""risk_factor"": ""HIGH"",
			""stig_severity"": ""II"",
			""cpe"": [
				""cpe:/o:microsoft:windows""
			],
			""plugin_modification_date"": ""2021-12-30T00:00:00Z"",
			""plugin_version"": 1.47,
			""always_run"": false,
			""compliance"": false,
			""cvss_vector"": {
				""raw"": ""AV:L/AC:L/Au:N/C:C/I:C/A:C"",
				""AccessVector"": ""Local"",
				""AccessComplexity"": ""Low"",
				""Authentication"": ""None required"",
				""Confidentiality-Impact"": ""Complete"",
				""Integrity-Impact"": ""Complete"",
				""Availability-Impact"": ""Complete""
			},
			""cvss_temporal_vector"": {
				""raw"": ""E:U/RL:OF/RC:C"",
				""Exploitability"": ""Unproven"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss_temporal_score"": 5.3,
			""cvss_base_score"": 7.2,
			""cvss3_vector"": {
				""raw"": ""AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"",
				""AttackVector"": ""Local"",
				""AttackComplexity"": ""Low"",
				""PrivilegesRequired"": ""None"",
				""UserInteraction"": ""None"",
				""Scope"": ""Changed"",
				""Confidentiality-Impact"": ""High"",
				""Integrity-Impact"": ""High"",
				""Availability-Impact"": ""High""
			},
			""cvss3_temporal_vector"": {
				""raw"": ""E:U/RL:O/RC:C"",
				""ExploitCodeMaturity"": ""Unproven"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss3_temporal_score"": 8.1,
			""cvss3_base_score"": 9.3,
			""cve"": [
				""CVE-2020-10713"",
				""CVE-2020-14308"",
				""CVE-2020-14309"",
				""CVE-2020-14310"",
				""CVE-2020-14311"",
				""CVE-2020-14372"",
				""CVE-2020-15705"",
				""CVE-2020-15706"",
				""CVE-2020-15707"",
				""CVE-2020-25632"",
				""CVE-2020-25647"",
				""CVE-2020-27749"",
				""CVE-2020-27779"",
				""CVE-2021-20225"",
				""CVE-2021-20233"",
				""CVE-2021-3418""
			],
			""bid"": [],
			""xref"": [
				""IAVA:2020-A-0349""
			],
			""xrefs"": [
				{
					""type"": ""IAVA"",
					""id"": ""2020-A-0349""
				}
			],
			""vpr"": {
				""score"": 10,
				""drivers"": {
					""age_of_vuln"": {
						""lower_bound"": 366,
						""upper_bound"": 730
					},
					""exploit_code_maturity"": ""UNPROVEN"",
					""cvss_impact_score_predicted"": false,
					""threat_intensity_last28"": ""LOW"",
					""threat_recency"": {
						""lower_bound"": 8,
						""upper_bound"": 30
					},
					""threat_sources_last28"": [
						""Mainstream Media""
					],
					""product_coverage"": ""LOW""
				},
				""updated"": ""2021-12-31T05:26:28Z""
			}
		}
	},
	{
		""id"": 151461,
		""name"": ""F5 Networks BIG-IP : Linux kernel vulnerability (K07020416)"",
		""attributes"": {
			""plugin_type"": ""LOCAL"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""The remote device is missing a vendor-supplied security patch."",
			""description"": ""The timer_create syscall implementation in kernel/time/posix-timers.c in the Linux kernel before 4.14.8 doesn't properly validate the sigevent->sigev_notify field, which leads to out-of-bounds access in the show_timer function (called when /proc/$PID/timers is read). This allows userspace applications to read arbitrary kernel memory (on a kernel built with CONFIG_POSIX_TIMERS and CONFIG_CHECKPOINT_RESTORE).\n(CVE-2017-18344)\n\nImpact\n\nA local attacker may use this vulnerability to expose sensitive information or cause a denial of service (DoS), makingthe system unresponsive."",
			""solution"": ""Upgrade to one of the non-vulnerable versions listed in the F5 Solution K07020416."",
			""see_also"": [
				""https://support.f5.com/csp/article/K07020416""
			],
			""plugin_publication_date"": ""2021-07-08T00:00:00Z"",
			""vuln_publication_date"": ""2018-07-26T00:00:00Z"",
			""patch_publication_date"": ""2020-12-07T00:00:00Z"",
			""has_patch"": true,
			""exploitability_ease"": ""AVAILABLE"",
			""exploit_available"": true,
			""risk_factor"": ""LOW"",
			""cpe"": [
				""cpe:/a:f5:big-ip_access_policy_manager"",
				""cpe:/a:f5:big-ip_advanced_firewall_manager"",
				""cpe:/a:f5:big-ip_application_acceleration_manager"",
				""cpe:/a:f5:big-ip_application_security_manager"",
				""cpe:/a:f5:big-ip_application_visibility_and_reporting"",
				""cpe:/a:f5:big-ip_global_traffic_manager"",
				""cpe:/a:f5:big-ip_link_controller"",
				""cpe:/a:f5:big-ip_local_traffic_manager"",
				""cpe:/a:f5:big-ip_policy_enforcement_manager"",
				""cpe:/h:f5:big-ip""
			],
			""plugin_modification_date"": ""2021-12-30T00:00:00Z"",
			""plugin_version"": 1.3,
			""always_run"": false,
			""compliance"": false,
			""exploited_by_malware"": true,
			""potential_vulnerability"": true,
			""exploit_framework_canvas"": true,
			""exploit_framework_exploithub"": false,
			""exploit_framework_core"": false,
			""exploit_framework_d2_elliot"": false,
			""exploit_framework_metasploit"": false,
			""cvss_vector"": {
				""raw"": ""AV:L/AC:L/Au:N/C:P/I:N/A:N"",
				""AccessVector"": ""Local"",
				""AccessComplexity"": ""Low"",
				""Authentication"": ""None required"",
				""Confidentiality-Impact"": ""Partial"",
				""Integrity-Impact"": ""None"",
				""Availability-Impact"": ""None""
			},
			""cvss_temporal_vector"": {
				""raw"": ""E:H/RL:OF/RC:C"",
				""Exploitability"": ""High"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss_temporal_score"": 1.8,
			""cvss_base_score"": 2.1,
			""cvss3_vector"": {
				""raw"": ""AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N"",
				""AttackVector"": ""Local"",
				""AttackComplexity"": ""Low"",
				""PrivilegesRequired"": ""Low"",
				""UserInteraction"": ""None"",
				""Scope"": ""Unchanged"",
				""Confidentiality-Impact"": ""High"",
				""Integrity-Impact"": ""None"",
				""Availability-Impact"": ""None""
			},
			""cvss3_temporal_vector"": {
				""raw"": ""E:H/RL:O/RC:C"",
				""ExploitCodeMaturity"": ""High"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss3_temporal_score"": 5.3,
			""cvss3_base_score"": 5.5,
			""cve"": [
				""CVE-2017-18344""
			],
			""bid"": [],
			""xref"": [],
			""xrefs"": [],
			""vpr"": {
				""score"": 6.9,
				""drivers"": {
					""age_of_vuln"": {
						""lower_bound"": 731
					},
					""exploit_code_maturity"": ""HIGH"",
					""cvss_impact_score_predicted"": false,
					""threat_intensity_last28"": ""VERY_LOW"",
					""threat_recency"": {
						""lower_bound"": 8,
						""upper_bound"": 30
					},
					""threat_sources_last28"": [
						""Security Research""
					],
					""product_coverage"": ""HIGH""
				},
				""updated"": ""2022-01-02T05:27:38Z""
			}
		}
	},
	{
		""id"": 152780,
		""name"": ""OpenSSL 1.0.2 < 1.0.2za Vulnerability"",
		""attributes"": {
			""plugin_type"": ""REMOTE"",
			""intel_type"": ""SENSOR"",
			""synopsis"": ""The remote service is affected by a vulnerability."",
			""description"": ""The version of OpenSSL installed on the remote host is prior to 1.0.2za. It is, therefore, affected by a vulnerability as referenced in the 1.0.2za advisory.\n\n  - ASN.1 strings are represented internally within OpenSSL as an ASN1_STRING structure which contains a     buffer holding the string data and a field holding the buffer length. This contrasts with normal C strings     which are repesented as a buffer for the string data which is terminated with a NUL (0) byte. Although not     a strict requirement, ASN.1 strings that are parsed using OpenSSL's own d2i functions (and other similar     parsing functions) as well as any string whose value has been set with the ASN1_STRING_set() function will     additionally NUL terminate the byte array in the ASN1_STRING structure. However, it is possible for     applications to directly construct valid ASN1_STRING structures which do not NUL terminate the byte array     by directly setting the data and length fields in the ASN1_STRING array. This can also happen by using     the ASN1_STRING_set0() function. Numerous OpenSSL functions that print ASN.1 data have been found to     assume that the ASN1_STRING byte array will be NUL terminated, even though this is not guaranteed for     strings that have been directly constructed. Where an application requests an ASN.1 structure to be     printed, and where that ASN.1 structure contains ASN1_STRINGs that have been directly constructed by the     application without NUL terminating the data field, then a read buffer overrun can occur. The same thing     can also occur during name constraints processing of certificates (for example if a certificate has been     directly constructed by the application instead of loading it via the OpenSSL parsing functions, and the     certificate contains non NUL terminated ASN1_STRING structures). It can also occur in the     X509_get1_email(), X509_REQ_get1_email() and X509_get1_ocsp() functions. If a malicious actor can cause an     application to directly construct an ASN1_STRING and then process it through one of the affected OpenSSL     functions then this issue could be hit. This might result in a crash (causing a Denial of Service attack).\n    It could also result in the disclosure of private memory contents (such as private keys, or sensitive     plaintext). Fixed in OpenSSL 1.0.2za (Affected 1.0.2-1.0.2y). (CVE-2021-3712)\n\nNote that Nessus has not tested for this issue but has instead relied only on the application's self-reported version number."",
			""solution"": ""Upgrade to OpenSSL version 1.0.2za or later."",
			""see_also"": [
				""https://www.openssl.org/news/secadv/20210824.txt"",
				""http://www.nessus.org/u?6c9379ce""
			],
			""plugin_publication_date"": ""2021-08-24T00:00:00Z"",
			""vuln_publication_date"": ""2021-08-24T00:00:00Z"",
			""patch_publication_date"": ""2021-08-24T00:00:00Z"",
			""has_patch"": true,
			""exploitability_ease"": ""NOT_AVAILABLE"",
			""exploit_available"": false,
			""risk_factor"": ""MEDIUM"",
			""stig_severity"": ""I"",
			""cpe"": [
				""cpe:/a:openssl:openssl""
			],
			""plugin_modification_date"": ""2021-12-30T00:00:00Z"",
			""plugin_version"": 1.6,
			""always_run"": false,
			""compliance"": false,
			""cvss_vector"": {
				""raw"": ""AV:N/AC:M/Au:N/C:P/I:N/A:P"",
				""AccessVector"": ""Network"",
				""AccessComplexity"": ""Medium"",
				""Authentication"": ""None required"",
				""Confidentiality-Impact"": ""Partial"",
				""Integrity-Impact"": ""None"",
				""Availability-Impact"": ""Partial""
			},
			""cvss_temporal_vector"": {
				""raw"": ""E:U/RL:OF/RC:C"",
				""Exploitability"": ""Unproven"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss_temporal_score"": 4.3,
			""cvss_base_score"": 5.8,
			""cvss3_vector"": {
				""raw"": ""AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:N/A:H"",
				""AttackVector"": ""Network"",
				""AttackComplexity"": ""High"",
				""PrivilegesRequired"": ""None"",
				""UserInteraction"": ""None"",
				""Scope"": ""Unchanged"",
				""Confidentiality-Impact"": ""High"",
				""Integrity-Impact"": ""None"",
				""Availability-Impact"": ""High""
			},
			""cvss3_temporal_vector"": {
				""raw"": ""E:U/RL:O/RC:C"",
				""ExploitCodeMaturity"": ""Unproven"",
				""RemediationLevel"": ""Official Fix"",
				""ReportConfidence"": ""Confirmed""
			},
			""cvss3_temporal_score"": 6.4,
			""cvss3_base_score"": 7.4,
			""cve"": [
				""CVE-2021-3712""
			],
			""bid"": [],
			""xref"": [
				""IAVA:2021-A-0395-S""
			],
			""xrefs"": [
				{
					""type"": ""IAVA"",
					""id"": ""2021-A-0395-S""
				}
			],
			""vpr"": {
				""score"": 7.7,
				""drivers"": {
					""age_of_vuln"": {
						""lower_bound"": 61,
						""upper_bound"": 180
					},
					""exploit_code_maturity"": ""UNPROVEN"",
					""cvss_impact_score_predicted"": false,
					""threat_intensity_last28"": ""LOW"",
					""threat_recency"": {
						""lower_bound"": 8,
						""upper_bound"": 30
					},
					""threat_sources_last28"": [
						""Social Media""
					],
					""product_coverage"": ""VERY_HIGH""
				},
				""updated"": ""2021-12-22T05:29:55Z""
			}
		}
	}
]";
	}
}
