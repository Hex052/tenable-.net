using System;
using Tenable.IO.Exports.Result.Vulns;

namespace Tenable.IO.Exports {
	public class VulnEnumerable : ExportEnumerable<Vuln, VulnExportStatus> {
		internal protected VulnEnumerable(ITenableIO api, Guid uuid) : base(api, uuid) {
		}

		public override string RelativePageUri(int pageId) {
			return string.Format("vulns/export/{0}/chunks/{1}", this.Uuid, pageId);
		}
		public override string RelativeStatusUri() {
			return string.Format("vulns/export/{0}/status", this.Uuid);
		}
		public override string RelativeCancelUri() {
			return string.Format("vulns/export/{0}/cancel", this.Uuid);
		}
	}
}
