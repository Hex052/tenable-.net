using RestSharp;
using RichardSzalay.MockHttp;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Tests;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.IO.Tests {
	public class TestTenableIO {
		private readonly ITestOutputHelper output;
		public TestTenableIO(ITestOutputHelper output) {
			this.output = output;
		}

		[Fact]
		public async Task TestConstruct() {
			var secret = "fakevalue";
			var access = "fakekey";
			var header = "accessKey=fakekey;secretKey=fakevalue;";

			Assert.Throws<ArgumentNullException>(() => new TenableIO(null!, secret));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(null!, secret, TenableIO.CLOUD_URL));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(null!, secret, new RestClientOptions()));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(access, null!));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(access, null!, TenableIO.CLOUD_URL));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(access, null!, new RestClientOptions()));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(access, secret, default(string)!));
			Assert.Throws<ArgumentNullException>(() => new TenableIO(access, secret, default(RestClientOptions)!));

			using var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Get, "https://cloud.tenable.com/test/api/query/4")
				.WithHeaders("X-ApiKeys", header)
				.Respond("text/plain", "confirmed");
			var sensorCall = mockHttp.When(HttpMethod.Get, "https://sensor.cloud.tenable.com/second/api/query/1880")
				.WithHeaders("X-ApiKeys", header)
				.Respond("text/plain", "alternate");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var options = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };
			var sensorOptions = new RestClientOptions("https://sensor.cloud.tenable.com/") { ConfigureMessageHandler = _ => mockHttp };


			using (var tio = new TenableIO(access, secret)) {
				Assert.Equal(access, tio.AccessKey);
				Assert.Equal(secret, tio.SecretKey);
				Assert.Equal(
					new Uri("https://cloud.tenable.com/"),
					tio.RestClient.BuildUri(new RestRequest("/")));
			}

			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500); // If it hasn't finished in that time, it isn't going to.
			using (var tio = new TenableIO(access, secret, options)) {
				Assert.Equal("confirmed", await tio.RequestAsync(
					"test/api/query/4", Method.Get, tokenSource.Token));
				Assert.Equal(1, mockHttp.GetMatchCount(call));
				Assert.Equal(0, mockHttp.GetMatchCount(sensorCall));
			}
			using (var tio = new TenableIO(access, secret, sensorOptions)) {
				Assert.Equal("alternate", await tio.RequestAsync(
					"second/api/query/1880", Method.Get, tokenSource.Token));
				Assert.Equal(1, mockHttp.GetMatchCount(call));
				Assert.Equal(1, mockHttp.GetMatchCount(sensorCall));
			}
		}
	}
}
