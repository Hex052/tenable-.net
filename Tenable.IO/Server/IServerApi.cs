using System.Threading;
using System.Threading.Tasks;
using Tenable.IO.Server.Results;

namespace Tenable.IO.Server {
	public interface IServerApi : IEndpoint {
		/// <summary>
		/// Get the server status
		/// </summary>
		/// <returns>The server status</returns>
		Task<ServerStatus> StatusAsync(CancellationToken cancellationToken = default);
		/// <summary>
		/// Lists the server version and other properties.
		/// </summary>
		/// <returns>Server properties</returns>
		Task<object> PropertiesAsync(CancellationToken cancellationToken = default); // TODO
	}
}
