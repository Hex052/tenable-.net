using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Exports.Result.Vulns {
	public class Vuln {
		/// <summary>
		/// Information about the asset where the scan detected the vulnerability
		/// </summary>
		[JsonPropertyName("asset")]
		public VulnAsset Asset {
			get; set;
		}

		/// <summary>The text output of the scan</summary>
		[JsonPropertyName("output")]
		public string? Output {
			get; set;
		}

		/// <summary>
		/// Information about the plugin that detected this vulnerability
		/// </summary>
		[JsonPropertyName("plugin")]
		public Plugin Plugin {
			get; set;
		}

		/// <summary>
		/// Information about the port the scanner used to connect to the asset
		/// </summary>
		[JsonPropertyName("port")]
		public VulnPort? Port {
			get; set;
		}

		/// <summary>
		/// The text that appears in the Comment field of the recast rule
		/// </summary>
		[JsonPropertyName("recast_reason")]
		public string? RecastReason {
			get; set;
		}
		/// <summary>
		/// The UUID of the recast rule
		/// </summary>
		[JsonPropertyName("recast_rule_uuid")]
		public Guid? RecastRuleUuid {
			get; set;
		}

		/// <summary>
		/// Information about the latest scan that detected the vulnerability
		/// </summary>
		[JsonPropertyName("scan")]
		public VulnScan? Scan {
			get; set;
		}

		/// <summary>
		/// The severity of the vulnerability as defined
		/// using the Common Vulnerability Scoring System (CVSS) base score
		/// </summary>
		/// <remarks>
		/// Values are "info", "low", "medium", "high", and "critical"
		/// </remarks>
		[JsonPropertyName("severity")]
		public string Severity {
			get; set;
		}

		/// <summary>
		/// The code for the severity assigned when a user recast
		/// the risk associated with the vulnerability
		/// </summary>
		/// <remarks>
		/// Values are 0-5, corrosponding to "info" through "critical"
		/// </remarks>
		[JsonPropertyName("severity_id")]
		public int SeverityId {
			get; set;
		}

		/// <summary>
		/// The code for the severity originally assigned to a vulnerability
		/// before a user recast the risk associated with the vulnerability
		/// </summary>
		[JsonPropertyName("severity_default_id")]
		public int SeverityDefaultId {
			get; set;
		}

		/// <summary>
		/// The type of modification a user made to the vulnerability's severity
		/// </summary>
		/// <remarks>
		/// Values are "none", "recasted", and "accepted"
		/// </remarks>
		[JsonPropertyName("severity_modification_type")]
		public string? SeverityModificationType {
			get; set;
		}

		/// <summary>
		/// When a scan first detected the vulnerability
		/// </summary>
		[JsonPropertyName("first_found")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime FirstFound {
			get; set;
		}
		/// <summary>
		/// When a scan last detected the vulnerability
		/// </summary>
		[JsonPropertyName("last_found")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime LastFound {
			get; set;
		}
		/// <summary>
		/// When a scan no longer detects the previously detected vulnerability
		/// </summary>
		[JsonPropertyName("last_fixed")]
		[JsonConverter(typeof(UtcDateTimeConverter))]
		public DateTime? LastFixed {
			get; set;
		}

		/// <summary>
		/// The state of the vulnerability
		/// </summary>
		/// <remarks>
		/// Values are "open", "reopened", and "fixed"
		/// </remarks>
		[JsonPropertyName("state")]
		public string State {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Vuln(VulnAsset asset, Plugin plugin, string severity,
				int severityId, int severityDefaultId, DateTime firstFound,
				DateTime lastFound, string state) {
			this.Asset = asset;
			this.Plugin = plugin;
			this.Severity = severity;
			this.SeverityId = severityId;
			this.SeverityDefaultId = severityDefaultId;
			this.FirstFound = firstFound;
			this.LastFound = lastFound;
			this.State = state;
		}
	}
}
