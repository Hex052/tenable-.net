using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.IO.Agents;
using Tenable.IO.Agents.Results;
using Xunit;

namespace Tenable.IO.Tests.Agents {
	public class TestAgentEnumerable {
		private sealed class AgentEnumerableQueryParameters : AgentEnumerable {
			public AgentEnumerableQueryParameters(ITenableIO api, AgentFilter filter, int scannerId) : base(api, filter, scannerId) {
			}

			public IEnumerable<KeyValuePair<string, string>> QueryParametersPublic(int pageId) {
				return base.QueryParameters(pageId);
			}
		}

		public AgentFilter DefaultFilter {
			get; set;
		}

		public TestAgentEnumerable() {
			this.DefaultFilter = new AgentFilter();
		}

		[Fact]
		public void TestGetEnumerator() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var agentEnumerable = new AgentEnumerable(tio.Object, this.DefaultFilter, 1);
			Assert.IsType<AgentEnumerator>(agentEnumerable.GetEnumerator());
			Assert.Equal(
				default,
				Assert.IsType<AgentEnumerator>(agentEnumerable.GetAsyncEnumerator()).CancellationToken);
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public void TestQueryParameters() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var agentEnumerable = new AgentEnumerableQueryParameters(tio.Object, this.DefaultFilter, 1);
			int pageId = 38;
			var expectedParameters = new List<KeyValuePair<string, string>> {
				new KeyValuePair<string, string>("limit", this.DefaultFilter.Limit.ToString()),
				new KeyValuePair<string, string>("offset", pageId.ToString()),
			};
			var param = agentEnumerable.QueryParametersPublic(pageId);
			Assert.Equal(
				expectedParameters,
				param.OrderBy(t => t.Key).ThenBy(t => t.Value));


			var filter = new AgentFilter() {
				Filter = new List<Tuple<string, string, string>>() {
					new Tuple<string, string, string>("name", "eq", "example")
				},
				FilterType = Base.FilterType.OR,
				Sort = new List<SortOrder>() {
					new SortOrder("name", SortDirection.ASC),
					new SortOrder("linked_on", SortDirection.DESC)
				},
				Limit = 500,
				Wildcard = "win-"
			};
			agentEnumerable.GetType()
				.GetProperty(nameof(agentEnumerable.Filter))!
				.SetValue(agentEnumerable, filter);
			pageId = 836;
			expectedParameters = new List<KeyValuePair<string, string>> {
				new KeyValuePair<string, string>("f", "name:eq:example"),
				new KeyValuePair<string, string>("limit", filter.Limit.ToString()),
				new KeyValuePair<string, string>("offset", pageId.ToString()),
				new KeyValuePair<string, string>("sort", "name:asc,linked_on:desc"),
				new KeyValuePair<string, string>("w", "win-")
			};
			param = agentEnumerable.QueryParametersPublic(pageId);
			Assert.Equal(
				expectedParameters,
				param.OrderBy(t => t.Key).ThenBy(t => t.Value));

			filter.Filter.Add(
				new Tuple<string, string, string>("field1", "match", "sometext"));
			filter.WildcardFields = new List<string>() { "distro", "platform" };
			expectedParameters.Add(
				new KeyValuePair<string, string>("f", "field1:match:sometext"));
			expectedParameters.Add(
				new KeyValuePair<string, string>("ft", "or"));
			expectedParameters.Add(
				new KeyValuePair<string, string>("wf", "distro,platform"));
			param = agentEnumerable.QueryParametersPublic(pageId);
			Assert.Equal(
				expectedParameters.OrderBy(t => t.Key).ThenBy(t => t.Value),
				param.OrderBy(t => t.Key).ThenBy(t => t.Value));

			// No calls should have been made to the API at all
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(2358)]
		[InlineData(533)]
		[InlineData(5865)]
		public void TestRelativePageUri(int scannerId) {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var agentEnumerable = new AgentEnumerable(
				tio.Object,
				this.DefaultFilter,
				scannerId);
			string expected = string.Format("scanners/{0}/agents", scannerId);
			Assert.Equal(expected, agentEnumerable.RelativePageUri(20));
			Assert.Equal(expected, agentEnumerable.RelativePageUri(385));
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestGetPageAsync() {
			var filter = new AgentFilter() { Limit = 2 };
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var agentEnumerable = new AgentEnumerable(tio.Object, filter, 1);
			var page0 = new Agent[] {
				new Agent(0, Guid.Empty, "agent6", "asdf", "asdf", "219.150.175.50", Guid.Empty, "default"),
				new Agent(0, Guid.Empty, "agent7", "asdf", "asdf", "127.128.33.29", Guid.Empty, "default"),
			};
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "0"))),
					It.IsAny<CancellationToken>()))
				.Callback((string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken) => {
					cancellationToken.ThrowIfCancellationRequested();
					Assert.NotNull(queryParameters);
					var f = queryParameters!.ToList();
					Assert.Equal(2, f.Count);
					Assert.Contains(new KeyValuePair<string, string>("offset", "0"), f);
					Assert.Contains(new KeyValuePair<string, string>("limit", "2"), f);
				})
				.Returns(Task.FromResult<AgentPage?>(new AgentPage(page0, new Pagination(2, 2, 0, new SortOrder[] { new SortOrder("name", SortDirection.ASC) }))));

			using var tokenSource = new CancellationTokenSource();
			var firstPage = await agentEnumerable.GetFullPageAsync(0, tokenSource.Token);
			// Should be cached with only one call to rest api
			Assert.Same(firstPage.Items, await agentEnumerable.GetPageAsync(0, tokenSource.Token));

			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestGetPageAsyncThrow() {
			var filter = new AgentFilter() { Limit = 2 };
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var agentEnumerable = new AgentEnumerable(tio.Object, filter, 1);
			tio.Setup(tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<IEnumerable<KeyValuePair<string, string>>>(f => f.Contains(new KeyValuePair<string, string>("offset", "0"))),
					It.IsAny<CancellationToken>()))
				.Callback((string partialUrl, Method method, IEnumerable<KeyValuePair<string, string>>? queryParameters, CancellationToken cancellationToken) => {
					cancellationToken.ThrowIfCancellationRequested();
					Assert.NotNull(queryParameters);
					var f = queryParameters!.ToList();
					Assert.Equal(2, f.Count);
					Assert.Contains(new KeyValuePair<string, string>("offset", "0"), f);
					Assert.Contains(new KeyValuePair<string, string>("limit", "2"), f);
				})
				.Returns(Task.FromResult<AgentPage?>(null));

			using var tokenSource = new CancellationTokenSource();
			await Assert.ThrowsAsync<UnexpectedNullResultException>(() => agentEnumerable.GetFullPageAsync(0, tokenSource.Token).AsTask());

			tio.Verify(
				tio => tio.RequestJsonAsync<AgentPage>(
					It.Is<string>("scanners/1/agents", StringComparer.InvariantCulture),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.IsAny<IEnumerable<KeyValuePair<string, string>>>(),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
