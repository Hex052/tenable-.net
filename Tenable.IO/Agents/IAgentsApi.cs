using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.IO.Agents.Results;

namespace Tenable.IO.Agents {
	public interface IAgentsApi : IEndpoint {
		/// <summary>
		/// Get an enumerable over all the nessus agents on
		/// a particular scanner that match a particular filter
		/// </summary>
		/// <param name="filter">
		/// Filters that limit the agents that are returned
		/// </param>
		/// <param name="scannerId">
		/// Scanner id to request the agents from
		/// </param>
		/// <returns>Enumerable over all agents</returns>
		AgentEnumerable List(AgentFilter filter, int scannerId);
		/// <summary>
		/// Get an enumerable over all the nessus agents
		/// that match a particular filter
		/// </summary>
		/// <inheritdoc cref="List(AgentFilter, int)"/>
		AgentEnumerable List(AgentFilter filter);
		/// <summary>
		/// Get an enumerable over all the nessus agents
		/// on a particular scanner
		/// </summary>
		/// <inheritdoc cref="List(AgentFilter, int)"/>
		AgentEnumerable List(int scannerId);
		/// <summary>
		/// Get an enumerable over all the nessus agents
		/// </summary>
		/// <inheritdoc cref="List(AgentFilter, int)"/>
		AgentEnumerable List();

		/// <summary>
		/// Get the details for a particular agent on a particular scanner
		/// </summary>
		/// <param name="agentId">Agent ID to retrieve</param>
		/// <inheritdoc cref="List(AgentFilter, int)" path="/param"/>
		/// <returns>Agent from that scanner</returns>
		Task<Agent> DetailsAsync(int agentId, int scannerId, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="Details(int, int)"/>
		Task<Agent> DetailsAsync(int agentId, CancellationToken cancellationToken = default);

		/// <summary>
		/// Unlink an agent from a particular scanner
		/// </summary>
		/// <param name="agentId">Agent ID to unlink</param>
		/// <param name="scannerId">Scanner ID to unlink agents from</param>
		Task<EmptyJsonObject> UnlinkAsync(int agentId, int scannerId, CancellationToken cancellationToken = default);
		/// <summary>
		/// Unlink an agent
		/// </summary>
		/// <inheritdoc cref="UnlinkAsync(int, int, CancellationToken)" path="/param"/>
		Task<EmptyJsonObject> UnlinkAsync(int agentId, CancellationToken cancellationToken = default);
		/// <summary>
		/// Unlink multiple agents from a particular scanner
		/// </summary>
		/// <param name="agentIds">IDs of agents to unlink</param>
		/// <inheritdoc cref="UnlinkAsync(int, int, CancellationToken)" path="/param"/>
		Task<EmptyJsonObject> UnlinkAsync(IEnumerable<int> agentIds, int scannerId, CancellationToken cancellationToken = default);
		/// <summary>
		/// Unlink multiple agents
		/// </summary>
		/// <inheritdoc cref="UnlinkAsync(IEnumerable{int}, int, CancellationToken)" path="/param"/>
		Task<EmptyJsonObject> UnlinkAsync(IEnumerable<int> agentIds, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="UnlinkAsync(IEnumerable{int}, int, CancellationToken)"/>
		Task<EmptyJsonObject> UnlinkAsync(IEnumerable<string> agentIds, int scannerId, CancellationToken cancellationToken = default);
		/// <inheritdoc cref="UnlinkAsync(IEnumerable{int}, CancellationToken)"/>
		Task<EmptyJsonObject> UnlinkAsync(IEnumerable<string> agentIds, CancellationToken cancellationToken = default);
	}
}
