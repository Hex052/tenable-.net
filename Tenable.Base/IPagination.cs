namespace Tenable.Base {
	public interface IPagination {
		/// <summary>The total number of items, not just this page</summary>
		int Total {
			get;
		}

		/// <summary>The number of items requested in this page</summary>
		int Limit {
			get;
		}

		/// <summary>The number of items before the start of this page</summary>
		int Offset {
			get;
		}
	}
}
