using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.IO.Agents.Results;
using Xunit;

namespace Tenable.IO.Tests.Agents.Results {
	public class TestStatus {
		private class Example {
			public List<Status> Status {get;set;}

			[JsonConstructor]
			public Example(List<Status> status) {
				Assert.NotNull(status);
				this.Status = status;
			}
		}

		[Fact]
		public void TestConvert() {
			var json = "{ \"Status\": [\"on\", \"off\", \"init\"] }";
			Assert.True(
				JsonSerializer.Deserialize<Example>(json)!
				.Status
				.SequenceEqual(new[] { Status.ON, Status.OFF, Status.INIT }));
		}
	}
}
