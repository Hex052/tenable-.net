using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestTimeSpanSecondConverter {
		public class TestObject {
			[JsonConverter(typeof(TimeSpanSecondConverter))]
			public TimeSpan TimeSpan {
				get; set;
			}
		}

		[Theory]
		[InlineData(15552000, "180.00:00:00")]
		[InlineData(68055, "18:54:15")]
		[InlineData(680556, "7.21:02:36")]
		[InlineData(0, "00:00:00")]
		public void TestDeserialize(long span, string expectedspan) {
			var expected = TimeSpan.Parse(expectedspan);
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.TimeSpan), span);
			var result = JsonSerializer.Deserialize<TestObject>(json)!.TimeSpan;
			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("false")]
		[InlineData("\"\"")]
		[InlineData("null")]
		[InlineData("0.4")]
		[InlineData("\"0\"")]
		public void TestDeserializeException(object badData) {
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.TimeSpan), badData);
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("180.00:00:00", "15552000")]
		[InlineData("7.21:02:36", "680556")]
		[InlineData("18:54:15", "68055")]
		[InlineData("00:00:00", "0")]
		public void TestSerialize(string timespan, string expected) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new TimeSpanSecondConverter();

			converter.Write(jsonWriter, TimeSpan.Parse(timespan), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
	}
}
