using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.IO.Agents;
using Tenable.IO.Agents.Results;
using Xunit;

namespace Tenable.IO.Tests.Agents {
	public class TestAgentsApi {
		[Fact]
		public void TestFromTenableIO() {
			var tio = new TenableIO(
				"testKey",
				"testSecret",
				"https://example.com/");
			var agents = Assert.IsType<AgentsApi>(tio.Agents);
			Assert.NotNull(agents);
			Assert.Same(tio, agents.Api);
		}

		[Fact]
		public void TestList() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new AgentsApi(tio.Object);
			var filter = new AgentFilter();

			// Tests
			var result = Assert.IsType<AgentEnumerable>(api.List());
			Assert.Equal(filter, result.Filter);
			Assert.Equal(1, result.ScannerId);
			// Verify no calls were made yet
			tio.VerifyNoOtherCalls();
			// test with explicit scanner id
			result = Assert.IsType<AgentEnumerable>(api.List(285));
			Assert.Equal(filter, result.Filter);
			Assert.Equal(285, result.ScannerId);
			tio.VerifyNoOtherCalls();

			// With filter
			filter.Filter = new List<Tuple<string, string, string>>();
			filter.Filter.Add(
				new Tuple<string, string, string>("name", "eq", "HexTheDragon-PC"));
			result = Assert.IsType<AgentEnumerable>(api.List(filter));
			Assert.Same(filter, result.Filter);
			Assert.Equal(1, result.ScannerId);
			// Verify no calls were made yet
			tio.VerifyNoOtherCalls();
			// test with explicit scanner id
			result = Assert.IsType<AgentEnumerable>(api.List(filter, 285));
			Assert.Same(filter, result.Filter);
			Assert.Equal(285, result.ScannerId);
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(3852358, 533)]
		[InlineData(83686, 358865, true)]
		[InlineData(83686, 358865, false)]
		public async Task TestDetailsAsync(int agentId, int scannerId, bool throws = false) {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new AgentsApi(tio.Object);
			var agent = new Agent(
				agentId,
				Guid.Empty,
				"DragonDen",
				"DARWIN",
				"something?",
				"10.0.0.5",
				Guid.Empty,
				"default");
			tio.Setup(o => o.RequestJsonAsync<Agent>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(throws ? null : agent);

			// Test
			using var tokenSource = new CancellationTokenSource();
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.DetailsAsync(agentId, tokenSource.Token));
			}
			else {
				Assert.Same(agent, await api.DetailsAsync(agentId, tokenSource.Token));
			}

			// Verify
			tio.Verify(
				o => o.RequestJsonAsync<Agent>(
					It.Is<string>(string.Format("scanners/1/agents/{0}", agentId), StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with explicit scanner id
			if (throws) {
				await Assert.ThrowsAsync<UnexpectedNullResultException>(() => api.DetailsAsync(agentId, scannerId, tokenSource.Token));
			}
			else {
				Assert.Same(agent, await api.DetailsAsync(agentId, scannerId, tokenSource.Token));
			}

			// Verify
			tio.Verify(
				o => o.RequestJsonAsync<Agent>(
					It.Is<string>(string.Format("scanners/{0}/agents/{1}", scannerId, agentId), StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Get, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData(3852358, 533)]
		[InlineData(83686, 358865, false)]
		[InlineData(83686, 358865, true)]
		public async Task TestUnlink(int agentId, int scannerId, bool isnull = false) {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new AgentsApi(tio.Object);
			var result = new EmptyJsonObject();
			tio.Setup(t => t.RequestJsonAsync<EmptyJsonObject>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(isnull ? null : result);

			// Test
			using var tokenSource = new CancellationTokenSource();
			if (isnull) {
				Assert.Null((await api.UnlinkAsync(agentId, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentId, tokenSource.Token));
			}
			// Verify
			tio.Verify(
				t => t.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>(string.Format("scanners/1/agents/{0}", agentId), StringComparer.InvariantCulture),
					It.Is<Method>(Method.Delete, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with explicit scanner id
			if (isnull) {
				Assert.Null((await api.UnlinkAsync(agentId, scannerId, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentId, scannerId, tokenSource.Token));
			}
			// Verify
			tio.Verify(
				t => t.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>(string.Format("scanners/{0}/agents/{1}", scannerId, agentId), StringComparer.InvariantCulture),
					It.Is<Method>(Method.Delete, EqualityComparer<Method>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Theory]
		[InlineData("3852358", 533)]
		[InlineData("3852358,278,3845", 533)]
		[InlineData("83686,38235", 358865, false)]
		[InlineData("83686,38235", 358865, true)]
		public async Task TestUnlinkMultiple(string agentIdString, int scannerId, bool isnull = false) {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new AgentsApi(tio.Object);
			var jsonStringResult = @"{
""task_id"":""eade034c-aa9c-41f6-9296-cf3fa5a60421"",
""container_uuid"":""3cc182bb-b8ba-4025-9e59-9a81b8a64d5a"",
""status"":""NEW""
}";
			var result = JsonSerializer.Deserialize<EmptyJsonObject>(jsonStringResult)!;
			Assert.NotNull(result);
			tio.Setup(tio => tio.RequestJsonAsync<EmptyJsonObject>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<BulkItems>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(isnull ? null : result);
			var agentIdStrings = agentIdString.Split(',');
			var agentIds = agentIdStrings.Select(f => int.Parse(f));
			var itemsStrings = new BulkItems(agentIdStrings);
			var items = new BulkItems(agentIds.ToList());

			// Test
			using var tokenSource = new CancellationTokenSource();
			if (isnull) {
				Assert.Null(Assert.IsType<EmptyJsonObject>(await api.UnlinkAsync(agentIds, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentIds, tokenSource.Token));
			}
			tio.Verify(
				tio => tio.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>("scanners/1/agents/_bulk/unlink", StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<BulkItems>(items, EqualityComparer<BulkItems>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with strings
			if (isnull) {
				Assert.Null(Assert.IsType<EmptyJsonObject>(await api.UnlinkAsync(agentIdStrings, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentIdStrings, tokenSource.Token));
			}
			tio.Verify(
				tio => tio.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>("scanners/1/agents/_bulk/unlink", StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<BulkItems>(itemsStrings, EqualityComparer<BulkItems>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test With explicit scanner id
			if (isnull) {
				Assert.Null(Assert.IsType<EmptyJsonObject>(await api.UnlinkAsync(agentIds, scannerId, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentIds, scannerId, tokenSource.Token));
			}
			tio.Verify(
				tio => tio.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>(string.Format("scanners/{0}/agents/_bulk/unlink", scannerId), StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<BulkItems>(items, EqualityComparer<BulkItems>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();

			// Test with strings and scanner id
			if (isnull) {
				Assert.Null(Assert.IsType<EmptyJsonObject>(await api.UnlinkAsync(agentIdStrings, scannerId, tokenSource.Token)).ExtensionData);
			}
			else {
				Assert.Same(result, await api.UnlinkAsync(agentIdStrings, scannerId, tokenSource.Token));
			}
			tio.Verify(
				tio => tio.RequestJsonAsync<EmptyJsonObject>(
					It.Is<string>(string.Format("scanners/{0}/agents/_bulk/unlink", scannerId), StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<BulkItems>(itemsStrings, EqualityComparer<BulkItems>.Default),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
