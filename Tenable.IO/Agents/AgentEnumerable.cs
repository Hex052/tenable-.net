using System.Collections.Generic;
using System.Threading;
using Tenable.Base;
using Tenable.Base.Collections;
using Tenable.IO.Agents.Results;

namespace Tenable.IO.Agents {
	public class AgentEnumerable : PageEnumerable<Agent, AgentPage, Pagination> {
		public AgentFilter Filter {
			get; protected set;
		}
		public int ScannerId {
			get; protected set;
		}

		public AgentEnumerable(ITenableIO api, AgentFilter filter,
				int scannerId) : base(api) {
			this.Filter = filter;
			this.ScannerId = scannerId;
		}

		protected override IEnumerable<KeyValuePair<string, string>> QueryParameters(int pageId) {
			AgentFilter filter = this.Filter;
			if (!ReferenceEquals(filter.Filter, null)) {
				foreach (var field in filter.Filter) {
					yield return new KeyValuePair<string, string>(
						"f",
						string.Format("{0}:{1}:{2}", field.Item1, field.Item2, field.Item3));
				}
				if (filter.Filter.Count > 1) {
					yield return new KeyValuePair<string, string>(
						"ft",
						filter.FilterType == FilterType.AND ? "and" : "or");
				}
			}
			if (!ReferenceEquals(filter.Wildcard, null)) {
				yield return new KeyValuePair<string, string>("w", filter.Wildcard);
				if (!ReferenceEquals(filter.WildcardFields, null)
						&& filter.WildcardFields.Count > 0) {
					yield return new KeyValuePair<string, string>(
						"wf",
						string.Join(',', filter.WildcardFields));
				}
			}
			yield return new KeyValuePair<string, string>("limit", filter.Limit.ToString());
			yield return new KeyValuePair<string, string>("offset", pageId.ToString());
			if (!ReferenceEquals(filter.Sort, null) && filter.Sort.Count > 0) {
				yield return new KeyValuePair<string, string>(
					"sort",
					string.Join(',', filter.Sort));
			}
		}

		/// <summary>Get an enumerator over the agents</summary>
		/// <returns>A <see cref="AgentEnumerator"/></returns>
		public override IEnumerator<Agent> GetEnumerator() {
			return new AgentEnumerator(this, default);
		}
		/// <summary>Get an enumerator over the agents</summary>
		/// <returns>A <see cref="AgentEnumerator"/></returns>
		public override IAsyncEnumerator<Agent> GetAsyncEnumerator(CancellationToken cancellationToken = default) {
			return new AgentEnumerator(this, cancellationToken);
		}

		public override string RelativePageUri(int pageId) {
			return string.Format("scanners/{0}/agents", this.ScannerId);
		}
	}
}
