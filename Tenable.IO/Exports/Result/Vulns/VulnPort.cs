using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports.Result.Vulns {
	// Named VulnPort because can't have properties with same name as class
	public class VulnPort {
		/// <summary>
		/// The port the vulnerability was discovered on
		/// </summary>
		[JsonPropertyName("port")]
		public int Port {
			get; set;
		}
		/// <summary>
		/// The protocol used
		/// </summary>
		[JsonPropertyName("protocol")]
		public string Protocol {
			get; set;
		}
		/// <summary>
		/// The service running on this port
		/// </summary>
		[JsonPropertyName("service")]
		public string? Service {
			get; set;
		}

		/// <summary>
		/// Holds all other keys in the JSON object, if there were any extra
		/// </summary>
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public VulnPort(int port, string protocol) {
			this.Port = port;
			this.Protocol = protocol;
		}
	}
}
