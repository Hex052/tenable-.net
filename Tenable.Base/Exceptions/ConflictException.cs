using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// The action attempted would cause a conflict (409 error)
	/// </summary>
	public class ConflictException : HttpStatusException {
		public ConflictException() : base(HttpStatusCode.Conflict) {
		}
		public ConflictException(string message) : base(HttpStatusCode.Conflict, message) {
		}
		public ConflictException(string message, string? body) : base(HttpStatusCode.Conflict, message, body) {
		}
		public ConflictException(string message, Exception innerException) : base(HttpStatusCode.Conflict, message, innerException) {
		}
		public ConflictException(string message, string? body, Exception innerException) : base(HttpStatusCode.Conflict, message, body, innerException) {
		}
	}
}
