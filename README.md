# Tenable.NET

[![pipeline status](https://gitlab.com/Hex052/tenable-.net/badges/master/pipeline.svg)](https://gitlab.com/Hex052/tenable-.net/-/commits/master)
[![coverage report](https://gitlab.com/Hex052/tenable-.net/badges/master/coverage.svg)](https://gitlab.com/Hex052/tenable-.net/-/commits/master)

This is a .NET library for Tenable.io, inspired by pyTenable for Python.

This is an UNOFFICIAL library!
