using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.Base.JsonConverters {
	public class SortDirectionConverter : JsonConverter<SortDirection> {
		public override SortDirection Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			if (reader.TokenType != JsonTokenType.String) {
				throw new JsonException();
			}
			switch (reader.GetString()) {
				case "asc":
					return SortDirection.ASC;
				case "desc":
					return SortDirection.DESC;
				default:
					throw new JsonException();
			}
		}

		public override void Write(Utf8JsonWriter writer, SortDirection value, JsonSerializerOptions options) {
			writer.WriteStringValue(value == SortDirection.ASC ? "asc" : "desc");
		}
	}
}
