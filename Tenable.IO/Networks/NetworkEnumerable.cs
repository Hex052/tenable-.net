using System.Collections.Generic;
using System.Threading;
using Tenable.Base;
using Tenable.Base.Collections;
using Tenable.IO.Networks.Results;

namespace Tenable.IO.Networks {
	public class NetworkEnumerable : PageEnumerable<Network, NetworkPage, Pagination> {
		public NetworkFilter Filter {
			get; protected set;
		}

		public NetworkEnumerable(ITenableIO api, NetworkFilter filter) : base(api) {
			this.Filter = filter;
		}

		protected override IEnumerable<KeyValuePair<string, string>> QueryParameters(int pageId) {
			if (!ReferenceEquals(this.Filter.Filter, null)) {
				foreach (var field in this.Filter.Filter) {
					yield return new KeyValuePair<string, string>(
						"f",
						string.Format("{0}:{1}:{2}", field.Item1, field.Item2, field.Item3));
				}
				if (this.Filter.Filter.Count > 1) {
					yield return new KeyValuePair<string, string>(
						"ft",
						this.Filter.FilterType == FilterType.AND ? "and" : "or");
				}
			}
			yield return new KeyValuePair<string, string>("limit", this.Filter.Limit.ToString());
			yield return new KeyValuePair<string, string>("offset", pageId.ToString());
			if (!ReferenceEquals(this.Filter.Sort, null) && this.Filter.Sort.Count > 0) {
				yield return new KeyValuePair<string, string>(
					"sort",
					string.Join(',', this.Filter.Sort));
			}
			if (this.Filter.IncludeDeleted.HasValue) {
				yield return new KeyValuePair<string, string>(
					"includeDeleted",
					this.Filter.IncludeDeleted.Value ? "true" : "false");
			}
		}

		/// <summary>Get an enumerator over the networks</summary>
		/// <returns>A <see cref="NetworkEnumerator"/></returns>
		public override IEnumerator<Network> GetEnumerator() {
			return new NetworkEnumerator(this, default);
		}
		/// <summary>Get an enumerator over the networks</summary>
		/// <returns>A <see cref="NetworkEnumerator"/></returns>
		public override IAsyncEnumerator<Network> GetAsyncEnumerator(
				CancellationToken cancellationToken = default) {
			return new NetworkEnumerator(this, cancellationToken);
		}

		public override string RelativePageUri(int pageId) {
			return "networks";
		}
	}
}
