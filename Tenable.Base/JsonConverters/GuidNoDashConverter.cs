using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.Base.JsonConverters {
	public class GuidNoDashConverter : JsonConverter<Guid> {
		public override Guid Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			Guid result;
			string str = reader.GetString()!;
			if (Guid.TryParse(str, out result) || Guid.TryParseExact(str, "N", out result)) {
				return result;
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, Guid value, JsonSerializerOptions options) {
			writer.WriteStringValue(value.ToString("N"));
		}
	}
}
