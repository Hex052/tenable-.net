using System;
using Tenable.IO.Plugins.Results;
using Tenable.IO.Plugins.Results.Comparers;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results.Comparers {
	public class TestPluginAttributeComparerComparer {
		[Fact]
		public void TestSameAndNull() {
			var attrib = new PluginAttribute("key", "value");
			var comparer = new PluginAttributeComparer();
			Assert.True(comparer.Equals(attrib, attrib));
			Assert.True(comparer.Equals(null, null));
			Assert.False(comparer.Equals(attrib, null));
			Assert.False(comparer.Equals(null, attrib));
		}

		[Fact]
		public void TestGetHashCodeException() {
			var comparer = new PluginAttributeComparer();
			Assert.Throws<ArgumentNullException>(() => {
				comparer.GetHashCode(null!);
			});
		}

		[Theory]
		[InlineData("CWE", "271")]
		[InlineData("CVE", "CVE-2021-1048")]
		public void TestEqual(string name, string value) {
			var first = new PluginAttribute(name, value);
			var second = new PluginAttribute(name, value);
			var comparer = new PluginAttributeComparer();
			Assert.Equal(comparer.GetHashCode(first), comparer.GetHashCode(second));
			Assert.True(comparer.Equals(first, second));
		}
		[Theory]
		[InlineData("CWE", "271", "CWE", "270")]
		[InlineData("CVE", "270", "CWE", "270")]
		public void TestUnequal(string name1, string value1, string name2, string value2) {
			var first = new PluginAttribute(name1, value1);
			var second = new PluginAttribute(name2, value2);
			var comparer = new PluginAttributeComparer();
			Assert.False(comparer.Equals(first, second));
		}
	}
}
