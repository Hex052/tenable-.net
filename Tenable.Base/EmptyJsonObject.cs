using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.Base {
	/// <summary>
	/// Represents an object that is believed to be empty, but if Tenable
	/// adds items in the future, this will record them.
	/// </summary>
	public class EmptyJsonObject {
		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}
	}
}
