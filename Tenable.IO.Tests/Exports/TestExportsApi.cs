using Moq;
using RestSharp;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Tests;
using Tenable.IO.Exports;
using Tenable.IO.Exports.Result;
using Xunit;
using Xunit.Abstractions;

namespace Tenable.IO.Tests.Exports {
	public class TestExportsApi {
		private readonly ITestOutputHelper output;
		public TestExportsApi(ITestOutputHelper output) {
			this.output = output;
		}

		[Fact]
		public void TestFromTenableIO() {
			var tio = new TenableIO("testKey", "testSecret", "https://example.com/");
			var exportsApi = tio.Exports;
			Assert.NotNull(exportsApi);
			Assert.IsType<ExportsApi>(exportsApi);
			Assert.Same(tio, exportsApi.Api);
			Assert.Same(exportsApi, tio.Exports);
		}

		[Fact]
		public async Task TestVulnsAsync() {
			// Setup
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("11a211d1-0475-4f8d-9143-f3c8f0b9cbac");
			var result = new ExportRequestResult(uuid);
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<ExportRequestResult?>(result));
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<object>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<ExportRequestResult?>(result));

			// Test
			using CancellationTokenSource tokenSource = new CancellationTokenSource();
			var enumerable = Assert.IsAssignableFrom<VulnEnumerable>(await api.VulnsAsync(tokenSource.Token));

			// Verify
			Assert.NotNull(enumerable);
			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			try {
				tio.Verify(
					tio => tio.RequestJsonAsync<ExportRequestResult>(
						It.Is<string>("vulns/export", StringComparer.InvariantCultureIgnoreCase),
						It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			catch (MockException) {
				tio.Verify(
					tio => tio.RequestJsonAsync<ExportRequestResult>(
						It.Is<string>("vulns/export", StringComparer.InvariantCultureIgnoreCase),
						It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
						It.Is<object>(o => o == null),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public void TestVulnsUuid() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("e7341935-c3ce-4ced-881a-e0c48d89860a");


			var enumerable = Assert.IsAssignableFrom<VulnEnumerable>(api.Vulns(uuid));

			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestVulnsAsyncFilters() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("1e22a2d7-b201-47ca-99ac-f374dd6150b9");
			var result = new ExportRequestResult(uuid);
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(
					It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<VulnExportOptions>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<ExportRequestResult?>(result));
			var options = new VulnExportOptions() {
				AssetsPerChunk = 782,
				IncludeUnlicensed = false,
				Filters = new VulnExportFilters() {
					PluginId = new[] { 45218, 19856 },
					Since = new DateTime(2021, 1, 18)
				}
			};

			using var tokenSource = new CancellationTokenSource();
			var enumerable = Assert.IsAssignableFrom<VulnEnumerable>(await api.VulnsAsync(options, tokenSource.Token));

			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			tio.Verify(
				tio => tio.RequestJsonAsync<ExportRequestResult>(
					It.Is<string>("vulns/export", StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<VulnExportOptions>(o => ReferenceEquals(o, options)),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}

		[Fact]
		public async Task TestVulnAsyncFiltersJson() {
			using var mockHttp = new MockHttpMessageHandler();
			var call = mockHttp.When(HttpMethod.Post, "https://cloud.tenable.com/vulns/export")
				.WithExactQueryString(string.Empty)
				.WithContent("{\"num_assets\":50,\"include_unlicensed\":false,\"filters\":{\"network_id\":\"8160f5d1-224b-4ed1-a45e-95bbbfb87302\"}}")
				.Respond("text/plain", "{\"export_uuid\": \"71cd6cb2-17d5-47ee-bd4f-5ea0dc6d1348\"}");
			mockHttp.Fallback.Respond(Util.HttpMockNoResponseException(this.output));
			var restOptions = new RestClientOptions(TenableIO.CLOUD_URL) { ConfigureMessageHandler = _ => mockHttp };

			using var tio = new TenableIO("accesskey", "secretkey", restOptions);
			using var tokenSource = new CancellationTokenSource();
			tokenSource.CancelAfter(500);
			var exportOptions = new VulnExportOptions() { Filters = new VulnExportFilters() { NetworkId = new Guid("8160f5d1-224b-4ed1-a45e-95bbbfb87302") } };

			Assert.Equal(
				new Guid("71cd6cb2-17d5-47ee-bd4f-5ea0dc6d1348"),
				(await tio.Exports.VulnsAsync(exportOptions, tokenSource.Token)).Uuid);

			Assert.Equal(1, mockHttp.GetMatchCount(call));
		}

		[Fact]
		public async Task TestAssets() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("11a211d1-0475-4f8d-9143-f3c8f0b9cbac");
			var result = Task.FromResult<ExportRequestResult?>(new ExportRequestResult(uuid));
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
				.Returns(result);
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<object>(), It.IsAny<CancellationToken>()))
				.Returns(result);

			using var tokenSource = new CancellationTokenSource();
			var enumerable = Assert.IsAssignableFrom<AssetEnumerable>(await api.AssetsAsync(tokenSource.Token));

			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			try {
				tio.Verify(
					tio => tio.RequestJsonAsync<ExportRequestResult>(
						It.Is<string>("assets/export", StringComparer.InvariantCultureIgnoreCase),
						It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			catch (MockException) {
				tio.Verify(
					tio => tio.RequestJsonAsync<ExportRequestResult>(
						It.Is<string>("assets/export", StringComparer.InvariantCultureIgnoreCase),
						It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
						It.Is<object>(o => o == null),
						It.Is<CancellationToken>(c => c == tokenSource.Token)),
					Times.Once());
			}
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public void TestAssetsUuid() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("ba8d8d4f-9e2f-4902-8e84-ca5ab7498f68");

			var enumerable = Assert.IsAssignableFrom<AssetEnumerable>(api.Assets(uuid));

			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			tio.VerifyNoOtherCalls();
		}
		[Fact]
		public async Task TestAssetsFiltersAsync() {
			var tio = new Mock<ITenableIO>(MockBehavior.Strict);
			var api = new ExportsApi(tio.Object);
			var uuid = new Guid("d9e65707-aa15-419a-8cdf-8003705f7f40");
			var result = new ExportRequestResult(uuid);
			tio.Setup(tio => tio.RequestJsonAsync<ExportRequestResult>(It.IsAny<string>(), It.IsAny<Method>(), It.IsAny<AssetExportOptions>(), It.IsAny<CancellationToken>()))
				.Returns(Task.FromResult<ExportRequestResult?>(result));
			var options = new AssetExportOptions() {
				ChunkSize = 384,
				Filters = new AssetExportFilters() {
					HasPluginResults = true,
					LastAuthenticatedScanTime = new DateTime(2017, 8, 26)
				}
			};

			using var tokenSource = new CancellationTokenSource();
			var enumerable = Assert.IsAssignableFrom<AssetEnumerable>(await api.AssetsAsync(options, tokenSource.Token));

			Assert.Same(tio.Object, enumerable.Api);
			Assert.Equal(uuid, enumerable.Uuid);
			tio.Verify(
				tio => tio.RequestJsonAsync<ExportRequestResult>(
					It.Is<string>("assets/export", StringComparer.InvariantCultureIgnoreCase),
					It.Is<Method>(Method.Post, EqualityComparer<Method>.Default),
					It.Is<AssetExportOptions>(o => ReferenceEquals(o, options)),
					It.Is<CancellationToken>(c => c == tokenSource.Token)),
				Times.Once());
			tio.VerifyNoOtherCalls();
		}
	}
}
