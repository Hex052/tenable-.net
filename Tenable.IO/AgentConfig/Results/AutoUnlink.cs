using System;
using System.Text.Json.Serialization;

namespace Tenable.IO.AgentConfig.Results {
	public class AutoUnlink {
		/// <summary>
		/// The expiration time for agents, in days (1-365 days)
		/// </summary>
		/// <remarks>
		/// Agents will automatically be expired after this many days and
		/// unlinked if <see cref="Enabled"> is true. Valid values are 1-365.
		/// </remarks>
		[JsonPropertyName("expiration")]
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public int? Expiration {
			get; set;
		}

		/// <summary>Whether or not agent auto-unlink is enabled</summary>
		/// <remarks>
		/// Enabling auto-unlink causes it to take effect against
		/// all agents retroactively
		/// </remarks>
		[JsonPropertyName("enabled")]
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public bool? Enabled {
			get; set;
		}

		public AutoUnlink() {
		}

		/// <summary>
		/// This is really only for System.Text.Json! If you're creating
		/// objects to call <see cref="AgentConfigApi.Set(AgentConfig, int)"/>,
		/// you should probably call the other constructor
		/// <see cref="AutoUnlink.AutoUnlink()"/>
		/// </summary>
		[JsonConstructor]
		public AutoUnlink(int? expiration, bool? enabled) {
			if (!expiration.HasValue) {
				throw new ArgumentNullException(nameof(expiration));
			}
			if (!enabled.HasValue) {
				throw new ArgumentNullException(nameof(enabled));
			}
			this.Expiration = expiration;
			this.Enabled = enabled;
		}
	}
}
