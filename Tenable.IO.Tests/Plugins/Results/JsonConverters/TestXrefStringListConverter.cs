using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.IO.Plugins.Results;
using Tenable.IO.Plugins.Results.Comparers;
using Tenable.IO.Plugins.Results.JsonConverters;
using Xunit;

namespace Tenable.IO.Tests.Plugins.Results.JsonConverters {
	public class TestXrefStringListConverter {
		private class TestObject {
			[JsonConverter(typeof(XrefStringListConverter<IList<Xref>, List<Xref>>))]
			public IList<Xref>? Xrefs {
				get; set;
			}
		}

		[Theory]
		[InlineData("[]")]
		[InlineData("[\"CWE:79\", null]", "CWE", "79", null, null)]
		[InlineData("[\"CWE:79\", \"IAVT:0001-T-0878\"]", "CWE", "79", "IAVT", "0001-T-0878")]
		[InlineData("[\"CWE:79\", \"IAVT:0001-T-0878\", \"IAVA:2020-A-0349\", \"IAVA:2021-A-0395-S\"]", "CWE", "79", "IAVT", "0001-T-0878", "IAVA", "2020-A-0349", "IAVA", "2021-A-0395-S")]
		public void TestDeserialize(string jsonValue, params string?[] expected) {
			expected ??= Array.Empty<string>();
			Assert.Equal(0, expected.Length % 2);
			var expectedValue = new List<Xref>(expected.Length / 2);
			using (var enumerator = ((IEnumerable<string?>)expected).GetEnumerator()) {
				while (enumerator.MoveNext()) {
					var first = enumerator.Current;
					enumerator.MoveNext();
					if (ReferenceEquals(null, first)) {
						expectedValue.Add(null!);
					}
					else {
						expectedValue.Add(new Xref(first, enumerator.Current!));
					}
				}
			}
			var json = $"{{ \"{nameof(TestObject.Xrefs)}\": {jsonValue} }}";
			IList<Xref?> result = JsonSerializer.Deserialize<TestObject>(json)!.Xrefs!;
			Assert.NotNull(result);
			Assert.Equal<Xref>(expectedValue, result!, new XrefComparer());
			Assert.Empty(result.Where(x => x != null && x.ExtensionData != null));
		}

		[Fact]
		public void TestDeserializeNull() {
			var json = $"{{ \"{nameof(TestObject.Xrefs)}\": null }}";
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Xrefs;
			Assert.Null(result);
		}


		[Theory]
		[InlineData("\"\"")]
		[InlineData("\"a;sfogihad;sgih\"")]
		[InlineData("-10")]
		[InlineData("\"CWE:79\"")]
		[InlineData("\"2017-12-31T20:40:23.447Z\"")]
		[InlineData("false")]
		[InlineData("[true]")]
		public void TestDeserializeException(string badData) {
			var json = $"{{ \"{nameof(TestObject.Xrefs)}\": {badData} }}";
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("CWE", "79")]
		[InlineData("CWE", "79", "IAVT", "0001-T-0878", "IAVA", "2020-A-0349", "IAVA", "2021-A-0395-S")]
		[InlineData("IAVT", "0001-T-0878", null, null)]
		public void TestWrite(params string?[] expected) {
			expected ??= Array.Empty<string>();
			Assert.Equal(0, expected.Length % 2);
			var input = new List<Xref>(expected.Length / 2);
			var output = new List<string?>(expected.Length / 2);
			using (var enumerator = ((IEnumerable<string?>)expected).GetEnumerator()) {
				while (enumerator.MoveNext()) {
					var first = enumerator.Current;
					enumerator.MoveNext();
					var second = enumerator.Current;
					if (ReferenceEquals(null, first)) {
						input.Add(null!);
						output.Add(null!);
					}
					else {
						input.Add(new Xref(first, second!));
						output.Add(string.Join(':', first, second));
					}
				}
			}
			var buffer = new ArrayBufferWriter<byte>();
			using var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new XrefStringListConverter<IList<Xref>, List<Xref>>();

			converter.Write(jsonWriter, input, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(
				output,
				JsonSerializer.Deserialize<IEnumerable<string?>?>(result),
				StringComparer.InvariantCulture);
		}
		[Fact]
		public void TestWriteNull() {
			var buffer = new ArrayBufferWriter<byte>();
			using var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new XrefStringListConverter<IList<Xref>, List<Xref>>();

			converter.Write(jsonWriter, null, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal("null", result);
		}
	}
}
