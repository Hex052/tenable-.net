using System.Collections.Generic;

namespace Tenable.Base {
	/// <summary>For endpoints that are paginated, this is the</summary>
	/// <typeparam name="TItem">Type of the item from the API</typeparam>
	/// <typeparam name="TPagination">The pagination results</typeparam>
	public interface IPage<out TItem, TPagination> where TPagination : IPagination {
		/// <summary>The items from the API</summary>
		IReadOnlyList<TItem> Items {
			get;
		}

		/// <summary>The pagination information</summary>
		TPagination Pagination {
			get;
		}
	}
}
