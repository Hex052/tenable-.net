using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// Used when Tenable returns a 403 error, because the user does not have
	/// the right permissions to make such a request
	/// </summary>
	public class NoPermissionException : HttpStatusException {
		public NoPermissionException() : base(HttpStatusCode.Forbidden) {
		}
		public NoPermissionException(string message) : base(HttpStatusCode.Forbidden, message) {
		}
		public NoPermissionException(string message, string? body) : base(HttpStatusCode.Forbidden, message, body) {
		}
		public NoPermissionException(string message, Exception innerException) : base(HttpStatusCode.Forbidden, message, innerException) {
		}
		public NoPermissionException(string message, string? body, Exception innerException) : base(HttpStatusCode.Forbidden, message, body, innerException) {
		}
	}
}
