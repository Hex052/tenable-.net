using Xunit;
using System.Text.Json;
using Tenable.Base;
using System.Buffers;
using System.Text;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Tests.Agents {
	public class TestSortDirectionConverter {
		public class TestObject {
			public SortDirection Direction {
				get; set;
			}
		}

		[Theory]
		[InlineData("asc", SortDirection.ASC)]
		[InlineData("desc", SortDirection.DESC)]
		public void TestRead(string value, SortDirection expected) {
			var json = string.Format(
				"{{ \"{0}\":\"{1}\" }}", nameof(TestObject.Direction), value);
			var page = JsonSerializer.Deserialize<TestObject>(json)!;
			Assert.Equal(expected, page.Direction);
		}
		[Theory]
		[InlineData("null")]
		[InlineData("\"neither\"")]
		public void TestReadNonsense(string value) {
			var json = string.Format(
				"{{ \"{0}\":{1} }}", nameof(TestObject.Direction), value);
			Assert.Throws<JsonException>(() => JsonSerializer.Deserialize<TestObject>(json));
		}

		[Theory]
		[InlineData("\"asc\"", SortDirection.ASC)]
		[InlineData("\"desc\"", SortDirection.DESC)]
		public void TestWrite(string expected, SortDirection value) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new SortDirectionConverter();

			converter.Write(jsonWriter, value, new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
	}
}
