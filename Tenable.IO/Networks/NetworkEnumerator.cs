using System.Threading;
using Tenable.Base;
using Tenable.Base.Collections;
using Tenable.IO.Networks.Results;

namespace Tenable.IO.Networks {
	public class NetworkEnumerator : PageEnumerator<Network, NetworkEnumerable, NetworkPage, Pagination> {
		public NetworkEnumerator(NetworkEnumerable enumerable,
				CancellationToken cancellationToken = default) :
				base(enumerable, cancellationToken) {
		}

		protected override int NextPageId() {
			return this.PageId.HasValue
				? this.PageId.Value + this.Enumerable.Filter.Limit
				: this.Enumerable.Filter.Offset;
		}
	}
}
