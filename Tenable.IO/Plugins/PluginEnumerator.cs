using System.Threading;
using Tenable.Base.Collections;
using Tenable.IO.Plugins.Results;

namespace Tenable.IO.Plugins {
	public class PluginEnumerator : PageEnumerator<Plugin, PluginEnumerable, PluginPage, PluginPagination> {
		public PluginEnumerator(PluginEnumerable enumerable,
				CancellationToken cancellationToken) :
				base(enumerable, cancellationToken) {
		}

		protected override int NextPageId() {
			return this.PageId.HasValue ? this.PageId.Value + 1 : this.Enumerable.Filter.Page;
		}
	}
}
