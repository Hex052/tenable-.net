using System;
using System.Text.Json;
using Tenable.IO.Exports.Result.Vulns;
using Xunit;

namespace Tenable.IO.Tests.Exports.Result.Vulns {
	public class TestVuln {
		[Fact]
		public void TestDeserialize() {
			var json = @"{
	""asset"": {
		""fqdn"": ""example.com"",
		""hostname"": ""192.0.2.225"",
		""uuid"": ""cf165808-6a31-48e1-9cf3-c6c3174df51d"",
		""ipv4"": ""192.0.2.8"",
		""operating_system"": [
			""Apple Mac OS X 10.5.8""
		],
		""network_id"": ""00000000-0000-0000-0000-000000000000"",
		""tracked"": true
	},
	""output"": ""The observed version of Google Chrome is : \n Chrome/21.0.1180.90"",
	""plugin"": {
		""cve"": [
			""CVE-2016-1620"",
			""CVE-2016-1614"",
			""CVE-2016-1613"",
			""CVE-2016-1612"",
			""CVE-2016-1618"",
			""CVE-2016-1617"",
			""CVE-2016-1616"",
			""CVE-2016-1615"",
			""CVE-2016-1619""
		],
		""cvss_base_score"": 9.3,
		""cvss_temporal_score"": 6.9,
		""cvss_temporal_vector"": {
			""exploitability"": ""Unproven"",
			""remediation_level"": ""Official-fix"",
			""report_confidence"": ""Confirmed"",
			""raw"": ""E:U/RL:OF/RC:C""
		},
		""cvss_vector"": {
			""access_complexity"": ""Medium"",
			""access_vector"": ""Network"",
			""authentication"": ""None required"",
			""confidentiality_impact"": ""Complete"",
			""integrity_impact"": ""Complete"",
			""availability_impact"": ""Complete"",
			""raw"": ""AV:N/AC:M/Au:N/C:C/I:C/A:C""
		},
		""description"": ""The version of Google Chrome on the remote host is prior to 48.0.2564.82 and is affected by the following vulnerabilities: \n\n - An unspecified vulnerability exists in Google V8 when handling compatible receiver checks hidden behind receptors.  An attacker can exploit this to have an unspecified impact.  No other details are available. (CVE-2016-1612)\n - A use-after-free error exists in `PDFium` due to improper invalidation of `IPWL_FocusHandler` and `IPWL_Provider` upon destruction.  An attacker can exploit this to dereference already freed memory, resulting in the execution of arbitrary code. (CVE-2016-1613)\n - An unspecified vulnerability exists in `Blink` that is related to the handling of bitmaps.  An attacker can exploit this to access sensitive information.  No other details are available. (CVE-2016-1614)\n - An unspecified vulnerability exists in `omnibox` that is related to origin confusion.  An attacker can exploit this to have an unspecified impact.  No other details are available. (CVE-2016-1615)\n - An unspecified vulnerability exists that allows an attacker to spoof a displayed URL.  No other details are available. (CVE-2016-1616)\n - An unspecified vulnerability exists that is related to history sniffing with HSTS and CSP. No other details are available. (CVE-2016-1617)\n - A flaw exists in `Blink` due to the weak generation of random numbers by the ARC4-based random number generator.  An attacker can exploit this to gain access to sensitive information.  No other details are available. (CVE-2016-1618)\n - An out-of-bounds read error exists in `PDFium` in file `fx_codec_jpx_opj.cpp` in the `sycc4{22,44}_to_rgb()` functions. An attacker can exploit this to cause a denial of service by crashing the application linked using the library. (CVE-2016-1619)\n - Multiple vulnerabilities exist, the most serious of which allow an attacker to execute arbitrary code via a crafted web page. (CVE-2016-1620)\n - A flaw in `objects.cc` is triggered when handling cleared `WeakCells`, which may allow a context-dependent attacker to have an unspecified impact. No further details have been provided. (CVE-2016-2051)"",
		""family"": ""Web Clients"",
		""family_id"": 1000020,
		""has_patch"": false,
		""id"": 9062,
		""name"": ""Google Chrome < 48.0.2564.82 Multiple Vulnerabilities"",
		""risk_factor"": ""HIGH"",
		""see_also"": [
			""http://googlechromereleases.blogspot.com/2016/01/beta-channel-update_20.html""
		],
		""solution"": ""Update the Chrome browser to 48.0.2564.82 or later."",
		""synopsis"": ""The remote host is utilizing a web browser that is affected by multiple vulnerabilities."",
		""vpr"": {
			""score"": 5.9,
			""drivers"": {
				""age_of_vuln"": {
					""lower_bound"": 366,
					""upper_bound"": 730
				},
				""exploit_code_maturity"": ""UNPROVEN"",
				""cvss_impact_score_predicted"": false,
				""cvss3_impact_score"": 5.9,
				""threat_intensity_last28"": ""VERY_LOW"",
				""threat_sources_last28"": [
					""No recorded events""
				],
				""product_coverage"": ""LOW""
			},
			""updated"": ""2019-12-31T10:08:58Z""
		}
	},
	""port"": {
		""port"": 0,
		""protocol"": ""TCP""
	},
	""scan"": {
		""completed_at"": ""2018-12-31T20:59:47Z"",
		""schedule_uuid"": ""6f7db010-9cb6-4870-b745-70a2aea2f81ce1b6640fe8a2217b"",
		""started_at"": ""2018-12-31T20:59:47Z"",
		""uuid"": ""0e55ec5d-c7c7-4673-a618-438a84e9d1b78af3a9957a077904""
	},
	""severity"": ""high"",
	""severity_id"": 3,
	""severity_default_id"": 3,
	""severity_modification_type"": ""NONE"",
	""first_found"": ""2018-12-31T20:59:47Z"",
	""last_found"": ""2018-12-31T20:59:47Z"",
	""state"": ""OPEN""
}";
			var result = JsonSerializer.Deserialize<Vuln>(json)!;
			Assert.Equal("example.com", result.Asset.Fqdn);
			Assert.Equal("192.0.2.225", result.Asset.Hostname);
			Assert.Equal(
				new Guid("cf165808-6a31-48e1-9cf3-c6c3174df51d"),
				result.Asset.Uuid);
			Assert.Equal("192.0.2.8", result.Asset.IPv4);
			Assert.Equal(
				new[] { "Apple Mac OS X 10.5.8" },
				result.Asset.OperatingSystem);
			Assert.Equal(Guid.Empty, result.Asset.NetworkId);
			Assert.True(result.Asset.Tracked);
			Assert.Null(result.Asset.AgentUuid);
			Assert.Null(result.Asset.BiosUuid);
			Assert.Null(result.Asset.NetbiosName);
			Assert.Null(result.Asset.NetbiosWorkgroup);
			Assert.Null(result.Asset.IPv6);
			Assert.Null(result.Asset.MacAddress);
			Assert.Null(result.Asset.DeviceType);
			Assert.Null(result.Asset.LastAuthenticatedResults);
			Assert.Null(result.Asset.LastUnauthenticatedResults);
			if (!ReferenceEquals(result.Asset.ExtensionData, null)) {
				Assert.Empty(result.Asset.ExtensionData);
			}
			Assert.Equal(
				"The observed version of Google Chrome is : \n Chrome/21.0.1180.90",
				result.Output);
			Assert.Equal(
				new[] {
					"CVE-2016-1620",
					"CVE-2016-1614",
					"CVE-2016-1613",
					"CVE-2016-1612",
					"CVE-2016-1618",
					"CVE-2016-1617",
					"CVE-2016-1616",
					"CVE-2016-1615",
					"CVE-2016-1619"
				},
				result.Plugin.Cve);
			Assert.Equal(new Decimal(9.3), result.Plugin.CvssBaseScore);
			Assert.Equal(new Decimal(6.9), result.Plugin.CvssTemporalScore);
			Assert.NotNull(result.Plugin.CvssTemporalVector);
			Assert.Equal("Unproven", result.Plugin.CvssTemporalVector!.Exploitability);
			Assert.Equal("Official-fix", result.Plugin.CvssTemporalVector!.RemediationLevel);
			Assert.Equal("Confirmed", result.Plugin.CvssTemporalVector!.ReportConfidence);
			Assert.Equal("E:U/RL:OF/RC:C", result.Plugin.CvssTemporalVector!.Raw);
			if (!ReferenceEquals(result.Plugin.CvssTemporalVector!.ExtensionData, null)) {
				Assert.Empty(result.Plugin.CvssTemporalVector!.ExtensionData);
			}
			Assert.NotNull(result.Plugin.CvssVector);
			Assert.Equal("Medium", result.Plugin.CvssVector!.AccessComplexity);
			Assert.Equal("Network", result.Plugin.CvssVector!.AccessVector);
			Assert.Equal("None required", result.Plugin.CvssVector!.Authentication);
			Assert.Equal("Complete", result.Plugin.CvssVector!.ConfidentialityImpact);
			Assert.Equal("Complete", result.Plugin.CvssVector!.IntegrityImpact);
			Assert.Equal("Complete", result.Plugin.CvssVector!.AvailabilityImpact);
			Assert.Equal("AV:N/AC:M/Au:N/C:C/I:C/A:C", result.Plugin.CvssVector!.Raw);
			if (!ReferenceEquals(result.Plugin.CvssVector!.ExtensionData, null)) {
				Assert.Empty(result.Plugin.CvssVector!.ExtensionData);
			}
			Assert.Null(result.Plugin.Cvss3TemporalVector);
			Assert.Null(result.Plugin.Cvss3Vector);
			Assert.Null(result.Plugin.Cvss3BaseScore);
			Assert.Null(result.Plugin.Cvss3TemporalScore);
			Assert.Equal(2138, result.Plugin.Description.Length);
			Assert.Equal("Web Clients", result.Plugin.Family);
			Assert.Equal(1000020, result.Plugin.FamilyId);
			Assert.False(result.Plugin.HasPatch);
			Assert.Equal(9062, result.Plugin.Id);
			Assert.Equal(
				"Google Chrome < 48.0.2564.82 Multiple Vulnerabilities",
				result.Plugin.Name);
			Assert.Equal("HIGH", result.Plugin.RiskFactor);
			Assert.Equal(
				new[] {
					"http://googlechromereleases.blogspot.com/2016/01/beta-channel-update_20.html"
				},
				result.Plugin.SeeAlso);
			Assert.Equal(
				"Update the Chrome browser to 48.0.2564.82 or later.",
				result.Plugin.Solution);
			Assert.Equal(
				"The remote host is utilizing a web browser that is affected by multiple vulnerabilities.",
				result.Plugin.Synopsis);
			Assert.NotNull(result.Plugin.Vpr);
			Assert.Equal(new Decimal(5.9), result.Plugin.Vpr!.Score);
			Assert.NotNull(result.Plugin.Vpr!.Drivers);
			Assert.Equal(366, result.Plugin.Vpr!.Drivers!.Age.LowerBound);
			Assert.Equal(730, result.Plugin.Vpr!.Drivers!.Age.UpperBound);
			if (!ReferenceEquals(result.Plugin.Vpr!.Drivers!.Age.ExtensionData, null)) {
				Assert.Empty(result.Plugin.Vpr!.Drivers!.Age.ExtensionData);
			}
			Assert.Equal(
				"UNPROVEN",
				result.Plugin.Vpr!.Drivers!.ExploitCodeMaturity);
			Assert.False(result.Plugin.Vpr!.Drivers!.CvssImpactScorePredicted);
			Assert.Equal(
				new Decimal(5.9),
				result.Plugin.Vpr!.Drivers!.Cvss3ImpactScore);
			Assert.Equal(
				"VERY_LOW",
				result.Plugin.Vpr!.Drivers!.ThreatIntensityLast28);
			Assert.Equal(
				new[] { "No recorded events" },
				result.Plugin.Vpr!.Drivers!.ThreatSourcesLast28);
			Assert.Equal("LOW", result.Plugin.Vpr!.Drivers!.ProductCoverage);
			if (!ReferenceEquals(result.Plugin.Vpr!.Drivers!.ExtensionData, null)) {
				Assert.Empty(result.Plugin.Vpr!.Drivers!.ExtensionData);
			}
			Assert.Equal(
				new DateTime(2019, 12, 31, 10, 08, 58),
				result.Plugin.Vpr!.Updated);
			Assert.Null(result.Plugin.BugtraqId);
			Assert.Null(result.Plugin.CanvasPackage);
			Assert.Null(result.Plugin.ChecksForDefaultAccount);
			Assert.Null(result.Plugin.ChecksForMalware);
			Assert.Null(result.Plugin.Cpe);
			Assert.Null(result.Plugin.D2ElliotName);
			Assert.Null(result.Plugin.ExploitAvailable);
			Assert.Null(result.Plugin.ExploitFrameworkCanvas);
			Assert.Null(result.Plugin.ExploitFrameworkCore);
			Assert.Null(result.Plugin.ExploitFrameworkD2Elliot);
			Assert.Null(result.Plugin.ExploitFrameworkMetasploit);
			Assert.Null(result.Plugin.ExploitabilityEase);
			Assert.Null(result.Plugin.ExploitedByNessus);
			Assert.Null(result.Plugin.ExploithubSku);
			Assert.Null(result.Plugin.InTheNews);
			Assert.Null(result.Plugin.MetasploitName);
			Assert.Null(result.Plugin.MicrosoftBulletin);
			Assert.Null(result.Plugin.PatchPublicationDate);
			Assert.Null(result.Plugin.ModificationDate);
			Assert.Null(result.Plugin.StigSeverity);
			Assert.Null(result.Plugin.UnsupportedByVendor);
			Assert.Null(result.Plugin.UbuntuSecurityNotice);
			Assert.Null(result.Plugin.VulnPublicationDate);
			Assert.Null(result.Plugin.Xrefs);
			Assert.Null(result.Plugin.Xrefs);
			if (!ReferenceEquals(result.Plugin.Vpr!.ExtensionData, null)) {
				Assert.Empty(result.Plugin.Vpr!.ExtensionData);
			}
			if (!ReferenceEquals(result.Plugin.ExtensionData, null)) {
				Assert.Empty(result.Plugin.ExtensionData);
			}
			Assert.NotNull(result.Port);
			Assert.Equal(0, result.Port!.Port);
			Assert.Equal("TCP", result.Port!.Protocol);
			Assert.Null(result.Port!.Service);
			if (!ReferenceEquals(result.Port!.ExtensionData, null)) {
				Assert.Empty(result.Port!.ExtensionData);
			}
			Assert.NotNull(result.Scan);
			Assert.Equal(
				new DateTime(2018, 12, 31, 20, 59, 47),
				result.Scan!.CompletedAt);
			Assert.Equal(
				"6f7db010-9cb6-4870-b745-70a2aea2f81ce1b6640fe8a2217b",
				result.Scan!.ScheduleUuid);
			Assert.Equal(
				new DateTime(2018, 12, 31, 20, 59, 47),
				result.Scan!.StartedAt);
			Assert.Equal(
				"0e55ec5d-c7c7-4673-a618-438a84e9d1b78af3a9957a077904",
				result.Scan!.Uuid);
			if (!ReferenceEquals(result.Scan!.ExtensionData, null)) {
				Assert.Empty(result.Scan!.ExtensionData);
			}
			Assert.Equal("high", result.Severity);
			Assert.Equal(3, result.SeverityId);
			Assert.Equal(3, result.SeverityDefaultId);
			Assert.Equal("NONE", result.SeverityModificationType);
			Assert.Equal(new DateTime(2018, 12, 31, 20, 59, 47), result.FirstFound);
			Assert.Equal(new DateTime(2018, 12, 31, 20, 59, 47), result.LastFound);
			Assert.Null(result.LastFixed);
			Assert.Equal("OPEN", result.State);
			Assert.Null(result.RecastReason);
			Assert.Null(result.RecastRuleUuid);
			if (!ReferenceEquals(result.ExtensionData, null)) {
				Assert.Empty(result.ExtensionData);
			}
		}
	}
}
