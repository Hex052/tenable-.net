using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class PluginAttributeComparer : EqualityComparer<PluginAttribute> {
		public PluginAttributeComparer() {
		}

		[Pure]
		public override bool Equals([AllowNull] PluginAttribute x, [AllowNull] PluginAttribute y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.AttributeName == y.AttributeName
					&& x.AttributeValue == y.AttributeValue
					&& x.ExtensionData == y.ExtensionData;
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] PluginAttribute obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return obj.AttributeName.GetHashCode()
						+ obj.AttributeValue.GetHashCode();
				}
			}
		}
	}
}
