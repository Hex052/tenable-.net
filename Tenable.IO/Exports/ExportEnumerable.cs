using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Collections;
using Tenable.Base.Exceptions;
using Tenable.IO.Exports.Result;

namespace Tenable.IO.Exports {
	/// <summary>
	/// Enumerable over exports
	/// </summary>
	/// <typeparam name="TResult">Enumerator result type</typeparam>
	/// <typeparam name="TStatus">Type of the export status object</typeparam>
	public abstract class ExportEnumerable<TResult, TStatus> : ResultEnumerable<TResult, IReadOnlyList<TResult>> where TStatus : RequestExportStatus {
		/// <summary>
		/// Export uuid
		/// </summary>
		public Guid Uuid {
			get; protected set;
		}

		/// <summary>
		/// The relative url formatted to access the status
		/// </summary>
		/// <example><code>"vulns/export/(uuid)/status"</code></example>
		public abstract string RelativeStatusUri();

		/// <summary>
		/// The relative url formatted to cancel the export
		/// </summary>
		/// <example><code>"vulns/export/(uuid)/cancel"</code></example>
		public abstract string RelativeCancelUri();

		/// <summary>
		/// Gets a page of the export
		/// </summary>
		/// <param name="pageId">Page to retrieve</param>
		/// <returns>A list of the items in that page</returns>
		/// <inheritdoc cref="ITenableBase.RequestJson{T}(string, Method)" path="/exception"/>
		public override async ValueTask<IReadOnlyList<TResult>> GetPageAsync(int pageId,
				CancellationToken cancellationToken = default) {
			IReadOnlyList<TResult>? result;
			// If we can, use the object from the cache
			WeakReference<IReadOnlyList<TResult>>? listref;
			if (this.PageCache.TryGetValue(pageId, out listref)
					&& listref.TryGetTarget(out result!)) {
				return result;
			}
			string partialUrl = this.RelativePageUri(pageId);
			result = await this.Api.RequestJsonAsync<IReadOnlyList<TResult>>(
				partialUrl, Method.Get, cancellationToken);
			if (result is null) {
				throw new UnexpectedNullResultException(partialUrl);
			}
			if (listref == null) {
				this.PageCache.Add(pageId, new WeakReference<IReadOnlyList<TResult>>(result));
			}
			else {
				listref.SetTarget(result);
			}
			return result;
		}

		/// <summary>
		/// Get the current status of the in progress export
		/// </summary>
		/// <returns>The status of the export</returns>
		/// <inheritdoc cref="ITenableBase.RequestJson{T}(string, Method)" path="/exception"/>
		public async Task<TStatus> GetStatusAsync(CancellationToken cancellationToken = default) {
			string partialUrl = this.RelativeStatusUri();
			TStatus? result = await this.Api.RequestJsonAsync<TStatus>(partialUrl, Method.Get, cancellationToken);
			return result ?? throw new UnexpectedNullResultException(partialUrl);
		}

		/// <summary>Cancel the in-progress export</summary>
		/// <returns>
		/// The new status, which should be <see cref="Status.CANCELLED"/>
		/// <returns>
		/// <inheritdoc cref="ITenableBase.RequestJson{T}(string, Method)" path="/exception"/>
		public async Task<Status> CancelAsync(CancellationToken cancellationToken = default) {
			var result = await this.Api.RequestJsonAsync<ExportStatus>(
				this.RelativeCancelUri(), Method.Post, cancellationToken);
			return result!.Status;
		}

		protected ExportEnumerable(ITenableIO api, Guid uuid) : base(api) {
			this.Uuid = uuid;
		}

		/// <summary>Get an enumerator over the export</summary>
		/// <returns><see cref="ExportEnumerator{T, S}"/></returns>
		public override IEnumerator<TResult> GetEnumerator() {
			return new ExportEnumerator<TResult, TStatus>(this, default);
		}
		/// <summary>Get an enumerator over the export</summary>
		/// <returns><see cref="ExportEnumerator{T, S}"/></returns>
		public override IAsyncEnumerator<TResult> GetAsyncEnumerator(CancellationToken cancellationToken = default) {
			return new ExportEnumerator<TResult, TStatus>(this, cancellationToken);
		}
	}
}
