using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Agents.Results {
	public class Agent {
		/// <summary>The unique ID of the agent</summary>
		[JsonPropertyName("id")]
		public int Id {
			get; set;
		}
		/// <summary>The UUID of the agent</summary>
		/// <remarks>
		/// This value corresponds to the ID of the asset where the
		/// agent is installed.
		/// You can use this attribute to match agent data to asset data.
		/// </remarks>
		[JsonPropertyName("uuid")]
		public Guid Uuid {
			get; set;
		}

		/// <summary>The name of the agent</summary>
		[JsonPropertyName("name")]
		public string Name {
			get; set;
		}
		/// <summary>The platform of the agent</summary>
		[JsonPropertyName("platform")]
		public string Platform {
			get; set;
		}
		/// <summary>The agent software distribution</summary>
		[JsonPropertyName("distro")]
		public string Distro {
			get; set;
		}

		/// <summary>The agent ip address</summary>
		[JsonPropertyName("ip")]
		public string Ip {
			get; set;
		}

		/// <summary>Timestamp when the agent last scanned the asset</summary>
		[JsonPropertyName("last_scanned")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime? LastScanned {
			get; set;
		}
		/// <summary>Timestamp when the agent was linked</summary>
		[JsonPropertyName("linked_on")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime? LinkedOn {
			get; set;
		}
		/// <summary>
		/// Timestamp when the agent last communicated with Tenable
		/// </summary>
		[JsonPropertyName("last_connect")]
		[JsonConverter(typeof(UnixSecondConverter))]
		public DateTime? LastConnect {
			get; set;
		}

		/// <summary>The currently loaded plugin set of the agent</summary>
		/// <remarks>
		/// <see langword="null"/> if the agent has no plugin set loaded
		/// </remarks>
		[JsonPropertyName("plugin_feed_id")]
		public string? PluginFeedId {
			get; set;
		}
		/// <summary>Build number for the agent</summary>
		[JsonPropertyName("core_build")]
		public string? CoreBuild {
			get; set;
		}
		/// <summary>Build version for the agent</summary>
		[JsonPropertyName("core_version")]
		[JsonConverter(typeof(VersionStringConverter))]
		public Version? CoreVersion {
			get; set;
		}

		/// <summary>Whether the agent is connected or ready to scan</summary>
		// /// <remarks>Values are "on", "off", and "init"</remarks>
		[JsonPropertyName("status")]
		public Status Status {
			get; set;
		}

		/// <summary>Groups to which the agent belongs</summary>
		[JsonPropertyName("groups")]
		public List<Group>? Groups {
			get; set;
		}

		/// <summary>UUID of the network that the agent belongs to</summary>
		[JsonPropertyName("network_uuid")]
		public Guid NetworkUuid {
			get; set;
		}
		/// <summary>Name of the network that the agent belongs to</summary>
		[JsonPropertyName("network_name")]
		public string NetworkName {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Agent(int id, Guid uuid, string name, string platform,
				string distro, string ip, Guid networkUuid, string networkName) {
			this.Id = id;
			this.Uuid = uuid;
			this.Name = name;
			this.Platform = platform;
			this.Distro = distro;
			this.Ip = ip;
			this.NetworkUuid = networkUuid;
			this.NetworkName = networkName;
		}
	}
}
