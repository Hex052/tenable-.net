using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;

namespace Tenable.IO.Networks.Results {
	public class AssetCount {
		/// <summary>
		/// The number of assets in the network not seen since
		/// the specified number of days
		/// </summary>
		[JsonPropertyName("numAssetsNotSeen")]
		public int NumAssetsNotSeen {
			get; set;
		}

		/// <summary>
		/// The total number of assets in the network
		/// </summary>
		[JsonPropertyName("numAssetsTotal")]
		public int NumAssetsTotal {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public AssetCount(int numAssetsNotSeen, int numAssetsTotal) {
			this.NumAssetsNotSeen = numAssetsNotSeen;
			this.NumAssetsTotal = numAssetsTotal;
		}
	}
}
