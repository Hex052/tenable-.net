
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base;
using Tenable.Base.Exceptions;
using Tenable.IO.Networks.Results;

namespace Tenable.IO.Networks {
	public interface INetworksApi : IEndpoint {
		/// <summary>
		/// List networks, using the default filters
		/// </summary>
		/// <returns>An enumerable over all the networks</returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		NetworkEnumerable ListNetworks();
		/// <summary>
		/// List networks, applying a filter to the results
		/// </summary>
		/// <param name="filter">filter to limit what is returned</param>
		/// <inheritdoc cref="ListNetworks"/>
		NetworkEnumerable ListNetworks(NetworkFilter filter);

		/// <summary>
		/// Get details for a particular network
		/// </summary>
		/// <param name="networkId">
		/// UUID of the network to get details for
		/// </param>
		/// <returns>Details for the network in question</returns>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<Network> GetNetworkDetailsAsync(Guid networkId,
				CancellationToken cancellationToken = default);
		// /// <inheritdoc cref="GetNetworkDetails(Guid)"/>
		// Task<Network> GetNetworkDetailsAsync(Guid networkId);

		/// <summary>
		/// Updates the name, description, or assets_ttl_days of a network
		/// object
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to update, which cannot be the default network
		/// </param>
		/// <param name="settings"></param>
		/// <returns>The updated network configuration</returns>
		/// <exception cref="BadRequestException">
		/// You attempted to update the default network
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, object?, CancellationToken)" path="/exception"/>
		Task<Network> UpdateNetworkAsync(Guid networkId, NetworkSettings settings,
				CancellationToken cancellationToken = default);

		/// <summary>
		/// Delete a network
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to delete, which cannot be the default network</param>
		/// <returns></returns>
		/// <exception cref="BadRequestException">
		/// You attempted to delete the default network
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<EmptyJsonObject> DeleteNetworkAsync(Guid networkId,
				CancellationToken cancellationToken = default);

		/// <summary>
		/// Get the total number of assets in the network and how many of those
		/// that have not been seen for <paramref name="numDays"/>
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to get data from
		/// </param>
		/// <param name="numDays">
		/// Number of days after which assets will be considered not seen
		/// </param>
		/// <returns>
		/// Total number of assets and how many of those
		/// have not been seen for <paramref name="numDays"/>
		/// </returns>
		/// <exception cref="BadRequestException">
		/// <paramref name="numDays"/> is out of the required range
		/// and the server returned a 400 error.
		/// </exception>
		/// <exception cref="NotFoundException">
		/// Network represented by <paramref name="networkId"/> is not found
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<AssetCount> GetNetworkAssetCountAsync(Guid networkId,
				int numDays, CancellationToken cancellationToken = default);

		/// <summary>
		/// Assign a scanner or scanner group to a network
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to assign <paramref name="scannerUuid"/> to
		/// </param>
		/// <param name="scannerUuid">
		/// Scanner or scanner group to assign to <paramref name="networkId"/>
		/// </param>
		/// <exception cref="NotFoundException">
		/// Scanner or scanner group represented by
		/// <paramref name="scannerUuid"/> is not found
		/// </exception>
		/// <exception cref="NotFoundException">
		/// Network represented by <paramref name="networkId"/> is not found
		/// </exception>
		/// <exception cref="BadRequestException">
		/// Attempted to assign an AWS scanner to the network object
		/// </exception>
		/// <exception cref="ConflictException">
		/// Returned if Tenable.io determines that assigning the scanner or
		/// scanner group causes a network conflict.
		/// See <see href="https://developer.tenable.com/reference/networks-assign-scanner"/>
		/// for more details and the schema of <see cref="HttpStatusException.Body"/>
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<EmptyJsonObject> AssignScannerAsync(Guid networkId, string scannerUuid, CancellationToken cancellationToken = default);
		/// <summary>
		/// Assign scanners and scanner groups to a network
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to assign <paramref name="scannerUuids"/> to
		/// </param>
		/// <param name="scannerUuids">
		/// Scanners or scanner groups (can be mixed) to assign to
		/// <paramref name="networkId"/>
		/// </param>
		/// <exception cref="NotFoundException">
		/// One or more of scanners or scanner groups in
		/// <paramref name="scannerUuids"/> was not found
		/// </exception>
		/// <exception cref="NotFoundException">
		/// Network represented by <paramref name="networkId"/> is not found
		/// </exception>
		/// <exception cref="BadRequestException">
		/// Attempted to assign an AWS scanner to the network object
		/// </exception>
		/// <exception cref="ConflictException">
		/// Returned if Tenable.io determines that assigning the scanner or
		/// scanner group causes a network conflict.
		/// See <see href="https://developer.tenable.com/reference/networks-assign-scanner-bulk"/>
		/// for more details and the schema of <see cref="HttpStatusException.Body"/>
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, object?, CancellationToken)" path="/exception"/>
		Task<EmptyJsonObject> AssignScannersAsync(Guid networkId, IEnumerable<string> scannerUuids, CancellationToken cancellationToken = default);

		/// <summary>
		/// List all scanners and scanner groups belonging to the
		/// network specified by <paramref name="networkId"/>
		/// </summary>
		/// <param name="networkId">
		/// Uuid of the network to list scanners of
		/// </param>
		/// <returns>
		/// Scanners and scanner groups belonging to the
		/// network specified by <paramref name="networkId"/>
		/// </returns>
		/// <exception cref="NotFoundException">
		/// Network represented by <paramref name="networkId"/> cannot be found
		/// </exception>
		/// <inheritdoc cref="ITenableBase.RequestJsonAsync{T}(string, RestSharp.Method, CancellationToken)" path="/exception"/>
		Task<Scanners> ListScannersAsync(Guid networkId, CancellationToken cancellationToken = default);
		/// <summary>
		/// List all unassigned scanners and scanner groups belonging to the
		/// network specified by <paramref name="networkId"/>
		/// </summary>
		/// <remarks>
		/// Includes all scanner groups within the default network AND
		/// any scanners within the default network that do not
		/// belong to a scanner group
		/// </remarks>
		/// <inheritdoc cref="ListScannersAsync(Guid, CancellationToken)"/>
		/// <seealso href="https://developer.tenable.com/reference/networks-list-assignable-scanners"/>
		Task<Scanners> ListAssignableScannersAsync(Guid networkId, CancellationToken cancellationToken = default);
	}
}
