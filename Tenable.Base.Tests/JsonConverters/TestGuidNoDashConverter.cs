using System;
using System.Buffers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tenable.Base.JsonConverters;
using Xunit;

namespace Tenable.Base.Tests.JsonConverters {
	public class TestGuidNoDashConverter {
		public class TestObject {
			[JsonConverter(typeof(GuidNoDashConverter))]
			public Guid Uuid {
				get; set;
			}
		}

		[Theory]
		[InlineData("eefd0397b5a64a8ca1d9da3bb82ccde1", "eefd0397-b5a6-4a8c-a1d9-da3bb82ccde1")]
		[InlineData("D4DCEC00B8F54698AE984F7CEBBB2AE2", "d4dcec00-b8f5-4698-ae98-4f7cebbb2ae2")]
		[InlineData("c07dd209-ab6b-4472-aa88-9704cab2f7d0", "c07dd209-ab6b-4472-aa88-9704cab2f7d0")]
		[InlineData("6B8AC17A-7383-4D10-B410-F01E8DD8065E", "6b8ac17a-7383-4d10-b410-f01e8dd8065e")]
		public void TestDeserialize(string jsonGuid, string expectedGuid) {
			var expected = new Guid(expectedGuid);
			var json = string.Format("{{ \"{0}\": \"{1}\" }}", nameof(TestObject.Uuid), jsonGuid);
			var result = JsonSerializer.Deserialize<TestObject>(json)!.Uuid;
			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("false")]
		[InlineData(0)]
		[InlineData("\"\"")]
		[InlineData("null")]
		[InlineData("0.4")]
		[InlineData("\"27d4c820e344-4087-82b6-906305c1cf7e\"")] // One dash removed
		[InlineData("\"f3faed40b10ec45b99dfcc1-21b05f4b\"")] // Right length but extra dash in wrong spot
		public void TestDeserializeException(object badData) {
			var json = string.Format("{{ \"{0}\": {1} }}", nameof(TestObject.Uuid), badData);
			Assert.Throws<JsonException>(() => {
				JsonSerializer.Deserialize<TestObject>(json);
			});
		}


		[Theory]
		[InlineData("e343cb21-983e-4546-bfc9-cf6bf97aac0f", "\"e343cb21983e4546bfc9cf6bf97aac0f\"")]
		[InlineData("5b7d5283-3931-4ba4-854b-501fa8bcb127", "\"5b7d528339314ba4854b501fa8bcb127\"")]
		public void TestSerialize(string uuid, string expected) {
			var buffer = new ArrayBufferWriter<byte>();
			var jsonWriter = new Utf8JsonWriter(buffer);
			var converter = new GuidNoDashConverter();

			converter.Write(jsonWriter, new Guid(uuid), new JsonSerializerOptions());
			jsonWriter.Flush();
			var result = Encoding.UTF8.GetString(buffer.WrittenSpan);
			Assert.Equal(expected, result);
		}
	}
}
