using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tenable.IO.Exports {
	public interface IExportsApi : IEndpoint {

		/// <summary>Export vulnerabilities with default filters</summary>
		/// <returns>Enumerable over export</returns>
		Task<VulnEnumerable> VulnsAsync(CancellationToken cancellationToken = default);
		/// <summary>Export vulnerabilities, with the specified options</summary>
		/// <param name="options">Filters to use with export</param>
		/// <returns>Enumerable over export</returns>
		Task<VulnEnumerable> VulnsAsync(VulnExportOptions options,
				CancellationToken cancellationToken = default);
		/// <summary>Reuse a previously-requested export</summary>
		/// <param name="uuid">UUID of a previously-requested export</param>
		/// <returns>Enumerable over export</returns>
		VulnEnumerable Vulns(Guid uuid);

		/// <summary>Export assets with default filters</summary>
		/// <returns>Enumerable over export</returns>
		Task<AssetEnumerable> AssetsAsync(CancellationToken cancellationToken = default);
		/// <summary>Export assets, with the specified options</summary>
		/// <param name="options">Filters to use with export</param>
		/// <returns>Enumerable over export</returns>
		Task<AssetEnumerable> AssetsAsync(AssetExportOptions options,
				CancellationToken cancellationToken = default);
		/// <summary>Reuse a previously-requested export</summary>
		/// <param name="uuid">UUID of a previously-requested export</param>
		/// <returns>Enumerable over export</returns>
		AssetEnumerable Assets(Guid uuid);
	}
}
