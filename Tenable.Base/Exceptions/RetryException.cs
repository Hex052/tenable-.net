using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// These are exceptions where you should wait a period of time and
	/// try again, the problem is likely temporary
	/// </summary>
	public class RetryException : HttpStatusException {
		public RetryException(HttpStatusCode statusCode, uint retryAfter) : base(statusCode) {
			this.RetryAfter = retryAfter;
		}
		public RetryException(HttpStatusCode statusCode, uint retryAfter, string message) : base(statusCode, message) {
			this.RetryAfter = retryAfter;
		}
		public RetryException(HttpStatusCode statusCode, uint retryAfter, string message, string? body) : base(statusCode, message, body) {
			this.RetryAfter = retryAfter;
		}
		public RetryException(HttpStatusCode statusCode, uint retryAfter, string message, Exception innerException) : base(statusCode, message, innerException) {
			this.RetryAfter = retryAfter;
		}
		public RetryException(HttpStatusCode statusCode, uint retryAfter, string message, string? body, Exception innerException) : base(statusCode, message, body, innerException) {
			this.RetryAfter = retryAfter;
		}

		public uint RetryAfter {
			get; protected set;
		}
	}
}
