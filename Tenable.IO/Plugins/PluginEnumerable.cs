using System.Collections.Generic;
using System.Threading;
using Tenable.Base.Collections;
using Tenable.IO.Plugins.Results;

namespace Tenable.IO.Plugins {
	public class PluginEnumerable : PageEnumerable<Plugin, PluginPage, PluginPagination> {
		public PluginFilter Filter {
			get; protected set;
		}

		public PluginEnumerable(ITenableIO api, PluginFilter filter) : base(api) {
			this.Filter = filter;
		}

		protected override IEnumerable<KeyValuePair<string, string>> QueryParameters(int pageId) {
			if (this.Filter.LastUpdated.HasValue) {
				return new KeyValuePair<string, string>[] {
					new KeyValuePair<string, string>("last_updated", this.Filter.LastUpdated.Value.ToString("yyyy-MM-dd")),
					new KeyValuePair<string, string>("size", this.Filter.Size.ToString()),
					new KeyValuePair<string, string>("page", pageId.ToString()),
				};
			}
			else {
				return new KeyValuePair<string, string>[] {
					new KeyValuePair<string, string>("size", this.Filter.Size.ToString()),
					new KeyValuePair<string, string>("page", pageId.ToString()),
				};
			}
		}

		/// <summary>Get an enumerator over the plugins</summary>
		/// <returns>A <see cref="PluginEnumerator"/></returns>
		public override IEnumerator<Plugin> GetEnumerator() {
			return new PluginEnumerator(this, default(CancellationToken));
		}
		/// <summary>Get an enumerator over the plugins</summary>
		/// <returns>A <see cref="PluginEnumerator"/></returns>
		public override IAsyncEnumerator<Plugin> GetAsyncEnumerator(
				CancellationToken cancellationToken = default) {
			return new PluginEnumerator(this, cancellationToken);
		}

		public override string RelativePageUri(int pageId) {
			return "plugins/plugin";
		}
	}
}
