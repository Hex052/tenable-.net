using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace Tenable.IO.Plugins.Results.Comparers {
	public class PluginComparer : EqualityComparer<Plugin> {
		private IEqualityComparer<PluginAttributes> _attributesComparer;
		public IEqualityComparer<PluginAttributes> AttributesComparer {
			get => this._attributesComparer;
			set => this._attributesComparer = value ?? throw new ArgumentNullException(nameof(value));
		}

		public PluginComparer(IEqualityComparer<PluginAttributes>? driverComparer = null) {
			this._attributesComparer = driverComparer ?? new PluginAttributesComparer();
		}

		[Pure]
		public override bool Equals([AllowNull] Plugin x, [AllowNull] Plugin y) {
			if (ReferenceEquals(x, y)) {
				return true;
			}
			else if (ReferenceEquals(null, x) || ReferenceEquals(null, y)) {
				return false;
			}
			else {
				return x.Id == y.Id
					&& x.Name == y.Name
					&& this._attributesComparer.Equals(x.Attributes, y.Attributes);
			}
		}

		[Pure]
		public override int GetHashCode([DisallowNull] Plugin obj) {
			if (obj is null) {
				throw new ArgumentNullException(nameof(obj));
			}
			else {
				unchecked {
					return obj.Id.GetHashCode()
						+ (ReferenceEquals(obj.Name, null) ? 0 : obj.Name.GetHashCode())
						+ (ReferenceEquals(obj.Attributes, null) ? 0 : this._attributesComparer.GetHashCode(obj.Attributes));
				}
			}
		}
	}
}
