using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tenable.IO.Exports {
	public class AssetExportFilters : ExportFilters {
		/// <summary>
		/// Returns all assets created later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("created_at")]
		public DateTime? CreatedAt {
			get; set;
		}
		/// <summary>
		/// Returns all assets updated later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("updated_at")]
		public DateTime? UpdatedAt {
			get; set;
		}
		/// <summary>
		/// Returns all assets terminated later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("terminated_at")]
		public DateTime? TerminatedAt {
			get; set;
		}
		/// <summary>
		/// Returns all assets deleted later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("deleted_at")]
		public DateTime? DeletedAt {
			get; set;
		}
		/// <summary>
		/// Returns all assets with a first scan time later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("first_scan_time")]
		public DateTime? FirstScanTime {
			get; set;
		}
		/// <summary>
		/// Returns all assets with a last credentialed scan
		/// time later than the date specified
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("last_authenticated_scan_time")]
		public DateTime? LastAuthenticatedScanTime {
			get; set;
		}
		/// <summary>
		/// Returns all assets with a last assessed time
		/// later than the date specified
		/// </summary>
		/// <remarks>
		/// Tenable.io considers an asset assessed if it has been
		/// scanned by a credentialed or non-credentialed scan
		/// </remarks>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("last_assessed")]
		public DateTime? LastAssessed {
			get; set;
		}


		/// <summary>
		/// When set to true, returns assets which have
		/// any value for the deleted_at attribute
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("is_deleted")]
		public bool? IsDeleted {
			get; set;
		}
		/// <summary>
		/// Specifies whether the asset is included in the asset count
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("is_licensed")]
		public bool? IsLicensed {
			get; set;
		}
		/// <summary>
		/// If true, returns all assets that have a ServiceNow Sys ID,
		/// regardless of value.
		/// If false, returns all assets that do not have a ServiceNow Sys ID.
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("servicenow_sysid")]
		public bool? ServiceNowSysId {
			get; set;
		}
		/// <summary>
		/// Filter by whether or not the asset has plugin
		/// results associated with it
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("has_plugin_results")]
		public bool? HasPluginResults {
			get; set;
		}

		/// <summary>
		/// Returns assets that have the specified source. An asset source
		/// is the entity that reported the asset details. Sources can
		/// include sensors, connectors, and API imports. If your
		/// request specifies multiple sources, Tenable.io returns all
		/// assets that have been seen by any of the specified sources.
		/// </summary>
		[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		[JsonPropertyName("sources")]
		public IEnumerable<string>? Sources {
			get; set;
		}
	}
}
