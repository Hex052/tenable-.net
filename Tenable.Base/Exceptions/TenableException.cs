using System;

namespace Tenable.Base.Exceptions {
	public abstract class TenableException : Exception {
		protected TenableException() : base() {
		}
		protected TenableException(string message) : base(message) {
		}
		protected TenableException(string message, Exception innerException) : base(message, innerException) {
		}
	}
}
