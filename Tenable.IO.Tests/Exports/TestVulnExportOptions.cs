using System;
using System.Text.Json;
using Tenable.IO.Exports;
using Xunit;

namespace Tenable.IO.Tests.Exports {
	public class TestVulnExportOptions {
		[Fact]
		public void TestSerialize() {
			VulnExportOptions options = new VulnExportOptions() {
				AssetsPerChunk = 300,
				Filters = new VulnExportFilters() {
					PluginId = new[] { 10902 },
					Severity = new[] { "info" },
					LastFound = new DateTime(2022, 2, 21, 0, 0, 0, DateTimeKind.Utc),
				},
			};
			string str = JsonSerializer.Serialize(options);
			Assert.Equal(
				"{\"num_assets\":300,\"include_unlicensed\":false,\"filters\":{\"last_found\":1645401600,\"plugin_id\":[10902],\"severity\":[\"info\"]}}",
				str);
		}
		[Fact]
		public void TestSerializeDefault() {
			VulnExportOptions options = new VulnExportOptions();
			string str = JsonSerializer.Serialize(options);
			Assert.Equal(
				string.Format(
					"{{\"num_assets\":{0},\"include_unlicensed\":{1}}}",
					options.AssetsPerChunk,
					options.IncludeUnlicensed ? "true" : "false"),
				str);
		}
	}
}
