using System;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.Base.JsonConverters {
	public class UtcDateTimeConverter : JsonConverter<DateTime> {
		public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
			var str = reader.GetString();
			if (DateTime.TryParse(str, null, DateTimeStyles.AdjustToUniversal, out DateTime result)) {
				if (str!.EndsWith('Z')) {
					return DateTime.SpecifyKind(result, DateTimeKind.Utc);
				}
				else {
					return result!;
				}
			}
			throw new JsonException();
		}

		public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options) {
			writer.WriteStringValue(
				value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"));
		}
	}
}
