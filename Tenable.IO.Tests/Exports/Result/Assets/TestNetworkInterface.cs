using System.Text.Json;
using Tenable.IO.Exports.Result.Assets;
using Xunit;

namespace Tenable.IO.Tests.Exports.Result.Assets {
	public class TestNetworkInterface {
		[Fact]
		public void TestDeserialize() {
			var json = @"
{
			""name"": ""enccw0.0.1234"",
			""mac_address"": [
				""00-00-5E-00-53-00"",
				""00-00-5E-00-53-FF""
			],
			""ipv4"": [
				""192.0.2.57"",
				""192.0.2.177""
			],
			""ipv6"": [
				""2001:DB8:1234:1234/32""
			],
			""fqdn"": [
				""example.com""
			]
		}";
			var result = JsonSerializer.Deserialize<NetworkInterface>(json)!;
			Assert.Equal("enccw0.0.1234", result.Name);
			Assert.Equal(
				new[] { "00-00-5E-00-53-00", "00-00-5E-00-53-FF" },
				result.MacAddress);
			Assert.Equal(new[] { "192.0.2.57", "192.0.2.177" }, result.Ipv4);
			Assert.Equal(new[] { "2001:DB8:1234:1234/32" }, result.Ipv6);
			Assert.Equal(new[] { "example.com" }, result.Fqdn);
		}
	}
}
