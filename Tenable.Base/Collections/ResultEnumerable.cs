using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tenable.Base.Collections {
	/// <summary>Enumerable over some results</summary>
	/// <typeparam name="TResult">Enumerator result type</typeparam>
	/// <typeparam name="TCache">
	/// Type that should be cached in <see cref="PageCache"/>
	/// </typeparam>
	public abstract class ResultEnumerable<TResult, TCache> : IEndpointBase<ITenableBase>, IAsyncEnumerable<TResult>, IEnumerable<TResult> where TCache : class {
		public ITenableBase Api {
			get;
		}
		protected ResultEnumerable(ITenableBase api) {
			this.Api = api;
		}

		/// <summary>
		/// Cache pages if there is multiple requests for the same thing
		/// </summary>
		/// <remarks>
		/// <see cref="WeakReference"/> is used so these huge lists
		/// don't hog memory if they are thrown away
		/// </remarks>
		protected Dictionary<int, WeakReference<TCache>> PageCache {
			get;
		} = new Dictionary<int, WeakReference<TCache>>();

		/// <summary>
		/// The relative url formatted to access a particular page
		/// </summary>
		/// <example><code>"vulns/export/(uuid)/chunks/563"</code></example>
		public abstract string RelativePageUri(int pageId);

		/// <summary>
		/// Gets a page of the export
		/// </summary>
		/// <param name="pageId">Page to retrieve</param>
		/// <returns>A list of the items in that page</returns>
		public abstract ValueTask<IReadOnlyList<TResult>> GetPageAsync(int pageId,
				CancellationToken cancellationToken = default);

		/// <summary>Get an enumerator over the results</summary>
		public abstract IEnumerator<TResult> GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		/// <summary>Get an asynchronous enumerator over the results</summary>
		/// <param name="cancellationToken">
		/// A token to observe to cancel the iteration
		/// </param>
		public abstract IAsyncEnumerator<TResult> GetAsyncEnumerator(
				CancellationToken cancellationToken = default);
	}
}
