using RestSharp;
using System.Threading;
using System.Threading.Tasks;
using Tenable.Base.Exceptions;

namespace Tenable.IO.AgentConfig {
	public class AgentConfigApi : IAgentConfigApi {
		public ITenableIO Api {
			get;
		}

		internal AgentConfigApi(ITenableIO api) {
			this.Api = api;
		}

		public async Task<Results.AgentConfig> GetAsync(int scannerId, CancellationToken cancellationToken = default) {
			var url = string.Format("scanners/{0}/agents/config", scannerId);
			return await this.Api.RequestJsonAsync<Results.AgentConfig>(
				url,
				Method.Get,
				cancellationToken)
				?? throw new UnexpectedNullResultException(url);
		}
		public Task<Results.AgentConfig> GetAsync(CancellationToken cancellationToken = default) {
			return this.GetAsync(1, cancellationToken);
		}

		public async Task<Results.AgentConfig> SetAsync(Results.AgentConfig config, int scannerId, CancellationToken cancellationToken = default) {
			var url = string.Format("scanners/{0}/agents/config", scannerId);
			return await this.Api.RequestJsonAsync<Results.AgentConfig>(
				string.Format("scanners/{0}/agents/config", scannerId),
				Method.Put,
				jsonBody: config,
				cancellationToken)
				?? throw new UnexpectedNullResultException(url);
		}
		public Task<Results.AgentConfig> SetAsync(Results.AgentConfig config, CancellationToken cancellationToken = default) {
			return this.SetAsync(config, 1, cancellationToken);
		}
	}
}
