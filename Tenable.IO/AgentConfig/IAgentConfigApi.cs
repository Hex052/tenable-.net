using System.Threading;
using System.Threading.Tasks;

namespace Tenable.IO.AgentConfig {
	public interface IAgentConfigApi : IEndpoint {
		/// <summary>
		/// Get the current configuration for agents from Tenable
		/// for a particular scanner
		/// </summary>
		/// <param name="scannerId">Scanner id to get config from</param>
		/// <returns>The configuration on this scanner</returns>
		Task<Results.AgentConfig> GetAsync(int scannerId, CancellationToken cancellationToken = default);
		/// <summary>
		/// Get the current configuration for agents from Tenable
		/// </summary>
		/// <inheritdoc cref="GetAsync(int)"/>
		Task<Results.AgentConfig> GetAsync(CancellationToken cancellationToken = default);

		/// <summary>
		/// Updates the configuration for agents on a scanner
		/// </summary>
		/// <param name="config">
		/// The config to replace the current config with
		/// </param>
		/// <param name="scannerId">The scanner to set it on</param>
		/// <returns>The new configuration that was set</returns>
		Task<Results.AgentConfig> SetAsync(Results.AgentConfig config, int scannerId, CancellationToken cancellationToken = default);
		/// <summary>
		/// Updates the configuration for agents on the default scanner
		/// </summary>
		/// <inheritdoc cref="SetAsync(Results.AgentConfig, int)"/>
		Task<Results.AgentConfig> SetAsync(Results.AgentConfig config, CancellationToken cancellationToken = default);
	}
}
