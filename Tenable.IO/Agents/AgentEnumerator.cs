using System.Threading;
using Tenable.Base;
using Tenable.Base.Collections;
using Tenable.IO.Agents.Results;

namespace Tenable.IO.Agents {
	public class AgentEnumerator : PageEnumerator<Agent, AgentEnumerable, AgentPage, Pagination> {
		public AgentEnumerator(AgentEnumerable enumerable,
				CancellationToken cancellationToken) :
				base(enumerable, cancellationToken) {
		}

		protected override int NextPageId() {
			return this.PageId.HasValue
				? this.PageId.Value + this.Enumerable.Filter.Limit
				: this.Enumerable.Filter.Offset;
		}
	}
}
