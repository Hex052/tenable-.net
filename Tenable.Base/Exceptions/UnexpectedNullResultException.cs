using System;

namespace Tenable.Base.Exceptions {
	public class UnexpectedNullResultException : Exception {
		public UnexpectedNullResultException() : base() {
		}
		public UnexpectedNullResultException(string message) : base(message) {
		}
		public UnexpectedNullResultException(string message, Exception innerException) : base(message, innerException) {
		}
	}
}
