using System;
using System.Net;

namespace Tenable.Base.Exceptions {
	/// <summary>
	/// Used when Tenable returns a 401 error, because the API keys are invalid
	/// </summary>
	public class InvalidCredentialsException : HttpStatusException {
		public InvalidCredentialsException() : base(HttpStatusCode.Unauthorized) {
		}
		public InvalidCredentialsException(string message) : base(HttpStatusCode.Unauthorized, message) {
		}
		public InvalidCredentialsException(string message, string? body) : base(HttpStatusCode.Unauthorized, message, body) {
		}
		public InvalidCredentialsException(string message, Exception innerException) : base(HttpStatusCode.Unauthorized, message, innerException) {
		}
		public InvalidCredentialsException(string message, string? body, Exception innerException) : base(HttpStatusCode.Unauthorized, message, body, innerException) {
		}
	}
}
