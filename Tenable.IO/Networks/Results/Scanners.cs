using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Tenable.IO.Networks.Results {
	public class Scanners {
		/// <summary>The scanners from the API</summary>
		[JsonPropertyName("scanners")]
		public IReadOnlyList<Scanner> Items {
			get; set;
		}

		[JsonExtensionData]
		public Dictionary<string, JsonElement>? ExtensionData {
			get; set;
		}

		[JsonConstructor]
		public Scanners(IReadOnlyList<Scanner> items) {
			this.Items = items;
		}
	}
}
